﻿namespace PixelLionWin
{
    partial class PLProgressBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PN_Bar = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // PN_Bar
            // 
            this.PN_Bar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_Bar.Location = new System.Drawing.Point(0, 0);
            this.PN_Bar.Name = "PN_Bar";
            this.PN_Bar.Size = new System.Drawing.Size(678, 33);
            this.PN_Bar.TabIndex = 0;
            this.PN_Bar.Paint += new System.Windows.Forms.PaintEventHandler(this.PN_Bar_Paint);
            // 
            // PLProgressBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PN_Bar);
            this.Name = "PLProgressBar";
            this.Size = new System.Drawing.Size(678, 33);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN_Bar;
    }
}
