﻿namespace PixelLionWin
{
    partial class ClassManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbxNom = new System.Windows.Forms.TextBox();
            this.CB_Type = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CB_Mouvement = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CB_Direction = new System.Windows.Forms.ComboBox();
            this.CB_TriggerType = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NUM_Vision = new System.Windows.Forms.NumericUpDown();
            this.NUM_Speed = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CHK_Flying = new System.Windows.Forms.CheckBox();
            this.CHK_Disposable = new System.Windows.Forms.CheckBox();
            this.CHK_Through = new System.Windows.Forms.CheckBox();
            this.LB_Classes = new System.Windows.Forms.ListBox();
            this.BTN_Ajouter = new System.Windows.Forms.Button();
            this.BTN_Supprimer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Vision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Speed)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom: ";
            // 
            // tbxNom
            // 
            this.tbxNom.Location = new System.Drawing.Point(88, 10);
            this.tbxNom.Name = "tbxNom";
            this.tbxNom.Size = new System.Drawing.Size(120, 20);
            this.tbxNom.TabIndex = 1;
            // 
            // CB_Type
            // 
            this.CB_Type.FormattingEnabled = true;
            this.CB_Type.Location = new System.Drawing.Point(88, 37);
            this.CB_Type.Name = "CB_Type";
            this.CB_Type.Size = new System.Drawing.Size(121, 21);
            this.CB_Type.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Type: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Mouvement: ";
            // 
            // CB_Mouvement
            // 
            this.CB_Mouvement.FormattingEnabled = true;
            this.CB_Mouvement.Location = new System.Drawing.Point(88, 91);
            this.CB_Mouvement.Name = "CB_Mouvement";
            this.CB_Mouvement.Size = new System.Drawing.Size(121, 21);
            this.CB_Mouvement.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Direction: ";
            // 
            // CB_Direction
            // 
            this.CB_Direction.FormattingEnabled = true;
            this.CB_Direction.Location = new System.Drawing.Point(88, 118);
            this.CB_Direction.Name = "CB_Direction";
            this.CB_Direction.Size = new System.Drawing.Size(121, 21);
            this.CB_Direction.TabIndex = 2;
            // 
            // CB_TriggerType
            // 
            this.CB_TriggerType.FormattingEnabled = true;
            this.CB_TriggerType.Location = new System.Drawing.Point(88, 64);
            this.CB_TriggerType.Name = "CB_TriggerType";
            this.CB_TriggerType.Size = new System.Drawing.Size(121, 21);
            this.CB_TriggerType.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Trigger: ";
            // 
            // NUM_Vision
            // 
            this.NUM_Vision.Location = new System.Drawing.Point(88, 146);
            this.NUM_Vision.Name = "NUM_Vision";
            this.NUM_Vision.Size = new System.Drawing.Size(120, 20);
            this.NUM_Vision.TabIndex = 6;
            // 
            // NUM_Speed
            // 
            this.NUM_Speed.Location = new System.Drawing.Point(88, 172);
            this.NUM_Speed.Name = "NUM_Speed";
            this.NUM_Speed.Size = new System.Drawing.Size(120, 20);
            this.NUM_Speed.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Vision: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Vitesse: ";
            // 
            // CHK_Flying
            // 
            this.CHK_Flying.AutoSize = true;
            this.CHK_Flying.Location = new System.Drawing.Point(183, 198);
            this.CHK_Flying.Name = "CHK_Flying";
            this.CHK_Flying.Size = new System.Drawing.Size(41, 17);
            this.CHK_Flying.TabIndex = 10;
            this.CHK_Flying.Text = "Vol";
            this.CHK_Flying.UseVisualStyleBackColor = true;
            // 
            // CHK_Disposable
            // 
            this.CHK_Disposable.AutoSize = true;
            this.CHK_Disposable.Location = new System.Drawing.Point(103, 198);
            this.CHK_Disposable.Name = "CHK_Disposable";
            this.CHK_Disposable.Size = new System.Drawing.Size(74, 17);
            this.CHK_Disposable.TabIndex = 12;
            this.CHK_Disposable.Text = "Éphémère";
            this.CHK_Disposable.UseVisualStyleBackColor = true;
            // 
            // CHK_Through
            // 
            this.CHK_Through.AutoSize = true;
            this.CHK_Through.Location = new System.Drawing.Point(15, 198);
            this.CHK_Through.Name = "CHK_Through";
            this.CHK_Through.Size = new System.Drawing.Size(82, 17);
            this.CHK_Through.TabIndex = 11;
            this.CHK_Through.Text = "Traversable";
            this.CHK_Through.UseVisualStyleBackColor = true;
            // 
            // LB_Classes
            // 
            this.LB_Classes.FormattingEnabled = true;
            this.LB_Classes.Location = new System.Drawing.Point(230, 10);
            this.LB_Classes.Name = "LB_Classes";
            this.LB_Classes.Size = new System.Drawing.Size(129, 251);
            this.LB_Classes.TabIndex = 13;
            // 
            // BTN_Ajouter
            // 
            this.BTN_Ajouter.BackgroundImage = global::PixelLionWin.Properties.Resources.AddIcon;
            this.BTN_Ajouter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Ajouter.Location = new System.Drawing.Point(57, 221);
            this.BTN_Ajouter.Name = "BTN_Ajouter";
            this.BTN_Ajouter.Size = new System.Drawing.Size(40, 40);
            this.BTN_Ajouter.TabIndex = 14;
            this.BTN_Ajouter.UseVisualStyleBackColor = true;
            this.BTN_Ajouter.Click += new System.EventHandler(this.BTN_Ajouter_Click);
            // 
            // BTN_Supprimer
            // 
            this.BTN_Supprimer.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.BTN_Supprimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Supprimer.Location = new System.Drawing.Point(137, 221);
            this.BTN_Supprimer.Name = "BTN_Supprimer";
            this.BTN_Supprimer.Size = new System.Drawing.Size(40, 40);
            this.BTN_Supprimer.TabIndex = 15;
            this.BTN_Supprimer.UseVisualStyleBackColor = true;
            this.BTN_Supprimer.Click += new System.EventHandler(this.BTN_Supprimer_Click);
            // 
            // ClassManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 271);
            this.Controls.Add(this.BTN_Supprimer);
            this.Controls.Add(this.BTN_Ajouter);
            this.Controls.Add(this.LB_Classes);
            this.Controls.Add(this.CHK_Flying);
            this.Controls.Add(this.CHK_Disposable);
            this.Controls.Add(this.CHK_Through);
            this.Controls.Add(this.NUM_Speed);
            this.Controls.Add(this.NUM_Vision);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CB_Direction);
            this.Controls.Add(this.CB_TriggerType);
            this.Controls.Add(this.CB_Mouvement);
            this.Controls.Add(this.CB_Type);
            this.Controls.Add(this.tbxNom);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ClassManager";
            this.Text = "Class Manager";
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Vision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Speed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxNom;
        private System.Windows.Forms.ComboBox CB_Type;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CB_Mouvement;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CB_Direction;
        private System.Windows.Forms.ComboBox CB_TriggerType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUM_Vision;
        private System.Windows.Forms.NumericUpDown NUM_Speed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox CHK_Flying;
        private System.Windows.Forms.CheckBox CHK_Disposable;
        private System.Windows.Forms.CheckBox CHK_Through;
        private System.Windows.Forms.ListBox LB_Classes;
        private System.Windows.Forms.Button BTN_Ajouter;
        private System.Windows.Forms.Button BTN_Supprimer;
    }
}