﻿namespace PixelLionWin
{
	partial class PixelCodeEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PixelCodeEditor));
            this.TB_CodeBox = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.éditeurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LB_LineCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.enregistrerQuitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pixelMonkeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vérifierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fichierMaîtreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TB_CodeBox
            // 
            this.TB_CodeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TB_CodeBox.BackColor = System.Drawing.Color.DimGray;
            this.TB_CodeBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TB_CodeBox.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_CodeBox.ForeColor = System.Drawing.Color.White;
            this.TB_CodeBox.Location = new System.Drawing.Point(0, 25);
            this.TB_CodeBox.Name = "TB_CodeBox";
            this.TB_CodeBox.Size = new System.Drawing.Size(1068, 638);
            this.TB_CodeBox.TabIndex = 0;
            this.TB_CodeBox.Text = "";
            this.TB_CodeBox.WordWrap = false;
            this.TB_CodeBox.TextChanged += new System.EventHandler(this.TB_CodeBox_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.éditeurToolStripMenuItem,
            this.pixelMonkeyToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1068, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // éditeurToolStripMenuItem
            // 
            this.éditeurToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enregistrerQuitterToolStripMenuItem,
            this.toolStripMenuItem1,
            this.quitterToolStripMenuItem});
            this.éditeurToolStripMenuItem.Name = "éditeurToolStripMenuItem";
            this.éditeurToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.éditeurToolStripMenuItem.Text = "Éditeur";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LB_LineCount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 663);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1068, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // LB_LineCount
            // 
            this.LB_LineCount.Name = "LB_LineCount";
            this.LB_LineCount.Size = new System.Drawing.Size(56, 17);
            this.LB_LineCount.Text = "Lignes : 0";
            // 
            // enregistrerQuitterToolStripMenuItem
            // 
            this.enregistrerQuitterToolStripMenuItem.Name = "enregistrerQuitterToolStripMenuItem";
            this.enregistrerQuitterToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.enregistrerQuitterToolStripMenuItem.Text = "Enregistrer";
            this.enregistrerQuitterToolStripMenuItem.Click += new System.EventHandler(this.enregistrerQuitterToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 6);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // pixelMonkeyToolStripMenuItem
            // 
            this.pixelMonkeyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vérifierToolStripMenuItem});
            this.pixelMonkeyToolStripMenuItem.Name = "pixelMonkeyToolStripMenuItem";
            this.pixelMonkeyToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.pixelMonkeyToolStripMenuItem.Text = "Pixel Monkey";
            // 
            // vérifierToolStripMenuItem
            // 
            this.vérifierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierMaîtreToolStripMenuItem});
            this.vérifierToolStripMenuItem.Name = "vérifierToolStripMenuItem";
            this.vérifierToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.vérifierToolStripMenuItem.Text = "Vérifier";
            // 
            // fichierMaîtreToolStripMenuItem
            // 
            this.fichierMaîtreToolStripMenuItem.Name = "fichierMaîtreToolStripMenuItem";
            this.fichierMaîtreToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.fichierMaîtreToolStripMenuItem.Text = "Fichier maître";
            this.fichierMaîtreToolStripMenuItem.Click += new System.EventHandler(this.fichierMaîtreToolStripMenuItem_Click);
            // 
            // PixelCodeEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1068, 685);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.TB_CodeBox);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PixelCodeEditor";
            this.Text = "Éditeur de code";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private System.Windows.Forms.RichTextBox TB_CodeBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem éditeurToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LB_LineCount;
        private System.Windows.Forms.ToolStripMenuItem enregistrerQuitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pixelMonkeyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vérifierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fichierMaîtreToolStripMenuItem;
	}
}