﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using SplashScreen;
using System.Threading;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Design;
using System.IO;

namespace PixelLionWin
{
    public partial class AnimationStudio : Form
    {
        Animation CurrentAnimation;
        Bitmap AnimationBuffer;

        Int32 CurrentIndex;
        Int32 CurrentFrameIndex;
        Int32? PatternFrameIndex;
        Rectangle Bounderies;
        Point AbsoluteCenter;

        public readonly Boolean UsingXna = GlobalPreferences.Prefs.AnimationStudio.UseXna;
        public readonly Int32 RulerWidth = GlobalPreferences.Prefs.AnimationStudio.RulerSize;
        public readonly Int32 BounderiesSize = 200;
        public readonly Int32 LagThreshold = 30;

        Boolean Saving = false;
        Boolean Lagging = false;
        DateTime LastFrame;
        volatile frmSplash splash;

        GraphicsDevice device;
        SpriteBatch batch;
        List<Texture2D> textures;
        Texture2D blank;
        Texture2D screen;
        Texture2D target;

        public AnimationStudio(List<Animation> list, List<Battler> battlers, SpriteBatch batch)
        {
            splash = new frmSplash(Properties.Resources.AnimationStudioSplash);
            splash.TopMost = true;
            if (GlobalPreferences.Prefs.AnimationStudio.ShowSplashScreen) splash.Show();
            Thread.Sleep(LagThreshold);

            InitializeComponent();
            DDL_Target.Items.AddRange(battlers.ToArray());

            InitializeDevice(batch);

            textures = new List<Texture2D>();
            AnimationBuffer = new Bitmap(PN_Canvas.Width, PN_Canvas.Height);

            this.SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer, true);

            Bounderies = new Rectangle(PN_Canvas.Width / 2 - BounderiesSize / 2, PN_Canvas.Height / 2 - BounderiesSize / 2, BounderiesSize, BounderiesSize);
            AbsoluteCenter = new Point(PN_Canvas.Width / 2, PN_Canvas.Height / 2);
            CurrentFrameIndex = 0;

            foreach (Animation anim in list)
	        {
                LIST_Animations.Items.Add(anim);
	        }

            if (list.Count() != 0) LIST_Animations.SelectedIndex = 0;
            LB_VersionTitle.Text = String.Format("{0} V.{1}", Properties.Resources.AnimationStudioName, Properties.Resources.AnimationStudioVersion);

            splash.Close();
        }

        private void BTN_AddAnimation_Click(object sender, EventArgs e)
        {
            LIST_Animations.Items.Add(new Animation());
        }

        private void InitializeDevice(SpriteBatch batch)
        {
            device = XNADevices.graphicsdevice;
            RefreshDevice();

            this.batch = batch;
            blank = new Texture2D(device, 1, 1, false, SurfaceFormat.Color);
            blank.SetData<Microsoft.Xna.Framework.Color>(new[] { Microsoft.Xna.Framework.Color.White });
            screen = new Texture2D(device, 1, 1, false, SurfaceFormat.Color);

            if (DDL_Target.Items.Count != 0) target = (DDL_Target.Items[0] as Battler).Img;
            
        }

        private void RefreshDevice()
        {
            PresentationParameters pp = new PresentationParameters();

            pp.BackBufferHeight = PN_Canvas.Height;
            pp.BackBufferWidth = PN_Canvas.Width;
            pp.DeviceWindowHandle = PN_Canvas.Handle;
            pp.IsFullScreen = false;

            device.Reset(pp);
        }

        private void DrawLoadingNotice(String message, String name)
        {
            using (Graphics g = Graphics.FromImage(AnimationBuffer as Image))
            using (Brush b = new LinearGradientBrush(new Point(0, 0), new Point(0, PN_Canvas.Height), Color.FromArgb(30,0,50), Color.Black))
            using (Font font = new System.Drawing.Font("Courrier New", 18))
            using (Font fontmessage = new System.Drawing.Font("Courrier New", 12))
            using (Brush whitebrush = new SolidBrush(Color.White))
            {
                g.Clear(Color.Black);
                g.FillRectangle(b, new Rectangle(new Point(0, 0), Size));
                g.DrawString("Chargement de l'animation " + name, font, whitebrush, new Point(RulerWidth, RulerWidth));
                g.DrawString(message, fontmessage, whitebrush, new Point(RulerWidth+2, RulerWidth + 30));
            }

            using (Graphics g = PN_Canvas.CreateGraphics())
            {
                g.DrawImage(AnimationBuffer, new Point(0, 0));
            }
        }

        private void LIST_Animations_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Saving) return;

            if (LIST_Animations.SelectedItem != null)
            {
                String animationname = LIST_Animations.SelectedItem.ToString();

                DrawLoadingNotice("Initialisation de l'animation", animationname);
                Thread.Sleep(20);

                Animation animation = (LIST_Animations.SelectedItem as Animation).Clone() as Animation;

                if (animation.GetPattern() != null && animation.GetPattern().GetImage() != null)
                {
                    // Set pattern in pattern panel
                    DrawLoadingNotice("Chargement du motif", animationname);
                    SetPatternImage(animation, animation.GetPattern().GetImage());

                    // Dispose old textures
                    DrawLoadingNotice("Libération de la mémoire", animationname);
                    foreach (Texture2D texture in textures) texture.Dispose();
                    
                    // Clear texture table
                    textures.Clear();

                    // Convert images into Texture2D for them to be used with XNA
                    DrawLoadingNotice("Adaptation du motif", animationname);

                    foreach (Control c in PN_Pattern.Controls)
                    using (XNAUtils utils = new XNAUtils())
                    {
                        textures.Add(utils.ConvertToTexture(c.BackgroundImage));
                    }
                }
                else
                {
                    // Dispose old textures
                    DrawLoadingNotice("Libération de la mémoire", animationname);
                    foreach (Texture2D texture in textures) texture.Dispose();

                    // Clear texture table
                    textures.Clear();

                    PN_Pattern.Controls.Clear();
                }

                // Set informations
                DrawLoadingNotice("Affichage des informations", animationname);
                LBL_AnimationName.Text = animation.ToString();

                // Update indexes
                DrawLoadingNotice("Mise à jour des index", animationname);
                CurrentAnimation = animation;
                CurrentIndex = LIST_Animations.SelectedIndex;

                // Update timeline
                DrawLoadingNotice("Chargement de la ligne de temps", animationname);
                animationTimeLine1.LoadAnimation(animation);
                DrawFrame(CurrentAnimation.FrameAt(0));

                GROUP_Animation.Visible = true;
            }
            else
            {
                CurrentAnimation = null;
                CurrentIndex = LIST_Animations.SelectedIndex;

                GROUP_Animation.Visible = false;
            }
        }

        private void animationTimeLine1_FrameChanged(object sender, AnimationFrame frame)
        {
            LastFrame = DateTime.Now;

            DrawFrame(frame);
            CurrentFrameIndex = animationTimeLine1.GetCurrentIndex();

            Lagging = (LastFrame != null && animationTimeLine1.GetCurrentIndex() > 1 && DateTime.Now.Subtract(LastFrame).Milliseconds > LagThreshold);
        }


        // Called when a frame is drawn
        private void DrawFrame(AnimationFrame frame)
        {
            // If graphical library is used, highly recommanded
            if (!UsingXna)
            {
                DrawFrameOnPanel(frame);
            }
            else
            {
                device.Clear(xColor(GlobalPreferences.Prefs.AnimationStudio.BackgroundColor));

                if (frame.Overlay != null && frame.Overlay.A != 0)
                    screen.SetData<Microsoft.Xna.Framework.Color>(new[] { xColor(frame.Overlay) });
                else
                    screen.SetData<Microsoft.Xna.Framework.Color>(new[] { xColor(0,0,0,0) });

                if (frame.Images != null)
                {
                    batch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
                    if (target != null) batch.Draw(target, new Microsoft.Xna.Framework.Rectangle(AbsoluteCenter.X - target.Width / 2, AbsoluteCenter.Y - target.Height / 2, target.Width, target.Height), xColor(frame.Upperlay));
                    batch.End();

                    batch.Begin(SpriteSortMode.BackToFront, BlendState.Additive);
                    foreach (AnimationPatternFrame f in frame.Images)
                    {
                        if (f.Blending == 1) batch.GraphicsDevice.BlendState = BlendState.Additive;
                        else if (f.Blending == 0) batch.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                        else if (f.Blending == -1) batch.GraphicsDevice.BlendState = BlendState.NonPremultiplied;
                        
                        batch.Draw(textures[f.ImageIndex], new Microsoft.Xna.Framework.Rectangle(f.Position.X + AbsoluteCenter.X, f.Position.Y + AbsoluteCenter.Y, (Int32)(textures[f.ImageIndex].Bounds.Width * f.Ratio), (Int32)(textures[f.ImageIndex].Bounds.Height * f.Ratio)), Microsoft.Xna.Framework.Color.White);
                    }

                    batch.End();
                }

                batch.Begin();
                batch.Draw(screen, new Microsoft.Xna.Framework.Rectangle(0, 0, PN_Canvas.Width, PN_Canvas.Height), White());
                DrawRulers();
                batch.End();

                device.Present();
            }
        }

        private void DrawFrameOnPanel(AnimationFrame frame)
        {
            if (CurrentAnimation == null) return;

            // Draw on buffer
            using (Graphics canvas = Graphics.FromImage(AnimationBuffer as Image))
            using (Brush gradient = new LinearGradientBrush(new Point(0, 0), new Point(0, PN_Canvas.Height), Color.FromArgb(30, 0, 50), Color.Black))
            using (Brush overlaybrush = new SolidBrush(frame.Overlay))
            using (Font font = new System.Drawing.Font("Courrier New", 18))
            using (Brush redbrush = new SolidBrush(Color.Red))
            {
                // Clear layer with black background and draw rulers
                canvas.FillRectangle(gradient, new Rectangle(new Point(0, 0), Size));
                DrawRulers(PN_Canvas, canvas);

                // Draw pattern frames
                if (frame.Images != null)
                {
                    foreach (AnimationPatternFrame apf in frame.Images)
                    {
                        Image original = PN_Pattern.Controls[apf.ImageIndex].BackgroundImage;
                        Bitmap img = new Bitmap(original, new Size((Int32)(original.Width * apf.Ratio), (Int32)(original.Height * apf.Ratio)));
                        if (img == null) continue;

                        // Color matrix for color manipulation ~ A little explanation after Matrix comprehension
                        //
                        // Red Ratio [        ] [         ] [           ] [      ] 
                        // [       ] Blue Ratio [         ] [           ] [      ]
                        // [       ] [        ] Green Ratio [           ] [      ]
                        // [       ] [        ] [         ] Opacity Ratio [      ]
                        // Red Add   Blue Add   Green Add   Opacity Add   Always 1
                        //
                        // A ratio is a multiplication done on current channel in order
                        // to get a new color. Ex pixel = (50, 0, 0) with a Red Ratio of 2 becomes (100, 0, 0)
                        // An add is a simple addition. Ex pixel = (0, 0, 3) with a Red Add of 100 and a Blue Add of 30
                        // becomes (100, 0, 33)
                        //
                        // Since all numbers are floats, the results are very precise.

                        Single a = apf.Opacity;
                        Single r = apf.RGBratio[0];
                        Single g = apf.RGBratio[1];
                        Single b = apf.RGBratio[2];
                        Single R = apf.RGBadd[0];
                        Single G = apf.RGBadd[1];
                        Single B = apf.RGBadd[2];

                        ColorMatrix matrix = new ColorMatrix
                        (
                            new Single[][]  
                            { 
                                new Single[] {r, 0, 0, 0, 0},
                                new Single[] {0, g, 0, 0, 0},
                                new Single[] {0, 0, b, 0, 0},
                                new Single[] {0, 0, 0, a, 0},
                                new Single[] {R, G, B, 0, 1}
                            }
                        );

                        ImageAttributes imgAttribs = new ImageAttributes();
                        imgAttribs.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Default);

                        // Draw pattern frame to buffer
                        canvas.DrawImage
                        (
                            img, 
                            new Rectangle(new Point(apf.Position.X + AbsoluteCenter.X, apf.Position.Y + AbsoluteCenter.Y), 
                            new Size((Int32)((Single)img.Size.Width * apf.Ratio), (Int32)((Single)img.Size.Height * apf.Ratio))), 
                            0, 
                            0, 
                            img.Size.Width * apf.Ratio, 
                            img.Size.Height * apf.Ratio, 
                            GraphicsUnit.Pixel, 
                            imgAttribs
                        );
                    }
                }

                // Draw particules


                // Draw color Overlay
                if (frame.Overlay != null) canvas.FillRectangle(overlaybrush, new Rectangle(new Point(0, 0), PN_Canvas.Size));

                // Draw CurrentFrame corner tag
                canvas.DrawString("Cadre #" + (animationTimeLine1.GetCurrentIndex()+1).ToString() + (Lagging ? " [Lag]" : ""), font, redbrush, new Point(RulerWidth+1, RulerWidth+1));
            }

            // Draw buffer on Panel
            using (Graphics g = PN_Canvas.CreateGraphics())
            {
                g.DrawImage(AnimationBuffer, new Point(0, 0));
            }

            
        }

        public void DrawRulers(Panel pn, Graphics g)
        {
            using (Graphics buffer = Graphics.FromImage(AnimationBuffer as Image))
            using (Font font = new Font("Courrier New", 10))
            using (Brush brush = new SolidBrush(Color.White))
            using (Pen p = new Pen(Color.White, 1))
            {
                // Vertical rulers
                buffer.DrawLine(p, new Point(RulerWidth, 0), new Point(RulerWidth, pn.Height));
                buffer.DrawLine(p, new Point(pn.Width - RulerWidth, 0), new Point(pn.Width - RulerWidth, pn.Height));
                
                // Horizontal rulers
                buffer.DrawLine(p, new Point(0, RulerWidth), new Point(pn.Width, RulerWidth));
                buffer.DrawLine(p, new Point(0, pn.Height - RulerWidth), new Point(pn.Width, pn.Height - RulerWidth));

                // Clip rectangle
                p.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                buffer.DrawRectangle(p, Bounderies);
                buffer.DrawString(String.Format("{0} x {1}", Bounderies.Width, Bounderies.Height), font, brush, new Point(Bounderies.X, Bounderies.Y - 20));
            }

            // Draw buffer on Panel
            g.DrawImage(AnimationBuffer, new Point(0, 0));
        }

        private void DrawRulers()
        {
            DrawLine(1f, White(), Vect(RulerWidth, 0), Vect(RulerWidth, PN_Canvas.Height));
            DrawLine(1f, White(), Vect(PN_Canvas.Width - RulerWidth, 0), Vect(PN_Canvas.Width - RulerWidth, PN_Canvas.Height));
            DrawLine(1f, White(), Vect(0, RulerWidth), Vect(PN_Canvas.Width, RulerWidth));
            DrawLine(1f, White(), Vect(0, PN_Canvas.Height - RulerWidth), Vect(PN_Canvas.Width, PN_Canvas.Height - RulerWidth));

            DrawRectangle(1f, xColor(255, 255, 255, 120), Bounderies);
        }

        private void DrawLine(float width, Microsoft.Xna.Framework.Color color, Microsoft.Xna.Framework.Vector2 point1, Microsoft.Xna.Framework.Vector2 point2)
        {
            float angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            float length = Microsoft.Xna.Framework.Vector2.Distance(point1, point2);

            batch.Draw(blank, point1, null, color,
                       angle, Vect(0, 0), Vect(length, width),
                       SpriteEffects.None, 0);
        }

        private void DrawText(String str, Int32 x, Int32 y)
        {
            //batch.DrawString(font, str, Vect(x, y), White());
        }

        private void DrawRectangle(float width, Microsoft.Xna.Framework.Color color, Rectangle rect)
        {
            // Top, Bottom, Left, Right
            DrawLine(width, color, Vect(rect.X, rect.Y), Vect(rect.X + rect.Width, rect.Y));
            DrawLine(width, color, Vect(rect.X, rect.Y + rect.Height), Vect(rect.X + rect.Width, rect.Y + rect.Height));
            DrawLine(width, color, Vect(rect.X, rect.Y), Vect(rect.X, rect.Y + rect.Height));
            DrawLine(width, color, Vect(rect.X + rect.Width, rect.Y), Vect(rect.X + rect.Width, rect.Y + rect.Height));
        }

        private Microsoft.Xna.Framework.Vector2 Vect(Single x, Single y)
        {
            return new Microsoft.Xna.Framework.Vector2(x, y);
        }

        private Microsoft.Xna.Framework.Color White()
        {
            return Microsoft.Xna.Framework.Color.White;
        }

        private Microsoft.Xna.Framework.Color xColor(Int32 r, Int32 g, Int32 b, Int32 a)
        {
            return Microsoft.Xna.Framework.Color.FromNonPremultiplied(r, g, b, a);
        }

        private Microsoft.Xna.Framework.Color xColor(Color c)
        {
            return xColor(c.R, c.G, c.B, c.A);
        }

        private void LBL_AnimationName_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SingleStringInput dlg = new SingleStringInput("Nom de l'animation", LBL_AnimationName.Text);
            dlg.ShowDialog();

            CurrentAnimation.Name = dlg.GetContent();
            LBL_AnimationName.Text = dlg.GetContent();
        }

        private void BTN_Flash_Click(object sender, EventArgs e)
        {
            AnimationFlashCreator dlg = new AnimationFlashCreator();
            dlg.ShowDialog();
            Int32 Index = CurrentFrameIndex;

            if (dlg.GetDuration() != 0)
            {
                for (int i = Index; i < Index + dlg.GetDuration() && i < CurrentAnimation.GetFrameCount(); i++)
                {
                    AnimationFrame frame = CurrentAnimation.FrameAt(i);
                    Color c = dlg.GetColor();
                    Color newcolor;
                    AttackType type = dlg.GetTarget();

                    if (type == AttackType.Screen)
                    {
                        Single ratio = (Single)dlg.GetDuration() / (Single)i;
                        Single diff = 255f / ratio;

                        Byte alpha = (byte)(255f - diff);

                        newcolor = Color.FromArgb(alpha, c.R, c.G, c.B);

                        frame.Overlay = newcolor;
                    }
                    else if (type == AttackType.Melee)
                    {
                        // init + ((255-init) / duration) * i
                        newcolor = Color.FromArgb
                        (
                            (Byte)((Single)c.A + ((255f - (Single)c.A) / (Single)dlg.GetDuration()) * (Single)i),
                            (Byte)((Single)c.R + ((255f - (Single)c.R) / (Single)dlg.GetDuration()) * (Single)i),
                            (Byte)((Single)c.G + ((255f - (Single)c.G) / (Single)dlg.GetDuration()) * (Single)i),
                            (Byte)((Single)c.B + ((255f - (Single)c.B) / (Single)dlg.GetDuration()) * (Single)i)
                        );

                        frame.Upperlay = newcolor;
                    }

                    CurrentAnimation.AlterFrameAt(i, frame);
                }
            }

            Refresh();
        }

        private void PN_Canvas_Paint(object sender, PaintEventArgs e)
        {
            if (UsingXna)
            {
                DrawFrame(CurrentAnimation.FrameAt(CurrentFrameIndex));
            }
            else
            {
                using (Graphics g = PN_Canvas.CreateGraphics())
                {
                    DrawRulers(PN_Canvas, g);
                    if (CurrentAnimation != null) DrawFrame(CurrentAnimation.FrameAt(CurrentFrameIndex));
                }
            }
        }

        public void PatternFrameSelected(object sender, EventArgs e)
        {
            foreach (Control c in PN_Pattern.Controls) c.BackColor = Color.White;

            ((Control)sender).BackColor = Color.Teal;
            PatternFrameIndex = Int32.Parse(((Control)sender).Name);
        }

        private void BTN_LoadPattern_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Images|*.jpg;*.jpeg;*.png;*.bmp";
            dlg.Multiselect = false;

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) SetPatternImage(CurrentAnimation, Image.FromFile(dlg.FileName));

            foreach (Control c in PN_Pattern.Controls)
            {
                MemoryStream mem = new MemoryStream();
                c.BackgroundImage.Save(mem, ImageFormat.Png);
                textures.Add(Texture2D.FromStream(device, mem));
            }

            PN_Canvas.Refresh();
        }

        private void SetPatternImage(Animation animation, Image image)
        {
            foreach (Control c in PN_Pattern.Controls)
                c.BackgroundImage.Dispose();

            PN_Pattern.Controls.Clear();

            animation.SetPattern(image);

            Int32 Count = 0;
            PatternFrameIndex = null;
            foreach (Image img in animation.CropPattern())
            {
                Panel pn = new Panel();

                // TODO : Change default values to current values
                pn.Size = new Size(192, 192);

                Int32 X = Count % (PN_Pattern.Size.Width / pn.Size.Width) * pn.Size.Width;
                Int32 Y = Count / (PN_Pattern.Size.Width / pn.Size.Height) * pn.Size.Height;

                pn.Location = new Point(X, Y);
                pn.BackgroundImageLayout = ImageLayout.None;
                pn.BackgroundImage = img.Clone() as Image;
                pn.BorderStyle = BorderStyle.FixedSingle;
                pn.Click += new EventHandler(PatternFrameSelected);
                pn.Name = Count.ToString();
                pn.BackColor = Color.White;

                PN_Pattern.Controls.Add(pn);

                img.Dispose();
                Count++;
            }
        }

        private void PN_Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            Point real = ComputeClickPosition(e);
            LB_MousePosition.Text = String.Format("{0} x {1} ({2} x {3})", e.X - AbsoluteCenter.X, e.Y - AbsoluteCenter.Y, real.X, real.Y);
        }

        private void PN_Canvas_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && PatternFrameIndex != null)
            {
                AnimationFrame frame = CurrentAnimation.FrameAt(CurrentFrameIndex);
                AnimationPatternFrame apf = new AnimationPatternFrame();

                apf.Position = ComputeClickPosition(e);
                apf.ImageIndex = PatternFrameIndex ?? -1;
                apf.Ratio = 1;
                apf.Rotation = 0;
                apf.Fliped = false;
                apf.Blending = 0;

                frame.Images.Add(apf);

                CurrentAnimation.AlterFrameAt(CurrentFrameIndex, frame);
                PN_Canvas.Refresh();
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                AnimationPatternFrame a = new AnimationPatternFrame() { Blending = -1000 };
                AnimationFrame frame = CurrentAnimation.FrameAt(CurrentFrameIndex);
                Int32 index = 0;

                foreach (AnimationPatternFrame apf in frame.Images)
                {
                    if (e.X > apf.Position.X + AbsoluteCenter.X && e.Y > apf.Position.Y + AbsoluteCenter.Y && e.X < apf.Position.X + AbsoluteCenter.X + 192 && e.Y < apf.Position.Y + AbsoluteCenter.Y + 192)
                    {
                        a = apf;
                        break;
                    }

                    index++;
                }

                if (a.Blending != -1000)
                {
                    Point location = MousePosition;

                    AnimationFramePanelEditor dlg = new AnimationFramePanelEditor();

                    if (location.X + dlg.Width > Screen.PrimaryScreen.Bounds.Width) location.X = Screen.PrimaryScreen.Bounds.Width - dlg.Width;
                    if (location.Y + dlg.Height > Screen.PrimaryScreen.Bounds.Height) location.Y = Screen.PrimaryScreen.Bounds.Height - dlg.Height;

                    dlg.StartPosition = FormStartPosition.Manual;
                    dlg.Location = location;
                    dlg.SetFrame(a, PN_Pattern.Controls.Count <= a.ImageIndex ? new Bitmap(1, 1) : PN_Pattern.Controls[a.ImageIndex].BackgroundImage);

                    switch(dlg.ShowDialog())
                    {
                        case System.Windows.Forms.DialogResult.OK:
                            {
                                a = dlg.GetFrame();
                                frame.Images[index] = a;

                                CurrentAnimation.AlterFrameAt(CurrentFrameIndex, frame);
                            }
                            break;

                        case System.Windows.Forms.DialogResult.Abort:
                            {
                                frame.Images.RemoveAt(index);

                                CurrentAnimation.AlterFrameAt(CurrentFrameIndex, frame);
                            }
                        break;

                        case System.Windows.Forms.DialogResult.Cancel:
                        break;

                        default:
                        break;
                    }

                    PN_Canvas.Refresh();
                }
            }
        }

        public Point ComputeClickPosition(MouseEventArgs e)
        {
            Int32 FrameSize = CurrentAnimation.GetPattern() == null ? 192 : CurrentAnimation.GetPattern().GetFrameSize().Width;
            return new Point(e.X - FrameSize / 2 - AbsoluteCenter.X, e.Y - FrameSize / 2 - AbsoluteCenter.Y);
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            Saving = true;

            LIST_Animations.Items.RemoveAt(CurrentIndex);
            LIST_Animations.Items.Insert(CurrentIndex, CurrentAnimation.Clone() as Animation);

            List<Animation> list = GetAnimationsFromList().ToList();
            LIST_Animations.Items.Clear();
            LIST_Animations.Items.AddRange(list.ToArray());

            Saving = false;

            if (MessageBox.Show("Mise à jour des animations terminée.\nVoulez-vous fermer l'éditeur d'animations?", "Sauvegarde terminée", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                Close();
        }

        public IEnumerable<Animation> GetAnimationsFromList()
        {
            for (int i = 0; i < LIST_Animations.Items.Count; i++) yield return LIST_Animations.Items[i] as Animation;
        }

        private void BTN_PutEntirePattern_Click(object sender, EventArgs e)
        {
            if (CurrentAnimation.GetPattern() == null)
            {
                if (MessageBox.Show("Il n'y a aucun motif associé avec cette animation.\nVoulez-vous en charger un?", "Aucun motif", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    BTN_LoadPattern_Click(sender, e);

                    if (CurrentAnimation.GetPattern() == null) return;
                }
                else
                {
                    return;
                }
            }

            AnimationPutEntirePattern dlg = new AnimationPutEntirePattern(this, CurrentAnimation, CurrentFrameIndex);
            dlg.ShowDialog();

            CurrentAnimation = dlg.GetAnimation();

            animationTimeLine1.LoadAnimation(CurrentAnimation);
        }

        private void AnimationManager_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (AnimationBuffer != null) AnimationBuffer.Dispose();

            foreach (Texture2D t in textures) t.Dispose();
            blank.Dispose();
            screen.Dispose();
        }

        private void BTN_RemoveAnimation_Click(object sender, EventArgs e)
        {
            if (LIST_Animations.SelectedIndex != -1) LIST_Animations.Items.RemoveAt(LIST_Animations.SelectedIndex);
        }

        private void PN_Canvas_Resize(object sender, EventArgs e)
        {
            AnimationBuffer.Dispose();
            AnimationBuffer = new Bitmap(PN_Canvas.Width, PN_Canvas.Height);

            Bounderies = new Rectangle(PN_Canvas.Width / 2 - BounderiesSize / 2, PN_Canvas.Height / 2 - BounderiesSize / 2, BounderiesSize, BounderiesSize);
            AbsoluteCenter = new Point(PN_Canvas.Width / 2, PN_Canvas.Height / 2);

            RefreshDevice();
        }

        private void DDL_Target_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DDL_Target.SelectedItem != null)
            {
                target = (DDL_Target.SelectedItem as Battler).Img;

                Refresh();
            }
        }
    }
}
