﻿namespace PixelLionWin
{
    partial class DrawLimits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DrawLimits));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.effacerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zonerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supporterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bloqierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.sauvegarderEtQuitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PN_Map = new System.Windows.Forms.Panel();
            this.PN_Container = new System.Windows.Forms.Panel();
            this.statusStrip1.SuspendLayout();
            this.PN_Container.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripSplitButton1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 443);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(633, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.effacerToolStripMenuItem,
            this.zonerToolStripMenuItem,
            this.supporterToolStripMenuItem,
            this.bloqierToolStripMenuItem});
            this.toolStripDropDownButton1.Image = global::PixelLionWin.Properties.Resources.FillIcon;
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 20);
            this.toolStripDropDownButton1.Text = "toolStripDropDownButton1";
            // 
            // effacerToolStripMenuItem
            // 
            this.effacerToolStripMenuItem.Name = "effacerToolStripMenuItem";
            this.effacerToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.effacerToolStripMenuItem.Text = "Effacer";
            this.effacerToolStripMenuItem.Click += new System.EventHandler(this.effacerToolStripMenuItem_Click);
            // 
            // zonerToolStripMenuItem
            // 
            this.zonerToolStripMenuItem.Name = "zonerToolStripMenuItem";
            this.zonerToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.zonerToolStripMenuItem.Text = "Zoner";
            this.zonerToolStripMenuItem.Click += new System.EventHandler(this.zonerToolStripMenuItem_Click);
            // 
            // supporterToolStripMenuItem
            // 
            this.supporterToolStripMenuItem.Name = "supporterToolStripMenuItem";
            this.supporterToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.supporterToolStripMenuItem.Text = "Supporter";
            this.supporterToolStripMenuItem.Click += new System.EventHandler(this.supporterToolStripMenuItem_Click);
            // 
            // bloqierToolStripMenuItem
            // 
            this.bloqierToolStripMenuItem.Name = "bloqierToolStripMenuItem";
            this.bloqierToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.bloqierToolStripMenuItem.Text = "Bloquer";
            this.bloqierToolStripMenuItem.Click += new System.EventHandler(this.bloqierToolStripMenuItem_Click);
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sauvegarderEtQuitterToolStripMenuItem});
            this.toolStripSplitButton1.Image = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(32, 20);
            this.toolStripSplitButton1.Text = "toolStripSplitButton1";
            // 
            // sauvegarderEtQuitterToolStripMenuItem
            // 
            this.sauvegarderEtQuitterToolStripMenuItem.Name = "sauvegarderEtQuitterToolStripMenuItem";
            this.sauvegarderEtQuitterToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.sauvegarderEtQuitterToolStripMenuItem.Text = "Sauvegarder et fermer";
            this.sauvegarderEtQuitterToolStripMenuItem.Click += new System.EventHandler(this.sauvegarderEtQuitterToolStripMenuItem_Click);
            // 
            // PN_Map
            // 
            this.PN_Map.Location = new System.Drawing.Point(0, 0);
            this.PN_Map.Name = "PN_Map";
            this.PN_Map.Size = new System.Drawing.Size(330, 260);
            this.PN_Map.TabIndex = 1;
            this.PN_Map.Paint += new System.Windows.Forms.PaintEventHandler(this.PN_Map_Paint);
            this.PN_Map.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PN_Map_MouseDown);
            this.PN_Map.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PN_Map_MouseMove);
            this.PN_Map.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PN_Map_MouseUp);
            // 
            // PN_Container
            // 
            this.PN_Container.AutoSize = true;
            this.PN_Container.Controls.Add(this.PN_Map);
            this.PN_Container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_Container.Location = new System.Drawing.Point(0, 0);
            this.PN_Container.Name = "PN_Container";
            this.PN_Container.Size = new System.Drawing.Size(633, 443);
            this.PN_Container.TabIndex = 2;
            // 
            // DrawLimits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 465);
            this.Controls.Add(this.PN_Container);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DrawLimits";
            this.Text = "Limites";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.PN_Container.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem effacerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zonerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supporterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bloqierToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem sauvegarderEtQuitterToolStripMenuItem;
        private System.Windows.Forms.Panel PN_Map;
        private System.Windows.Forms.Panel PN_Container;
    }
}