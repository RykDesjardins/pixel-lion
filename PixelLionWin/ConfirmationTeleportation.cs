﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class ConfirmationTeleportation : Form
    {
        Point depart;
        Point Destination;
        int layerDepart;
        int layerDestination;
        Map premiere;
        Map deuxieme;
        public bool pEventAjouté = false;
        public ConfirmationTeleportation(Point depart, int layerDepart, Point Destination, int layerDestination, Map premiere, Map deuxieme)
        {
            InitializeComponent();
            this.depart = depart;
            this.Destination = Destination;
            this.layerDepart = layerDepart;
            this.layerDestination = layerDestination;
            this.premiere = premiere;
            this.deuxieme = deuxieme;
            LBL_Edit.Text = "de " + premiere.Name + " (" + layerDepart + ", " + depart.X + ", " + depart.Y + ") à " + deuxieme.Name + " (" + layerDestination + ", " + Destination.X + ", " + Destination.Y + ") ?";
        }

        private void BTN_Accept_Click(object sender, EventArgs e)
        {
            // TELEPORT [NomMap] [Layer,X,Y]

            if (premiere.Tiles[layerDepart, depart.X, depart.Y].pEvent != null)
                premiere.Tiles[layerDepart, depart.X, depart.Y].pEvent.Script += "\r\nTELEPORT " + deuxieme.Name + " " + layerDestination + "," + Destination.X + "," + Destination.Y;
            else
            {
                // Ajouter un nouveau pEvent à la première map
                Pevent peventDepart = Pevent.CreateNew();
                peventDepart.Name = "Téléportation Départ";
                peventDepart.Position = depart;
                peventDepart.Script = "TELEPORT " + deuxieme.Name + " " + layerDestination + "," + Destination.X + "," + Destination.Y;
                premiere.Tiles[layerDepart, depart.X, depart.Y].pEvent = peventDepart;
            }

            if (deuxieme.Tiles[layerDestination, Destination.X, Destination.Y].pEvent != null)
                deuxieme.Tiles[layerDestination, Destination.X, Destination.Y].pEvent.Script += "\r\nTELEPORT " + premiere.Name + " " + layerDepart + "," + depart.X + "," + depart.Y;
            else
            {
                // Ajouter un nouveau pEvent à la deuxième map
                Pevent peventDestination = Pevent.CreateNew();
                peventDestination.Name = "Téléportation Destination";
                peventDestination.Position = Destination;
                peventDestination.Script = "TELEPORT " + premiere.Name + " " + layerDepart + "," + depart.X + "," + depart.Y;
                deuxieme.Tiles[layerDestination, Destination.X, Destination.Y].pEvent = peventDestination;
            }
            premiere.Save();
            deuxieme.Save();
            pEventAjouté = true;
            this.Close();
        }

        private void BTN_Refuse_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
