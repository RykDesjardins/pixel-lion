﻿namespace PixelLionWin
{
    partial class ExportProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportProject));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LB_CurrentAction = new System.Windows.Forms.Label();
            this.plProgressBar1 = new PixelLionWin.PLProgressBar();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LB_CurrentAction);
            this.groupBox1.Controls.Add(this.plProgressBar1);
            this.groupBox1.Location = new System.Drawing.Point(12, 297);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(634, 101);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Avancement";
            // 
            // LB_CurrentAction
            // 
            this.LB_CurrentAction.AutoSize = true;
            this.LB_CurrentAction.Location = new System.Drawing.Point(13, 30);
            this.LB_CurrentAction.Name = "LB_CurrentAction";
            this.LB_CurrentAction.Size = new System.Drawing.Size(80, 13);
            this.LB_CurrentAction.TabIndex = 1;
            this.LB_CurrentAction.Text = "[Current Action]";
            // 
            // plProgressBar1
            // 
            this.plProgressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plProgressBar1.Location = new System.Drawing.Point(16, 50);
            this.plProgressBar1.Max = 100;
            this.plProgressBar1.Name = "plProgressBar1";
            this.plProgressBar1.Size = new System.Drawing.Size(601, 33);
            this.plProgressBar1.TabIndex = 0;
            this.plProgressBar1.Value = 100;
            // 
            // ExportProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 410);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExportProject";
            this.Text = "Exportation du projet";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private PLProgressBar plProgressBar1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label LB_CurrentAction;
    }
}