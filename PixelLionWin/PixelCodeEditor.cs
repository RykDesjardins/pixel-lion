﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelMonkey;

namespace PixelLionWin
{
    public partial class PixelCodeEditor : Form
    {
        const String LignesKW = "Lignes : ";
        String Code;

        public PixelCodeEditor(String code)
        {
            InitializeComponent();
            Code = code;

            TB_CodeBox.Text = code;
            LB_LineCount.Text = LignesKW + TB_CodeBox.Lines.Count();
        }

        private void TB_CodeBox_TextChanged(object sender, EventArgs e)
        {
            LB_LineCount.Text = LignesKW + TB_CodeBox.Lines.Count();
        }

        public String GetCode()
        {
            return Code;
        }

        private void enregistrerQuitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Code = TB_CodeBox.Text;
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void fichierMaîtreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                PixelMonkeyLanguage language = PixelMonkeyLanguage.LoadFromString(TB_CodeBox.Text);

                MessageBox.Show("Interprétation terminée avec succès!", "Réussie", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (PixelMonkeyCoreException ex)
            {
                MessageBox.Show("Impossible de terminer l'interprétation.\n" + ex.CustomMessage + "\n" + ex.Command, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
