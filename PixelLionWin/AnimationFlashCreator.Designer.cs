﻿namespace PixelLionWin
{
    partial class AnimationFlashCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BTN_ColorPick = new System.Windows.Forms.Button();
            this.LB_Color = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.NUM_Duree = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DDL_Target = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Duree)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BTN_ColorPick);
            this.groupBox1.Controls.Add(this.LB_Color);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(176, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Couleur";
            // 
            // BTN_ColorPick
            // 
            this.BTN_ColorPick.Location = new System.Drawing.Point(90, 38);
            this.BTN_ColorPick.Name = "BTN_ColorPick";
            this.BTN_ColorPick.Size = new System.Drawing.Size(69, 48);
            this.BTN_ColorPick.TabIndex = 2;
            this.BTN_ColorPick.Text = "Choisir la couleur";
            this.BTN_ColorPick.UseVisualStyleBackColor = true;
            this.BTN_ColorPick.Click += new System.EventHandler(this.BTN_ColorPick_Click);
            // 
            // LB_Color
            // 
            this.LB_Color.AutoSize = true;
            this.LB_Color.Location = new System.Drawing.Point(87, 22);
            this.LB_Color.Name = "LB_Color";
            this.LB_Color.Size = new System.Drawing.Size(50, 13);
            this.LB_Color.TabIndex = 1;
            this.LB_Color.Text = "#000000";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(16, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(65, 64);
            this.panel1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.NUM_Duree);
            this.groupBox2.Location = new System.Drawing.Point(12, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(176, 57);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Durée";
            // 
            // NUM_Duree
            // 
            this.NUM_Duree.Location = new System.Drawing.Point(16, 19);
            this.NUM_Duree.Name = "NUM_Duree";
            this.NUM_Duree.Size = new System.Drawing.Size(143, 20);
            this.NUM_Duree.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::PixelLionWin.Properties.Resources.AcceptIcon;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Location = new System.Drawing.Point(148, 246);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 35);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.DDL_Target);
            this.groupBox3.Location = new System.Drawing.Point(12, 181);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(176, 59);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cible";
            // 
            // DDL_Target
            // 
            this.DDL_Target.FormattingEnabled = true;
            this.DDL_Target.Items.AddRange(new object[] {
            "Écran entier",
            "Cible uniquement"});
            this.DDL_Target.Location = new System.Drawing.Point(16, 22);
            this.DDL_Target.Name = "DDL_Target";
            this.DDL_Target.Size = new System.Drawing.Size(143, 21);
            this.DDL_Target.TabIndex = 0;
            // 
            // AnimationFlashCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(201, 293);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AnimationFlashCreator";
            this.Text = "Animation ~ Flash";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Duree)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BTN_ColorPick;
        private System.Windows.Forms.Label LB_Color;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown NUM_Duree;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox DDL_Target;
    }
}