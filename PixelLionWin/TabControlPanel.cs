﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLion_Win
{
    public partial class TabControlPanel : UserControl
    {
        Bitmap Buffer;

        public event EventHandler TabChanged;

        const Int32 MarginTop = 2;
        const Int32 MarginBottom = 1;
        const Int32 SlashLength = 3;
        const Int32 SpaceBetweenTabs = 15;
        Color LineColor = Color.Black;
        Color GradiantStartColor;
        Color GradiantEndColor;
        Point CurrentPoint;

        Int32 CurrentIndex;
        Int32 OldIndex;
        List<Object> Tabs;
        List<Rectangle> Areas;

        public TabControlPanel()
        {
            InitializeComponent();
            Buffer = new Bitmap(Width, Height);

            CurrentPoint = new Point(0, Height - MarginBottom);
            Tabs = new List<Object>();
            Areas = new List<Rectangle>();
            CurrentIndex = 0;
            OldIndex = 0;

            GradiantStartColor = Color.FromArgb(255, 80, 180, 230);
            GradiantEndColor = Color.FromArgb(255, 120, 220, 255);
        }

        public void AddTab(Object o)
        {
            if (!Tabs.Select(x => x.ToString()).Contains(o.ToString()))
            {
                Tabs.Add(o);
                Areas.Add(new Rectangle());
                CurrentIndex = Tabs.Count - 1;
            }
            else
            {
                Object original = Tabs.Where(x => x.ToString() == o.ToString()).FirstOrDefault();
                CurrentIndex = Tabs.IndexOf(original);
                Tabs[CurrentIndex] = o;
            }

            DrawPanel();
        }

        public Object RemoveCurrentObject()
        {
            Tabs.RemoveAt(CurrentIndex);

            if (Tabs.Count == 0)
                CurrentIndex = 0;
            else if (Tabs.Count == CurrentIndex)
                return Tabs[--CurrentIndex];
            else if (CurrentIndex < Tabs.Count)
                return Tabs[CurrentIndex];
            
            return null;
        }

        public Object GetOldObject()
        {
            return Tabs[OldIndex];
        }

        public void SetOldObject(Object o)
        {
            Tabs[OldIndex] = o;
        }

        public void DestroyTabs()
        {
            Tabs.Clear();
            Areas.Clear();
        }

        public Object GetCurrentTabItem()
        {
            return Tabs[CurrentIndex];
        }

        private void TabControlPanel_Resize(object sender, EventArgs e)
        {
            if (Width > 0 && Height > 0)
            {
                Buffer.Dispose();
                Buffer = new Bitmap(Width, Height);
            }
        }

        private void TabControlPanel_Paint(object sender, PaintEventArgs e)
        {
            DrawPanel();
        }

        private void DrawPanel()
        {
            if (Tabs.Count() == 0) return;

            GradiantStartColor = GlobalPreferences.Prefs.Tabs.FirstColorGradiant;
            GradiantEndColor = GlobalPreferences.Prefs.Tabs.SecondColorGradiant;

            CurrentPoint = new Point(0, Height - MarginBottom);
            Int32 Count = 0;

            using (Graphics g = Graphics.FromImage(Buffer as Image))
            using (Pen p = new Pen(LineColor, 1f))
            using (Brush mainBrush = new SolidBrush(LineColor))
            using (Brush secondBrush = new SolidBrush(Color.FromArgb(LineColor.A, LineColor.R + 50, LineColor.G + 50, LineColor.B + 50)))
            using (Font font = new System.Drawing.Font("Calibri", 9f))
            {
                g.Clear(BackColor);

                foreach (Object o in Tabs)
                {
                    String str = o.ToString();

                    SizeF StringSize = g.MeasureString(str, font);

                    Point StartPoint  = new Point(CurrentPoint.X + SpaceBetweenTabs, Height - MarginBottom);
                    Point SecondPoint = new Point(StartPoint.X + SlashLength, MarginTop);
                    Point ThirdPoint  = new Point(SecondPoint.X + (Int32)StringSize.Width + 30, SecondPoint.Y);
                    Point FourthPoint = new Point(ThirdPoint.X + SlashLength, Height - MarginBottom);

                    Point[] DestinationPoints = new Point[] { StartPoint, SecondPoint, ThirdPoint, FourthPoint };
                    Point[] GradiantPoints = new Point[] { new Point(StartPoint.X, StartPoint.Y + 1), SecondPoint, ThirdPoint, new Point(FourthPoint.X, FourthPoint.Y + 1) };

                    Areas[Count] = new Rectangle(StartPoint.X, SecondPoint.Y, FourthPoint.X - StartPoint.X, StartPoint.Y - SecondPoint.Y);

                    Brush gradiant = new LinearGradientBrush(StartPoint, FourthPoint, GradiantStartColor, CurrentIndex == Count ? Color.FromArgb(255, GradiantEndColor.R + 50 > 255 ? 255 : GradiantEndColor.R + 50, GradiantEndColor.G + 50 > 255 ? 255 : GradiantEndColor.G + 50, GradiantEndColor.B + 50 > 255 ? 255 : GradiantEndColor.B + 50) : GradiantEndColor);
                    
                    g.FillPolygon(gradiant, GradiantPoints);

                    if (CurrentIndex == Count)
                    {
                        g.DrawLine(p, StartPoint, SecondPoint);
                        g.DrawLine(p, SecondPoint, ThirdPoint);
                        g.DrawLine(p, ThirdPoint, FourthPoint);
                        g.DrawString(str, font, secondBrush, new PointF(SecondPoint.X + 15, MarginTop * 3));

                        g.DrawLine(p, new Point(0, StartPoint.Y), StartPoint);
                        g.DrawLine(p, FourthPoint, new Point(Width, FourthPoint.Y));
                    }
                    else
                    {
                        g.DrawPolygon(p, DestinationPoints);
                        g.DrawString(str, font, mainBrush, new PointF(SecondPoint.X + 15, MarginTop * 2));
                    }

                    CurrentPoint = FourthPoint;

                    gradiant.Dispose();

                    // End of drawings
                    Count++;
                }
            }

            using (Graphics g = CreateGraphics()) g.DrawImage(Buffer as Image, new Point(0, 0));
        }

        public void NextTab()
        {
            if (CurrentIndex + 1 != Tabs.Count)
            {
                CurrentIndex++;
                DrawPanel();
                TabChanged(this, new EventArgs());
            }
            else if (Tabs.Count > 1)
            {
                CurrentIndex = 0;
                DrawPanel();
                TabChanged(this, new EventArgs());
            }
        }

        public void PreviousTab()
        {
            if (CurrentIndex != 0)
            {
                CurrentIndex--;
                DrawPanel();
                TabChanged(this, new EventArgs());
            }
            else if (Tabs.Count > 1)
            {
                CurrentIndex = Tabs.Count - 1;
                DrawPanel();
                TabChanged(this, new EventArgs());
            }
        }

        private void TabControlPanel_MouseClick(object sender, MouseEventArgs e)
        {
            OldIndex = CurrentIndex;

            for (int i = 0; i < Tabs.Count(); i++)
            {
                if (i != CurrentIndex && IsInside(Areas[i], new Point(e.X, e.Y)))
                {
                    CurrentIndex = i;
                    DrawPanel();

                    TabChanged(sender, new EventArgs());
                }
            }
        }

        public static Boolean IsInside(System.Drawing.Rectangle source, System.Drawing.Point point)
        {
            return point.X >= source.X && point.X <= source.X + source.Width && point.Y >= source.Y && point.Y <= source.Y + source.Height;
        }
    }
}
