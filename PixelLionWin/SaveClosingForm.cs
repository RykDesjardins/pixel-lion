﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PixelLionWin
{
    public partial class SaveClosingForm : Form
    {
        public bool Sauvegarder = false;
        public SaveClosingForm(string FormName)
        {
            InitializeComponent();
            LabelText.Text += FormName + "?";
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            Sauvegarder = true;
            this.Close();
        }

        private void BTN_DontSave_Click(object sender, EventArgs e)
        {
            Sauvegarder = false;
            this.Close();
        }
    }
}
