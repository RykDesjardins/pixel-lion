﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class BattlerManager : Form
    {
        public BattlerManager(List<Battler> list)
        {
            InitializeComponent();
            LIST_Battlers.Items.AddRange(list.ToArray());
        }

        private void BTN_Add_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Multiselect = true;
            dlg.Filter = "Fichiers png (*.png)|*.png";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                WaitingForm form = new WaitingForm();
                form.Show();
                Application.DoEvents();

                for (int i = 0; i < dlg.FileNames.Count(); i++)
                {
                    using (XNAUtils utils = new XNAUtils())
                    {
                        byte[] data = File.ReadAllBytes(dlg.FileNames[i]);
                        Image image = byteArrayToImage(data);
                        Battler battler = new Battler(dlg.SafeFileNames[i], utils.ConvertToTexture(image));
                        LIST_Battlers.Items.Add(battler);
                    }
                }

                form.Close();
            }
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public IEnumerable<Battler> GetAllBattlers()
        {
            foreach (Battler item in LIST_Battlers.Items) yield return item;
        }

        private void BTN_Delete_Click(object sender, EventArgs e)
        {
            if (LIST_Battlers.SelectedItem != null) LIST_Battlers.Items.RemoveAt(LIST_Battlers.SelectedIndex);
        }

        private void LIST_Battlers_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (XNAUtils utils = new XNAUtils()) PN_Preview.BackgroundImage = utils.ConvertToImage(LIST_Battlers.SelectedItem == null ? null : (LIST_Battlers.SelectedItem as Battler).Img);
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}
