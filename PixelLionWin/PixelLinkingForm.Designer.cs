﻿namespace PixelLionWin
{
    partial class PixelLinkingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PixelLinkingForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CB_CreateFolders = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CB_SingleFile = new System.Windows.Forms.CheckBox();
            this.CB_IncludeMaps = new System.Windows.Forms.CheckBox();
            this.BTN_Browse = new System.Windows.Forms.Button();
            this.TB_Path = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TB_Name = new System.Windows.Forms.TextBox();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::PixelLionWin.Properties.Resources.PixelLinkSideBanner;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(180, 501);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(184, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pixel Link";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label2.Location = new System.Drawing.Point(188, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(295, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Communication facile entre un jeu XNA et un projet Pixel Lion";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nom";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CB_CreateFolders);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.CB_SingleFile);
            this.groupBox1.Controls.Add(this.CB_IncludeMaps);
            this.groupBox1.Controls.Add(this.BTN_Browse);
            this.groupBox1.Controls.Add(this.TB_Path);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TB_Name);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(203, 93);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(367, 293);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informations sur le jeu";
            // 
            // CB_CreateFolders
            // 
            this.CB_CreateFolders.AutoSize = true;
            this.CB_CreateFolders.Location = new System.Drawing.Point(32, 224);
            this.CB_CreateFolders.Name = "CB_CreateFolders";
            this.CB_CreateFolders.Size = new System.Drawing.Size(176, 17);
            this.CB_CreateFolders.TabIndex = 11;
            this.CB_CreateFolders.Text = "Créer des dossiers catégoriques";
            this.CB_CreateFolders.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label6.Location = new System.Drawing.Point(29, 244);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(284, 26);
            this.label6.TabIndex = 10;
            this.label6.Text = "Si cette case est cochée, Pixel Lion créera un dossier pour\r\nchaque type de resso" +
    "urces";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label5.Location = new System.Drawing.Point(29, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(290, 26);
            this.label5.TabIndex = 10;
            this.label5.Text = "Si cette case est cochée, Pixel Lion exportera le jeu en tant \r\nqu\'un seul fichie" +
    "r contenant toutes les ressources";
            // 
            // CB_SingleFile
            // 
            this.CB_SingleFile.AutoSize = true;
            this.CB_SingleFile.Location = new System.Drawing.Point(32, 117);
            this.CB_SingleFile.Name = "CB_SingleFile";
            this.CB_SingleFile.Size = new System.Drawing.Size(117, 17);
            this.CB_SingleFile.TabIndex = 9;
            this.CB_SingleFile.Text = "Faire un seul fichier";
            this.CB_SingleFile.UseVisualStyleBackColor = true;
            // 
            // CB_IncludeMaps
            // 
            this.CB_IncludeMaps.AutoSize = true;
            this.CB_IncludeMaps.Location = new System.Drawing.Point(32, 177);
            this.CB_IncludeMaps.Name = "CB_IncludeMaps";
            this.CB_IncludeMaps.Size = new System.Drawing.Size(172, 17);
            this.CB_IncludeMaps.TabIndex = 8;
            this.CB_IncludeMaps.Text = "Inclure les cartes dans le projet";
            this.CB_IncludeMaps.UseVisualStyleBackColor = true;
            // 
            // BTN_Browse
            // 
            this.BTN_Browse.Location = new System.Drawing.Point(305, 70);
            this.BTN_Browse.Name = "BTN_Browse";
            this.BTN_Browse.Size = new System.Drawing.Size(30, 23);
            this.BTN_Browse.TabIndex = 7;
            this.BTN_Browse.Text = "...";
            this.BTN_Browse.UseVisualStyleBackColor = true;
            this.BTN_Browse.Click += new System.EventHandler(this.BTN_Browse_Click);
            // 
            // TB_Path
            // 
            this.TB_Path.Location = new System.Drawing.Point(114, 72);
            this.TB_Path.Name = "TB_Path";
            this.TB_Path.ReadOnly = true;
            this.TB_Path.Size = new System.Drawing.Size(185, 20);
            this.TB_Path.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Emplacement";
            // 
            // TB_Name
            // 
            this.TB_Name.Location = new System.Drawing.Point(114, 33);
            this.TB_Name.Name = "TB_Name";
            this.TB_Name.Size = new System.Drawing.Size(221, 20);
            this.TB_Name.TabIndex = 4;
            // 
            // BTN_Save
            // 
            this.BTN_Save.BackgroundImage = global::PixelLionWin.Properties.Resources.AcceptIcon;
            this.BTN_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Save.Location = new System.Drawing.Point(508, 436);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(62, 53);
            this.BTN_Save.TabIndex = 5;
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // PixelLinkingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 501);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PixelLinkingForm";
            this.Text = "Pixel Link";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BTN_Browse;
        private System.Windows.Forms.TextBox TB_Path;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TB_Name;
        private System.Windows.Forms.CheckBox CB_IncludeMaps;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox CB_SingleFile;
        private System.Windows.Forms.CheckBox CB_CreateFolders;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button BTN_Save;
    }
}