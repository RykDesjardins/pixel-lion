﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace PixelLionWin
{
    public enum PopupReason
    {
        Success, Error, Warning
    }

    public partial class PopupNotification : Form
    {
        static Int32 NotifIndex = 0;
        Int32 CurrentIndex = 0;
        Thread FadeThread;

        delegate void DELSetOpacity(Single op);
        delegate void DELSetLocation(Point loc);
        delegate void DELRefresh();
        delegate void DELClose();
        volatile Boolean IsClosed;

        public PopupNotification(String Title, String Details, PopupReason reason)
        {
            InitializeComponent();

            label1.Text = Title;
            label2.Text = Details;

            if (NotifIndex < 1) NotifIndex = 1;
            else NotifIndex++;

            switch (reason)
            {
                case PopupReason.Success:
                    PN_Icon.BackgroundImage = Properties.Resources.AcceptIcon;
                    break;

                case PopupReason.Error:
                    PN_Icon.BackgroundImage = Properties.Resources.CloseIcon;
                    break;

                case PopupReason.Warning:
                    PN_Icon.BackgroundImage = Properties.Resources.WarningIcon;
                    break;

                default:
                    PN_Icon.BackgroundImage = Properties.Resources.AcceptIcon;
                    break;
            }

            CurrentIndex = NotifIndex;

            IsClosed = false;
        }

        void SetOpacity(Single op)
        {
            if (this.InvokeRequired)
            {
                DELSetOpacity d = new DELSetOpacity(SetOpacity);
                this.Invoke(d, new object[] { op });
            }
            else
            {
                this.Opacity = op;
            }
        }

        void SetLocation(Point loc)
        {
            if (this.InvokeRequired)
            {
                DELSetLocation d = new DELSetLocation(SetLocation);
                this.Invoke(d, new object[] { loc });
            }
            else
            {
                this.Location = loc;
            }
        }

        void RefreshPopup()
        {
            if (this.InvokeRequired)
            {
                DELRefresh d = new DELRefresh(RefreshPopup);
                this.Invoke(d);
            }
            else
            {
                Refresh();
            }
        }

        void ClosePopup()
        {
            if (this.InvokeRequired)
            {
                DELClose d = new DELClose(ClosePopup);
                this.Invoke(d);
            }
            else
            {
                Close();
            }
        }

        public Int32 GetIndex()
        {
            return CurrentIndex;
        }

        private void PopupNotification_FormClosed(object sender, FormClosedEventArgs e)
        {
            IsClosed = true;
            NotifIndex--;

            if (CurrentIndex < NotifIndex) NotifIndex = CurrentIndex;
        }

        public void FadeOut()
        {
            FadeThread = new Thread(new ThreadStart(THREADFadeOut));
            FadeThread.Start();
        }

        public void THREADFadeOut()
        {
            Thread.Sleep(10);

            try
            {
                SetOpacity(0f);

                while (Opacity < 1f)
                {
                    SetOpacity((Single)Opacity + 0.02f);
                    SetLocation(new Point(Location.X - 1, Location.Y));
                    Thread.Sleep(2);

                    if (IsClosed) throw new Exception("Popup closed");
                }

                Thread.Sleep(3000);

                for (Single i = 1; i > 0; i -= 0.05f)
                {
                    SetLocation(new Point(Location.X, Location.Y + 1));
                    SetOpacity(i);
                    Thread.Sleep(5);

                    if (IsClosed) throw new Exception("Popup closed");
                }

            }
            catch (Exception)
            {

            }
            finally
            {
                if (!IsClosed) ClosePopup();
            }
        }

        private void BTN_Close_Click(object sender, EventArgs e)
        {
            if (!IsClosed) ClosePopup();
        }

        private void PopupNotification_Paint(object sender, PaintEventArgs e)
        {
            
        }
    }
}
