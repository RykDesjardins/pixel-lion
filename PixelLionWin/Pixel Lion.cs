﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                                           //
//  __/\\\\\\\\\\\\\_______________________________________/\\\\\\_______________/\\\_______________________________________________         //
//   _\/\\\/////////\\\____________________________________\////\\\______________\/\\\_______________________________________________        //
//    _\/\\\_______\/\\\__/\\\_________________________________\/\\\______________\/\\\______________/\\\_____________________________       //
//     _\/\\\\\\\\\\\\\/__\///___/\\\____/\\\_____/\\\\\\\\_____\/\\\______________\/\\\_____________\///______/\\\\\_____/\\/\\\\\\___      //
//      _\/\\\/////////_____/\\\_\///\\\/\\\/____/\\\/////\\\____\/\\\______________\/\\\______________/\\\___/\\\///\\\__\/\\\////\\\__     //
//       _\/\\\_____________\/\\\___\///\\\/_____/\\\\\\\\\\\_____\/\\\______________\/\\\_____________\/\\\__/\\\__\//\\\_\/\\\__\//\\\_    //
//        _\/\\\_____________\/\\\____/\\\/\\\___\//\\///////______\/\\\______________\/\\\_____________\/\\\_\//\\\__/\\\__\/\\\___\/\\\_   //
//         _\/\\\_____________\/\\\__/\\\/\///\\\__\//\\\\\\\\\\__/\\\\\\\\\___________\/\\\\\\\\\\\\\\\_\/\\\__\///\\\\\/___\/\\\___\/\\\_  //
//          _\///______________\///__\///____\///____\//////////__\/////////____________\///////////////__\///_____\/////_____\///____\///__ //
//                                                                                                                                           //
//****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****-****//
//                                                                                                                                           //
//  Pixel Lion ~                                                                                                                             //
//                                                                                                                                           //
//   Pixel Lion est un logiciel qui permet de créer un projet complet représentant un jeu vidéo.                                             //
//   Les fichiers exportés peuvent être ouverts avec pratiquement n'importe quel librarie puisqu'ils sont sérialisés en objets.              //
//   La DLL PixelLionLib est libre de droit et peut-être utilisée par quiconque. Cependant, aucune valeur monaitaire                         //
//   ne peut être échangée contre la librarie puisqu'elle est gratuite. Le projet est aussi une manière de se pratiquer                      //
//   à appliquer les connaissances actuelles dans un context nouveau et inconnu.                                                             //
//                                                                                                                                           //
//   L'équipe est constituée de deux sections distinctes : Team G33k et Team Art                                                             //
//                                                                                                                                           //
//                                                                                                                                           //
//              "Un projet complet comprend une équipe astucieuse, travaillant sur chaque petit détail; pixel par pixel."                    //
//                                                                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using PixelLionWin;
using System.Threading;
using System.IO;
using SplashScreen;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Design;
using System.Runtime.Serialization.Formatters.Binary;

namespace PixelLionWin
{
    public partial class MainFormPixelLion : Form
    {
        #region MainObjects

        // The project contains all ressources and information on current user session
        PLProject Project;

        // The map buffer is a bitmap on which the map is drawn tile by tile
        // When done with drawing on the buffer, it is drawn like an image on Map Panel
        Texture2D TileSetImage;
        SpriteBatch MapBatch;

        // These objects represent the selected tile(s) in the tileset
        Rectangle CurrentTile;
        Rectangle[,] CurrentBlockTiles;

        // ContextTile is an array of tile representing tiles on which the user right-clicked
        Tile[] ContextTile;

        // The map on which the user is currently working.
        public Map CurrentMap;

        // Defines if at least a map is opened
        Boolean MapIsOpened;

        // Current selected layer, from 1 to 3
        Int32 CurrentLayer;

        // The X and Y position of the current selected tile in the tileset
        Point CurrentTileIndex;

        // The current size of the map in tile count (not by pixels)
        SizeI MapSize;

        // The current selected tool for drawing (Tile, Multiselection, Fill, etc.)
        DrawingTools CurrentTool;

        // You can drag a pEvent by clicking on it without releasing the mouse button and moving it around
        // The Boolean is true when the user is dragging a pEvent with the mouse and the object contains the pEvent being dragged
        Pevent DraggingPevent;
        
        // XNA utilities
        XNAUtils utils;

        // Contains a filepath pointing on the last map that was opened
        String LastOpenedMapFile;

        // True when the user has clicked a first time in the tileset using the Multiselection Tool
        // The Boolean is then set to false when the second click is made
        Boolean MultiSelectionClicked;

        // A control representing a square with semi-transparent white hovering above selected tile in tileset
        SelectedTile sTile = new SelectedTile();

        // The preferences of the current user
        UserPreferences Prefs;

        // This object contains previous states of the map after it has been altered
        MapStateHistory History;

        // A control showing further informations about a pEvent when hovering one with the mouse pointer
        HoverTooltip peventtip;

        // Thie boolean can be accessed from multiple thread at the same time
        // It is true when Pixel Lion is doing background job and the user needs to wait
        volatile Boolean IsWaiting;

        // This thread shows a waiting form to the user while the main thread performs actions
        Thread WaitingThread;

        // List of bubbles to be shown on main form before opening a map
        List<Bubble> Bubbles;

        // The main random feed passed everywhere. There should not be any other Random feed.
        Random Rand;

        #endregion

        #region Initialization

        // First method called when loading main form
        public MainFormPixelLion()
        {
            // Show Splashscreen
            frmSplash splash = new frmSplash(Properties.Resources.AlternateSplashScreen);
            splash.Show();

            MainFormPixelLion variable = Parent as MainFormPixelLion;

            // Initialize controls
            splash.ChangeStatus("Initialisation de Pixel Lion");
            InitializeComponent();
            DisableMapEditing();

            // Object creations
            splash.ChangeStatus("Initialisation d'XNA");
            PresentationParameters pp = new PresentationParameters()
            {
                BackBufferHeight = PN_Map.Height,
                BackBufferWidth = PN_Map.Width,
                DeviceWindowHandle = PN_Map.Handle,
                IsFullScreen = false
            };

            XNADevices.graphicsdevice = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, GraphicsProfile.HiDef, pp);

            splash.ChangeStatus("Création des objets cartes");
            CurrentMap = new Map();
            CurrentTool = DrawingTools.Pen;
            CurrentLayer = 0;
            CurrentTileIndex = new Point();

            MapBatch = new SpriteBatch(XNADevices.graphicsdevice);

            // Loading user preferences
            splash.ChangeStatus("Chargement des préférences");
            GlobalPreferences.Prefs = UserPreferences.LoadFromFile(UserPreferences.DefaultFileName);
            Prefs = GlobalPreferences.Prefs;
            PN_Tileset.BackColor = Prefs.TileSet.BackgroundColor;

            // Variable initialization
            splash.ChangeStatus("Initialisation des variables");
            Rand = new Random();
            MapSize = new SizeI();
            LastOpenedMapFile = "";
            MultiSelectionClicked = false;
            MapIsOpened = false;
            Bubbles = new Bubble[60].Select(x => Bubble.Create(PN_Map.Size, Rand)).ToList();
            CurrentTile = new Rectangle(-Prefs.Map.TileSize.Width, -Prefs.Map.TileSize.Height, 0, 0);
            utils = new XNAUtils();
            XNADevices.EvMarkers = new Texture2D[]
            {
                utils.ConvertToTexture(Properties.Resources.EvMarker1),
                utils.ConvertToTexture(Properties.Resources.EvMarker2),
                utils.ConvertToTexture(Properties.Resources.EvMarker3)
            };

            // Buffering a number of map states for history management
            splash.ChangeStatus("Mise en mémoire tampon de l'historique");
            History = new MapStateHistory(Prefs.Memory.HistoryCount);

            // Creating a map buffer to be drawn on Map Panel
            splash.ChangeStatus("Création d'une image tampon");

            // Project Creation
            splash.ChangeStatus("Création d'un projet");
            Project = new PLProject();

            // This block tries to open the last opened project
            // If not found, will simply continue loading the rest of the form
            try
            {
                splash.ChangeStatus("Vérification du dernier projet ouvert");
                if (File.Exists("LastProjectPath.plcfg"))
                {
                    String proj = File.ReadAllText("LastProjectPath.plcfg").Trim();
                    if (File.Exists(proj))
                    {
                        splash.ChangeStatus("Ouverture du dernier projet");

                        // If a project was found, try opening it
                        try
                        {
                            OpenProject(proj);
                        }
                        catch (Exception)
                        {
                            splash.ChangeStatus("Impossible d'ouvrir le dernier projet");

                            ShowPopup("Projet invalide", "Impossible d'ouvrir le dernier projet ouvert. Celui-ci est peut-être introuvable ou est corrompu.", PopupReason.Error);
                        }
                    }
                    else
                    {
                        ShowPopup("Projet introuvable", "Le dernier projet est introuvable. Il est possible que le fichier aie changé d'emplacement ou qu'il aie été supprimé.", PopupReason.Warning);
                    }
                }
            }
            catch (ArgumentException) { }
            catch (IOException) { }
            catch (InvalidOperationException) { }

            // Locating Welcome Screen
            WelcomeScreenPopup.Location = new System.Drawing.Point(this.Width / 2 - WelcomeScreenPopup.Width / 2, this.Height / 2 - WelcomeScreenPopup.Height / 2);
            WelcomeScreenPopup.Visible = Project.Name != String.Empty;

            // Deletion of temporary files like songs preview
            splash.ChangeStatus("Destruction des fichers temporaires");
            if (Directory.Exists("temps")) Directory.Delete("temp", true);

            // Recreation of a folder meant to contain teporary files
            splash.ChangeStatus("Création des dossiers temporaires");
            Directory.CreateDirectory("temp");
            ShowPopup("Dossier temporaire créé", "Le dossier temporaire contenant les fichiers de sons et autres a été créé avec succès");

            // Check user preferences for if user wants to use double buffering
            if (Prefs.Video.DoubleBuffering)
            {
                try
                {
                    // Turn on double buffering on main Map Panel
                    splash.ChangeStatus("Chargement de l'optimisation graphique");
                    this.SetStyle(
                        ControlStyles.UserPaint |
                        ControlStyles.AllPaintingInWmPaint |
                        ControlStyles.OptimizedDoubleBuffer, true);

                    Thread.Sleep(10);
                    ShowPopup("Optimisation graphique", "La mise en mémoire tampon double (Double Buffering) a été activé avec succès et est bien géré par votre carte vidéo.");
                }
                catch (Exception)
                {
                    ShowPopup("Optimisation graphique", "La mise en mémoire tampon double (Double Buffering) ne peut être activée. Peut-être que votre carte vidéo ne peut prendre en charge cette fonctionnalité.", PopupReason.Warning);
                }
            }

            // Will browse project folder for maps to add in the quick load menu
            splash.ChangeStatus("Chargement des cartes rapides");

            // Initialization of the pEvent tooltip
            peventtip = new HoverTooltip();
            peventtip.Visible = false;
            peventtip.MouseEnter += new EventHandler(peventtip_MouseEnter);
            PN_Map.Controls.Add(peventtip);

            // End of loading
            splash.Close();
        }

        private void RefreshDevice()
        {
            PresentationParameters pp = new PresentationParameters()
            {
                BackBufferHeight = MapSize.Height * Prefs.Map.TileSize.Height,
                BackBufferWidth = MapSize.Width * Prefs.Map.TileSize.Width,
                DeviceWindowHandle = PN_Map.Handle,
                IsFullScreen = false
            };

            XNADevices.graphicsdevice.Reset(pp);
        }

        private void MainFormPixelLion_FormClosed(object sender, FormClosedEventArgs e)
        {
            SongBundle.StopActivity();
            Application.Exit();
        }

        private void MainFormPixelLion_FormClosing(object sender, FormClosingEventArgs e)
        {
            MemoryStream streamCurrent = new MemoryStream();
            BinaryFormatter bFormatterCurrent = new BinaryFormatter();
            bFormatterCurrent.Serialize(streamCurrent, CurrentMap);
            if (File.Exists(LastOpenedMapFile))
            {
                Stream streamOld = File.Open(LastOpenedMapFile, FileMode.Open);
                if (streamCurrent.Length != streamOld.Length)
                {
                    SaveClosingForm closeForm = new SaveClosingForm(this.Text);
                    closeForm.ShowDialog();
                    streamCurrent.Close();
                    streamOld.Close();
                    if (closeForm.Sauvegarder)
                    {
                        SaveMap();
                        SaveProject();
                    }
                }
                else
                {
                    using (PLUtils utils = new PLUtils())
                    {
                        byte[] byteCurrent = utils.ReadToEnd(streamCurrent);
                        byte[] byteOld = utils.ReadToEnd(streamOld);
                        streamCurrent.Close();
                        streamOld.Close();
                        if (!byteCurrent.SequenceEqual(byteOld))
                        {
                            SaveClosingForm closeForm = new SaveClosingForm(this.Text);
                            closeForm.ShowDialog();
                            if (closeForm.Sauvegarder)
                            {
                                SaveMap();
                                SaveProject();
                            }
                        }
                    }
                }
            }
            else
                CheckIfProjectChanged();
        }

        private void CheckIfProjectChanged()
        {
            // Serialize the current project in memory to later compare it
            MemoryStream streamCurrent = new MemoryStream();
            BinaryFormatter bFormatterCurrent = new BinaryFormatter();
            bFormatterCurrent.Serialize(streamCurrent, Project);
            // Get the old project to compare it with the current project
            if (File.Exists("LastProjectPath.plcfg"))
            {
                String proj = File.ReadAllText("LastProjectPath.plcfg").Trim();
                if (File.Exists(proj))
                {
                    Stream streamOld = File.Open(proj, FileMode.Open);
                    // First, just compare the lengh. If they are different, the project has obviously changed
                    if (streamCurrent.Length != streamOld.Length)
                    {
                        streamCurrent.Close();
                        streamOld.Close();
                        SaveClosingForm closeForm = new SaveClosingForm(this.Text);
                        closeForm.ShowDialog();
                        if (closeForm.Sauvegarder)
                            SaveProject();
                    }
                    else
                    {
                        using (PLUtils utils = new PLUtils())
                        {
                            byte[] byteCurrent = utils.ReadToEnd(streamCurrent);
                            byte[] byteOld = utils.ReadToEnd(streamOld);
                            streamCurrent.Close();
                            streamOld.Close();
                            if (!byteCurrent.SequenceEqual(byteOld))
                            {
                                SaveClosingForm closeForm = new SaveClosingForm(this.Text);
                                closeForm.ShowDialog();
                                if (closeForm.Sauvegarder)
                                    SaveProject();
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Waiting Threads

        // Start thread that shows waiting form
        private void StartWaiting()
        {
            WaitingThread = new Thread(new ThreadStart(THREADShowWaitingForm));
            WaitingThread.Start();
        }

        // Tell the waiting form thread to hide the waiting form
        private void StopWaiting()
        {
            IsWaiting = false;
        }

        // The thread showing the waiting form
        private void THREADShowWaitingForm()
        {
            IsWaiting = true;
            WaitingForm waitingform = new WaitingForm();
            waitingform.Show();

            while (IsWaiting)
            {
                waitingform.Refresh();

                Thread.Sleep(10);
            }

            waitingform.Close();
        }

        #endregion

        #region Drawing Map On Panel

        // This method draws the entire map including pEvents covered by a thin grid.                       
        // The LandScape is first drawn so that the player can see it when a tile is empty or transparent.  
        // All tiles and pEvents are then drawn covered by a Fog with a certain blending.                   
        // Finally, a grid is drawn on top of everything following user preferences.                        
        //                                                                                                  
        // In other words, this method draws all the stuff on the main thing in a shiny way.                
        public void DrawMap()
        {
            if (MapSize.Width != 0)
            {
                Clear();

                CurrentMap.Draw(MapBatch, utils);

                Present();
            }
        }

        private void ForceRedraw()
        {
            PN_Map.Refresh();
            DrawMap();
        }

        private void Clear(Color c)
        {
            XNADevices.graphicsdevice.Clear(c);
        }

        private void Clear()
        {
            Clear(Color.White);
        }

        private void Present()
        {
            XNADevices.graphicsdevice.Present();
        }

        #endregion

        #region Tileset Related

        private void OpenTileSet()
        {
            OpenFileDialog dlg = new OpenFileDialog();

            using (XNAUtils utils = new XNAUtils())
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    PN_Tileset.BackgroundImage = System.Drawing.Bitmap.FromFile(dlg.FileName);
                    CurrentMap.TileSet = utils.ConvertToTexture(PN_Tileset.BackgroundImage);
                    TileSetImage = CurrentMap.TileSet;

                    PN_Tileset.Size = new System.Drawing.Size(256, PN_Tileset.BackgroundImage.Height + 2);
                }
                catch (Exception)
                {
                    ShowPopup("Tuiles", "Impossible d'ouvrir l'image. Le fichier n'est pas une image valide.", PopupReason.Warning);
                }
            }
        }

        private void PN_Tileset_MouseClick(object sender, MouseEventArgs e)
        {
            if (Project.Name == null) return;

            if (CurrentMap.TileSet == null) OpenTileSet();
            else
            {
                if (CurrentTool == DrawingTools.Multi)
                {
                    if (e.Button == System.Windows.Forms.MouseButtons.Left)
                    {
                        if (!MultiSelectionClicked)
                        {
                            sTile.Size = Prefs.Map.TileSize;
                            System.Drawing.Point p = new System.Drawing.Point(e.X - e.X % Prefs.Map.TileSize.Width, e.Y - e.Y % Prefs.Map.TileSize.Width);
                            sTile.Location = p;

                            PN_Tileset.Controls.Add(sTile);
                            CurrentTile = new Rectangle(e.X - e.X % Prefs.Map.TileSize.Width, e.Y - e.Y % Prefs.Map.TileSize.Width, Prefs.Map.TileSize.Width, Prefs.Map.TileSize.Width);
                        }
                        else
                        {
                            Point OtherTileIndex = new Point(e.X - e.X % Prefs.Map.TileSize.Width, e.Y - e.Y % Prefs.Map.TileSize.Width);

                            int Width = ((OtherTileIndex.X - CurrentTileIndex.X) / Prefs.Map.TileSize.Width) + 1;
                            int Height = ((OtherTileIndex.Y - CurrentTileIndex.Y) / Prefs.Map.TileSize.Width) + 1;

                            if (Width < 1 && Height < 1)
                            {
                                Point temp;
                                temp = OtherTileIndex;
                                OtherTileIndex = CurrentTileIndex;
                                CurrentTileIndex = temp;

                            }
                            else if (Width < 1)
                            {
                                int temp = CurrentTileIndex.X;
                                CurrentTileIndex.X = OtherTileIndex.X;
                                OtherTileIndex.X = temp;

                            }
                            else if (Height < 1)
                            {
                                int temp = CurrentTileIndex.Y;
                                CurrentTileIndex.Y = OtherTileIndex.Y;
                                OtherTileIndex.Y = temp;

                            }

                            System.Drawing.Point p = new System.Drawing.Point(CurrentTileIndex.X - CurrentTileIndex.X % Prefs.Map.TileSize.Width, CurrentTileIndex.Y - CurrentTileIndex.Y % Prefs.Map.TileSize.Width);
                            sTile.Location = p;

                            Width = ((OtherTileIndex.X - CurrentTileIndex.X) / Prefs.Map.TileSize.Width) + 1;
                            Height = ((OtherTileIndex.Y - CurrentTileIndex.Y) / Prefs.Map.TileSize.Width) + 1;

                            sTile.Size = new System.Drawing.Size(Width * Prefs.Map.TileSize.Width, Height * Prefs.Map.TileSize.Height);
                            CurrentBlockTiles = new Rectangle[Math.Abs(Width), Math.Abs(Height)];

                            for (int i = 0; i < Width; i++)
                            {
                                for (int j = 0; j < Height; j++)
                                {
                                    Rectangle cropRect = new Rectangle((CurrentTileIndex.X - CurrentTileIndex.X % Prefs.Map.TileSize.Width) + i * Prefs.Map.TileSize.Width, (CurrentTileIndex.Y - CurrentTileIndex.Y % Prefs.Map.TileSize.Width) + j * Prefs.Map.TileSize.Height, Prefs.Map.TileSize.Width, Prefs.Map.TileSize.Width);

                                    CurrentBlockTiles[i, j] = cropRect;
                                }
                            }
                        }

                        sTile.Refresh();
                        MultiSelectionClicked = !MultiSelectionClicked;
                    }
                }
                else
                {
                    sTile.Size = Prefs.Map.TileSize;
                    System.Drawing.Point p = new System.Drawing.Point(e.X - e.X % Prefs.Map.TileSize.Width, e.Y - e.Y % Prefs.Map.TileSize.Width);
                    sTile.Location = p;

                    PN_Tileset.Controls.Add(sTile);
                    CurrentTile = new Rectangle(e.X - e.X % Prefs.Map.TileSize.Width, e.Y - e.Y % Prefs.Map.TileSize.Width, Prefs.Map.TileSize.Width, Prefs.Map.TileSize.Width);
                    CurrentTileIndex = new Point(e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Width);
                }
            }
        }

        private void PN_Tileset_MouseMove(object sender, MouseEventArgs e)
        {
            if (CurrentMap.TileSet != null)
            {
                LB_STATUS_Position.Text = e.X / Prefs.Map.TileSize.Width + " x " + e.Y / Prefs.Map.TileSize.Width;
                LB_STATUS_Position.ForeColor = System.Drawing.Color.Blue;
            }
        }

        #endregion

        #region Map Panel Related

        private void PN_Map_MouseClick(object sender, MouseEventArgs e)
        {
            if (Project.Name == null) return;
            if (!CurrentMap.WasInit) return;

            // Avoid out of bound exceptions
            if (e.X / Prefs.Map.TileSize.Width + 1 > CurrentMap.MapSize.Width || e.Y / Prefs.Map.TileSize.Width + 1 > CurrentMap.MapSize.Height)
                return;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                PointI pos = new PointI(e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Width);

                if (CurrentTool == DrawingTools.Pen && CurrentMap.Tiles[CurrentLayer, pos.X, pos.Y].IsAt(new Vector2(CurrentTile.X, CurrentTile.Y)))
                    return;

                History.PushHistory(new MapState(CurrentMap.CloneTiles()));

                switch (CurrentTool)
                {
                    case DrawingTools.Eraser:
                        Erase(e.X, e.Y);
                        break;

                    case DrawingTools.Pen:
                        Draw(e.X, e.Y);
                        break;

                    case DrawingTools.Event:
                        EditEvent(e.X, e.Y);
                        break;

                    case DrawingTools.Fill:
                        FillMap();
                        break;

                    case DrawingTools.Multi:
                        MultiSelect(e.X, e.Y);
                        break;

                    default:
                        break;
                }
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextTile = new Tile[]
                { 
                    CurrentMap.Tiles[0, e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Width], 
                    CurrentMap.Tiles[1, e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Width], 
                    CurrentMap.Tiles[2, e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Width] 
                };

                foreach (Tile tile in ContextTile)
                    tile.ClipboardMode = true;

                OrganizeContextMenu();

                Tile_ContextMenu.Show(MousePosition);
            }
        }

        private void PN_Map_MouseDown(object sender, MouseEventArgs e)
        {
            if (CurrentMap.Tiles != null && CurrentTool == DrawingTools.Event && CurrentMap.Tiles[CurrentLayer, e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Height].pEvent != null)
            {
                DraggingPevent = CurrentMap.Tiles[CurrentLayer, e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Height].pEvent.Clone() as Pevent;
            }
        }

        private void PN_Map_MouseLeave(object sender, EventArgs e)
        {
            LB_STATUS_Position.Text = "Hors du canvas";
            LB_STATUS_Position.ForeColor = System.Drawing.Color.Red;

            DraggingPevent = null;
        }

        private void PN_Map_MouseMove(object sender, MouseEventArgs e)
        {
            LB_STATUS_Position.Text = String.Format("{0} x {1} ({2} x {3})", e.X, e.Y, e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Width);

            if (e.X < 0 || e.X > CurrentMap.MapSize.Width * Prefs.Map.TileSize.Width - 1 || e.Y < 0 || e.Y > CurrentMap.MapSize.Height * Prefs.Map.TileSize.Width - 1)
                LB_STATUS_Position.ForeColor = System.Drawing.Color.Red;
            else
            {
                LB_STATUS_Position.ForeColor = System.Drawing.Color.Black;

                tabControlPanel1.Visible = e.Y < 2;

                if (e.Button == MouseButtons.Left && CurrentTool != DrawingTools.Event) PN_Map_MouseClick(sender, e);
                else
                {
                    if (DraggingPevent != null)
                    {
                        LB_STATUS_Position.Text += " Déplacement de " + (DraggingPevent.Name == String.Empty ? Pevent.NameMissingString : DraggingPevent.Name);
                    }

                    if (CurrentMap.Tiles != null && CurrentMap.Tiles[CurrentLayer, e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Height].pEvent != null)
                    {
                        String name = CurrentMap.Tiles[CurrentLayer, e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Height].pEvent.Name;
                        LB_STATUS_Position.Text += " @" + (name == String.Empty ? Pevent.NameMissingString : name);

                        System.Drawing.Point tiplocation = new System.Drawing.Point(e.X - e.X % Prefs.Map.TileSize.Width + (e.X > PN_Map.Width / 2 ? Prefs.Map.TileSize.Width / 2 - peventtip.Width : Prefs.Map.TileSize.Width / 2), e.Y - e.Y % Prefs.Map.TileSize.Height + (e.Y > PN_Map.Height / 2 ? -peventtip.Height : Prefs.Map.TileSize.Height));

                        if (tiplocation != peventtip.Location && CurrentTool == DrawingTools.Event)
                        {
                            peventtip.SetInfos(CurrentMap.Tiles[CurrentLayer, e.X / Prefs.Map.TileSize.Width, e.Y / Prefs.Map.TileSize.Height].pEvent);
                            peventtip.Location = tiplocation;
                            peventtip.Visible = true;
                        }
                    }
                    else
                    {
                        peventtip.Visible = false;
                        peventtip.Location = new System.Drawing.Point(0, 0);
                    }
                }
            }
        }

        private void PN_Map_Paint(object sender, PaintEventArgs e)
        {
            DrawMap();
        }

        private void tabControlPanel1_TabChanged(object sender, EventArgs e)
        {
            // Pour éviter les anormalités de positions de carte
            PN_Map.Focus();

            Clear(BackColor.ToXnaColor());
            Present();

            CurrentMap = tabControlPanel1.GetCurrentTabItem() as Map;

            PN_Tileset.BackgroundImage = utils.ConvertToImage(CurrentMap.TileSet);
            TileSetImage = CurrentMap.TileSet;

            if (PN_Tileset.BackgroundImage != null) PN_Tileset.Size = new System.Drawing.Size(256, PN_Tileset.BackgroundImage.Height + 2);

            MapSize = CurrentMap.MapSize;

            PN_Map.Dock = DockStyle.None;
            PN_Map.Location = new System.Drawing.Point(0, 0);
            PN_Map.Size = new System.Drawing.Size(CurrentMap.MapSize.Width * Prefs.Map.TileSize.Width + 1, CurrentMap.MapSize.Height * Prefs.Map.TileSize.Width + 1);

            PN_Map.BackgroundImage = null;

            LastOpenedMapFile = CurrentMap.FilePath;
            Text = "Pixel Lion - " + CurrentMap.Name;

            // Enable History
            History.Clear();
            History.PushHistory(new MapState(CurrentMap.CloneTiles()));

            RefreshDevice();
            DrawMap();
        }

        #endregion

        #region Dragging Files Onto Form

        private void PN_Map_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;

            ((Control)sender).Refresh();
        }

        private void PN_Map_DragLeave(object sender, EventArgs e)
        {
            ((Control)sender).Refresh();
        }

        private void PN_Map_DragDrop(object sender, DragEventArgs e)
        {
            String filename = (e.Data.GetData(DataFormats.FileDrop) as Array).GetValue(0).ToString();

            try
            {
                switch (filename.Substring(filename.LastIndexOf('.')))
                {
                    case ".plm":
                        if (Project.Name != String.Empty) Open(filename);
                        break;

                    case ".plproj":
                        OpenProject(filename);
                        break;

                    default:
                        throw new Exception("Fichier invalide");
                }


            }
            catch (Exception)
            {
                ShowPopup("Fichier glissé", "Le fichier glissé est invalide", PopupReason.Error);
            }

            ((Control)sender).Refresh();
        }

        #endregion

        #region Tools Action On Map Panel

        private void Draw(Int32 X, Int32 Y)
        {
            if (CurrentTile.Width != 0 && CurrentTile.Width > 0)
            {
                if (X < PN_Map.Width - 1 && X > 0 && Y < PN_Map.Height - 1 && Y > 0)
                {
                    CurrentMap.Tiles[CurrentLayer, X / Prefs.Map.TileSize.Width, Y / Prefs.Map.TileSize.Width].SetIndex(CurrentTileIndex);
                    CurrentMap.Tiles[CurrentLayer, X / Prefs.Map.TileSize.Width, Y / Prefs.Map.TileSize.Width].WasDrawn = true;
                }

                //DrawTile(X / Prefs.Map.TileSize.Width, Y / Prefs.Map.TileSize.Width);
                DrawMap();
            }
        }

        private void EditEvent(Int32 X, Int32 Y)
        {
            if (!(X < PN_Map.Width - 1 && X > 0 && Y < PN_Map.Height - 1 && Y > 0)) return;

            Int32 l = CurrentLayer;
            Int32 x = X / Prefs.Map.TileSize.Width;
            Int32 y = Y / Prefs.Map.TileSize.Width;

            if (CurrentMap.Tiles[l, x, y].pEvent == null)
            {
                if (DraggingPevent != null)
                {
                    CurrentMap.Tiles[l, x, y].pEvent = DraggingPevent.Clone() as Pevent;
                    if (!((Control.ModifierKeys & Keys.Control) > 0)) CurrentMap.Tiles[l, DraggingPevent.Position.X, DraggingPevent.Position.Y].pEvent = null;

                    CurrentMap.Tiles[l, x, y].pEvent.Position = new System.Drawing.Point(x, y);
                }
                else
                {
                    PeventEditor dlg = new PeventEditor(new System.Drawing.Point(x, y), Project.Ressources.Sprites, Project.Ressources.Classes, new System.Drawing.Size(MapSize.Width, MapSize.Height));
                    dlg.ShowDialog();

                    if (dlg.HasAccepted)
                    {
                        CurrentMap.Tiles[l, x, y].pEvent = dlg.pEvent;
                    }
                    else if (dlg.HasDeleted)
                    {
                        CurrentMap.Tiles[l, x, y].pEvent = null;
                    }
                }
            }
            else
            {
                if (DraggingPevent != null && DraggingPevent.Position == CurrentMap.Tiles[l, x, y].pEvent.Position) DraggingPevent = null;

                if (DraggingPevent != null)
                {
                    CurrentMap.Tiles[l, DraggingPevent.Position.X, DraggingPevent.Position.Y].pEvent = CurrentMap.Tiles[l, x, y].pEvent.Clone() as Pevent;
                    CurrentMap.Tiles[l, DraggingPevent.Position.X, DraggingPevent.Position.Y].pEvent.Position = new System.Drawing.Point(DraggingPevent.Position.X, DraggingPevent.Position.Y);

                    CurrentMap.Tiles[l, x, y].pEvent = DraggingPevent.Clone() as Pevent;
                    CurrentMap.Tiles[l, x, y].pEvent.Position = new System.Drawing.Point(x, y);
                }
                else
                {
                    PeventEditor dlg = new PeventEditor(CurrentMap.Tiles[l, x, y].pEvent, Project.Ressources.Sprites, Project.Ressources.Classes);
                    dlg.ShowDialog();

                    if (dlg.HasAccepted) CurrentMap.Tiles[l, x, y].pEvent = dlg.pEvent.Clone() as Pevent;
                    else if (dlg.HasDeleted) CurrentMap.Tiles[l, x, y].pEvent = null;
                }
            }

            DraggingPevent = null;
            Refresh();
            DrawMap();
        }

        private void FillMap()
        {
            if (CurrentTile.Width == 0 || CurrentTile.Width < 0) return;

            if (CurrentTile != null && MessageBox.Show("Voulez-vous vraiment remplir la carte?", "Remplir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                for (int i = 0; i < MapSize.Width; i++)
                    for (int j = 0; j < MapSize.Height; j++)
                    {
                        CurrentMap.Tiles[CurrentLayer, i, j].SetIndex(CurrentTileIndex);
                        CurrentMap.Tiles[CurrentLayer, i, j].WasDrawn = true;
                    }

            DrawMap();
        }

        private void Erase(Int32 X, Int32 Y)
        {
            if (!(X < PN_Map.Width - 1 && X > 0 && Y < PN_Map.Height - 1 && Y > 0)) return;

            Int32 x = X / Prefs.Map.TileSize.Width;
            Int32 y = Y / Prefs.Map.TileSize.Width;

            CurrentMap.Tiles[CurrentLayer, x, y].WasDrawn = false;

            DrawMap();
        }

        private void MultiSelect(Int32 X, Int32 Y)
        {
            if (CurrentBlockTiles != null && CurrentTile != null && (X + CurrentBlockTiles.GetLength(0) < PN_Map.Width - 1 && X > 0 && Y + CurrentBlockTiles.GetLength(1) < PN_Map.Height - 1 && Y > 0))
            {
                for (int i = 0; i < CurrentBlockTiles.GetLength(0); i++)
                    for (int j = 0; j < CurrentBlockTiles.GetLength(1); j++)
                    {
                        try
                        {
                            Point p = new Point(CurrentTileIndex.X + (i * Prefs.Map.TileSize.Width), CurrentTileIndex.Y + (j * Prefs.Map.TileSize.Height));
                            CurrentMap.Tiles[CurrentLayer, (X / Prefs.Map.TileSize.Width) + i, (Y / Prefs.Map.TileSize.Width) + j].SetIndex(p);
                            CurrentMap.Tiles[CurrentLayer, (X / Prefs.Map.TileSize.Width) + i, (Y / Prefs.Map.TileSize.Width) + j].WasDrawn = true;
                        }
                        catch (IndexOutOfRangeException)
                        { }
                    }

                DrawMap();
            }
        }

        #endregion

        #region Map File Management

        private void SaveMap()
        {
            if (LastOpenedMapFile == "")
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Filter = "Carte Pixel Lion (*.plm)|*.plm";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    if (!dlg.FileName.EndsWith(".plm")) dlg.FileName += ".plm";
                    CurrentMap.SaveToFile(dlg.FileName);
                    LastOpenedMapFile = dlg.FileName;
                }
            }
            else
            {
                StartWaiting();
                Application.DoEvents();

                CurrentMap.SaveToFile(LastOpenedMapFile);
                StopWaiting();

                ShowPopup("Carte sauvegardée", "La carte " + CurrentMap.Name + " à été sauvegardée avec succès dans le fichier " + LastOpenedMapFile);
            }
        }

        private void Open()
        {
            MapNavigator dlg = new MapNavigator(Project.Dir, MapBatch);
            dlg.ShowDialog();

            using (XNAUtils utils = new XNAUtils())
                if (dlg.OpenedFile)
                {
                    // Pour éviter les anormalités de positions de carte
                    PN_Map.Focus();

                    Clear(BackColor.ToXnaColor());
                    Present();

                    StartWaiting();
                    Application.DoEvents();

                    CurrentMap = dlg.SelectedMap;
                    tabControlPanel1.AddTab(CurrentMap);

                    PN_Tileset.BackgroundImage = utils.ConvertToImage(CurrentMap.TileSet);
                    TileSetImage = CurrentMap.TileSet;

                    if (PN_Tileset.BackgroundImage != null) PN_Tileset.Size = new System.Drawing.Size(256, PN_Tileset.BackgroundImage.Height + 2);

                    MapSize = CurrentMap.MapSize;

                    PN_Map.Dock = DockStyle.None;
                    PN_Map.Location = new System.Drawing.Point(0, 0);
                    PN_Map.Size = new System.Drawing.Size(CurrentMap.MapSize.Width * Prefs.Map.TileSize.Width + 1, CurrentMap.MapSize.Height * Prefs.Map.TileSize.Width + 1);

                    PN_Map.BackgroundImage = null;

                    LastOpenedMapFile = dlg.DirPath;
                    Text = "Pixel Lion - " + CurrentMap.Name;

                    // Enable History
                    History.Clear();
                    History.PushHistory(new MapState(CurrentMap.CloneTiles()));

                    // Enable History
                    History.Clear();
                    History.PushHistory(new MapState(CurrentMap.CloneTiles()));

                    EnableMapEditing();

                    StopWaiting();

                    RefreshDevice();
                    DrawMap();
                }
                else
                    RefreshDevice();
        }

        private void Open(String filepath)
        {
            CurrentMap = Map.LoadFromFile(filepath);
            tabControlPanel1.AddTab(CurrentMap);

            Clear(BackColor.ToXnaColor());
            Present();

            using (XNAUtils utils = new XNAUtils()) PN_Tileset.BackgroundImage = utils.ConvertToImage(CurrentMap.TileSet);

            if (PN_Tileset.BackgroundImage != null) PN_Tileset.Size = new System.Drawing.Size(256, PN_Tileset.BackgroundImage.Height + 2);

            MapSize = CurrentMap.MapSize;

            PN_Map.Dock = DockStyle.None;
            PN_Map.Location = new System.Drawing.Point(0, 0);
            PN_Map.Size = new System.Drawing.Size(CurrentMap.MapSize.Width * Prefs.Map.TileSize.Width + 1, CurrentMap.MapSize.Height * Prefs.Map.TileSize.Width + 1);

            PN_Map.BackgroundImage = null;

            TileSetImage = CurrentMap.TileSet;

            LastOpenedMapFile = filepath;
            Text = "Pixel Lion - " + CurrentMap.Name;

            // Enable History
            History.Clear();
            History.PushHistory(new MapState(CurrentMap.CloneTiles()));

            ShowPopup("Carte ouverte", "Ouverture de la carte " + CurrentMap.Name + " réussie à partir du fichier " + LastOpenedMapFile);

            RefreshDevice();
            DrawMap();
        }

        private void CreateNewMap()
        {
            Texture2D CurrentTileFile = CurrentMap.TileSet;
            NewMap dlg = new NewMap();

            dlg.StartPosition = FormStartPosition.CenterParent;

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            using (XNAUtils utils = new XNAUtils())
            {
                if (File.Exists(Project.Dir + "\\" + dlg.GetName() + ".plm"))
                {
                    ShowPopup("Création d'une carte", "Une carte portant le même nom existe déjà. Vous devez choisir une carte avec un nom différent de " + dlg.Name, PopupReason.Error);
                    return;
                }

                // Pour éviter le bug des de la map out of bounds du Panel lors des scrolls à l'extrême bas-droit 
                PN_Map.Focus();

                MapSize = new SizeI(dlg.GetSize());

                CurrentMap = new Map(MapSize, Prefs.Map.TileSize.Width);
                CurrentMap.Name = dlg.GetName();
                CurrentMap.WasInit = true;
                CurrentMap.TileSet = utils.ConvertToTexture(dlg.GetImage());
                CurrentMap.FilePath = Project.Dir + "\\" + dlg.GetName() + ".plm";
                CurrentMap.Save();

                tabControlPanel1.AddTab(CurrentMap);

                PN_Map.Dock = DockStyle.None;
                PN_Map.Location = new System.Drawing.Point(0, 0);
                PN_Map.Size = new System.Drawing.Size(CurrentMap.MapSize.Width * Prefs.Map.TileSize.Width + 1, CurrentMap.MapSize.Height * Prefs.Map.TileSize.Width + 1);

                PN_Map.BackgroundImage = null;
                PN_Map.Refresh();

                PN_Tileset.Size = new System.Drawing.Size(CurrentMap.TileSet.Width, CurrentMap.TileSet.Height);

                LastOpenedMapFile = CurrentMap.FilePath;
                Text = "Pixel Lion - " + CurrentMap.Name;

                EnableMapEditing();

                // Enable History
                History.Clear();
                History.PushHistory(new MapState(CurrentMap.CloneTiles()));

                // Reload Quick Maps
                RefreshDevice();
                DrawMap();
            }
        }

        #endregion

        #region ButtonActions

        private void BTN_New_Click(object sender, EventArgs e)
        {
            CreateNewMap();
        }

        private void BTN_Refresh_Click(object sender, EventArgs e)
        {
            StartWaiting();

            ForceRedraw();
            PN_Tileset.BackColor = Prefs.TileSet.BackgroundColor;

            StopWaiting();
        }

        private void BTN_Open_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            if (sauvegarderToolStripMenuItem.Enabled) SaveMap();
        }

        private void BTN_Layer1_Click(object sender, EventArgs e)
        {
            CurrentLayer = 0;
            BTN_Layer1.Enabled = false;
            BTN_Layer2.Enabled = true;
            BTN_Layer3.Enabled = true;
        }

        private void BTN_Layer2_Click(object sender, EventArgs e)
        {
            CurrentLayer = 1;
            BTN_Layer1.Enabled = true;
            BTN_Layer2.Enabled = false;
            BTN_Layer3.Enabled = true;
        }

        private void BTN_Layer3_Click(object sender, EventArgs e)
        {
            CurrentLayer = 2;
            BTN_Layer1.Enabled = true;
            BTN_Layer2.Enabled = true;
            BTN_Layer3.Enabled = false;
        }

        private void BTN_TransparencyLayers_Click(object sender, EventArgs e)
        {
            LayerTransparencyForm form = new LayerTransparencyForm(CurrentMap.LayersOpacity, this);
            form.ShowDialog();
        }

        private void BTN_PEN_Draw_Click(object sender, EventArgs e)
        {
            CurrentTool = DrawingTools.Pen;

            BTN_PEN_Draw.Enabled = false;
            BTN_PEN_Multi.Enabled = true;
            BTN_PEN_Eraser.Enabled = true;
            BTN_PEN_Fill.Enabled = true;
            BTN_PEN_Event.Enabled = true;
        }

        private void BTN_PEN_Multi_Click(object sender, EventArgs e)
        {
            CurrentTool = DrawingTools.Multi;

            BTN_PEN_Draw.Enabled = true;
            BTN_PEN_Multi.Enabled = false;
            BTN_PEN_Eraser.Enabled = true;
            BTN_PEN_Fill.Enabled = true;
            BTN_PEN_Event.Enabled = true;
        }

        private void BTN_PEN_Eraser_Click(object sender, EventArgs e)
        {
            CurrentTool = DrawingTools.Eraser;

            BTN_PEN_Draw.Enabled = true;
            BTN_PEN_Multi.Enabled = true;
            BTN_PEN_Eraser.Enabled = false;
            BTN_PEN_Fill.Enabled = true;
            BTN_PEN_Event.Enabled = true;
        }

        private void BTN_PEN_Fill_Click(object sender, EventArgs e)
        {
            CurrentTool = DrawingTools.Fill;

            BTN_PEN_Draw.Enabled = true;
            BTN_PEN_Multi.Enabled = true;
            BTN_PEN_Eraser.Enabled = true;
            BTN_PEN_Fill.Enabled = false;
            BTN_PEN_Event.Enabled = true;
        }

        private void BTN_PEN_Event_Click(object sender, EventArgs e)
        {
            CurrentTool = DrawingTools.Event;

            BTN_PEN_Draw.Enabled = true;
            BTN_PEN_Multi.Enabled = true;
            BTN_PEN_Eraser.Enabled = true;
            BTN_PEN_Fill.Enabled = true;
            BTN_PEN_Event.Enabled = false;
        }

        private void Selectionner_Click(object sender, EventArgs e)
        {
            SelectionnerTuile();
        }

        private void SelectionnerTuile()
        {
            Int32 X = ContextTile[0].X;
            Int32 Y = ContextTile[1].Y;

            Tile tile = CurrentMap.Tiles[CurrentLayer, X, Y];

            sTile.Size = Prefs.Map.TileSize;
            sTile.Location = new System.Drawing.Point(tile.TileIndexX, tile.TileIndexY);

            CurrentTile = new Rectangle(tile.TileIndexX * Prefs.Map.TileSize.Width, tile.TileIndexY * Prefs.Map.TileSize.Height, Prefs.Map.TileSize.Width, Prefs.Map.TileSize.Width);
        }

        public void QuickMapLoadClick(Object sender, EventArgs e)
        {
            Open(((ToolStripMenuItem)sender).Name);
            EnableMapEditing();
        }

        private void BTN_Undo_Click(object sender, EventArgs e)
        {
            annulerToolStripMenuItem_Click(sender, e);
        }

        private void BTN_Script_Click(object sender, EventArgs e)
        {
            interpréteurToolStripMenuItem_Click(sender, e);
        }

        private void BTN_ScreenShot_Click(object sender, EventArgs e)
        {
            SaveScreenShot();
        }

        private void BTN_Map_Click(object sender, EventArgs e)
        {
            if (détailsDeLaMapToolStripMenuItem.Enabled) détailsDeLaMapToolStripMenuItem_Click(sender, e);
        }

        private void BTN_Play_Click(object sender, EventArgs e)
        {
            exécuterToolStripMenuItem_Click(sender, e);
        }

        private void EnableEditing()
        {
            sauvegarderToolStripMenuItem1.Enabled = true;
            mapsToolStripMenuItem.Enabled = true;
            outilsToolStripMenuItem.Enabled = true;
            ressourcesToolStripMenuItem.Enabled = true;

            BTN_New.Enabled = true;
            BTN_Open.Enabled = true;
            BTN_Undo.Enabled = true;

            WelcomeScreenPopup.Location = new System.Drawing.Point(this.Width / 2 - WelcomeScreenPopup.Width / 2, this.Height / 2 - WelcomeScreenPopup.Height / 2);
            WelcomeScreenPopup.Visible = true;
        }

        private void EnableMapEditing()
        {
            éditeurToolStripMenuItem.Enabled = true;
            fermerLaMapToolStripMenuItem.Enabled = true;
            sauvegarderToolStripMenuItem.Enabled = true;
            détailsDeLaMapToolStripMenuItem.Enabled = true;
            BTN_Save.Enabled = true;
            BTN_Refresh.Enabled = true;
            aperçuToolStripMenuItem.Enabled = true;
            BTN_ScreenShot.Enabled = true;
            BTN_TransparencyLayers.Enabled = true;
            BTN_Map.Enabled = true;

            MapIsOpened = true;

            WelcomeScreenPopup.Visible = false;
        }

        private void DisableMapEditing()
        {
            éditeurToolStripMenuItem.Enabled = false;
            fermerLaMapToolStripMenuItem.Enabled = false;
            sauvegarderToolStripMenuItem.Enabled = false;
            détailsDeLaMapToolStripMenuItem.Enabled = false;
            BTN_Save.Enabled = false;
            BTN_Refresh.Enabled = false;
            aperçuToolStripMenuItem.Enabled = false;
            BTN_ScreenShot.Enabled = false;
            BTN_TransparencyLayers.Enabled = false;
            BTN_Map.Enabled = false;

            MapIsOpened = false;

            WelcomeScreenPopup.Location = new System.Drawing.Point(this.Width / 2 - WelcomeScreenPopup.Width / 2, this.Height / 2 - WelcomeScreenPopup.Height / 2);
            WelcomeScreenPopup.Visible = true;
            PN_Tileset.Size = new System.Drawing.Size(PN_Tileset.Width, 0);
            PN_Map.Size = PN_MapContainer.Size;
        }

        #endregion

        #region ToolStripActions

        private void chargerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void sauvegarderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveMap();
        }

        private void nouvelleMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateNewMap();
        }

        private void ouvrirUnFichierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DrawMap();
        }

        private void àProposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutPL dlg = new AboutPL();
            dlg.ShowDialog();
        }

        private void configurerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PixelLinkingForm dlg = new PixelLinkingForm(Project.GameLink);
            dlg.ShowDialog();

            if (dlg.DialogResult == DialogResult.OK) Project.GameLink = dlg.GetLink();
        }

        private void combatantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BattlerManager dlg = new BattlerManager(Project.Ressources.Battlers);
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) Project.Ressources.Battlers = dlg.GetAllBattlers().ToList();
        }

        private void zoneDeTéléportationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Drawing.Point laCase = new System.Drawing.Point(ContextTile[0].X, ContextTile[1].Y);
            Map laMap;
            MapNavigator dlg = new MapNavigator(Project.Dir, MapBatch);
            dlg.ShowDialog();
            if (dlg.OpenedFile)
            {
                Application.DoEvents();
                laMap = dlg.SelectedMap;

                SelectTeleportationForm browser = new SelectTeleportationForm(laMap, laCase, CurrentLayer, CurrentMap, MapBatch);
                browser.ShowDialog();
                if (browser.ajouté)
                {
                    DrawMap();
                    // TODO: Little flaw here: the pEvent icon is not drawn until you scroll the panel
                    // How the fuck can we handle this?
                    // What has been tried so far: - Scroll vertical += 10 then -= 10
                    //                             - Refresh() instead of DrawMap() (makes it worse; the panel is grey until you scroll)
                    //                             - DrawMap() twice
                    //                             - Refresh() then DrawMap()
                }
            }

            RefreshDevice();
        }

        private void préférencesUtilisateurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PreferencesDialog dlg = new PreferencesDialog(GlobalPreferences.Prefs, this);
            UserPreferences oldPrefs = new UserPreferences( GlobalPreferences.Prefs );

            switch (dlg.ShowDialog())
            {
                case System.Windows.Forms.DialogResult.OK:
                    {
                        Prefs = dlg.GeneratePreferences();
                        GlobalPreferences.Prefs = Prefs;
                        Prefs.SaveToFile(UserPreferences.DefaultFileName);
                        ShowPopup("Sauvegarde des préférences", "Les préférences ont été sauvegardées avec succèes.");
                    }
                    break;

                case System.Windows.Forms.DialogResult.Retry:
                    {
                        Prefs = dlg.GeneratePreferences();
                        Prefs.SaveToFile(UserPreferences.DefaultFileName);

                        Application.Restart();
                    }
                    break;
                case System.Windows.Forms.DialogResult.Ignore:
                    {
                        GlobalPreferences.Prefs = oldPrefs;
                        History.Clear();
                        ShowPopup("Historique", "L'historique a été nettoyé avec succèes.");
                    }
                    break;

                case System.Windows.Forms.DialogResult.Yes:
                    {
                        GlobalPreferences.Prefs = oldPrefs;
                        importerToolStripMenuItem_Click(sender, e);
                    }
                    break;

                case System.Windows.Forms.DialogResult.No:
                    {
                        GlobalPreferences.Prefs = oldPrefs;
                        exporterToolStripMenuItem_Click(sender, e);
                    }
                    break;

                default:
                    GlobalPreferences.Prefs = oldPrefs;
                    break;
            }

            Refresh();
        }

        private void importerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Préférences Pixel Lion (*.plpref)|*.plpref";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    Prefs = UserPreferences.LoadFromFile(dlg.FileName);
                    GlobalPreferences.Prefs = Prefs;
                    ShowPopup("Importation", "Les préférences ont été importées avec succèes.");
                }
                catch (Exception)
                {
                    ShowPopup("Importation des préférences", "Impossible d'importer les préférences. Fichier invalide.", PopupReason.Error);
                }
            }
        }

        private void exporterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Préférences Pixel Lion (*.plpref)|*.plpref";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    Prefs.SaveToFile(dlg.FileName);
                    ShowPopup("Exportation", "Les préférences ont été exportées avec succèes.");
                }
                catch (Exception)
                {
                    ShowPopup("Exportation des préférences", "Impossible d'exporter les préférences.", PopupReason.Error);
                }
            }
        }

        private void exécuterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Project.GameLink.Path == String.Empty)
            {
                configurerToolStripMenuItem_Click(sender, e);
            }
            else
            {
                PixelLinkingGame dlg = new PixelLinkingGame(Project);
                dlg.ShowDialog();
            }
        }

        private void grandeurInitialeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExitFullScreen();
        }

        private void spritesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SpritesForm dlg = new SpritesForm(Project.Ressources.Sprites);
            dlg.ShowDialog();

            if (dlg.HasSaved)
            {
                foreach (Sprite sprite in dlg.SpriteBundle.Sprites)
                {
                    if (sprite.tempTileX != 0) sprite.TileCount.X = sprite.tempTileX;
                    if (sprite.tempTileY != 0) sprite.TileCount.Y = sprite.tempTileY;
                }

                Project.Ressources.Sprites = dlg.SpriteBundle;
            }
        }

        private void limitesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DrawLimits dlg = new DrawLimits(CurrentMap);
            dlg.ShowDialog();

            if (dlg.HasSaved) using (XNAUtils utils = new XNAUtils()) CurrentMap.Limits = utils.ConvertToTexture(dlg.GetLimits());
        }

        private void ouvrirUnFichierToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FogSelector dlg;

            if (CurrentMap.FogOverlay != null) dlg = new FogSelector(CurrentMap.FogOverlay);
            else dlg = new FogSelector();

            dlg.ShowDialog();

            if (dlg.HasSaved)
            {
                CurrentMap.FogOverlay = dlg.fog;
            }

            DrawMap();
        }

        private void annulerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (History.HasNodes()) CurrentMap.Tiles = History.GetLastState().GetTiles();
            
        }

        private void afficherLhistoriqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistoryManager dlg = new HistoryManager(History, CurrentMap.TileSet);

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                CurrentMap.Tiles = dlg.GetState().GetTiles();
                ForceRedraw();
            }
        }

        private void baseDeDonnéesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseEditor dlg = new DatabaseEditor(Project.Database, Project.Ressources);
            dlg.ShowDialog();

            Project.Database = dlg.DB;
        }

        private void classesDePEventToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClassManager dlg = new ClassManager(Project.Ressources.Classes);
            dlg.ShowDialog();
        }

        private void iconesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IconManager dlg = new IconManager(Project.Ressources.Icons);
            dlg.ShowDialog();

            if (dlg.HasAccepted)
                Project.Ressources.Icons = dlg.GetIcons();
        }

        private void sonsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Project.Ressources.Sounds == null) Project.Ressources.Sounds = new SoundBundle();

            SoundManager dlg = new SoundManager(Project.Ressources.Sounds);

            dlg.ShowDialog();
            if (dlg.HasSaved) Project.Ressources.Sounds = dlg.GetBundle;
        }

        private void pleinÉcranToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EnterFullScreen();
        }

        private void sauvegarderToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (!((ToolStripMenuItem)sender).Enabled) return;

            SaveProject();
        }

        private void SaveProject()
        {
            try
            {
                if (File.Exists("LastProjectPath.plcfg"))
                {
                    String proj = File.ReadAllText("LastProjectPath.plcfg").Trim();
                    if (File.Exists(proj))
                    {
                        try
                        {
                            Project.Save(proj);

                            ShowPopup("Projet sauvegardé", "Le projet " + Project.Name + " a été sauvegardé avec succès dans le fichier " + proj);
                            return;
                        }
                        catch (Exception)
                        {
                            ShowPopup("Erreur", "Impossible de sauvegarder le projet " + Project.Name + ".");
                        }
                    }
                }
            }
            catch (ArgumentException) { }
            catch (IOException) { }
            catch (InvalidOperationException) { }

            Project.Save();
        }

        private void détailsDeLaMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CurrentMap.Name != null && CurrentMap.TileSet != null)
            {
                MapInfos dlg = new MapInfos(CurrentMap, Project.Name, LastOpenedMapFile);
                dlg.ShowDialog();

                if (dlg.result == DialogResult.OK)
                {
                    StartWaiting();
                    Application.DoEvents();

                    String file = LastOpenedMapFile;

                    CurrentMap = dlg.GetMap();
                    CurrentMap.SaveToFile(file);

                    fermerLaMapToolStripMenuItem_Click(this, new EventArgs());
                    Open(file);
                    EnableMapEditing();

                    StopWaiting();
                }
            }
            else
            {
                MessageBox.Show("Aucune carte n'a été crée.\nVous devez créer une carte avant de l'éditer", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void détailsDuProjetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Project.Name != null)
            {
                ProjectDetails dlg = new ProjectDetails(Project);
                dlg.ShowDialog();
            }
        }

        private void supprimerLePaysageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentMap.LandScape = new Landscape();

            PN_Map.BackgroundImage = null;
            PN_Map.BackgroundImageLayout = ImageLayout.None;
        }

        private void choisirLaTrameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Project.Ressources.Songs == null) Project.Ressources.Songs = new SongBundle();

            SongManager dlg = new SongManager(Project.Ressources.Songs);
            dlg.ShowDialog();

            if (dlg.HasSaved) Project.Ressources.Songs = dlg.GetBundle;
        }

        private void fermerLaMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Map map = tabControlPanel1.RemoveCurrentObject() as Map;

            CurrentMap = map == null ? new Map() : map;

            if (map == null)
            {
                CurrentLayer = 0;
                CurrentTileIndex = new Point();
                MapSize = new SizeI();
                LastOpenedMapFile = "";
                PN_Tileset.BackgroundImage = null;
                Text = "Pixel Lion";

                DisableMapEditing();
            }

            RefreshDevice();
            Refresh();
            DrawMap();
        }

        private void nouveauProjetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewProject progdlg = new NewProject();

            if (progdlg.ShowDialog() == DialogResult.OK)
            {
                Project = new PLProject();
                Project.PixelMonkeyCore = Properties.Resources.DefaultPMSLCore;
                
                Project.Name = progdlg.GetName();

                Project.Save(progdlg.GetPath());
                Project.Description = progdlg.GetDescription();

                FileStream stream = File.Create("LastProjectPath.plcfg");
                StreamWriter writer = new StreamWriter(stream);
                writer.WriteLine(progdlg.GetPath());
                writer.Close();
                stream.Close();

                EnableEditing();

                ShowPopup("Projet créé", "Création du projet " + Project.Name + " réussie. Le projet est enregistrer à l'emplacement suivant : " + progdlg.GetPath());
            }
        }

        private void quitterToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void ouvrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Pixel Lion Project|*.plproj";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                StartWaiting();
                Application.DoEvents();

                OpenProject(dlg.FileName);

                StopWaiting();
            }
        }

        private void TS_PeventListe_Click(object sender, EventArgs e)
        {
            if (CurrentMap.GetAllPeventsList().Count > 0)
            {
                PeventLister dlg = new PeventLister(CurrentMap.GetAllPeventsList(), Project.Ressources.Sprites, Project.Ressources.Classes, CurrentMap);
                dlg.ShowDialog();
            }
            else
                MessageBox.Show("Aucun pEvent dans la carte!");
        }

        private void valeursParDéfautToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez-vous vraiment restaurer les valeurs par défaut?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                Prefs = new UserPreferences();
                GlobalPreferences.Prefs = new UserPreferences();

                ShowPopup("Préférences", "Les valeurs par défaut ont été affectées aux préférences utilisateur.");
            }
        }

        private void interpréteurToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region Key Pressed

        private void MainFormPixelLion_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Z:
                    if ((e.Modifiers & Keys.Control) > 0)
                        annulerToolStripMenuItem_Click(sender, new EventArgs());
                    break;

                case Keys.S:
                    if ((e.Modifiers & Keys.Control) > 0)
                        sauvegarderToolStripMenuItem1_Click(this, new EventArgs());
                    else
                        BTN_Save_Click(this, new EventArgs());
                    break;

                case Keys.Right:
                    if ((e.Modifiers & Keys.Control) > 0 && CurrentMap.Name != null)
                        tabControlPanel1.NextTab();
                    break;

                case Keys.Left:
                    if ((e.Modifiers & Keys.Control) > 0 && CurrentMap.Name != null)
                        tabControlPanel1.PreviousTab();
                    break;

                case Keys.W:
                    if ((e.Modifiers & Keys.Control) > 0 && fermerLaMapToolStripMenuItem.Enabled)
                         fermerLaMapToolStripMenuItem_Click(sender, e);
                    break;

                case Keys.O:
                    if ((e.Modifiers & Keys.Control) > 0)
                        ouvrirToolStripMenuItem_Click(this, new EventArgs());
                    else
                        BTN_Open_Click(this, new EventArgs());
                    break;

                case Keys.E:
                    BTN_PEN_Event_Click(this, new EventArgs());
                    break;

                case Keys.P:
                    BTN_PEN_Draw_Click(this, new EventArgs());
                    break;

                case Keys.F:
                    BTN_PEN_Fill_Click(this, new EventArgs());
                    break;

                case Keys.X:
                    BTN_PEN_Eraser_Click(this, new EventArgs());
                    break;

                case Keys.D1:
                    BTN_Layer1_Click(this, new EventArgs());
                    break;

                case Keys.D2:
                    BTN_Layer2_Click(this, new EventArgs());
                    break;

                case Keys.D3:
                    BTN_Layer3_Click(this, new EventArgs());
                    break;

                case Keys.Escape:
                    ExitFullScreen();
                    break;

                case Keys.F11:
                    EnterFullScreen();
                    break;
            }
        }

        #endregion

        #region Other Graphics

        public void ShowPopup(String Title, String Message, PopupReason reason = PopupReason.Success)
        {
            if (Prefs.Video.ShowPopups)
            {
                PopupNotification popup = new PopupNotification(Title, Message, reason);
                popup.Location = new System.Drawing.Point(Screen.PrimaryScreen.Bounds.Width - popup.Width + 50, Screen.PrimaryScreen.Bounds.Height - (popup.GetIndex() - 1) * popup.Height - popup.Height);
                popup.Opacity = 0;
                popup.Show();

                popup.FadeOut();

                Focus();
            }
        }

        private void EnterFullScreen()
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Size = new System.Drawing.Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            this.Location = new System.Drawing.Point(0, 0);
        }

        private void ExitFullScreen()
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Size = this.MinimumSize;
            this.Location = new System.Drawing.Point(20, 20);
        }

        private void MainFormPixelLion_Resize(object sender, EventArgs e)
        {
            if (MapIsOpened) DrawMap();
            else
            {
                PN_Map.Size = PN_MapContainer.Size;

                WelcomeScreenPopup.Location = new System.Drawing.Point(this.Width / 2 - WelcomeScreenPopup.Width / 2, this.Height / 2 - WelcomeScreenPopup.Height / 2);
            }
            
        }

        private void PN_MapContainer_Scroll(object sender, ScrollEventArgs e)
        {
            //DrawMap();
        }

        private void TIMER_MemoryUsage_Tick(object sender, EventArgs e)
        {
            LB_MemoryUsed.Text = "Mémoire vive allouée : " + (System.Diagnostics.Process.GetCurrentProcess().WorkingSet64 / 1024.0f / 1024.0f).ToString() + " Mb";
        }

        private void SaveScreenShot()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "PNG (*.png)|*.png";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Int32 w = XNADevices.graphicsdevice.PresentationParameters.BackBufferWidth;
                Int32 h = XNADevices.graphicsdevice.PresentationParameters.BackBufferHeight;

                //pull the picture from the buffer
                Int32[] backBuffer = new int[w * h];
                XNADevices.graphicsdevice.GetBackBufferData(backBuffer);
            
                //copy into a texture
                Clear();
                CurrentMap.Draw(MapBatch, utils);

                Texture2D texture = new Texture2D(XNADevices.graphicsdevice, w, h, false, XNADevices.graphicsdevice.PresentationParameters.BackBufferFormat);
                texture.SetData(backBuffer);

                //save to disk
                Stream stream = File.OpenWrite(dlg.FileName);
                texture.SaveAsPng(stream, w, h);
                stream.Close();

                Present();
            }
        }



        #endregion

        #region Context Menu And Tooltips

        private void OrganizeContextMenu()
        {
            pEventToolStripMenuItem.Enabled = ContextTile[CurrentLayer].pEvent != null;
        }

        void peventtip_MouseEnter(object sender, EventArgs e)
        {
            LB_STATUS_Position.ForeColor = System.Drawing.Color.Green;
            LB_STATUS_Position.Text = "Menu pEvent";
        }

        private void Tile_ContextMenu_MouseEnter(object sender, EventArgs e)
        {
            LB_STATUS_Position.Text = "Menu Contextuel";
            LB_STATUS_Position.ForeColor = System.Drawing.Color.Green;
        }

        private void premierÉtageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetData("Tile", ContextTile[0]);
        }

        private void deuxièmeÉtageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetData("Tile", ContextTile[1]);
        }

        private void troisièmeÉtageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetData("Tile", ContextTile[2]);
        }

        private void pEventToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetData("pEventArray",
                new Pevent[] 
                { 
                    ContextTile[0].pEvent,
                    ContextTile[1].pEvent,
                    ContextTile[2].pEvent
                }
            );
        }

        private void tuileEntièreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetData("TileArray", ContextTile);
        }

        private void effacerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Int32 X = ContextTile[0].X;
            Int32 Y = ContextTile[1].Y;

            for (int i = 0; i < 3; i++)
            {
                CurrentMap.Tiles[i, X, Y].WasDrawn = false;
                CurrentMap.Tiles[i, X, Y].pEvent = null;
                CurrentMap.Tiles[i, X, Y].TileIndexX = -Prefs.Map.TileSize.Width;
                CurrentMap.Tiles[i, X, Y].TileIndexY = -Prefs.Map.TileSize.Height;
            }

            Refresh();
            DrawMap();
        }

        private void collerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Int32 X = ContextTile[0].X;
            Int32 Y = ContextTile[1].Y;

            if (Clipboard.ContainsData("Tile"))
            {
                Tile tile = (Clipboard.GetData("Tile") as Tile).Clone() as Tile;
                tile.X = X;
                tile.Y = Y;

                CurrentMap.Tiles[CurrentLayer, X, Y] = tile;
            }
            else if (Clipboard.ContainsData("pEventArray"))
            {
                Pevent[] pevents = Clipboard.GetData("pEventArray") as Pevent[];

                for (int i = 0; i < 3; i++)
                {
                    //old code : CurrentMap.Tiles[i, X, Y].pEvent = pevents[i];
                    if (pevents[i] != null)
                    {
                        Pevent p = pevents[i];
                        System.Drawing.Point point = new System.Drawing.Point(X, Y);
                        p.Position = point;

                        CurrentMap.Tiles[i, X, Y].pEvent = p;
                    }
                }
            }
            else if (Clipboard.ContainsData("TileArray"))
            {
                Tile[] tiles = Clipboard.GetData("TileArray") as Tile[];

                // TODO: REMOVE BUG
                for (int i = 0; i < 3; i++)
                {
                    Tile tile = tiles[i].Clone() as Tile;
                    tile.SetPosition(new Point(X, Y));

                    CurrentMap.Tiles[CurrentLayer, X, Y] = tile;
                }
            }

            Refresh();
            DrawMap();
        }

        private void aperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapPreviewBrowser dlg = new MapPreviewBrowser(CurrentMap, MapBatch);
            dlg.ShowDialog();

            RefreshDevice();
        }

        private void infoSurLaTuileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DebugTileDialog dlg = new DebugTileDialog(ContextTile, Project.Ressources);
            dlg.ShowDialog();
        }

        private void animationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnimationStudio dlg = new AnimationStudio(Project.Ressources.Animations, Project.Ressources.Battlers, MapBatch);
            dlg.ShowDialog();

            Project.Ressources.Animations = dlg.GetAnimationsFromList().ToList();
            RefreshDevice();
        }

        private void remplirAvecCetteTuileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SelectionnerTuile();
            FillMap();
        }

        private void pEventToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            EditEvent(ContextTile[0].X * Prefs.Map.TileSize.Width, ContextTile[1].Y * Prefs.Map.TileSize.Height);
        }

        #endregion

        #region Project Related

        private void OpenProject(String path)
        {
            PLProject temp = PLProject.Load(path);

            if (temp == null)
                throw new Exception("Projet invalide");
            else
            {
                Project = temp;

                EnableEditing();
                ShowPopup("Projet chargée", "Le projet " + Project.Name + " a été chargé avec succès à partir du fichier " + Project.Dir);

                FileStream stream = File.Create("LastProjectPath.plcfg");
                StreamWriter writer = new StreamWriter(stream);
                writer.WriteLine(path);
                writer.Close();
                stream.Close();

                WelcomeScreenPopup.SetInformations(Project);
                WelcomeScreenPopup.Visible = true;
            }
        }

        #endregion

        #region Utility

        private Boolean CompareTiles(Tile o1, Tile o2)
        {
            return o1 == o2;
        }

        #endregion

        #region Pixel Monkey

        private void fichierMaîtreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PixelCodeEditor dlg = new PixelCodeEditor(Project.PixelMonkeyCore);
            dlg.ShowDialog();

            Project.PixelMonkeyCore = dlg.GetCode();
        }

        private void navigateurDuContenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                PixelMonkey.PixelMonkeyLanguage Language = PixelMonkey.PixelMonkeyLanguage.LoadFromString(Project.PixelMonkeyCore);

                PixelMonkeyNavigator dlg = new PixelMonkeyNavigator(Language);
                dlg.ShowDialog();
            }
            catch (PixelMonkey.PixelMonkeyCoreException)
            {
                MessageBox.Show("Impossible d'interpréter le code maître. Veuillez vérifier le contenu du fichier maître.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion

        #region Welcome Screen

        private void WelcomeScreenPopup_DBClick(object sender, EventArgs e)
        {
            baseDeDonnéesToolStripMenuItem_Click(sender, e);
        }

        private void WelcomeScreenPopup_InterpreterClick(object sender, EventArgs e)
        {
            interpréteurToolStripMenuItem_Click(sender, e);
        }

        private void WelcomeScreenPopup_NewMapClick(object sender, EventArgs e)
        {
            nouvelleMapToolStripMenuItem_Click(sender, e);
        }

        private void WelcomeScreenPopup_NotesClick(object sender, EventArgs e)
        {

        }

        private void WelcomeScreenPopup_PixelLinkConfigClick(object sender, EventArgs e)
        {
            configurerToolStripMenuItem_Click(sender, e);
        }

        private void WelcomeScreenPopup_SongListClick(object sender, EventArgs e)
        {
            choisirLaTrameToolStripMenuItem_Click(sender, e);
        }

        private void WelcomeScreenPopup_SpriteListClick(object sender, EventArgs e)
        {
            spritesToolStripMenuItem_Click(sender, e);
        }

        private void WelcomeScreenPopup_StatsClick(object sender, EventArgs e)
        {

        }

        #endregion
    }
}
