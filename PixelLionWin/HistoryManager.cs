﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionWin
{
    public partial class HistoryManager : Form
    {
        MapStateHistory history;
        Int32 length;
        MapState returnedState;

        public HistoryManager(MapStateHistory msh, Texture2D tileset)
        {
            InitializeComponent();

            history = msh;
            length = 10;

            using (XNAUtils utils = new XNAUtils())
            foreach (MapState item in history.GetAllStates())
            {
                Panel pn = new Panel();
                pn.Height = 320;
                pn.Width = 500;

                HistoryButton btn = new HistoryButton();
                btn.BackgroundImage = utils.ConvertToImage(item.GenerateMap(tileset));
                btn.BackgroundImageLayout = ImageLayout.Zoom;
                btn.Width = 480;
                btn.Height = 280;
                btn.Location = new System.Drawing.Point(10, 10);
                btn.mapState = item;
                btn.Click += new EventHandler(OnClick);

                Label lbl = new Label();
                lbl.Text = String.Format("[{0}] - {1}", item.TimeStamp.ToLongDateString(), item.TimeStamp.ToLongTimeString());
                lbl.Location = new System.Drawing.Point(10, 300);
                lbl.Size = new System.Drawing.Size(300, 30);

                pn.Controls.Add(btn);
                pn.Controls.Add(lbl);
                pn.Location = new System.Drawing.Point(length, 10);
                length += 520;

                PN_History.Controls.Add(pn);
            }
        }

        public void OnClick(Object sender, EventArgs o)
        {
            returnedState = ((HistoryButton)sender).mapState;
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        public MapState GetState()
        {
            return returnedState;
        }
    }

    class HistoryButton : Button
    {
        public MapState mapState;
    }
}
