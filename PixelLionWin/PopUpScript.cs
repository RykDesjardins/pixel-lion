﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class PopUpScript : Form
    {
        public string sScript{ get; private set;}
        public PopUpScript(Pevent pEvent)
        {
            InitializeComponent();
            TB_Script.Text = pEvent.Script;
        }

        public PopUpScript(string Script)
        {
            InitializeComponent();
            TB_Script.Text = Script;
        }

        private void BTN_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PopUpScript_FormClosing(object sender, FormClosingEventArgs e)
        {
            sScript = TB_Script.Text;
        }
    }
}
