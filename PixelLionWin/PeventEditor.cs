﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class PeventEditor : Form
    {
        public Boolean HasAccepted { set; get; }
        public Pevent pEvent { set; get; }
        public Boolean HasDeleted { set; get; }
        public SpriteBundle sBundle;
        public Sprite pSprite;
        public PeventClassBundle cBundle { get; set; }
        public Size Map;

        private const int SPEED_DEFAULT = 5;
        private const int SIGHT_DEFAULT = 5;

        public PeventEditor(Point position, SpriteBundle bundle, PeventClassBundle bundlec, Size map)
        {
            InitializeComponent();
            HasAccepted = false;

            LB_Position.Text = position.X + " x " + position.Y;

            pEvent = new Pevent();
            pEvent.Position = position;
            pEvent.ArretMouvement = new List<Point>();
            sBundle = bundle;
            cBundle = bundlec;
            Map = map;

            foreach (PeventClass pClass in cBundle.ListpEventClass)
                CB_Class.Items.Add(pClass);

            this.CB_MoveType.SelectedIndexChanged += new System.EventHandler(this.CB_MoveType_SelectedIndexChanged);
        }

        public PeventEditor(Pevent pevent, SpriteBundle bundle, PeventClassBundle bundlec)
        {
            InitializeComponent();

            using (XNAUtils utils = new XNAUtils())
            {
                sBundle = bundle;
                pEvent = pevent;
                pSprite = pevent.pSprite;
                cBundle = bundlec;

                foreach (PeventClass pClass in cBundle.ListpEventClass)
                    CB_Class.Items.Add(pClass);
                CB_Class.Text = pevent.Class.ToString();
                CB_Direction.SelectedItem = pevent.Direction.ToString();
                CHK_Flying.Checked = pevent.Flying;

                if (pSprite != null) 
                    PN_Graphics.BackgroundImage = utils.ConvertToImage(pevent.pSprite.SpriteImage);

                CB_MoveType.SelectedItem = pevent.MoveType.ToString();
                TB_Name.Text = pevent.Name;
                TB_Script.Text = pevent.Script;
                LB_Position.Text = pevent.Position.X + " x " + pevent.Position.Y;
                NUM_Sight.Value = new Decimal(pEvent.Sight);
                CHK_Through.Checked = pevent.Through;
                CB_TriggerType.SelectedItem = pevent.TriggerType.ToString();
                NUM_Speed.Value = new Decimal(pevent.Velocity);
                CHK_Disposable.Checked = pevent.Disposable;

                HasAccepted = false;
                this.CB_MoveType.SelectedIndexChanged += new System.EventHandler(this.CB_MoveType_SelectedIndexChanged);
            }
        }

        public void ReadOnly()
        {
            BTN_Save.Enabled = false;
            BTN_Delete.Enabled = false;
            BTN_Cancel.Text = "Fermer";
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            try
            {
                pEvent.Direction = (PeventDirection)Enum.Parse(typeof(PeventDirection), CB_Direction.Text);
                pEvent.Flying = CHK_Flying.Checked;
                pEvent.pSprite = pSprite;
                pEvent.MoveType = (PeventMoveType)Enum.Parse(typeof(PeventMoveType), CB_MoveType.Text);
                pEvent.Name = TB_Name.Text;
                pEvent.Script = TB_Script.Text;
                pEvent.Sight = Convert.ToInt32(NUM_Sight.Value);
                pEvent.Through = CHK_Through.Checked;
                pEvent.TriggerType = (PeventTriggerType)Enum.Parse(typeof(PeventTriggerType), CB_TriggerType.Text);
                pEvent.Velocity = Convert.ToInt32(NUM_Speed.Value);
                pEvent.Class = (PeventClass)CB_Class.SelectedItem;
                pEvent.Disposable = CHK_Disposable.Checked;

                HasAccepted = true;
                Close();
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Tous les champs doivent contenir des informations", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void BTN_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void PN_Graphics_Click(object sender, EventArgs e)
        {
            SpritePicker dlg = new SpritePicker(sBundle);
            dlg.ShowDialog();

            Application.DoEvents();
            pSprite = dlg.SpriteChoisi;

            if (dlg.Choisi) using (XNAUtils utils = new XNAUtils()) PN_Graphics.BackgroundImage = utils.ConvertToImage(pSprite.SpriteImage);
        }

        private void BTN_Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Êtes-vous certain de vouloir supprimer le Pevent?", "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
            {
                HasAccepted = false;
                HasDeleted = true;

                Close();
            }
        }

        private void CB_Class_SelectedIndexChanged(object sender, EventArgs e)
        {
            PeventClass classe = (PeventClass)CB_Class.SelectedItem;
            CB_MoveType.Text = classe.MoveType.ToString();
            CB_TriggerType.Text = classe.TriggerType.ToString();
            CHK_Disposable.Checked = classe.Disposable;
            CHK_Flying.Checked = classe.Flying;
            CHK_Through.Checked = classe.Through;
            NUM_Speed.Value = classe.Speed;
            NUM_Sight.Value = classe.Sight;
            CB_Direction.Text = classe.Direction.ToString();
        }

        private void CB_MoveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CB_MoveType.SelectedIndex == 4)
            {
                CustomMouvement dlg = new CustomMouvement(pEvent, LB_Position.Text, Map.Width, Map.Height);
                dlg.ShowDialog();
            }
        }
    }
}