﻿namespace PixelLionWin
{
    partial class SpritePicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpritePicker));
            this.LBSprites = new System.Windows.Forms.ListBox();
            this.plPreview = new System.Windows.Forms.Panel();
            this.btnChoisir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LBSprites
            // 
            this.LBSprites.FormattingEnabled = true;
            this.LBSprites.Location = new System.Drawing.Point(12, 12);
            this.LBSprites.Name = "LBSprites";
            this.LBSprites.Size = new System.Drawing.Size(157, 342);
            this.LBSprites.TabIndex = 1;
            this.LBSprites.SelectedIndexChanged += new System.EventHandler(this.LBSprites_SelectedIndexChanged);
            this.LBSprites.DoubleClick += new System.EventHandler(this.LBSprites_DoubleClick);
            // 
            // plPreview
            // 
            this.plPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.plPreview.Location = new System.Drawing.Point(175, 12);
            this.plPreview.Name = "plPreview";
            this.plPreview.Size = new System.Drawing.Size(365, 342);
            this.plPreview.TabIndex = 4;
            // 
            // btnChoisir
            // 
            this.btnChoisir.Enabled = false;
            this.btnChoisir.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChoisir.Location = new System.Drawing.Point(12, 360);
            this.btnChoisir.Name = "btnChoisir";
            this.btnChoisir.Size = new System.Drawing.Size(528, 55);
            this.btnChoisir.TabIndex = 5;
            this.btnChoisir.Text = "CHOISIR!";
            this.btnChoisir.UseVisualStyleBackColor = true;
            this.btnChoisir.Click += new System.EventHandler(this.btnChoisir_Click);
            // 
            // SpritePicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 427);
            this.Controls.Add(this.btnChoisir);
            this.Controls.Add(this.plPreview);
            this.Controls.Add(this.LBSprites);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SpritePicker";
            this.Text = "SpritePicker";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LBSprites;
        private System.Windows.Forms.Panel plPreview;
        private System.Windows.Forms.Button btnChoisir;
    }
}