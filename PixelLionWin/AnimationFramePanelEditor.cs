﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class AnimationFramePanelEditor : Form
    {
        public AnimationFramePanelEditor()
        {
            InitializeComponent();
        }

        public void SetFrame(AnimationPatternFrame f, Image img)
        {
            PN_Preview.BackgroundImage = img;

            NUM_Index.Value = f.ImageIndex;
            NUM_Opacity.Value = Convert.ToDecimal(f.Opacity);
            NUM_PosX.Value = f.Position.X;
            NUM_PosY.Value = f.Position.Y;
            NUM_Ratio.Value = Convert.ToDecimal(f.Ratio);
            NUM_Rotation.Value = Convert.ToDecimal(f.Rotation);

            CB_Inverted.Checked = f.Fliped;
            DDL_Blending.SelectedIndex = f.Blending+1;
        }

        public AnimationPatternFrame GetFrame()
        {
            return new AnimationPatternFrame()
            {
                Blending = DDL_Blending.SelectedIndex-1,
                Fliped = CB_Inverted.Checked,
                ImageIndex = Convert.ToInt32(NUM_Index.Value),
                Opacity = Convert.ToSingle(NUM_Opacity.Value),
                Position = new Point(Convert.ToInt32(NUM_PosX.Value), Convert.ToInt32(NUM_PosY.Value)),
                Ratio = Convert.ToSingle(NUM_Ratio.Value),
                Rotation = Convert.ToSingle(NUM_Rotation.Value)
            };
        }

        private void PN_FormContainer_Leave(object sender, EventArgs e)
        {
            Close();
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void BTN_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void BTN_Erase_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Abort;
            Close();
        }


    }
}
