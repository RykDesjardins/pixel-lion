﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class PopUpGraphics : Form
    {
        public Sprite pSprite { get; private set; }
        SpriteBundle sBundle;

        public PopUpGraphics(Pevent pEvent,SpriteBundle psBundle)
        {
            sBundle = psBundle;
            pSprite = pEvent.pSprite;
            InitializeComponent();

            using (XNAUtils utils = new XNAUtils())
            {
                if (pSprite != null)
                    PN_Graphics.BackgroundImage = utils.ConvertToImage(pSprite.SpriteImage);
            }
        }

        public PopUpGraphics(Sprite ppSprite, SpriteBundle psBundle)
        {
            sBundle = psBundle;
            pSprite = ppSprite;
            InitializeComponent();

            using (XNAUtils utils = new XNAUtils())
            {
                if (pSprite != null)
                    PN_Graphics.BackgroundImage = utils.ConvertToImage(pSprite.SpriteImage);
            }
        }

        private void BTN_Edit_Click(object sender, EventArgs e)
        {
            SpritePicker dlg = new SpritePicker(sBundle);
            dlg.ShowDialog();

            pSprite = dlg.SpriteChoisi;
            
            if (dlg.Choisi)
            {
                using (XNAUtils utils = new XNAUtils())
                    PN_Graphics.BackgroundImage = utils.ConvertToImage(pSprite.SpriteImage);
            }
        }

        private void BTN_Quit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
