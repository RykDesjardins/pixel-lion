﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class PixelLinkingGame : Form
    {
        volatile PLProject Project;
        delegate void DELIncrementProgress();
        delegate void DELSetProgress(Int32 value);
        delegate void DELUpdateStatus(String text);
        delegate void DELEnableLaunch();

        public PixelLinkingGame(PLProject Project)
        {
            InitializeComponent();

            this.Project = Project;
            this.Text += Project.Name + " -> " + Project.GameLink.Title;

            plProgressBar1.Max = ComputeWorkToDo() + 1;
        }

        public void AddProgress()
        {
            if (plProgressBar1.InvokeRequired)
            {
                DELIncrementProgress d = new DELIncrementProgress(AddProgress);
                Invoke(d);
            }
            else
            {
                plProgressBar1.Value++;
            }
        }

        public void SetProgress(Int32 value)
        {
            if (plProgressBar1.InvokeRequired)
            {
                DELSetProgress d = new DELSetProgress(SetProgress);
                Invoke(d, value);
            }
            else
            {
                plProgressBar1.Value = value;
            }
        }

        public void EnableLaunch()
        {
            if (BTN_Launch.InvokeRequired)
            {
                DELEnableLaunch d = new DELEnableLaunch(EnableLaunch);
                Invoke(d);
            }
            else
            {
                BTN_Launch.Enabled = true;
            }
        }

        public void UpdateStatus(String text)
        {
            if (LB_Progress.InvokeRequired)
            {
                DELUpdateStatus d = new DELUpdateStatus(UpdateStatus);
                Invoke(d, text);
            }
            else
            {
                LB_Progress.Text = text;
            }
        }

        public void PrepareGame()
        {
            PixelLink link = Project.GameLink;
            plProgressBar1.Value = 1;

            try
            {
                if (link.ProjectFileOnly && link.IncludeMapsInProject)
                {
                    UpdateStatus("Conversion des cartes");
                    Project.Ressources.Maps = Map.GetAllMaps(Project.Dir).Where(x => x.WasInit).ToList();

                    UpdateStatus("Exportation du projet");
                    Project.SaveExport(link.Path, link.Title);
                }
                else if (link.ProjectFileOnly)
                {

                }
                else if (link.CreateFolders)
                {

                }

                SetProgress(10000);
                UpdateStatus("Prêt à l'éxécution");
                EnableLaunch();
            }
            catch (Exception)
            {
                UpdateStatus("Impossible d'exécuter le jeu");
            }
        }

        private Int32 ComputeWorkToDo()
        {
            Int32 WorkCount = 0;

            WorkCount += Map.GetMapCount(Project.Dir);
            WorkCount += Project.Ressources.Animations.Count();
            WorkCount += Project.Ressources.Icons.Count();
            WorkCount += Project.Ressources.Songs.Songs.Count();
            WorkCount += Project.Ressources.Sounds.Sounds.Count();
            WorkCount += Project.Ressources.Sprites.Sprites.Count();

            return WorkCount;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            PrepareGame();
        }

        private void PixelLinkingGame_Shown(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void BTN_Abort_Click(object sender, EventArgs e)
        {
            AbortWork();

            Close();
        }

        private void BTN_Launch_Click(object sender, EventArgs e)
        {
            Project.GameLink.StartGame();
        }

        private void PixelLinkingGame_FormClosing(object sender, FormClosingEventArgs e)
        {
            AbortWork();
        }

        private void AbortWork()
        {
            backgroundWorker1.CancelAsync();
            Project.GameLink.AbortGame();
            Project.Ressources.Maps.Clear();
        }
    }
}
