﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class ClassManager : Form
    {
        PeventClassBundle pBundle;
        public bool HasAdded = false;
        public ClassManager(PeventClassBundle bundle)
        {
            InitializeComponent();
            pBundle = bundle;
            foreach(PeventClass pClass in pBundle.ListpEventClass)
                LB_Classes.Items.Add(pClass.Name);
            CB_Type.DataSource = System.Enum.GetValues(typeof(PeventType));
            CB_TriggerType.DataSource = System.Enum.GetValues(typeof(PeventTriggerType));
            CB_Mouvement.DataSource = System.Enum.GetValues(typeof(PeventMoveType));
            CB_Direction.DataSource = System.Enum.GetValues(typeof(PeventDirection));
        }

        private void BTN_Ajouter_Click(object sender, EventArgs e)
        {
            if (tbxNom.Text != "")
            {
                PeventClass uneClasse = new PeventClass()
                {
                    Name = tbxNom.Text,
                    Type = (PeventType)CB_Type.SelectedItem,
                    TriggerType = (PeventTriggerType)CB_TriggerType.SelectedItem,
                    MoveType = (PeventMoveType)CB_Mouvement.SelectedItem,
                    Direction = (PeventDirection)CB_Direction.SelectedItem,
                    Sight = (Int32)NUM_Vision.Value,
                    Speed = (Int32)NUM_Speed.Value,
                    Through = CHK_Through.Checked,
                    Disposable = CHK_Disposable.Checked,
                    Flying = CHK_Flying.Checked
                };
                pBundle.ListpEventClass.Add(uneClasse);
                LB_Classes.Items.Add(uneClasse);
                HasAdded = true;
            }
            else
                System.Windows.Forms.MessageBox.Show("Tous les champs doivent être remplis");
        }

        private void BTN_Supprimer_Click(object sender, EventArgs e)
        {
            if (LB_Classes.SelectedItem != null)
            {
                PeventClass pClass = (PeventClass)LB_Classes.SelectedItem;
                pBundle.ListpEventClass.Remove(pClass);
                LB_Classes.Items.Remove(pClass);
            }
        }
    }
}
