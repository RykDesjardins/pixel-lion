﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class IconPicker : Form
    {
        public Boolean HasAccepted = false;

        public IconPicker(List<pIcon> list)
        {
            InitializeComponent();

            LIST_Icons.Items.AddRange(list.ToArray());
        }

        private void BTN_Choose_Click(object sender, EventArgs e)
        {
            HasAccepted = true;
            Close();
        }

        public pIcon GetIcon()
        {
            return LIST_Icons.SelectedItem as pIcon;
        }

        private void LIST_Icons_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (XNAUtils utils = new XNAUtils())
            if (LIST_Icons.SelectedItem != null)
            {
                BTN_Choose.Enabled = true;
                PN_Icon.BackgroundImage = utils.ConvertToImage(((pIcon)LIST_Icons.SelectedItem).GetImage());
                PN_Icon.BackgroundImageLayout = ImageLayout.Center;
            }
        }
    }
}
