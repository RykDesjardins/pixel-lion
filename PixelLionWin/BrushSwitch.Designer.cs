﻿namespace PixelLionWin
{
    partial class BrushSwitch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PN_Preview = new System.Windows.Forms.Panel();
            this.RB_Square = new System.Windows.Forms.RadioButton();
            this.RB_Circle = new System.Windows.Forms.RadioButton();
            this.TRACK_Size = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.BTN_Close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TRACK_Size)).BeginInit();
            this.SuspendLayout();
            // 
            // PN_Preview
            // 
            this.PN_Preview.BackColor = System.Drawing.Color.White;
            this.PN_Preview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PN_Preview.Location = new System.Drawing.Point(12, 12);
            this.PN_Preview.Name = "PN_Preview";
            this.PN_Preview.Size = new System.Drawing.Size(108, 106);
            this.PN_Preview.TabIndex = 0;
            // 
            // RB_Square
            // 
            this.RB_Square.AutoSize = true;
            this.RB_Square.Location = new System.Drawing.Point(137, 12);
            this.RB_Square.Name = "RB_Square";
            this.RB_Square.Size = new System.Drawing.Size(50, 17);
            this.RB_Square.TabIndex = 1;
            this.RB_Square.TabStop = true;
            this.RB_Square.Text = "Carré";
            this.RB_Square.UseVisualStyleBackColor = true;
            this.RB_Square.CheckedChanged += new System.EventHandler(this.RB_Square_CheckedChanged);
            // 
            // RB_Circle
            // 
            this.RB_Circle.AutoSize = true;
            this.RB_Circle.Location = new System.Drawing.Point(137, 35);
            this.RB_Circle.Name = "RB_Circle";
            this.RB_Circle.Size = new System.Drawing.Size(55, 17);
            this.RB_Circle.TabIndex = 2;
            this.RB_Circle.TabStop = true;
            this.RB_Circle.Text = "Cercle";
            this.RB_Circle.UseVisualStyleBackColor = true;
            this.RB_Circle.CheckedChanged += new System.EventHandler(this.RB_Circle_CheckedChanged);
            // 
            // TRACK_Size
            // 
            this.TRACK_Size.Location = new System.Drawing.Point(126, 92);
            this.TRACK_Size.Maximum = 40;
            this.TRACK_Size.Minimum = 1;
            this.TRACK_Size.Name = "TRACK_Size";
            this.TRACK_Size.Size = new System.Drawing.Size(209, 45);
            this.TRACK_Size.TabIndex = 3;
            this.TRACK_Size.Value = 1;
            this.TRACK_Size.Scroll += new System.EventHandler(this.TRACK_Size_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(134, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Taille";
            // 
            // BTN_Close
            // 
            this.BTN_Close.ForeColor = System.Drawing.Color.Black;
            this.BTN_Close.Location = new System.Drawing.Point(300, 9);
            this.BTN_Close.Name = "BTN_Close";
            this.BTN_Close.Size = new System.Drawing.Size(26, 23);
            this.BTN_Close.TabIndex = 5;
            this.BTN_Close.Text = "X";
            this.BTN_Close.UseVisualStyleBackColor = true;
            this.BTN_Close.Click += new System.EventHandler(this.BTN_Close_Click);
            // 
            // BrushSwitch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(338, 133);
            this.Controls.Add(this.BTN_Close);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TRACK_Size);
            this.Controls.Add(this.RB_Circle);
            this.Controls.Add(this.RB_Square);
            this.Controls.Add(this.PN_Preview);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BrushSwitch";
            this.Opacity = 0.9D;
            this.Text = "BrushSwitch";
            this.Shown += new System.EventHandler(this.BrushSwitch_Shown);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.BrushSwitch_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.TRACK_Size)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PN_Preview;
        private System.Windows.Forms.RadioButton RB_Square;
        private System.Windows.Forms.RadioButton RB_Circle;
        private System.Windows.Forms.TrackBar TRACK_Size;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BTN_Close;
    }
}