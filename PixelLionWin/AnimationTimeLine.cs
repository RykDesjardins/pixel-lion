﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using System.Threading;
using System.Drawing.Drawing2D;

namespace PixelLionWin
{
    public delegate void AnimationFrameEventHandler(Object sender, AnimationFrame frame);

    public partial class AnimationTimeLine : UserControl
    {
        public event AnimationFrameEventHandler FrameChanged;

        Int32 CurrentFrame = 0;
        Single Bounce;
        Animation animation;
        Bitmap TimelineBuffer;
        Boolean IsPlaying;
        delegate Boolean MustDrawFrameNumber(Int32 index);

        public AnimationTimeLine()
        {
            InitializeComponent();
            TimelineBuffer = new Bitmap(PN_Timeline.Width, PN_Timeline.Height);
            IsPlaying = false;
        }

        public Int32 GetCurrentIndex()
        {
            return CurrentFrame;
        }

        private void TriggerFrameChange()
        {
            FrameChanged(this, animation.FrameAt(CurrentFrame));
        }

        public void LoadAnimation(Animation animation)
        {
            this.animation = animation;
            CurrentFrame = 0;
            DrawTimeLine();
            TriggerFrameChange();
        }

        public void DrawTimeLine()
        {
            Int32 FrameCount = animation.GetFrameCount();
            Bounce = (Single)PN_Timeline.Width / (Single)FrameCount;

            MustDrawFrameNumber WillDraw = delegate(Int32 index)
            {
                return 
                (    
                    FrameCount < 30 ||
                    index != 0 &&
                    (
                        (FrameCount < 40 && index % 2 != 0) ||
                        (FrameCount >= 40 && FrameCount < 70 && index % 5 == 1) ||
                        (FrameCount >= 70 && index % 10 == 1)
                    )
                );
            };

            using (Graphics g = Graphics.FromImage(TimelineBuffer as Image))
            using (Pen pen = new Pen(Color.Black, 1f))
            using (Font font = new System.Drawing.Font("Courrier New", 6f))
            using (Brush brush = new SolidBrush(Color.Blue))
            using (Brush wbrush = new LinearGradientBrush(new Point(0,0), new Point(0, Height), Color.FromArgb(250,220,255), Color.White))
            {
                g.FillRectangle(wbrush, new Rectangle(0, 0, PN_Timeline.Width, PN_Timeline.Height));

                for (Single i = 0; i < FrameCount; i++)
                {
                    g.DrawLine(pen, new Point((Int32)(Bounce * i - 1f), 0), new Point((Int32)(Bounce * i - 1f), PN_Timeline.Height));

                    if (WillDraw((Int32)i)) g.DrawString((i + 1).ToString(), font, brush, new Point((Int32)(Bounce * i + 1f), 3));
                }

                Pen redpen = new Pen(Color.Red, 2f);
                g.DrawLine(redpen, new Point((Int32)(Bounce * CurrentFrame), 0), new Point((Int32)(Bounce * CurrentFrame), PN_Timeline.Height / 2));
                redpen.Dispose();
            }

            using (Graphics g = PN_Timeline.CreateGraphics())
            {
                g.DrawImage(TimelineBuffer, new Point(0, 0));
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (animation != null) DrawTimeLine();
        }

        private void BTN_AddFrame_Click(object sender, EventArgs e)
        {
            animation.AddFrame();

            DrawTimeLine();
        }

        private void BTN_RemoveFrame_Click(object sender, EventArgs e)
        {
            animation.RemoveFrameAt(CurrentFrame);

            if (CurrentFrame >= animation.GetFrameCount()) CurrentFrame = animation.GetFrameCount() - 1; 
                
            
            TriggerFrameChange();
            DrawTimeLine();
        }

        private void BTN_FFW_Click(object sender, EventArgs e)
        {
            if (CurrentFrame < animation.GetFrameCount() - 1)
            {
                CurrentFrame++;
                TriggerFrameChange();
            } 

            DrawTimeLine();
        }

        private void BTN_RWN_Click(object sender, EventArgs e)
        {
            if (CurrentFrame >= 1)
            {
                CurrentFrame--;
                TriggerFrameChange();
            }
            
            DrawTimeLine();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Int32 PseudoFrame = (Int32)((Single)e.X / Bounce);
                if (PseudoFrame > animation.GetFrameCount() - 1) PseudoFrame = animation.GetFrameCount() - 1;

                CurrentFrame = PseudoFrame;
                TriggerFrameChange();
                DrawTimeLine();
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && e.X > 0)
            {
                Int32 PseudoFrame = (Int32)((Single)e.X / Bounce);
                if (PseudoFrame > animation.GetFrameCount() - 1) PseudoFrame = animation.GetFrameCount() - 1;

                if (PseudoFrame != CurrentFrame)
                {
                    CurrentFrame = PseudoFrame;
                    TriggerFrameChange();
                    DrawTimeLine();
                }
            }
        }

        private void BTN_Play_Click(object sender, EventArgs e)
        {
            if (CurrentFrame == animation.GetFrameCount() - 1) CurrentFrame = 0;
            IsPlaying = true;

            while (CurrentFrame < animation.GetFrameCount() - 1)
            {
                CurrentFrame++;
                TriggerFrameChange();
                DrawTimeLine();

                Thread.Sleep(30);
            }

            IsPlaying = false;
        }

        private void BTN_Insert_Click(object sender, EventArgs e)
        {
            animation.AddFrameAt(CurrentFrame);
            TriggerFrameChange();
            DrawTimeLine();
        }

        public Boolean Playing()
        {
            return IsPlaying;
        }

        private void PN_Timeline_Resize(object sender, EventArgs e)
        {
            TimelineBuffer.Dispose();
            TimelineBuffer = new Bitmap(PN_Timeline.Width, PN_Timeline.Height);
        }
    }
}
