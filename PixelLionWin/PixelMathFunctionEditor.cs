﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PixelLionWin
{
    public partial class PixelMathFunctionEditor : Form
    {
        PixelMath PMath;
        SpriteBatch Batch;

        public PixelMathFunctionEditor()
        {
            InitializeComponent();
            Init();
        }
        
        private void Init()
        {
            PMath = new PixelMath(TB_Function.Text);            

            XNADevices.graphicsdevice = new GraphicsDevice(
                GraphicsAdapter.DefaultAdapter, GraphicsProfile.Reach, new PresentationParameters()
                {
                    BackBufferHeight = PN_Graph.Height,
                    BackBufferWidth = PN_Graph.Width,
                    IsFullScreen = false,
                    DeviceWindowHandle = PN_Graph.Handle
                }
                );

            Batch = new SpriteBatch(XNADevices.graphicsdevice);
        }
        private void TB_Yat_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (TB_Yat.Text != String.Empty) LB_MathAnswer.Text = PMath.YAt(Single.Parse(TB_Yat.Text)).ToString();
            }
            catch (Exception) { LB_MathAnswer.Text = "Intrant invalide"; }
        }

        private void BTN_Show_Click(object sender, EventArgs e)
        {
            DrawGraph();
            XNADevices.graphicsdevice.Present();
        }
        private void DrawGraph()
        {
            //Boom!
            Batch.DrawLine(new Vector2(0, PN_Graph.Height / 2), new Vector2(PN_Graph.Width, PN_Graph.Height / 2), Color.Black, 1);
            Batch.DrawLine(new Vector2(PN_Graph.Width / 2, 0), new Vector2(PN_Graph.Width / 2, PN_Graph.Height), Color.Black, 1);
        }
    }
}
