﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PixelLionWin
{
    public partial class ImageButton : UserControl
    {
        public event EventHandler ButtonClick;

        public String ButtonText
        {
            get
            {
                return LB_Text.Text;
            }
            set
            {
                LB_Text.Text = value;
            }
        }

        public Image ButtonImage
        {
            get
            {
                return PN_Icon.BackgroundImage;
            }
            set
            {
                PN_Icon.BackgroundImage = value;
            }
        }

        public Color ButtonTextColor
        {
            get
            {
                return LB_Text.ForeColor;
            }
            set
            {
                LB_Text.ForeColor = value;
            }
        }

        public Color ButtonColorHover { get; set; }

        private Color OriginalColor;

        public ImageButton()
        {
            InitializeComponent();
            OriginalColor = BackColor;
        }

        private void ImageButton_MouseEnter(object sender, EventArgs e)
        {
            BackColor = ButtonColorHover;
        }

        private void ImageButton_MouseLeave(object sender, EventArgs e)
        {
            BackColor = OriginalColor;
        }

        private void LB_Text_MouseEnter(object sender, EventArgs e)
        {
            BackColor = ButtonColorHover;
        }

        private void LB_Text_MouseLeave(object sender, EventArgs e)
        {
            BackColor = OriginalColor;
        }

        private void PN_Icon_MouseEnter(object sender, EventArgs e)
        {
            BackColor = ButtonColorHover;
        }

        private void PN_Icon_MouseLeave(object sender, EventArgs e)
        {
            BackColor = OriginalColor;
        }

        private void ImageButton_Click(object sender, EventArgs e)
        {
            ButtonClick(sender, e);
        }

        private void LB_Text_Click(object sender, EventArgs e)
        {
            ButtonClick(sender, e);
        }

        private void PN_Icon_Click(object sender, EventArgs e)
        {
            ButtonClick(sender, e);
        }
    }
}
