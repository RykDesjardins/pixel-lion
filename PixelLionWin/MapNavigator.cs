﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using PixelLionLib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionWin
{
    public partial class MapNavigator : Form
    {
        public String DirPath;
        public Boolean OpenedFile;
        List<Map> Maps = new List<Map>();
        List<String> FilePath = new List<string>();
        public Map SelectedMap;

        SpriteBatch batch;

        public MapNavigator(String Dir, SpriteBatch Batch)
        {
            InitializeComponent();
            DirPath = Dir;

            PresentationParameters pp = new PresentationParameters()
            {
                BackBufferHeight = PN_Preview.Height,
                BackBufferWidth = PN_Preview.Width,
                DeviceWindowHandle = PN_Preview.Handle,
                IsFullScreen = false
            };

            XNADevices.graphicsdevice.Reset(pp);

            batch = Batch;
        }

        private void MapNavigator_Load(object sender, EventArgs e)
        {
            try
            {
                String[] files = Directory.GetFiles(DirPath);

                foreach (String str in files)
                {
                    if (str.EndsWith(".plm"))
                    {
                        Map map = Map.LoadFromFile(str);
                        Maps.Add(map);
                        FilePath.Add(str);

                        LIST_Maps.Items.Add(map);
                    }
                }
            }
            catch (IOException)
            {  }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (LIST_Maps.SelectedItem != null)
            using (XNAUtils utils = new XNAUtils())
            {
                Map map = LIST_Maps.SelectedItem as Map;

                LB_Name.Text = map.Name;
                LB_Size.Text = map.MapSize.Width + " X " + map.MapSize.Height;

                PN_Preview.Refresh();

                Single RatioX = (Single)PN_Preview.Width / ((Single)map.MapSize.Width * (Single)GlobalPreferences.Prefs.Map.TileSize.Width);
                Single RatioY = (Single)PN_Preview.Width / ((Single)map.MapSize.Height * (Single)GlobalPreferences.Prefs.Map.TileSize.Height);
                Single AbsRatio = RatioX > RatioY ? RatioX : RatioY;

                XNADevices.graphicsdevice.Clear(Color.White);
                map.Draw(batch, utils, AbsRatio);
                XNADevices.graphicsdevice.Present();
            }
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if (LIST_Maps.SelectedItem != null)
            {
                OpenedFile = true;
                SelectedMap = LIST_Maps.SelectedItem as Map;
                DirPath = FilePath[Maps.IndexOf(SelectedMap)];

                Close();
            }
        }

        private void ouvrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listBox1_DoubleClick(sender, e);
        }

        private void supprimerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LIST_Maps.SelectedItem != null)
            {
                SelectedMap = LIST_Maps.SelectedItem as Map;
                DirPath = FilePath[Maps.IndexOf(SelectedMap)];

                File.Delete(DirPath);
                LIST_Maps.Items.Remove(SelectedMap);
            }
        }

        private void MapNavigator_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
