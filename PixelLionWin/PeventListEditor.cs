﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class PeventListEditor : Form
    {
        bool SaveCalled;
        SpriteBundle sbundle;
        public PeventListEditor(List<Pevent> EventsToEdit, SpriteBundle bundle, PeventClassBundle bundlec, Map map)
        {
            InitializeComponent();

            SaveCalled = false;
            sbundle = bundle;

            for (int i = 0; i < EventsToEdit.Count; i++)
                PeventListPanel.Controls.Add(new PeventListItem(EventsToEdit[i], bundle, bundlec, map));

            if (PeventListPanel.Controls.Count <= (PeventListPanel.Height / PeventListPanel.Controls[0].Height))
                PeventListPanel.Width -= SystemInformation.VerticalScrollBarWidth;

            InitiateGeneralModifier(bundlec);

            this.Controls.Add(GeneralModifier);
        }                

        #region MscFunctions

        void SelectAll()
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
                item.CHK_Selected.Checked = true;
        }
        void DeselectAll()
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
                item.CHK_Selected.Checked = false;
        }
        void InvertSelection()
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
                item.CHK_Selected.Checked = !item.CHK_Selected.Checked;
        }

        private void Save()
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
                item.Save();

            SaveCalled = true;

            MessageBox.Show("Saved");
        }
        void SaveAndQuit()
        {
            Save();
            this.Close();
        }
        void Quit()
        {
            this.Close();
        }

        #endregion MscFunctions

        #region FormControlsHandlers

        private void BTN_Quit_Click(object sender, EventArgs e)
        {
            Quit();
        }
        private void BTN_Save_Click(object sender, EventArgs e)
        {
            Save();
        }
        private void BTN_SaveAndQuit_Click(object sender, EventArgs e)
        {
            SaveAndQuit();
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }
        private void saveAndQuitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAndQuit();
        }
        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Quit();
        }
        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeselectAll();
        }
        private void BTN_SelectAll_Click(object sender, EventArgs e)
        {
            SelectAll();
        }
        private void BTN_DeselectAll_Click(object sender, EventArgs e)
        {
            DeselectAll();
        }
        private void BTN_InvertSelection_Click(object sender, EventArgs e)
        {
            InvertSelection();
        }
        private void deselectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectAll();
        }
        private void invertSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InvertSelection();
        }     
        private void PeventListEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!SaveCalled)
            {
                SaveClosingForm closeForm = new SaveClosingForm(this.Name);
                closeForm.ShowDialog();
                if (closeForm.Sauvegarder)
                    Save();
            }
        }

        #endregion FormControlsHandlers

        #region GeneralModifier

        private void InitiateGeneralModifier(PeventClassBundle bundlec)
        {
            foreach (PeventClass pClass in bundlec.ListpEventClass)
                CB_Class.Items.Add(pClass);

            #region Handlers

            BTN_PositionXAdd.Click += new EventHandler(GeneralModifier_NUM_PosX_Add_Click);
            BTN_PositionYAdd.Click += new EventHandler(GeneralModifier_NUM_PosY_Add_Click);
            BTN_SightAdd.Click += new EventHandler(GeneralModifier_NUM_Sight_Add_Click);
            BTN_SpeedAdd.Click += new EventHandler(GeneralModifier_NUM_Speed_Add_Click);

            BTN_PositionXLower.Click += new EventHandler(GeneralModifier_NUM_PosX_Lower_Click);
            BTN_PositionYLower.Click += new EventHandler(GeneralModifier_NUM_PosY_Lower_Click);
            BTN_SightLower.Click += new EventHandler(GeneralModifier_NUM_Sight_Lower_Click);
            BTN_SpeedLower.Click += new EventHandler(GeneralModifier_NUM_Speed_Lower_Click);

            CB_Class.SelectedIndexChanged += new System.EventHandler(GeneralModifier_CB_Class_SelectedIndexChanged);
            CB_Direction.SelectedIndexChanged += new System.EventHandler(GeneralModifier_CB_Direction_SelectedIndexChanged);
            CB_MoveType.SelectedIndexChanged += new System.EventHandler(GeneralModifier_CB_MoveType_SelectedIndexChanged);
            CB_TriggerType.SelectedIndexChanged += new System.EventHandler(GeneralModifier_CB_TriggerType_SelectedIndexChanged);

            CHK_Flying.CheckedChanged += new System.EventHandler(GeneralModifier_CHK_Flying_CheckedChanged);
            CHK_Through.CheckedChanged += new System.EventHandler(GeneralModifier_CHK_Through_CheckedChanged);
            CHK_Disposable.CheckedChanged += new System.EventHandler(GeneralModifier_CHK_Disposable_CheckedChanged);

            BTN_Graphics.Click += new EventHandler(GeneralModifier_BTN_Graphics_Click);
            BTN_Script.Click += new EventHandler(GeneralModifier_BTN_Script_Click);

            #endregion

            #region Tooltips

            System.Windows.Forms.ToolTip THEtooltip = new System.Windows.Forms.ToolTip();

            THEtooltip.SetToolTip(TB_Name, "Name");

            THEtooltip.SetToolTip(CB_Class, "Class");
            THEtooltip.SetToolTip(CB_Direction, "Direction");
            THEtooltip.SetToolTip(CB_MoveType, "Movement type");
            THEtooltip.SetToolTip(CB_TriggerType, "Trigger type");

            THEtooltip.SetToolTip(PN_PositionX, "Position X");
            THEtooltip.SetToolTip(PN_PositionY, "Position Y");
            THEtooltip.SetToolTip(PN_Sight, "Sight");
            THEtooltip.SetToolTip(PN_Speed, "Speed");

            THEtooltip.SetToolTip(CHK_Flying, "Flying");
            THEtooltip.SetToolTip(CHK_Through, "Trough");
            THEtooltip.SetToolTip(CHK_Disposable, "Disposable");

            #endregion
        }

        #region Click

        void GeneralModifier_BTN_Script_Click(object sender, EventArgs e)
        {
            PopUpScript dlg = new PopUpScript(new Pevent());
            dlg.ShowDialog();

            string sScript = dlg.sScript;

            if (sScript != null)
                foreach (PeventListItem item in PeventListPanel.Controls)
                    if (item.CHK_Selected.Checked)
                        item.sScript = sScript;
        }
        void GeneralModifier_BTN_Graphics_Click(object sender, EventArgs e)
        {
            PopUpGraphics dlg = new PopUpGraphics(new Pevent(), sbundle);
            dlg.ShowDialog();

            Sprite pSprite = dlg.pSprite;

            if (pSprite != null)
                foreach (PeventListItem item in PeventListPanel.Controls)
                    if (item.CHK_Selected.Checked)
                        item.pSprite = pSprite;
        }

        #region AddClick

        void GeneralModifier_NUM_PosX_Add_Click(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    if (item.NUM_PosX.Value < item.NUM_PosX.Maximum)
                        item.NUM_PosX.Value++;
            }
        }
        void GeneralModifier_NUM_PosY_Add_Click(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    if (item.NUM_PosY.Value < item.NUM_PosY.Maximum)
                        item.NUM_PosY.Value++;
            }
        }
        void GeneralModifier_NUM_Sight_Add_Click(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    if (item.NUM_Sight.Value < item.NUM_Sight.Maximum)
                        item.NUM_Sight.Value++;
            }
        }
        void GeneralModifier_NUM_Speed_Add_Click(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    if (item.NUM_Speed.Value < item.NUM_Speed.Maximum)
                        item.NUM_Speed.Value++;
            }
        }

        #endregion AddClick

        #region LowerClick

        void GeneralModifier_NUM_PosX_Lower_Click(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    if (item.NUM_PosX.Value > item.NUM_PosX.Minimum)
                        item.NUM_PosX.Value--;
            }
        }
        void GeneralModifier_NUM_PosY_Lower_Click(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    if (item.NUM_PosY.Value > item.NUM_PosY.Minimum)
                        item.NUM_PosY.Value--;
            }
        }
        void GeneralModifier_NUM_Sight_Lower_Click(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    if (item.NUM_Sight.Value > item.NUM_Sight.Minimum)
                        item.NUM_Sight.Value--;
            }
        }
        void GeneralModifier_NUM_Speed_Lower_Click(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    if (item.NUM_Speed.Value > item.NUM_Speed.Minimum)
                        item.NUM_Speed.Value--;
            }
        }

        #endregion LowerClick

        #endregion Click

        #region SelectedIndexChanged

        private void GeneralModifier_CB_Class_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    item.CB_Class.SelectedIndex = CB_Class.SelectedIndex;
            }
        }
        private void GeneralModifier_CB_Direction_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    item.CB_Direction.SelectedIndex = CB_Direction.SelectedIndex;
            }
        }
        private void GeneralModifier_CB_MoveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    item.CB_MoveType.SelectedIndex = CB_MoveType.SelectedIndex;
            }
        }
        private void GeneralModifier_CB_TriggerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    item.CB_TriggerType.SelectedIndex = CB_TriggerType.SelectedIndex;
            }
        }

        #endregion SelectedIndexChanged

        #region CheckedChanged

        private void GeneralModifier_CHK_Flying_CheckedChanged(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    item.CHK_Flying.Checked = CHK_Flying.Checked;
            }
        }
        private void GeneralModifier_CHK_Through_CheckedChanged(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    item.CHK_Through.Checked = CHK_Through.Checked;
            }
        }
        private void GeneralModifier_CHK_Disposable_CheckedChanged(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    item.CHK_Disposable.Checked = CHK_Disposable.Checked;
            }
        }

        #endregion CheckedChanged

        #region TextChanged

        private void TB_Name_TextChanged(object sender, EventArgs e)
        {
            foreach (PeventListItem item in PeventListPanel.Controls)
            {
                if (item.CHK_Selected.Checked)
                    item.TB_Name.Text = TB_Name.Text;
            }
        }

        #endregion TextChanged

        #endregion GeneralModifier
    }
}
