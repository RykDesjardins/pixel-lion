﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class AnimationFlashCreator : Form
    {
        public AnimationFlashCreator()
        {
            InitializeComponent();
            DDL_Target.SelectedIndex = 0;
        }

        private void BTN_ColorPick_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                panel1.BackColor = dlg.Color;
                LB_Color.Text = String.Format("#{0}{1}{2}", dlg.Color.R, dlg.Color.G, dlg.Color.B);
            }
        }

        public Color GetColor()
        {
            return panel1.BackColor;
        }

        public Int32 GetDuration()
        {
            return Convert.ToInt32(NUM_Duree.Value);
        }

        public AttackType GetTarget()
        {
            return DDL_Target.SelectedIndex == 0 ? AttackType.Screen : AttackType.Melee;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
