﻿namespace PixelLionWin
{
    partial class SpritesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpritesForm));
            this.LBSprites = new System.Windows.Forms.ListBox();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.plPreview = new System.Windows.Forms.Panel();
            this.lblLargeur = new System.Windows.Forms.Label();
            this.lblHauteur = new System.Windows.Forms.Label();
            this.lblPixelsX = new System.Windows.Forms.Label();
            this.lblPixelsY = new System.Windows.Forms.Label();
            this.btnSauvegarder = new System.Windows.Forms.Button();
            this.GB_Infos = new System.Windows.Forms.GroupBox();
            this.NUDLargeur = new System.Windows.Forms.NumericUpDown();
            this.NUDHauteur = new System.Windows.Forms.NumericUpDown();
            this.GB_Infos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUDLargeur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDHauteur)).BeginInit();
            this.SuspendLayout();
            // 
            // LBSprites
            // 
            this.LBSprites.FormattingEnabled = true;
            this.LBSprites.Location = new System.Drawing.Point(13, 13);
            this.LBSprites.Name = "LBSprites";
            this.LBSprites.Size = new System.Drawing.Size(157, 342);
            this.LBSprites.TabIndex = 0;
            this.LBSprites.SelectedIndexChanged += new System.EventHandler(this.LBSprites_SelectedIndexChanged);
            // 
            // btnAjouter
            // 
            this.btnAjouter.BackgroundImage = global::PixelLionWin.Properties.Resources.AddIcon;
            this.btnAjouter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAjouter.Location = new System.Drawing.Point(13, 361);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(40, 40);
            this.btnAjouter.TabIndex = 1;
            this.btnAjouter.UseVisualStyleBackColor = true;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouter_Click);
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.btnSupprimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSupprimer.Location = new System.Drawing.Point(59, 361);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(40, 40);
            this.btnSupprimer.TabIndex = 2;
            this.btnSupprimer.UseVisualStyleBackColor = true;
            this.btnSupprimer.Click += new System.EventHandler(this.btnSupprimer_Click);
            // 
            // plPreview
            // 
            this.plPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.plPreview.Location = new System.Drawing.Point(177, 13);
            this.plPreview.Name = "plPreview";
            this.plPreview.Size = new System.Drawing.Size(365, 342);
            this.plPreview.TabIndex = 3;
            // 
            // lblLargeur
            // 
            this.lblLargeur.AutoSize = true;
            this.lblLargeur.Location = new System.Drawing.Point(6, 16);
            this.lblLargeur.Name = "lblLargeur";
            this.lblLargeur.Size = new System.Drawing.Size(49, 13);
            this.lblLargeur.TabIndex = 4;
            this.lblLargeur.Text = "Largeur: ";
            // 
            // lblHauteur
            // 
            this.lblHauteur.AutoSize = true;
            this.lblHauteur.Location = new System.Drawing.Point(107, 16);
            this.lblHauteur.Name = "lblHauteur";
            this.lblHauteur.Size = new System.Drawing.Size(51, 13);
            this.lblHauteur.TabIndex = 5;
            this.lblHauteur.Text = "Hauteur: ";
            // 
            // lblPixelsX
            // 
            this.lblPixelsX.AutoSize = true;
            this.lblPixelsX.Location = new System.Drawing.Point(232, 16);
            this.lblPixelsX.Name = "lblPixelsX";
            this.lblPixelsX.Size = new System.Drawing.Size(50, 13);
            this.lblPixelsX.TabIndex = 6;
            this.lblPixelsX.Text = "Pixels X: ";
            // 
            // lblPixelsY
            // 
            this.lblPixelsY.AutoSize = true;
            this.lblPixelsY.Location = new System.Drawing.Point(297, 16);
            this.lblPixelsY.Name = "lblPixelsY";
            this.lblPixelsY.Size = new System.Drawing.Size(50, 13);
            this.lblPixelsY.TabIndex = 6;
            this.lblPixelsY.Text = "Pixels Y: ";
            // 
            // btnSauvegarder
            // 
            this.btnSauvegarder.BackgroundImage = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.btnSauvegarder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSauvegarder.Location = new System.Drawing.Point(130, 361);
            this.btnSauvegarder.Name = "btnSauvegarder";
            this.btnSauvegarder.Size = new System.Drawing.Size(40, 40);
            this.btnSauvegarder.TabIndex = 7;
            this.btnSauvegarder.UseVisualStyleBackColor = true;
            this.btnSauvegarder.Click += new System.EventHandler(this.btnSauvegarder_Click);
            // 
            // GB_Infos
            // 
            this.GB_Infos.Controls.Add(this.NUDHauteur);
            this.GB_Infos.Controls.Add(this.NUDLargeur);
            this.GB_Infos.Controls.Add(this.lblLargeur);
            this.GB_Infos.Controls.Add(this.lblHauteur);
            this.GB_Infos.Controls.Add(this.lblPixelsY);
            this.GB_Infos.Controls.Add(this.lblPixelsX);
            this.GB_Infos.Location = new System.Drawing.Point(177, 362);
            this.GB_Infos.Name = "GB_Infos";
            this.GB_Infos.Size = new System.Drawing.Size(365, 39);
            this.GB_Infos.TabIndex = 8;
            this.GB_Infos.TabStop = false;
            this.GB_Infos.Text = "Informations";
            // 
            // NUDLargeur
            // 
            this.NUDLargeur.Location = new System.Drawing.Point(52, 14);
            this.NUDLargeur.Name = "NUDLargeur";
            this.NUDLargeur.Size = new System.Drawing.Size(49, 20);
            this.NUDLargeur.TabIndex = 7;
            this.NUDLargeur.ValueChanged += new System.EventHandler(this.NUDLargeur_ValueChanged);
            // 
            // NUDHauteur
            // 
            this.NUDHauteur.Location = new System.Drawing.Point(154, 14);
            this.NUDHauteur.Name = "NUDHauteur";
            this.NUDHauteur.Size = new System.Drawing.Size(49, 20);
            this.NUDHauteur.TabIndex = 7;
            this.NUDHauteur.ValueChanged += new System.EventHandler(this.NUDHauteur_ValueChanged);
            // 
            // SpritesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 405);
            this.Controls.Add(this.GB_Infos);
            this.Controls.Add(this.btnSauvegarder);
            this.Controls.Add(this.plPreview);
            this.Controls.Add(this.btnSupprimer);
            this.Controls.Add(this.btnAjouter);
            this.Controls.Add(this.LBSprites);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SpritesForm";
            this.Text = "Sprites";
            this.GB_Infos.ResumeLayout(false);
            this.GB_Infos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUDLargeur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDHauteur)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LBSprites;
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Panel plPreview;
        private System.Windows.Forms.Label lblLargeur;
        private System.Windows.Forms.Label lblHauteur;
        private System.Windows.Forms.Label lblPixelsX;
        private System.Windows.Forms.Label lblPixelsY;
        private System.Windows.Forms.Button btnSauvegarder;
        private System.Windows.Forms.GroupBox GB_Infos;
        private System.Windows.Forms.NumericUpDown NUDHauteur;
        private System.Windows.Forms.NumericUpDown NUDLargeur;
    }
}