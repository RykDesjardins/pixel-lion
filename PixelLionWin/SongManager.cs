﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using System.IO;

namespace PixelLionWin
{
    public partial class SongManager : Form
    {
        SongBundle Songbundle;
        public Boolean HasSaved;

        public SongManager(SongBundle bundle)
        {
            InitializeComponent();
            Songbundle = bundle;

            foreach (Song item in bundle.Songs)
            {
                LIST_Songs.Items.Add(item);
            }

            HasSaved = false;
        }

        public SongBundle GetBundle { get { return Songbundle; } }

        private void BTN_Add_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Multiselect = true;

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                WaitingForm form = new WaitingForm();
                form.Show();
                Application.DoEvents();

                for (int i = 0; i < dlg.FileNames.Count(); i++)
                {
                    byte[] data = File.ReadAllBytes(dlg.FileNames[i]);
                    Song song   = new Song() { Data = data, Name = dlg.SafeFileNames[i] };

                    Songbundle.Songs.Add(song);
                    LIST_Songs.Items.Add(song);
                }

                form.Close();
            }
        }

        private void BTN_Remove_Click(object sender, EventArgs e)
        {
            if (LIST_Songs.SelectedItem == null) return;

            Songbundle.Songs.Remove((Song)LIST_Songs.SelectedItem);
            LIST_Songs.Items.Remove(LIST_Songs.SelectedItem);
        }

        private void BTN_Play_Click(object sender, EventArgs e)
        {
            if (LIST_Songs.SelectedItem == null) return;

            Songbundle.Play((Song)LIST_Songs.SelectedItem);
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            HasSaved = true;
            Close();
        }

        private void BTN_Stop_Click(object sender, EventArgs e)
        {
            Songbundle.Stop();
        }

        private void LIST_Songs_DoubleClick(object sender, EventArgs e)
        {
            BTN_Play_Click(sender, e);
        }
    }
}
