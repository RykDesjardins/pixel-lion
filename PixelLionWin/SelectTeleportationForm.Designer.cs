﻿namespace PixelLionWin
{
    partial class SelectTeleportationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PN_Map = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // PN_Map
            // 
            this.PN_Map.Location = new System.Drawing.Point(0, 0);
            this.PN_Map.Name = "PN_Map";
            this.PN_Map.Size = new System.Drawing.Size(285, 243);
            this.PN_Map.TabIndex = 0;
            this.PN_Map.Paint += new System.Windows.Forms.PaintEventHandler(this.PN_Map_Paint);
            this.PN_Map.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PN_Map_MouseClick);
            // 
            // SelectTeleportationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 242);
            this.Controls.Add(this.PN_Map);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectTeleportationForm";
            this.Text = "Sélectionner la case de téléportation";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SelectTeleportationForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN_Map;

    }
}