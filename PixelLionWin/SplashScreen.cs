﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace PixelLionWin
{
    public partial class SplashScreen : Form
    {
        public SplashScreen()
        {
            InitializeComponent();
            Focus();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //  Do nothing here!
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            Graphics gfx = e.Graphics;

            gfx.DrawImage(Properties.Resources.PLSplash, new Rectangle(0, 0, this.Width, this.Height));

        }

        public void Splash()
        {
            Show();
            Focus();
            Thread th = new Thread(new ThreadStart(WaitAndClose));
            th.Start();
        }

        public void WaitAndClose()
        {
            Thread.Sleep(3000);
            Close();
        }
    }
}
