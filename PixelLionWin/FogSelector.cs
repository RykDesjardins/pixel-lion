﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class FogSelector : Form
    {
        public Boolean HasSaved = false;
        public Fog fog;

        public FogSelector()
        {
            InitializeComponent();
            fog = new Fog();
        }

        public FogSelector(Fog o)
        {
            InitializeComponent();
            fog = o;

            using (XNAUtils utils = new XNAUtils())
            {
                TRACK_Opacity.Value = fog.Opacity;
                PN_Preview.BackgroundImage = utils.ConvertToImage(fog.BackImage);
                LB_Opacity.Text = TRACK_Opacity.Value.ToString();
            }
        }

        private void TRACK_Opacity_Scroll(object sender, EventArgs e)
        {
            LB_Opacity.Text = TRACK_Opacity.Value.ToString();
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                HasSaved = true;
                fog.BackImage = utils.ConvertToTexture(PN_Preview.BackgroundImage);
                fog.Opacity = TRACK_Opacity.Value;
                fog.Repeat = true;
            }

            Close();
        }

        private void BTN_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PN_Preview.BackgroundImage = Image.FromFile(dlg.FileName);
            }
        }
    }
}
