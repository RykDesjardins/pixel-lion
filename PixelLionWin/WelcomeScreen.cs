﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class WelcomeScreen : UserControl
    {
        public event EventHandler NewMapClick;
        public event EventHandler DBClick;
        public event EventHandler InterpreterClick;
        public event EventHandler SpriteListClick;
        public event EventHandler NotesClick;
        public event EventHandler PixelLinkConfigClick;
        public event EventHandler StatsClick;
        public event EventHandler SongListClick;

        public WelcomeScreen()
        {
            InitializeComponent();
        }

        public void SetInformations(PLProject project)
        {
            project.Description.SetInLabel(LB_Description);
            project.Name.SetInLabel(LB_ProjectName);
        }

        private void BTN_Close_Click(object sender, EventArgs e)
        {
            Visible = false;
        }

        private void BTN_NewMap_Click(object sender, EventArgs e)
        {
            NewMapClick(sender, e);
        }

        private void BTN_DB_Click(object sender, EventArgs e)
        {
            DBClick(sender, e);
        }

        private void BTN_Interpreter_Click(object sender, EventArgs e)
        {
            InterpreterClick(sender, e);
        }

        private void BTN_SpriteList_Click(object sender, EventArgs e)
        {
            SpriteListClick(sender, e);
        }

        private void BTN_Notes_Click(object sender, EventArgs e)
        {
            NotesClick(sender, e);
        }

        private void BTN_PixelLinkConfig_Click(object sender, EventArgs e)
        {
            PixelLinkConfigClick(sender, e);
        }

        private void BTN_Stats_Click(object sender, EventArgs e)
        {
            StatsClick(sender, e);
        }

        private void BTN_SongList_Click(object sender, EventArgs e)
        {
            SongListClick(sender, e);
        }
    }
}
