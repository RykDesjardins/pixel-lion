﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelMonkey;

namespace PixelLionWin
{
    public partial class PixelMonkeyNavigator : Form
    {
        PixelMonkeyLanguage Language;

        public PixelMonkeyNavigator(PixelMonkeyLanguage language)
        {
            InitializeComponent();
            Language = language;

            FillTree();
        }

        public void FillTree()
        {
            foreach (PixelMonkeyFonction function in Language.GetFunctions())
            {
                TreeNode node = new TreeNode(function.Name, 0, 0);
                TREE_Content.Nodes.Add(node);
            }

            foreach (PixelMonkeyClass klass in Language.GetClasses())
            {
                TREE_Content.Nodes.Add
                (
                    new TreeNode
                    (
                        klass.Name,
                        1,
                        1,

                        klass.Methods.Select
                        (
                            x => new TreeNode(x.Name, 3, 3)
                        )
                        .Concat
                        (
                            klass.Properties.Select
                            (
                                x => (new TreeNode(x, 2, 2))
                            )
                        )
                        .ToArray()
                     )
                );
            }
        }

        private void TREE_Content_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node == null) return;

            LIST_ParamsType.Items.Clear();
            LB_Info1.Text = "";
            LB_Info1Type.Text = "";
            LB_ListTitle.Text = "";

            LB_Name.Text = e.Node.Text == "_CTOR" ? "Constructeur (_CTOR)" : e.Node.Text;
            PN_Icon.BackgroundImage = TREE_IconList.Images[e.Node.ImageIndex];

            switch (e.Node.ImageIndex)
            {
                case 0:
                    {
                        LB_Type.Text = "Fonction";
                        LB_ParentName.Text = "Pixel Monkey";
                        LB_ListTitle.Text = "Type de paramètres";
                        LB_Info1Type.Text = "Type de retour";

                        PixelMonkeyFonction function = Language.GetFunctions().Where(x => x.Name == e.Node.Text).FirstOrDefault();

                        LB_Info1.Text = function.ReturnType != null ? Language.GetTypeToStr(function.ReturnType) : "Aucun";
                        LIST_ParamsType.Items.AddRange(function.ParamsType.Select(x => Language.GetTypeToStr(x)).ToArray());

                        LIST_ParamsType.Visible = true;
                    }
                    break;

                case 1:
                    {
                        LB_Type.Text = "Classe";
                        LB_ParentName.Text = "Pixel Monkey";

                        LIST_ParamsType.Items.AddRange(Language.GetClasses().Where(x => x.Name == e.Node.Text).FirstOrDefault().Properties.ToArray());

                        LB_ListTitle.Text = "Propriétés";
                        LIST_ParamsType.Visible = true;
                    }
                    break;

                case 2:
                    {
                        LB_Type.Text = "Propriété";

                        PixelMonkeyClass klass = Language.GetClassList().Where(x => x.Name == e.Node.Parent.Text).FirstOrDefault();
                        LB_ParentName.Text = klass.Name;

                        LB_Info1Type.Text = "Type de la propriété";
                        LB_Info1.Text = Language.GetTypeToStr(klass.PropTypes[klass.Properties.IndexOf(e.Node.Text)]);

                        LIST_ParamsType.Visible = false;
                    }
                    break;

                case 3:
                    {
                        LB_Type.Text = "Méthode";

                        PixelMonkeyClass klass = Language.GetClassList().Where(x => x.Name == e.Node.Parent.Text).FirstOrDefault();
                        PixelMonkeyFonction function = klass.Methods.Where(x => x.Name == e.Node.Text).FirstOrDefault();
                        LB_ParentName.Text = klass.Name;

                        LB_ListTitle.Text = "Type de paramètres";
                        LB_Info1Type.Text = "Type de retour";

                        LB_Info1.Text = function.ReturnType != null ? Language.GetTypeToStr(function.ReturnType) : "Aucun";
                        LIST_ParamsType.Items.AddRange(function.ParamsType.Select(x => Language.GetTypeToStr(x)).ToArray());

                        LIST_ParamsType.Visible = true;
                    }
                    break;

                default:
                    break;
            }
        }
    }
}
