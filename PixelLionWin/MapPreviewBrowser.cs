﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionWin
{
    public partial class MapPreviewBrowser : Form
    {
        Map map;
        Single ratio;
        GraphicsDevice device;
        SpriteBatch batch;

        public MapPreviewBrowser(Map map, SpriteBatch batch)
        {
            InitializeComponent();
            this.map = map;
            Text += map.Name;

            PN_Map.Size = new System.Drawing.Size(map.MapSize.Width * GlobalPreferences.Prefs.Map.TileSize.Width, map.MapSize.Height * GlobalPreferences.Prefs.Map.TileSize.Height);

            PresentationParameters pp = new PresentationParameters()
            {
                BackBufferHeight = PN_Map.Height,
                BackBufferWidth = PN_Map.Width,
                DeviceWindowHandle = PN_Map.Handle,
                IsFullScreen = false
            };

            XNADevices.graphicsdevice.Reset(pp);

            device = XNADevices.graphicsdevice;
            this.batch = batch;

            SetSizeRatio(1f);
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SetSizeRatio(Single Ratio)
        {
            ratio = Ratio;
            PN_Map.Size = new System.Drawing.Size((Int32)((Single)map.MapSize.Width * (Single)GlobalPreferences.Prefs.Map.TileSize.Width * ratio), (Int32)((Single)map.MapSize.Height * (Single)GlobalPreferences.Prefs.Map.TileSize.Height * ratio));

            PresentationParameters pp = new PresentationParameters()
            {
                BackBufferHeight = PN_Map.Height,
                BackBufferWidth = PN_Map.Width,
                DeviceWindowHandle = PN_Map.Handle,
                IsFullScreen = false
            };

            XNADevices.graphicsdevice.Reset(pp);

            Refresh();
        }

        private void PN_Map_Paint(object sender, PaintEventArgs e)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                device.Clear(Color.White);

                map.Draw(batch, utils, ratio);

                device.Present();
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            SetSizeRatio(0.5f);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            SetSizeRatio(1f);
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            SetSizeRatio(1.5f);
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            SetSizeRatio(2f);
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            SetSizeRatio(4f);
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            SetSizeRatio(10f);
        }

        private void MapPreviewBrowser_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}
