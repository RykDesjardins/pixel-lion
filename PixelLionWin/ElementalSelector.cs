﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class ElementalSelector : UserControl
    {
        public Elemental CurrentElement { get; set; }
        private List<Button> ElementButtons;
        public List<Elemental> ElementsContenus;
        public SelectorType Type { get; set; }

        private const int DIFFX = 59;
        private const int DIFFY = 56;
        private const int BUTTON_DEFAULT_SIZE = 50;

        public ElementalSelector()
        {
            InitializeComponent();
        }

        public void Init(ListBox LIST_Elements, String LabelText)
        {
            if (LIST_Elements.SelectedItem != null)                        
                CurrentElement = LIST_Elements.SelectedItem as Elemental;
            else
                foreach (Elemental elems in LIST_Elements.Items)
                {
                    if (elems.GetName() == LabelText)
                        CurrentElement = elems;
                }
                
            ElementButtons = new List<Button>();
            ElementsContenus = new List<Elemental>();

            if (LIST_Elements.Items.Count != 0)
            {
                int i = 0;

                using (XNAUtils utils = new XNAUtils())
                foreach (Elemental item in LIST_Elements.Items)
                {
                    ElementsContenus.Add(item);

                    Button Temp = new Button();
                    Temp.Name = item.GetName();
                    Temp.FlatStyle = FlatStyle.Flat;
                    Temp.FlatAppearance.BorderSize = 1;
                    Temp.Height = BUTTON_DEFAULT_SIZE;
                    Temp.Width = BUTTON_DEFAULT_SIZE;
                    Temp.BackgroundImageLayout = ImageLayout.Center;
                    
                    if (item.Icon != null && item.Icon.GetImage() != null)
                    {
                        Temp.BackgroundImage = utils.ConvertToImage(item.Icon.GetImage());
                        Temp.FlatAppearance.BorderColor = AssignColor(item);
                    }
                    else
                    {
                        Temp.BackgroundImage = null;
                        Temp.FlatAppearance.BorderColor = Color.FromArgb(200,200,200);
                        Temp.ForeColor = Color.Black;
                        Temp.TextAlign = ContentAlignment.MiddleCenter;
                        Temp.Text = item.GetName();
                    }

                    switch (this.Type)
                    {
                        case SelectorType.Immunities:
                            Temp.Tag = CurrentElement.Immunities != null && CurrentElement.Immunities.ExceptNulls().Where(x => x.GetName() == item.GetName()).Count() > 0 ? true : false;
                            break;
                        case SelectorType.Weaknesses:
                            Temp.Tag = CurrentElement.Weaknesses != null && CurrentElement.Weaknesses.ExceptNulls().Where(x => x.GetName() == item.GetName()).Count() > 0 ? true : false;
                            break;
                        case SelectorType.Resistances:
                            Temp.Tag = CurrentElement.Resistances != null && CurrentElement.Resistances.ExceptNulls().Where(x => x.GetName() == item.GetName()).Count() > 0 ? true : false;
                            break;
                    }

                    if ((Boolean)Temp.Tag)
                        Temp.BackColor = Temp.FlatAppearance.BorderColor;

                    if (i == 0)
                    {
                        Temp.Location = new Point(0, 3);
                        
                    }
                    else if (i == 1)
                    {
                        Temp.Location = new Point(DIFFX, 3);
                        
                    }
                    else
                    {
                        if (i % 2 == 0)
                            Temp.Location = new Point(ElementButtons[0].Location.X, ElementButtons[i - 2].Location.Y + DIFFX);
                        else
                            Temp.Location = new Point(DIFFX, ElementButtons[i - 2].Location.Y + DIFFX);
                    }

                    Temp.Click += new EventHandler(ButtonClicked);
                    ElementButtons.Add(Temp);

                    i++;
                }

                this.Controls.Clear();
                this.Controls.AddRange(ElementButtons.ToArray());
            }
        }

        public Color AssignColor(Elemental Elem)
        {
            Image Conversion;

            using (XNAUtils utils = new XNAUtils())
            {
                Conversion = utils.ConvertToImage(Elem.Icon.GetImage());
            }

            Bitmap Image = new Bitmap(Conversion);
            Color PixelColor = Image.GetPixel(Image.Width / 2, Image.Height / 2);

            //if (PixelColor.IsNearBlack(25))
            //    PixelColor = Color.FromArgb(50, 50, 50);
            //else if (PixelColor.IsNearWhite(25))
            //    PixelColor = Color.FromArgb(205, 205, 205);

            return PixelColor;    
        }

        public void ButtonClicked(Object sender, EventArgs e)
        {
            Elemental pElement = new Elemental((((Control)sender).Name));
            Boolean Fini = false;

            foreach (Button button in ElementButtons)
                if (button.Name == pElement.GetName())
                {
                    switch (this.Type)
                    {
                        case SelectorType.Immunities:
                            if ((Boolean)button.Tag == false)
                            {
                                button.Tag = true;
                                button.BackColor = button.FlatAppearance.BorderColor;

                                for (int i = 0; i < ElementsContenus.Count && !Fini; i++)
                                {
                                    if (CurrentElement.GetName() == ElementsContenus.ElementAt(i).GetName())
                                    {
                                        if (ElementsContenus.ElementAt(i).Immunities == null)
                                            ElementsContenus.ElementAt(i).Immunities = new List<Elemental>();
                                        ElementsContenus.ElementAt(i).Immunities.Add(pElement);
                                        Fini = true;
                                    }
                                }
                            }

                            else
                            {
                                button.Tag = false;
                                button.BackColor = Button.DefaultBackColor;

                                for (int i = 0; i < ElementsContenus.Count && !Fini; i++)
                                {
                                    if (CurrentElement.GetName() == ElementsContenus.ElementAt(i).GetName())
                                    {
                                        ElementsContenus.ElementAt(i).Immunities.Remove(pElement);
                                        Fini = true;
                                    }
                                }
                            }
                            break;

                        case SelectorType.Weaknesses:
                            if ((Boolean)button.Tag == false)
                            {
                                button.Tag = true;
                                button.BackColor = button.FlatAppearance.BorderColor;

                                for (int i = 0; i < ElementsContenus.Count && !Fini; i++)
                                {
                                    if (CurrentElement.GetName() == ElementsContenus.ElementAt(i).GetName())
                                    {
                                        if (ElementsContenus.ElementAt(i).Weaknesses == null)
                                            ElementsContenus.ElementAt(i).Weaknesses = new List<Elemental>();
                                        ElementsContenus.ElementAt(i).Weaknesses.Add(pElement);
                                        Fini = true;
                                    }
                                }
                            }

                            else
                            {
                                button.Tag = false;
                                button.BackColor = Button.DefaultBackColor;

                                for (int i = 0; i < ElementsContenus.Count && !Fini; i++)
                                {
                                    if (CurrentElement.GetName() == ElementsContenus.ElementAt(i).GetName())
                                    {
                                        ElementsContenus.ElementAt(i).Weaknesses.Remove(pElement);
                                        Fini = true;
                                    }
                                }
                            }
                            break;

                        case SelectorType.Resistances:
                            if ((Boolean)button.Tag == false)
                            {
                                button.Tag = true;
                                button.BackColor = button.FlatAppearance.BorderColor;

                                for (int i = 0; i < ElementsContenus.Count && !Fini; i++)
                                {
                                    if (CurrentElement.GetName() == ElementsContenus.ElementAt(i).GetName())
                                    {
                                        if (ElementsContenus.ElementAt(i).Resistances == null)
                                            ElementsContenus.ElementAt(i).Resistances = new List<Elemental>();
                                        ElementsContenus.ElementAt(i).Resistances.Add(pElement);
                                        Fini = true;
                                    }
                                }
                            }

                            else
                            {
                                button.Tag = false;
                                button.BackColor = Button.DefaultBackColor;

                                for (int i = 0; i < ElementsContenus.Count && !Fini; i++)
                                {
                                    if (CurrentElement.GetName() == ElementsContenus.ElementAt(i).GetName())
                                    {
                                        ElementsContenus.ElementAt(i).Resistances.Remove(pElement);
                                        Fini = true;
                                    }
                                }
                            }
                            break;
                    }
                }         
        }

        public List<Elemental> UpdateList()
        {
            List<Elemental> Retour = new List<Elemental>();

            switch (this.Type)
            {     
                case SelectorType.Immunities:
                    foreach (Elemental item in ElementsContenus)
                        if (CurrentElement.GetName() == item.GetName())
                            Retour = item.Immunities;
                    break;
                case SelectorType.Resistances:
                    foreach (Elemental item in ElementsContenus)
                        if (CurrentElement.GetName() == item.GetName())
                            Retour = item.Resistances;
                    break;
                case SelectorType.Weaknesses:
                    foreach (Elemental item in ElementsContenus)
                        if (CurrentElement.GetName() == item.GetName())
                            Retour = item.Weaknesses;
                    break;
            }

            return Retour;
        }

        public enum SelectorType
        {
            Immunities, Resistances, Weaknesses, General
        }
    }
}