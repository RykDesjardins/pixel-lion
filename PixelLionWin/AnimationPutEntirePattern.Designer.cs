﻿namespace PixelLionWin
{
    partial class AnimationPutEntirePattern
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BTN_Start = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.NUM_Ratio = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.CB_Invert = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.NUM_Rotation = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.DDL_Blending = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.LB_Position = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CB_AddNewFrames = new System.Windows.Forms.CheckBox();
            this.NUM_FromFrame = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BTN_EntirePattern = new System.Windows.Forms.Button();
            this.NUM_EndIndex = new System.Windows.Forms.NumericUpDown();
            this.NUM_StartIndex = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PN_Canvas = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Ratio)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Rotation)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_FromFrame)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_EndIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_StartIndex)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "À partir du cadre #";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BTN_Start);
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(917, 221);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Appliquer tous les morceaux du motif";
            // 
            // BTN_Start
            // 
            this.BTN_Start.Location = new System.Drawing.Point(807, 177);
            this.BTN_Start.Name = "BTN_Start";
            this.BTN_Start.Size = new System.Drawing.Size(91, 29);
            this.BTN_Start.TabIndex = 8;
            this.BTN_Start.Text = "Commencer";
            this.BTN_Start.UseVisualStyleBackColor = true;
            this.BTN_Start.Click += new System.EventHandler(this.BTN_Start_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.NUM_Ratio);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.CB_Invert);
            this.groupBox7.Location = new System.Drawing.Point(672, 42);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(210, 87);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Transformation";
            // 
            // NUM_Ratio
            // 
            this.NUM_Ratio.DecimalPlaces = 4;
            this.NUM_Ratio.Location = new System.Drawing.Point(119, 27);
            this.NUM_Ratio.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NUM_Ratio.Name = "NUM_Ratio";
            this.NUM_Ratio.Size = new System.Drawing.Size(73, 20);
            this.NUM_Ratio.TabIndex = 2;
            this.NUM_Ratio.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NUM_Ratio.ValueChanged += new System.EventHandler(this.NUM_Ratio_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Ratio grandeur";
            // 
            // CB_Invert
            // 
            this.CB_Invert.AutoSize = true;
            this.CB_Invert.Location = new System.Drawing.Point(29, 53);
            this.CB_Invert.Name = "CB_Invert";
            this.CB_Invert.Size = new System.Drawing.Size(61, 17);
            this.CB_Invert.TabIndex = 0;
            this.CB_Invert.Text = "Inversé";
            this.CB_Invert.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.NUM_Rotation);
            this.groupBox6.Location = new System.Drawing.Point(466, 121);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(200, 65);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Rotation";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(139, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "degrés";
            // 
            // NUM_Rotation
            // 
            this.NUM_Rotation.Location = new System.Drawing.Point(23, 27);
            this.NUM_Rotation.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.NUM_Rotation.Name = "NUM_Rotation";
            this.NUM_Rotation.Size = new System.Drawing.Size(99, 20);
            this.NUM_Rotation.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.DDL_Blending);
            this.groupBox5.Location = new System.Drawing.Point(466, 42);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 73);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Mélange de couleur";
            // 
            // DDL_Blending
            // 
            this.DDL_Blending.FormattingEnabled = true;
            this.DDL_Blending.Items.AddRange(new object[] {
            "Addition",
            "Normal",
            "Soustraction"});
            this.DDL_Blending.Location = new System.Drawing.Point(23, 29);
            this.DDL_Blending.Name = "DDL_Blending";
            this.DDL_Blending.Size = new System.Drawing.Size(155, 21);
            this.DDL_Blending.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.LB_Position);
            this.groupBox4.Location = new System.Drawing.Point(247, 135);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 51);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Position";
            // 
            // LB_Position
            // 
            this.LB_Position.AutoSize = true;
            this.LB_Position.ForeColor = System.Drawing.Color.DarkRed;
            this.LB_Position.Location = new System.Drawing.Point(17, 22);
            this.LB_Position.Name = "LB_Position";
            this.LB_Position.Size = new System.Drawing.Size(74, 13);
            this.LB_Position.TabIndex = 0;
            this.LB_Position.Text = "0 x 0 (96 x 96)";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CB_AddNewFrames);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.NUM_FromFrame);
            this.groupBox3.Location = new System.Drawing.Point(247, 42);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(201, 87);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Choix des cadres";
            // 
            // CB_AddNewFrames
            // 
            this.CB_AddNewFrames.AutoSize = true;
            this.CB_AddNewFrames.Location = new System.Drawing.Point(20, 53);
            this.CB_AddNewFrames.Name = "CB_AddNewFrames";
            this.CB_AddNewFrames.Size = new System.Drawing.Size(161, 17);
            this.CB_AddNewFrames.TabIndex = 2;
            this.CB_AddNewFrames.Text = "Ajouter les cadres en surplus";
            this.CB_AddNewFrames.UseVisualStyleBackColor = true;
            // 
            // NUM_FromFrame
            // 
            this.NUM_FromFrame.Location = new System.Drawing.Point(118, 23);
            this.NUM_FromFrame.Name = "NUM_FromFrame";
            this.NUM_FromFrame.Size = new System.Drawing.Size(59, 20);
            this.NUM_FromFrame.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BTN_EntirePattern);
            this.groupBox2.Controls.Add(this.NUM_EndIndex);
            this.groupBox2.Controls.Add(this.NUM_StartIndex);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(31, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(186, 144);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Choix des morceaux";
            // 
            // BTN_EntirePattern
            // 
            this.BTN_EntirePattern.Location = new System.Drawing.Point(19, 94);
            this.BTN_EntirePattern.Name = "BTN_EntirePattern";
            this.BTN_EntirePattern.Size = new System.Drawing.Size(138, 33);
            this.BTN_EntirePattern.TabIndex = 4;
            this.BTN_EntirePattern.Text = "Motif en entier";
            this.BTN_EntirePattern.UseVisualStyleBackColor = true;
            this.BTN_EntirePattern.Click += new System.EventHandler(this.BTN_EntirePattern_Click);
            // 
            // NUM_EndIndex
            // 
            this.NUM_EndIndex.Location = new System.Drawing.Point(103, 58);
            this.NUM_EndIndex.Name = "NUM_EndIndex";
            this.NUM_EndIndex.Size = new System.Drawing.Size(54, 20);
            this.NUM_EndIndex.TabIndex = 3;
            // 
            // NUM_StartIndex
            // 
            this.NUM_StartIndex.Location = new System.Drawing.Point(103, 30);
            this.NUM_StartIndex.Name = "NUM_StartIndex";
            this.NUM_StartIndex.Size = new System.Drawing.Size(54, 20);
            this.NUM_StartIndex.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Index de fin";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Index de départ";
            // 
            // PN_Canvas
            // 
            this.PN_Canvas.BackColor = System.Drawing.Color.Black;
            this.PN_Canvas.Cursor = System.Windows.Forms.Cursors.Cross;
            this.PN_Canvas.Location = new System.Drawing.Point(12, 250);
            this.PN_Canvas.Name = "PN_Canvas";
            this.PN_Canvas.Size = new System.Drawing.Size(917, 375);
            this.PN_Canvas.TabIndex = 3;
            this.PN_Canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.PN_Canvas_Paint);
            this.PN_Canvas.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PN_Canvas_MouseClick);
            // 
            // AnimationPutEntirePattern
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 638);
            this.Controls.Add(this.PN_Canvas);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AnimationPutEntirePattern";
            this.Text = "Motif en entier";
            this.groupBox1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Ratio)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Rotation)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_FromFrame)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_EndIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_StartIndex)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown NUM_FromFrame;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox CB_AddNewFrames;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button BTN_EntirePattern;
        private System.Windows.Forms.NumericUpDown NUM_EndIndex;
        private System.Windows.Forms.NumericUpDown NUM_StartIndex;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel PN_Canvas;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label LB_Position;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox DDL_Blending;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown NUM_Rotation;
        private System.Windows.Forms.Button BTN_Start;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown NUM_Ratio;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox CB_Invert;
    }
}