﻿namespace PixelLionWin
{
    partial class AnimationFramePanelEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PN_FormContainer = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.DDL_Blending = new System.Windows.Forms.ComboBox();
            this.NUM_PosY = new System.Windows.Forms.NumericUpDown();
            this.NUM_PosX = new System.Windows.Forms.NumericUpDown();
            this.NUM_Rotation = new System.Windows.Forms.NumericUpDown();
            this.NUM_Ratio = new System.Windows.Forms.NumericUpDown();
            this.NUM_Index = new System.Windows.Forms.NumericUpDown();
            this.CB_Inverted = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PN_Preview = new System.Windows.Forms.Panel();
            this.NUM_Opacity = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.BTN_Cancel = new System.Windows.Forms.Button();
            this.BTN_Erase = new System.Windows.Forms.Button();
            this.PN_FormContainer.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PosX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Rotation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Ratio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Index)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Opacity)).BeginInit();
            this.SuspendLayout();
            // 
            // PN_FormContainer
            // 
            this.PN_FormContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_FormContainer.Controls.Add(this.BTN_Erase);
            this.PN_FormContainer.Controls.Add(this.BTN_Cancel);
            this.PN_FormContainer.Controls.Add(this.BTN_Save);
            this.PN_FormContainer.Controls.Add(this.label7);
            this.PN_FormContainer.Controls.Add(this.label5);
            this.PN_FormContainer.Controls.Add(this.groupBox5);
            this.PN_FormContainer.Controls.Add(this.NUM_PosY);
            this.PN_FormContainer.Controls.Add(this.NUM_PosX);
            this.PN_FormContainer.Controls.Add(this.NUM_Opacity);
            this.PN_FormContainer.Controls.Add(this.NUM_Rotation);
            this.PN_FormContainer.Controls.Add(this.NUM_Ratio);
            this.PN_FormContainer.Controls.Add(this.NUM_Index);
            this.PN_FormContainer.Controls.Add(this.CB_Inverted);
            this.PN_FormContainer.Controls.Add(this.label6);
            this.PN_FormContainer.Controls.Add(this.label4);
            this.PN_FormContainer.Controls.Add(this.label3);
            this.PN_FormContainer.Controls.Add(this.label2);
            this.PN_FormContainer.Controls.Add(this.label1);
            this.PN_FormContainer.Controls.Add(this.PN_Preview);
            this.PN_FormContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_FormContainer.Location = new System.Drawing.Point(0, 0);
            this.PN_FormContainer.Name = "PN_FormContainer";
            this.PN_FormContainer.Size = new System.Drawing.Size(665, 206);
            this.PN_FormContainer.TabIndex = 1;
            this.PN_FormContainer.Leave += new System.EventHandler(this.PN_FormContainer_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(366, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "X";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.DDL_Blending);
            this.groupBox5.Location = new System.Drawing.Point(452, 11);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 73);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Mélange de couleur";
            // 
            // DDL_Blending
            // 
            this.DDL_Blending.FormattingEnabled = true;
            this.DDL_Blending.Items.AddRange(new object[] {
            "Addition",
            "Normal",
            "Soustraction"});
            this.DDL_Blending.Location = new System.Drawing.Point(23, 29);
            this.DDL_Blending.Name = "DDL_Blending";
            this.DDL_Blending.Size = new System.Drawing.Size(155, 21);
            this.DDL_Blending.TabIndex = 0;
            // 
            // NUM_PosY
            // 
            this.NUM_PosY.Location = new System.Drawing.Point(387, 86);
            this.NUM_PosY.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.NUM_PosY.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            -2147483648});
            this.NUM_PosY.Name = "NUM_PosY";
            this.NUM_PosY.Size = new System.Drawing.Size(53, 20);
            this.NUM_PosY.TabIndex = 9;
            // 
            // NUM_PosX
            // 
            this.NUM_PosX.Location = new System.Drawing.Point(306, 86);
            this.NUM_PosX.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.NUM_PosX.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            -2147483648});
            this.NUM_PosX.Name = "NUM_PosX";
            this.NUM_PosX.Size = new System.Drawing.Size(53, 20);
            this.NUM_PosX.TabIndex = 9;
            // 
            // NUM_Rotation
            // 
            this.NUM_Rotation.DecimalPlaces = 2;
            this.NUM_Rotation.Location = new System.Drawing.Point(306, 60);
            this.NUM_Rotation.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.NUM_Rotation.Name = "NUM_Rotation";
            this.NUM_Rotation.Size = new System.Drawing.Size(134, 20);
            this.NUM_Rotation.TabIndex = 9;
            // 
            // NUM_Ratio
            // 
            this.NUM_Ratio.DecimalPlaces = 2;
            this.NUM_Ratio.Location = new System.Drawing.Point(307, 35);
            this.NUM_Ratio.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.NUM_Ratio.Name = "NUM_Ratio";
            this.NUM_Ratio.Size = new System.Drawing.Size(133, 20);
            this.NUM_Ratio.TabIndex = 9;
            // 
            // NUM_Index
            // 
            this.NUM_Index.Location = new System.Drawing.Point(307, 9);
            this.NUM_Index.Name = "NUM_Index";
            this.NUM_Index.Size = new System.Drawing.Size(133, 20);
            this.NUM_Index.TabIndex = 9;
            // 
            // CB_Inverted
            // 
            this.CB_Inverted.AutoSize = true;
            this.CB_Inverted.Location = new System.Drawing.Point(211, 175);
            this.CB_Inverted.Name = "CB_Inverted";
            this.CB_Inverted.Size = new System.Drawing.Size(51, 17);
            this.CB_Inverted.TabIndex = 8;
            this.CB_Inverted.Text = "Miroir";
            this.CB_Inverted.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(208, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Opacité";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(208, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Position";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(208, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Rotation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(208, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ratio";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(208, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Index du panneau";
            // 
            // PN_Preview
            // 
            this.PN_Preview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PN_Preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Preview.Location = new System.Drawing.Point(11, 11);
            this.PN_Preview.Name = "PN_Preview";
            this.PN_Preview.Size = new System.Drawing.Size(191, 181);
            this.PN_Preview.TabIndex = 1;
            // 
            // NUM_Opacity
            // 
            this.NUM_Opacity.DecimalPlaces = 2;
            this.NUM_Opacity.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.NUM_Opacity.Location = new System.Drawing.Point(306, 112);
            this.NUM_Opacity.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NUM_Opacity.Name = "NUM_Opacity";
            this.NUM_Opacity.Size = new System.Drawing.Size(134, 20);
            this.NUM_Opacity.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.PowderBlue;
            this.label7.Location = new System.Drawing.Point(520, 183);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Information sur le panneau";
            // 
            // BTN_Save
            // 
            this.BTN_Save.BackgroundImage = global::PixelLionWin.Properties.Resources.AcceptIcon;
            this.BTN_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Save.Location = new System.Drawing.Point(594, 132);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(58, 48);
            this.BTN_Save.TabIndex = 13;
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // BTN_Cancel
            // 
            this.BTN_Cancel.BackgroundImage = global::PixelLionWin.Properties.Resources.CloseIcon;
            this.BTN_Cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Cancel.Location = new System.Drawing.Point(540, 132);
            this.BTN_Cancel.Name = "BTN_Cancel";
            this.BTN_Cancel.Size = new System.Drawing.Size(48, 48);
            this.BTN_Cancel.TabIndex = 13;
            this.BTN_Cancel.UseVisualStyleBackColor = true;
            this.BTN_Cancel.Click += new System.EventHandler(this.BTN_Cancel_Click);
            // 
            // BTN_Erase
            // 
            this.BTN_Erase.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.BTN_Erase.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Erase.Location = new System.Drawing.Point(506, 151);
            this.BTN_Erase.Name = "BTN_Erase";
            this.BTN_Erase.Size = new System.Drawing.Size(28, 29);
            this.BTN_Erase.TabIndex = 13;
            this.BTN_Erase.UseVisualStyleBackColor = true;
            this.BTN_Erase.Click += new System.EventHandler(this.BTN_Erase_Click);
            // 
            // AnimationFramePanelEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 206);
            this.ControlBox = false;
            this.Controls.Add(this.PN_FormContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AnimationFramePanelEditor";
            this.Opacity = 0.95D;
            this.ShowIcon = false;
            this.Text = "AnimationFramePanelEditor";
            this.PN_FormContainer.ResumeLayout(false);
            this.PN_FormContainer.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PosY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PosX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Rotation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Ratio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Index)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Opacity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN_FormContainer;
        private System.Windows.Forms.Panel PN_Preview;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUM_PosY;
        private System.Windows.Forms.NumericUpDown NUM_PosX;
        private System.Windows.Forms.NumericUpDown NUM_Rotation;
        private System.Windows.Forms.NumericUpDown NUM_Ratio;
        private System.Windows.Forms.NumericUpDown NUM_Index;
        private System.Windows.Forms.CheckBox CB_Inverted;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox DDL_Blending;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown NUM_Opacity;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Button BTN_Erase;
        private System.Windows.Forms.Button BTN_Cancel;

    }
}