﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using System.IO;

namespace PixelLionWin
{
    public partial class MapInfos : Form
    {
        Map map;
        public DialogResult result;

        public MapInfos(Map CurrentMap, String ProjectName, String filename)
        {
            InitializeComponent();

            map = CurrentMap;
            
            PN_Title.BackgroundImage = new Bitmap(PN_Title.Width, PN_Title.Height);

            using (XNAUtils utils = new XNAUtils())
            using (Graphics g = Graphics.FromImage(PN_Title.BackgroundImage))
            using (Brush brush = new SolidBrush(Color.Black))
            using (Font bigfont = new Font("Calibri", 30))
            using (Font smallfont = new Font("Calibri", 14))
            {
                g.DrawImage(utils.ConvertToImage(map.DrawMap()), new Point(0, 0));
                g.DrawImage(Properties.Resources.FuzzyLineTitle, new Point(0,0));
                g.DrawImage(Properties.Resources.AeroEffect, new Point(0, 0));
                g.DrawString(map.Name, bigfont, brush, new Point(5, 5));

                g.DrawString(ProjectName, smallfont, brush, new Point(25, 50));

                PN_Title.Refresh();

                NUM_X.Value = Convert.ToDecimal(map.MapSize.Width);
                NUM_Y.Value = Convert.ToDecimal(map.MapSize.Height);

                TB_Rename.Text = map.Name;

                PB_Tileset.Image = utils.ConvertToImage(map.TileSet);
                PB_Tileset.Size = PB_Tileset.Image.Size;

                FileInfo info = new FileInfo(filename);
                LB_Filename.Text = info.Name;
                LB_Filesize.Text = (info.Length / 1024) + "ko";
            }

            this.Text += " - " + map.Name;
        }

        public Map GetMap()
        {
            return map;
        }

        private void BTN_Close_Click(object sender, EventArgs e)
        {
            result = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Toutes les modifications effectuées sont irréversibles.\nLa carte sera enregistrée et mise à jour.\n\nVoulez-vous continuer?", "Sauvegarde", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                result = System.Windows.Forms.DialogResult.OK;
                map.Resize(new SizeI(Convert.ToInt32(NUM_X.Value), Convert.ToInt32(NUM_Y.Value)));

                Close();
            }
        }

        private void PB_Tileset_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "PNG (*.png)|*.png";
            
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (XNAUtils utils = new XNAUtils())
                {
                    PB_Tileset.Image = Image.FromFile(dlg.FileName);
                    PB_Tileset.Size = PB_Tileset.Image.Size;

                    map.TileSet = utils.ConvertToTexture(PB_Tileset.Image);
                }
            }
        }

        
    }
}
