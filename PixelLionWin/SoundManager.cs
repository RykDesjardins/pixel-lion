﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using System.IO;

namespace PixelLionWin
{
    public partial class SoundManager : Form
    {
        SoundBundle SoundBundle;
        public Boolean HasSaved;

        public SoundManager(SoundBundle bundle)
        {
            InitializeComponent();
            SoundBundle = bundle;

            foreach (Sound item in bundle.Sounds)
                LIST_Sounds.Items.Add(item);

            HasSaved = false;
        }

        public SoundBundle GetBundle { get { return SoundBundle; } }

        private void BTN_Add_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Multiselect = true;

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                WaitingForm form = new WaitingForm();
                form.Show();
                Application.DoEvents();

                for (int i = 0; i < dlg.FileNames.Count(); i++)
                {
                    byte[] data = File.ReadAllBytes(dlg.FileNames[i]);
                    Sound sound = new Sound() { Data = data, Name = dlg.SafeFileNames[i] };

                    SoundBundle.Sounds.Add(sound);
                    LIST_Sounds.Items.Add(sound);
                }
                form.Close();
            }
        }

        private void BTN_Remove_Click(object sender, EventArgs e)
        {
            if (LIST_Sounds.SelectedItem == null) return;

            SoundBundle.Sounds.Remove((Sound)LIST_Sounds.SelectedItem);
            LIST_Sounds.Items.Remove(LIST_Sounds.SelectedItem);
        }

        private void BTN_Play_Click(object sender, EventArgs e)
        {
            if (LIST_Sounds.SelectedItem == null) return;

            SoundBundle.Play((Sound)LIST_Sounds.SelectedItem);
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            HasSaved = true;
            Close();
        }

        private void BTN_Stop_Click(object sender, EventArgs e)
        {
            SoundBundle.Stop();
        }

        private void LIST_Sounds_DoubleClick(object sender, EventArgs e)
        {
            BTN_Play_Click(sender, e);
        }

        private void SoundManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            SoundBundle.Stop();
        }
    }
}
