﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace PixelLionWin
{
    public partial class ProjectDetails : Form
    {
        PLProject CurrentProject;
        public ProjectDetails(PLProject projet)
        {
            InitializeComponent();
            CurrentProject = projet;
            String[] files = Directory.GetFiles(projet.Dir);
            int NBMaps = 0;
            for (int i = 0; i < files.Count(); i++)
                if (files[i].EndsWith(".plm"))
                    NBMaps++;
            lblProjectName.Text = projet.Name;
            if(NBMaps < 2)
                lblMapNumber.Text = "Contient " + NBMaps + " carte";
            else
                lblMapNumber.Text = "Contient " + NBMaps + " cartes";
            if (File.Exists("LastProjectPath.plcfg"))
            {
                String proj = File.ReadAllText("LastProjectPath.plcfg").Trim();
                if (File.Exists(proj))
                {
                    FileInfo info = new FileInfo(proj);
                    lblProjectSize.Text = "Taille: " + (info.Length / 1024) + " ko";
                }
                else
                    lblProjectSize.Text = "Fichier introuvable";
            }
            lblNbSprites.Text = "Contient " + projet.Ressources.Sprites.Sprites.Count.ToString() + " sprites";
            lblNbSongs.Text = "Contient " + projet.Ressources.Songs.Songs.Count.ToString() + " chansons";
            lblNbSounds.Text = "Contient " + projet.Ressources.Sounds.Sounds.Count.ToString() + " sons";
            lblPath.Text = "Chemin: " + projet.Dir;
        }

        private void lblProjectName_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SingleStringInput dlg = new SingleStringInput("Nom du projet", lblProjectName.Text);
            dlg.ShowDialog();
            lblProjectName.Text = dlg.GetContent();
            CurrentProject.Name = lblProjectName.Text;
        }

        private void btnChangePath_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = CurrentProject.Name + ".plproj";
            dlg.Filter = "Projet Pixel Lion (*.plproj)|*.plproj";
            dlg.InitialDirectory = CurrentProject.Dir;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!dlg.FileName.EndsWith(".plproj"))
                    dlg.FileName += ".plproj";

                // logic to move files (.plproj and .plm)
                // Delete current project file and save it elsewhere because some data has changed in it
                String proj = File.ReadAllText("LastProjectPath.plcfg").Trim();
                if (File.Exists(proj))
                    File.Delete(proj);
                CurrentProject.Dir = dlg.FileName.Substring(0, dlg.FileName.LastIndexOf("\\"));
                lblPath.Text = "Chemin: " + CurrentProject.Dir;
                Stream stream = File.Open(dlg.FileName, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();
                bFormatter.Serialize(stream, CurrentProject);
                stream.Close();

                // Reconstruct LastProjectPath.plcfg file because it is now outdated
                if (File.Exists("LastProjectPath.plcfg"))
                    File.Delete("LastProjectPath.plcfg");
                FileStream fstream = File.Create("LastProjectPath.plcfg");
                StreamWriter writer = new StreamWriter(fstream);
                writer.WriteLine(dlg.FileName);
                writer.Close();
                fstream.Close();

                // Move map files to CurrentProject.Dir
                String[] files = Directory.GetFiles(proj.Substring(0, proj.LastIndexOf("\\")));
                for (int i = 0; i < files.Count(); i++)
                    if (files[i].EndsWith(".plm"))
                        File.Move(files[i], CurrentProject.Dir + files[i].Substring(files[i].LastIndexOf("\\"), files[i].Length - files[i].LastIndexOf("\\")).TrimEnd());
            }
        }

        private void ProjectDetails_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}