﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class AnimationPutEntirePattern : Form
    {
        AnimationStudio parent;
        Animation animation;
        Bitmap Buffer;
        Point Position;
        Int32 RulerWidth;

        Point AbsoluteCenter;
        Size FrameSize;
        Size OriginalSize;

        public AnimationPutEntirePattern(AnimationStudio parent, Animation animation, Int32 CurrentFrameIndex)
        {
            InitializeComponent();
            this.parent = parent;
            this.animation = animation;
            Buffer = new Bitmap(PN_Canvas.Size.Width, PN_Canvas.Size.Height);
            RulerWidth = parent.RulerWidth;
            AbsoluteCenter = new Point(PN_Canvas.Width / 2, PN_Canvas.Height / 2);
            
            FrameSize = animation.GetPattern().GetFrameSize();
            OriginalSize = animation.GetPattern().GetFrameSize();
            Position = new Point(-FrameSize.Width/2, -FrameSize.Height/2);

            NUM_StartIndex.Maximum = animation.GetPattern().GetFrameCount();
            NUM_StartIndex.Value = 0;

            NUM_EndIndex.Maximum = animation.GetPattern().GetFrameCount();
            NUM_EndIndex.Value = NUM_EndIndex.Maximum;

            NUM_FromFrame.Value = CurrentFrameIndex;
            DDL_Blending.SelectedIndex = 1;

            this.SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer, true);
        }

        private void PN_Canvas_Paint(object sender, PaintEventArgs e)
        {
            using (Graphics g = Graphics.FromImage(Buffer as Image))
            using (Pen p = new Pen(Color.White, 1f))
            {
                g.Clear(PN_Canvas.BackColor);

                // Vertical rulers
                g.DrawLine(p, new Point(RulerWidth, 0), new Point(RulerWidth, PN_Canvas.Height));
                g.DrawLine(p, new Point(PN_Canvas.Width - RulerWidth, 0), new Point(PN_Canvas.Width - RulerWidth, PN_Canvas.Height));

                // Horizontal rulers
                g.DrawLine(p, new Point(0, RulerWidth), new Point(PN_Canvas.Width, RulerWidth));
                g.DrawLine(p, new Point(0, PN_Canvas.Height - RulerWidth), new Point(PN_Canvas.Width, PN_Canvas.Height - RulerWidth));

                if (Position != null)
                {
                    
                    using (Pen pen = new Pen(Color.Red, 1f))
                    {
                        pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                        g.DrawRectangle(pen, new Rectangle(new Point(Position.X + AbsoluteCenter.X, Position.Y + AbsoluteCenter.Y), FrameSize));
                    }
                }
            }

            using (Graphics g = PN_Canvas.CreateGraphics())
            {
                g.DrawImage(Buffer, new Point(0, 0));
            }
        }

        private void PN_Canvas_MouseClick(object sender, MouseEventArgs e)
        {
            Point click = ComputeClickPosition(e);
            LB_Position.Text = String.Format("{0} x {1} ({2} x {3})", e.X - AbsoluteCenter.X, e.Y - AbsoluteCenter.Y, click.X, click.Y);
            Position = click;

            PN_Canvas.Refresh();
        }

        public Point ComputeClickPosition(MouseEventArgs e)
        {
            return new Point(e.X - FrameSize.Width / 2 - AbsoluteCenter.X, e.Y - FrameSize.Width / 2 - AbsoluteCenter.Y);
        }

        private void BTN_EntirePattern_Click(object sender, EventArgs e)
        {
            NUM_StartIndex.Value = 0;
            NUM_EndIndex.Value = animation.GetPattern().GetFrameCount();
        }

        private void BTN_Start_Click(object sender, EventArgs e)
        {
            if (NUM_StartIndex.Value > NUM_EndIndex.Value)
            {
                Decimal temp = NUM_StartIndex.Value;
                NUM_StartIndex.Value = NUM_EndIndex.Value;
                NUM_EndIndex.Value = temp;
            }

            Int32 FrameAt = Convert.ToInt32(NUM_FromFrame.Value);
            for (int i = Convert.ToInt32(NUM_StartIndex.Value); i < Convert.ToInt32(NUM_EndIndex.Value); i++)
            {
                if (FrameAt >= animation.GetFrameCount() && CB_AddNewFrames.Checked)
                    animation.AddFrame();
                else if (FrameAt >= animation.GetFrameCount())
                    break;

                AnimationFrame frame = animation.FrameAt(FrameAt);
                AnimationPatternFrame apf = new AnimationPatternFrame();

                apf.Blending = DDL_Blending.SelectedIndex - 1;
                apf.Fliped = CB_Invert.Checked;
                apf.ImageIndex = i;
                apf.Position = Position;
                apf.Ratio = Convert.ToSingle(NUM_Ratio.Value);
                apf.Rotation = Convert.ToSingle(NUM_Rotation.Value);

                frame.Images.Add(apf);
                animation.AlterFrameAt(FrameAt, frame);

                FrameAt++;
            }

            Close();
        }

        public Animation GetAnimation()
        {
            return animation;
        }

        private void NUM_Ratio_ValueChanged(object sender, EventArgs e)
        {
            Int32 size = (Int32)(OriginalSize.Width * Convert.ToSingle(NUM_Ratio.Value));
            FrameSize = new Size(size, size);

            PN_Canvas.Refresh();
        }
    }
}
