﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PixelLionWin
{
    public partial class LayerSelector : Form
    {
        public int layer;
        public bool confirmed = false;
        public LayerSelector()
        {
            InitializeComponent();
        }

        private void BTN_Layer1_Click(object sender, EventArgs e)
        {
            layer = 0;
            confirmed = true;
            this.Close();
        }

        private void BTN_Layer2_Click(object sender, EventArgs e)
        {
            layer = 1;
            confirmed = true;
            this.Close();
        }

        private void BTN_Layer3_Click(object sender, EventArgs e)
        {
            layer = 2;
            confirmed = true;
            this.Close();
        }
    }
}
