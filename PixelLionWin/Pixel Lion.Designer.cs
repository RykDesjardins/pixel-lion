﻿namespace PixelLionWin
{
    partial class MainFormPixelLion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFormPixelLion));
            this.PN_MapContainer = new System.Windows.Forms.Panel();
            this.PN_Map = new System.Windows.Forms.Panel();
            this.pn_TilesetContainer = new System.Windows.Forms.Panel();
            this.PN_Tileset = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.BTN_Size = new System.Windows.Forms.ToolStripDropDownButton();
            this.pleinÉcranToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grandeurInitialeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LB_STATUS_Position = new System.Windows.Forms.ToolStripStatusLabel();
            this.LB_MemoryUsed = new System.Windows.Forms.ToolStripStatusLabel();
            this.Tile_ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.premierÉtageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deuxièmeÉtageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.troisièmeÉtageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.pEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tuileEntièreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.effacerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.insérerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pEventToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoneDeTéléportationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Selectionner = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.remplirAvecCetteTuileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.étendreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoSurLaTuileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStripMain = new System.Windows.Forms.MenuStrip();
            this.projetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauProjetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouvrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sauvegarderToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouvelleMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.aperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.chargerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sauvegarderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fermerLaMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.éditeurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afficherLhistoriqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.fondDécranToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouvrirUnFichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.supprimerLePaysageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brouillardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouvrirUnFichierToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.supprimerLeBrouillardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limitesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ressourcesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spritesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.musiqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sonsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iconesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.combatantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.TS_PeventListe = new System.Windows.Forms.ToolStripMenuItem();
            this.classesDePEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.animationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripSeparator();
            this.baseDeDonnéesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outilsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.détailsDeLaMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.détailsDuProjetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.préférencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.préférencesUtilisateurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.valeursParDéfautToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripSeparator();
            this.importerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exporterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pixelMonkeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fichierMaîtreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripSeparator();
            this.navigateurDuContenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interpréteurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pixelLinkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripSeparator();
            this.exécuterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.àProposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TIMER_MemoryUsage = new System.Windows.Forms.Timer(this.components);
            this.PN_Main = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.BTN_New = new System.Windows.Forms.ToolStripButton();
            this.BTN_Open = new System.Windows.Forms.ToolStripButton();
            this.BTN_Save = new System.Windows.Forms.ToolStripButton();
            this.BTN_Undo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BTN_Layer1 = new System.Windows.Forms.ToolStripButton();
            this.BTN_Layer2 = new System.Windows.Forms.ToolStripButton();
            this.BTN_Layer3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BTN_PEN_Draw = new System.Windows.Forms.ToolStripButton();
            this.BTN_PEN_Multi = new System.Windows.Forms.ToolStripButton();
            this.BTN_PEN_Eraser = new System.Windows.Forms.ToolStripButton();
            this.BTN_PEN_Fill = new System.Windows.Forms.ToolStripButton();
            this.BTN_PEN_Event = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BTN_Refresh = new System.Windows.Forms.ToolStripButton();
            this.BTN_TransparencyLayers = new System.Windows.Forms.ToolStripButton();
            this.BTN_ScreenShot = new System.Windows.Forms.ToolStripButton();
            this.BTN_Map = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.BTN_Script = new System.Windows.Forms.ToolStripButton();
            this.BTN_Play = new System.Windows.Forms.ToolStripButton();
            this.WelcomeScreenPopup = new PixelLionWin.WelcomeScreen();
            this.tabControlPanel1 = new PixelLion_Win.TabControlPanel();
            this.PN_MapContainer.SuspendLayout();
            this.pn_TilesetContainer.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.Tile_ContextMenu.SuspendLayout();
            this.MenuStripMain.SuspendLayout();
            this.PN_Main.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PN_MapContainer
            // 
            this.PN_MapContainer.AllowDrop = true;
            this.PN_MapContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_MapContainer.AutoScroll = true;
            this.PN_MapContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PN_MapContainer.BackColor = System.Drawing.Color.Gainsboro;
            this.PN_MapContainer.Controls.Add(this.tabControlPanel1);
            this.PN_MapContainer.Controls.Add(this.PN_Map);
            this.PN_MapContainer.Location = new System.Drawing.Point(0, 0);
            this.PN_MapContainer.Name = "PN_MapContainer";
            this.PN_MapContainer.Size = new System.Drawing.Size(632, 497);
            this.PN_MapContainer.TabIndex = 17;
            this.PN_MapContainer.Scroll += new System.Windows.Forms.ScrollEventHandler(this.PN_MapContainer_Scroll);
            // 
            // PN_Map
            // 
            this.PN_Map.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PN_Map.BackColor = System.Drawing.Color.Gainsboro;
            this.PN_Map.Cursor = System.Windows.Forms.Cursors.Cross;
            this.PN_Map.Location = new System.Drawing.Point(0, 0);
            this.PN_Map.Name = "PN_Map";
            this.PN_Map.Size = new System.Drawing.Size(632, 497);
            this.PN_Map.TabIndex = 0;
            this.PN_Map.Text = "doubleBufferedMapControl1";
            this.PN_Map.Paint += new System.Windows.Forms.PaintEventHandler(this.PN_Map_Paint);
            this.PN_Map.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PN_Map_MouseClick);
            this.PN_Map.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PN_Map_MouseDown);
            this.PN_Map.MouseLeave += new System.EventHandler(this.PN_Map_MouseLeave);
            this.PN_Map.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PN_Map_MouseMove);
            // 
            // pn_TilesetContainer
            // 
            this.pn_TilesetContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pn_TilesetContainer.AutoScroll = true;
            this.pn_TilesetContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pn_TilesetContainer.Controls.Add(this.PN_Tileset);
            this.pn_TilesetContainer.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pn_TilesetContainer.Location = new System.Drawing.Point(629, 0);
            this.pn_TilesetContainer.Name = "pn_TilesetContainer";
            this.pn_TilesetContainer.Size = new System.Drawing.Size(275, 497);
            this.pn_TilesetContainer.TabIndex = 2;
            // 
            // PN_Tileset
            // 
            this.PN_Tileset.BackColor = System.Drawing.Color.Gainsboro;
            this.PN_Tileset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PN_Tileset.Cursor = System.Windows.Forms.Cursors.Cross;
            this.PN_Tileset.Location = new System.Drawing.Point(0, 0);
            this.PN_Tileset.Name = "PN_Tileset";
            this.PN_Tileset.Size = new System.Drawing.Size(273, 0);
            this.PN_Tileset.TabIndex = 0;
            this.PN_Tileset.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PN_Tileset_MouseClick);
            this.PN_Tileset.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PN_Tileset_MouseMove);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BTN_Size,
            this.LB_STATUS_Position,
            this.LB_MemoryUsed});
            this.statusStrip1.Location = new System.Drawing.Point(0, 546);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(904, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // BTN_Size
            // 
            this.BTN_Size.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Size.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pleinÉcranToolStripMenuItem,
            this.grandeurInitialeToolStripMenuItem});
            this.BTN_Size.Image = global::PixelLionWin.Properties.Resources.DrawingIcon;
            this.BTN_Size.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Size.Name = "BTN_Size";
            this.BTN_Size.Size = new System.Drawing.Size(29, 20);
            this.BTN_Size.Text = "toolStripSplitButton1";
            // 
            // pleinÉcranToolStripMenuItem
            // 
            this.pleinÉcranToolStripMenuItem.Name = "pleinÉcranToolStripMenuItem";
            this.pleinÉcranToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.pleinÉcranToolStripMenuItem.Text = "Plein écran";
            this.pleinÉcranToolStripMenuItem.Click += new System.EventHandler(this.pleinÉcranToolStripMenuItem_Click);
            // 
            // grandeurInitialeToolStripMenuItem
            // 
            this.grandeurInitialeToolStripMenuItem.Name = "grandeurInitialeToolStripMenuItem";
            this.grandeurInitialeToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.grandeurInitialeToolStripMenuItem.Text = "Grandeur initiale";
            this.grandeurInitialeToolStripMenuItem.Click += new System.EventHandler(this.grandeurInitialeToolStripMenuItem_Click);
            // 
            // LB_STATUS_Position
            // 
            this.LB_STATUS_Position.BackColor = System.Drawing.SystemColors.Control;
            this.LB_STATUS_Position.Name = "LB_STATUS_Position";
            this.LB_STATUS_Position.Size = new System.Drawing.Size(30, 17);
            this.LB_STATUS_Position.Text = "0 x 0";
            // 
            // LB_MemoryUsed
            // 
            this.LB_MemoryUsed.BackColor = System.Drawing.SystemColors.Control;
            this.LB_MemoryUsed.Margin = new System.Windows.Forms.Padding(20, 3, 0, 2);
            this.LB_MemoryUsed.Name = "LB_MemoryUsed";
            this.LB_MemoryUsed.Size = new System.Drawing.Size(34, 17);
            this.LB_MemoryUsed.Text = "0 Mb";
            // 
            // Tile_ContextMenu
            // 
            this.Tile_ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copierToolStripMenuItem,
            this.collerToolStripMenuItem,
            this.effacerToolStripMenuItem,
            this.toolStripMenuItem8,
            this.insérerToolStripMenuItem,
            this.Selectionner,
            this.toolStripMenuItem9,
            this.remplirAvecCetteTuileToolStripMenuItem1,
            this.étendreToolStripMenuItem,
            this.toolStripMenuItem11,
            this.debugToolStripMenuItem});
            this.Tile_ContextMenu.Name = "contextMenuStrip1";
            this.Tile_ContextMenu.Size = new System.Drawing.Size(198, 198);
            this.Tile_ContextMenu.MouseEnter += new System.EventHandler(this.Tile_ContextMenu_MouseEnter);
            // 
            // copierToolStripMenuItem
            // 
            this.copierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.premierÉtageToolStripMenuItem,
            this.deuxièmeÉtageToolStripMenuItem,
            this.troisièmeÉtageToolStripMenuItem,
            this.toolStripMenuItem7,
            this.pEventToolStripMenuItem,
            this.tuileEntièreToolStripMenuItem});
            this.copierToolStripMenuItem.Name = "copierToolStripMenuItem";
            this.copierToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.copierToolStripMenuItem.Text = "Copier";
            // 
            // premierÉtageToolStripMenuItem
            // 
            this.premierÉtageToolStripMenuItem.Name = "premierÉtageToolStripMenuItem";
            this.premierÉtageToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.premierÉtageToolStripMenuItem.Text = "Premier étage";
            this.premierÉtageToolStripMenuItem.Click += new System.EventHandler(this.premierÉtageToolStripMenuItem_Click);
            // 
            // deuxièmeÉtageToolStripMenuItem
            // 
            this.deuxièmeÉtageToolStripMenuItem.Name = "deuxièmeÉtageToolStripMenuItem";
            this.deuxièmeÉtageToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.deuxièmeÉtageToolStripMenuItem.Text = "Deuxième étage";
            this.deuxièmeÉtageToolStripMenuItem.Click += new System.EventHandler(this.deuxièmeÉtageToolStripMenuItem_Click);
            // 
            // troisièmeÉtageToolStripMenuItem
            // 
            this.troisièmeÉtageToolStripMenuItem.Name = "troisièmeÉtageToolStripMenuItem";
            this.troisièmeÉtageToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.troisièmeÉtageToolStripMenuItem.Text = "Troisième étage";
            this.troisièmeÉtageToolStripMenuItem.Click += new System.EventHandler(this.troisièmeÉtageToolStripMenuItem_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(155, 6);
            // 
            // pEventToolStripMenuItem
            // 
            this.pEventToolStripMenuItem.Name = "pEventToolStripMenuItem";
            this.pEventToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.pEventToolStripMenuItem.Text = "pEvents";
            this.pEventToolStripMenuItem.Click += new System.EventHandler(this.pEventToolStripMenuItem_Click);
            // 
            // tuileEntièreToolStripMenuItem
            // 
            this.tuileEntièreToolStripMenuItem.Name = "tuileEntièreToolStripMenuItem";
            this.tuileEntièreToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.tuileEntièreToolStripMenuItem.Text = "Tuile entière";
            this.tuileEntièreToolStripMenuItem.Click += new System.EventHandler(this.tuileEntièreToolStripMenuItem_Click);
            // 
            // collerToolStripMenuItem
            // 
            this.collerToolStripMenuItem.Name = "collerToolStripMenuItem";
            this.collerToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.collerToolStripMenuItem.Text = "Coller";
            this.collerToolStripMenuItem.Click += new System.EventHandler(this.collerToolStripMenuItem_Click);
            // 
            // effacerToolStripMenuItem
            // 
            this.effacerToolStripMenuItem.Name = "effacerToolStripMenuItem";
            this.effacerToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.effacerToolStripMenuItem.Text = "Effacer";
            this.effacerToolStripMenuItem.Click += new System.EventHandler(this.effacerToolStripMenuItem_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(194, 6);
            // 
            // insérerToolStripMenuItem
            // 
            this.insérerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pEventToolStripMenuItem1,
            this.zoneDeTéléportationToolStripMenuItem});
            this.insérerToolStripMenuItem.Name = "insérerToolStripMenuItem";
            this.insérerToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.insérerToolStripMenuItem.Text = "Insérer";
            // 
            // pEventToolStripMenuItem1
            // 
            this.pEventToolStripMenuItem1.Name = "pEventToolStripMenuItem1";
            this.pEventToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.pEventToolStripMenuItem1.Text = "pEvent";
            this.pEventToolStripMenuItem1.Click += new System.EventHandler(this.pEventToolStripMenuItem1_Click);
            // 
            // zoneDeTéléportationToolStripMenuItem
            // 
            this.zoneDeTéléportationToolStripMenuItem.Name = "zoneDeTéléportationToolStripMenuItem";
            this.zoneDeTéléportationToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.zoneDeTéléportationToolStripMenuItem.Text = "Zone de téléportation";
            this.zoneDeTéléportationToolStripMenuItem.Click += new System.EventHandler(this.zoneDeTéléportationToolStripMenuItem_Click);
            // 
            // Selectionner
            // 
            this.Selectionner.Name = "Selectionner";
            this.Selectionner.Size = new System.Drawing.Size(197, 22);
            this.Selectionner.Text = "Sélectionner";
            this.Selectionner.Click += new System.EventHandler(this.Selectionner_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(194, 6);
            // 
            // remplirAvecCetteTuileToolStripMenuItem1
            // 
            this.remplirAvecCetteTuileToolStripMenuItem1.Name = "remplirAvecCetteTuileToolStripMenuItem1";
            this.remplirAvecCetteTuileToolStripMenuItem1.Size = new System.Drawing.Size(197, 22);
            this.remplirAvecCetteTuileToolStripMenuItem1.Text = "Remplir avec cette tuile";
            this.remplirAvecCetteTuileToolStripMenuItem1.Click += new System.EventHandler(this.remplirAvecCetteTuileToolStripMenuItem1_Click);
            // 
            // étendreToolStripMenuItem
            // 
            this.étendreToolStripMenuItem.Name = "étendreToolStripMenuItem";
            this.étendreToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.étendreToolStripMenuItem.Text = "Étendre";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(194, 6);
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infoSurLaTuileToolStripMenuItem});
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.debugToolStripMenuItem.Text = "Debug";
            // 
            // infoSurLaTuileToolStripMenuItem
            // 
            this.infoSurLaTuileToolStripMenuItem.Name = "infoSurLaTuileToolStripMenuItem";
            this.infoSurLaTuileToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.infoSurLaTuileToolStripMenuItem.Text = "Info sur la tuile";
            this.infoSurLaTuileToolStripMenuItem.Click += new System.EventHandler(this.infoSurLaTuileToolStripMenuItem_Click);
            // 
            // MenuStripMain
            // 
            this.MenuStripMain.BackColor = System.Drawing.SystemColors.Control;
            this.MenuStripMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.MenuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.projetToolStripMenuItem,
            this.mapsToolStripMenuItem,
            this.éditeurToolStripMenuItem,
            this.ressourcesToolStripMenuItem,
            this.outilsToolStripMenuItem,
            this.préférencesToolStripMenuItem,
            this.pixelMonkeyToolStripMenuItem,
            this.pixelLinkToolStripMenuItem,
            this.toolStripMenuItem12});
            this.MenuStripMain.Location = new System.Drawing.Point(0, 0);
            this.MenuStripMain.Name = "MenuStripMain";
            this.MenuStripMain.Size = new System.Drawing.Size(904, 24);
            this.MenuStripMain.TabIndex = 1;
            this.MenuStripMain.Text = "Menu principal";
            // 
            // projetToolStripMenuItem
            // 
            this.projetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouveauProjetToolStripMenuItem,
            this.ouvrirToolStripMenuItem,
            this.sauvegarderToolStripMenuItem1,
            this.toolStripMenuItem1,
            this.quitterToolStripMenuItem});
            this.projetToolStripMenuItem.Name = "projetToolStripMenuItem";
            this.projetToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.projetToolStripMenuItem.Text = "Projet";
            // 
            // nouveauProjetToolStripMenuItem
            // 
            this.nouveauProjetToolStripMenuItem.Name = "nouveauProjetToolStripMenuItem";
            this.nouveauProjetToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.nouveauProjetToolStripMenuItem.Text = "Nouveau Projet";
            this.nouveauProjetToolStripMenuItem.Click += new System.EventHandler(this.nouveauProjetToolStripMenuItem_Click);
            // 
            // ouvrirToolStripMenuItem
            // 
            this.ouvrirToolStripMenuItem.Name = "ouvrirToolStripMenuItem";
            this.ouvrirToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.ouvrirToolStripMenuItem.Text = "Ouvrir";
            this.ouvrirToolStripMenuItem.Click += new System.EventHandler(this.ouvrirToolStripMenuItem_Click);
            // 
            // sauvegarderToolStripMenuItem1
            // 
            this.sauvegarderToolStripMenuItem1.Enabled = false;
            this.sauvegarderToolStripMenuItem1.Name = "sauvegarderToolStripMenuItem1";
            this.sauvegarderToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.sauvegarderToolStripMenuItem1.Text = "Sauvegarder";
            this.sauvegarderToolStripMenuItem1.Click += new System.EventHandler(this.sauvegarderToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(153, 6);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click_1);
            // 
            // mapsToolStripMenuItem
            // 
            this.mapsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouvelleMapToolStripMenuItem,
            this.toolStripMenuItem2,
            this.aperçuToolStripMenuItem,
            this.toolStripMenuItem10,
            this.chargerToolStripMenuItem,
            this.sauvegarderToolStripMenuItem,
            this.fermerLaMapToolStripMenuItem});
            this.mapsToolStripMenuItem.Enabled = false;
            this.mapsToolStripMenuItem.Name = "mapsToolStripMenuItem";
            this.mapsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.mapsToolStripMenuItem.Text = "Maps";
            // 
            // nouvelleMapToolStripMenuItem
            // 
            this.nouvelleMapToolStripMenuItem.Name = "nouvelleMapToolStripMenuItem";
            this.nouvelleMapToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.nouvelleMapToolStripMenuItem.Text = "Nouvelle Map";
            this.nouvelleMapToolStripMenuItem.Click += new System.EventHandler(this.nouvelleMapToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(147, 6);
            // 
            // aperçuToolStripMenuItem
            // 
            this.aperçuToolStripMenuItem.Name = "aperçuToolStripMenuItem";
            this.aperçuToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.aperçuToolStripMenuItem.Text = "Aperçu";
            this.aperçuToolStripMenuItem.Click += new System.EventHandler(this.aperçuToolStripMenuItem_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(147, 6);
            // 
            // chargerToolStripMenuItem
            // 
            this.chargerToolStripMenuItem.Name = "chargerToolStripMenuItem";
            this.chargerToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.chargerToolStripMenuItem.Text = "Ouvrir";
            this.chargerToolStripMenuItem.Click += new System.EventHandler(this.chargerToolStripMenuItem_Click);
            // 
            // sauvegarderToolStripMenuItem
            // 
            this.sauvegarderToolStripMenuItem.Name = "sauvegarderToolStripMenuItem";
            this.sauvegarderToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.sauvegarderToolStripMenuItem.Text = "Sauvegarder";
            this.sauvegarderToolStripMenuItem.Click += new System.EventHandler(this.sauvegarderToolStripMenuItem_Click);
            // 
            // fermerLaMapToolStripMenuItem
            // 
            this.fermerLaMapToolStripMenuItem.Name = "fermerLaMapToolStripMenuItem";
            this.fermerLaMapToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.fermerLaMapToolStripMenuItem.Text = "Fermer la map";
            this.fermerLaMapToolStripMenuItem.Click += new System.EventHandler(this.fermerLaMapToolStripMenuItem_Click);
            // 
            // éditeurToolStripMenuItem
            // 
            this.éditeurToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.annulerToolStripMenuItem,
            this.toolStripMenuItem5,
            this.fondDécranToolStripMenuItem,
            this.brouillardToolStripMenuItem,
            this.limitesToolStripMenuItem});
            this.éditeurToolStripMenuItem.Enabled = false;
            this.éditeurToolStripMenuItem.Name = "éditeurToolStripMenuItem";
            this.éditeurToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.éditeurToolStripMenuItem.Text = "Éditeur";
            // 
            // annulerToolStripMenuItem
            // 
            this.annulerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.afficherLhistoriqueToolStripMenuItem});
            this.annulerToolStripMenuItem.Name = "annulerToolStripMenuItem";
            this.annulerToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.annulerToolStripMenuItem.Text = "Annuler";
            this.annulerToolStripMenuItem.Click += new System.EventHandler(this.annulerToolStripMenuItem_Click);
            // 
            // afficherLhistoriqueToolStripMenuItem
            // 
            this.afficherLhistoriqueToolStripMenuItem.Name = "afficherLhistoriqueToolStripMenuItem";
            this.afficherLhistoriqueToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.afficherLhistoriqueToolStripMenuItem.Text = "Afficher l\'historique";
            this.afficherLhistoriqueToolStripMenuItem.Click += new System.EventHandler(this.afficherLhistoriqueToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(122, 6);
            // 
            // fondDécranToolStripMenuItem
            // 
            this.fondDécranToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ouvrirUnFichierToolStripMenuItem,
            this.toolStripMenuItem3,
            this.supprimerLePaysageToolStripMenuItem});
            this.fondDécranToolStripMenuItem.Name = "fondDécranToolStripMenuItem";
            this.fondDécranToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.fondDécranToolStripMenuItem.Text = "Paysage";
            // 
            // ouvrirUnFichierToolStripMenuItem
            // 
            this.ouvrirUnFichierToolStripMenuItem.Name = "ouvrirUnFichierToolStripMenuItem";
            this.ouvrirUnFichierToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.ouvrirUnFichierToolStripMenuItem.Text = "Modifier le paysage";
            this.ouvrirUnFichierToolStripMenuItem.Click += new System.EventHandler(this.ouvrirUnFichierToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(184, 6);
            // 
            // supprimerLePaysageToolStripMenuItem
            // 
            this.supprimerLePaysageToolStripMenuItem.Name = "supprimerLePaysageToolStripMenuItem";
            this.supprimerLePaysageToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.supprimerLePaysageToolStripMenuItem.Text = "Supprimer le paysage";
            this.supprimerLePaysageToolStripMenuItem.Click += new System.EventHandler(this.supprimerLePaysageToolStripMenuItem_Click);
            // 
            // brouillardToolStripMenuItem
            // 
            this.brouillardToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ouvrirUnFichierToolStripMenuItem1,
            this.toolStripMenuItem4,
            this.supprimerLeBrouillardToolStripMenuItem});
            this.brouillardToolStripMenuItem.Name = "brouillardToolStripMenuItem";
            this.brouillardToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.brouillardToolStripMenuItem.Text = "Brouillard";
            // 
            // ouvrirUnFichierToolStripMenuItem1
            // 
            this.ouvrirUnFichierToolStripMenuItem1.Name = "ouvrirUnFichierToolStripMenuItem1";
            this.ouvrirUnFichierToolStripMenuItem1.Size = new System.Drawing.Size(195, 22);
            this.ouvrirUnFichierToolStripMenuItem1.Text = "Modifier le brouillard";
            this.ouvrirUnFichierToolStripMenuItem1.Click += new System.EventHandler(this.ouvrirUnFichierToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(192, 6);
            // 
            // supprimerLeBrouillardToolStripMenuItem
            // 
            this.supprimerLeBrouillardToolStripMenuItem.Name = "supprimerLeBrouillardToolStripMenuItem";
            this.supprimerLeBrouillardToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.supprimerLeBrouillardToolStripMenuItem.Text = "Supprimer le brouillard";
            // 
            // limitesToolStripMenuItem
            // 
            this.limitesToolStripMenuItem.Name = "limitesToolStripMenuItem";
            this.limitesToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.limitesToolStripMenuItem.Text = "Limites";
            this.limitesToolStripMenuItem.Click += new System.EventHandler(this.limitesToolStripMenuItem_Click);
            // 
            // ressourcesToolStripMenuItem
            // 
            this.ressourcesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spritesToolStripMenuItem1,
            this.musiqueToolStripMenuItem,
            this.sonsToolStripMenuItem,
            this.iconesToolStripMenuItem,
            this.combatantsToolStripMenuItem,
            this.toolStripMenuItem6,
            this.TS_PeventListe,
            this.classesDePEventToolStripMenuItem,
            this.animationsToolStripMenuItem,
            this.toolStripMenuItem13,
            this.baseDeDonnéesToolStripMenuItem});
            this.ressourcesToolStripMenuItem.Enabled = false;
            this.ressourcesToolStripMenuItem.Name = "ressourcesToolStripMenuItem";
            this.ressourcesToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.ressourcesToolStripMenuItem.Text = "Ressources";
            // 
            // spritesToolStripMenuItem1
            // 
            this.spritesToolStripMenuItem1.Name = "spritesToolStripMenuItem1";
            this.spritesToolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
            this.spritesToolStripMenuItem1.Text = "Sprites";
            this.spritesToolStripMenuItem1.Click += new System.EventHandler(this.spritesToolStripMenuItem_Click);
            // 
            // musiqueToolStripMenuItem
            // 
            this.musiqueToolStripMenuItem.Name = "musiqueToolStripMenuItem";
            this.musiqueToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.musiqueToolStripMenuItem.Text = "Musique";
            this.musiqueToolStripMenuItem.Click += new System.EventHandler(this.choisirLaTrameToolStripMenuItem_Click);
            // 
            // sonsToolStripMenuItem
            // 
            this.sonsToolStripMenuItem.Name = "sonsToolStripMenuItem";
            this.sonsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.sonsToolStripMenuItem.Text = "Sons";
            this.sonsToolStripMenuItem.Click += new System.EventHandler(this.sonsToolStripMenuItem_Click);
            // 
            // iconesToolStripMenuItem
            // 
            this.iconesToolStripMenuItem.Name = "iconesToolStripMenuItem";
            this.iconesToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.iconesToolStripMenuItem.Text = "Icones";
            this.iconesToolStripMenuItem.Click += new System.EventHandler(this.iconesToolStripMenuItem_Click);
            // 
            // combatantsToolStripMenuItem
            // 
            this.combatantsToolStripMenuItem.Name = "combatantsToolStripMenuItem";
            this.combatantsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.combatantsToolStripMenuItem.Text = "Combatants";
            this.combatantsToolStripMenuItem.Click += new System.EventHandler(this.combatantsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(164, 6);
            // 
            // TS_PeventListe
            // 
            this.TS_PeventListe.Name = "TS_PeventListe";
            this.TS_PeventListe.Size = new System.Drawing.Size(167, 22);
            this.TS_PeventListe.Text = "Liste de pEvent";
            this.TS_PeventListe.Click += new System.EventHandler(this.TS_PeventListe_Click);
            // 
            // classesDePEventToolStripMenuItem
            // 
            this.classesDePEventToolStripMenuItem.Name = "classesDePEventToolStripMenuItem";
            this.classesDePEventToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.classesDePEventToolStripMenuItem.Text = "Classes de pEvent";
            this.classesDePEventToolStripMenuItem.Click += new System.EventHandler(this.classesDePEventToolStripMenuItem_Click);
            // 
            // animationsToolStripMenuItem
            // 
            this.animationsToolStripMenuItem.Name = "animationsToolStripMenuItem";
            this.animationsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.animationsToolStripMenuItem.Text = "Animation Studio";
            this.animationsToolStripMenuItem.Click += new System.EventHandler(this.animationsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(164, 6);
            // 
            // baseDeDonnéesToolStripMenuItem
            // 
            this.baseDeDonnéesToolStripMenuItem.Name = "baseDeDonnéesToolStripMenuItem";
            this.baseDeDonnéesToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.baseDeDonnéesToolStripMenuItem.Text = "Base de données";
            this.baseDeDonnéesToolStripMenuItem.Click += new System.EventHandler(this.baseDeDonnéesToolStripMenuItem_Click);
            // 
            // outilsToolStripMenuItem
            // 
            this.outilsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.détailsDeLaMapToolStripMenuItem,
            this.détailsDuProjetToolStripMenuItem});
            this.outilsToolStripMenuItem.Enabled = false;
            this.outilsToolStripMenuItem.Name = "outilsToolStripMenuItem";
            this.outilsToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.outilsToolStripMenuItem.Text = "Infos";
            // 
            // détailsDeLaMapToolStripMenuItem
            // 
            this.détailsDeLaMapToolStripMenuItem.Name = "détailsDeLaMapToolStripMenuItem";
            this.détailsDeLaMapToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.détailsDeLaMapToolStripMenuItem.Text = "Détails de la map";
            this.détailsDeLaMapToolStripMenuItem.Click += new System.EventHandler(this.détailsDeLaMapToolStripMenuItem_Click);
            // 
            // détailsDuProjetToolStripMenuItem
            // 
            this.détailsDuProjetToolStripMenuItem.Name = "détailsDuProjetToolStripMenuItem";
            this.détailsDuProjetToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.détailsDuProjetToolStripMenuItem.Text = "Détails du projet";
            this.détailsDuProjetToolStripMenuItem.Click += new System.EventHandler(this.détailsDuProjetToolStripMenuItem_Click);
            // 
            // préférencesToolStripMenuItem
            // 
            this.préférencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.préférencesUtilisateurToolStripMenuItem,
            this.valeursParDéfautToolStripMenuItem,
            this.toolStripMenuItem15,
            this.importerToolStripMenuItem,
            this.exporterToolStripMenuItem});
            this.préférencesToolStripMenuItem.Name = "préférencesToolStripMenuItem";
            this.préférencesToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.préférencesToolStripMenuItem.Text = "Préférences";
            // 
            // préférencesUtilisateurToolStripMenuItem
            // 
            this.préférencesUtilisateurToolStripMenuItem.Name = "préférencesUtilisateurToolStripMenuItem";
            this.préférencesUtilisateurToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.préférencesUtilisateurToolStripMenuItem.Text = "Préférences utilisateur";
            this.préférencesUtilisateurToolStripMenuItem.Click += new System.EventHandler(this.préférencesUtilisateurToolStripMenuItem_Click);
            // 
            // valeursParDéfautToolStripMenuItem
            // 
            this.valeursParDéfautToolStripMenuItem.Name = "valeursParDéfautToolStripMenuItem";
            this.valeursParDéfautToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.valeursParDéfautToolStripMenuItem.Text = "Valeurs par défaut";
            this.valeursParDéfautToolStripMenuItem.Click += new System.EventHandler(this.valeursParDéfautToolStripMenuItem_Click);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(187, 6);
            // 
            // importerToolStripMenuItem
            // 
            this.importerToolStripMenuItem.Name = "importerToolStripMenuItem";
            this.importerToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.importerToolStripMenuItem.Text = "Importer";
            this.importerToolStripMenuItem.Click += new System.EventHandler(this.importerToolStripMenuItem_Click);
            // 
            // exporterToolStripMenuItem
            // 
            this.exporterToolStripMenuItem.Name = "exporterToolStripMenuItem";
            this.exporterToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.exporterToolStripMenuItem.Text = "Exporter";
            this.exporterToolStripMenuItem.Click += new System.EventHandler(this.exporterToolStripMenuItem_Click);
            // 
            // pixelMonkeyToolStripMenuItem
            // 
            this.pixelMonkeyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierMaîtreToolStripMenuItem,
            this.toolStripMenuItem16,
            this.navigateurDuContenuToolStripMenuItem,
            this.interpréteurToolStripMenuItem});
            this.pixelMonkeyToolStripMenuItem.Name = "pixelMonkeyToolStripMenuItem";
            this.pixelMonkeyToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.pixelMonkeyToolStripMenuItem.Text = "Pixel Monkey";
            // 
            // fichierMaîtreToolStripMenuItem
            // 
            this.fichierMaîtreToolStripMenuItem.Name = "fichierMaîtreToolStripMenuItem";
            this.fichierMaîtreToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.fichierMaîtreToolStripMenuItem.Text = "Fichier maître";
            this.fichierMaîtreToolStripMenuItem.Click += new System.EventHandler(this.fichierMaîtreToolStripMenuItem_Click);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(193, 6);
            // 
            // navigateurDuContenuToolStripMenuItem
            // 
            this.navigateurDuContenuToolStripMenuItem.Name = "navigateurDuContenuToolStripMenuItem";
            this.navigateurDuContenuToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.navigateurDuContenuToolStripMenuItem.Text = "Navigateur du contenu";
            this.navigateurDuContenuToolStripMenuItem.Click += new System.EventHandler(this.navigateurDuContenuToolStripMenuItem_Click);
            // 
            // interpréteurToolStripMenuItem
            // 
            this.interpréteurToolStripMenuItem.Name = "interpréteurToolStripMenuItem";
            this.interpréteurToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.interpréteurToolStripMenuItem.Text = "Interpréteur";
            this.interpréteurToolStripMenuItem.Click += new System.EventHandler(this.interpréteurToolStripMenuItem_Click);
            // 
            // pixelLinkToolStripMenuItem
            // 
            this.pixelLinkToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurerToolStripMenuItem,
            this.toolStripMenuItem14,
            this.exécuterToolStripMenuItem});
            this.pixelLinkToolStripMenuItem.Name = "pixelLinkToolStripMenuItem";
            this.pixelLinkToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.pixelLinkToolStripMenuItem.Text = "Pixel Link";
            // 
            // configurerToolStripMenuItem
            // 
            this.configurerToolStripMenuItem.Name = "configurerToolStripMenuItem";
            this.configurerToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.configurerToolStripMenuItem.Text = "Configurer";
            this.configurerToolStripMenuItem.Click += new System.EventHandler(this.configurerToolStripMenuItem_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(128, 6);
            // 
            // exécuterToolStripMenuItem
            // 
            this.exécuterToolStripMenuItem.Image = global::PixelLionWin.Properties.Resources.PlayButton;
            this.exécuterToolStripMenuItem.Name = "exécuterToolStripMenuItem";
            this.exécuterToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.exécuterToolStripMenuItem.Text = "Exécuter";
            this.exécuterToolStripMenuItem.Click += new System.EventHandler(this.exécuterToolStripMenuItem_Click);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.àProposToolStripMenuItem});
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(24, 20);
            this.toolStripMenuItem12.Text = "?";
            // 
            // àProposToolStripMenuItem
            // 
            this.àProposToolStripMenuItem.Name = "àProposToolStripMenuItem";
            this.àProposToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.àProposToolStripMenuItem.Text = "À propos";
            this.àProposToolStripMenuItem.Click += new System.EventHandler(this.àProposToolStripMenuItem_Click);
            // 
            // TIMER_MemoryUsage
            // 
            this.TIMER_MemoryUsage.Enabled = true;
            this.TIMER_MemoryUsage.Interval = 1000;
            this.TIMER_MemoryUsage.Tick += new System.EventHandler(this.TIMER_MemoryUsage_Tick);
            // 
            // PN_Main
            // 
            this.PN_Main.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PN_Main.BackColor = System.Drawing.Color.Gainsboro;
            this.PN_Main.Controls.Add(this.PN_MapContainer);
            this.PN_Main.Controls.Add(this.pn_TilesetContainer);
            this.PN_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_Main.Location = new System.Drawing.Point(0, 49);
            this.PN_Main.Name = "PN_Main";
            this.PN_Main.Size = new System.Drawing.Size(904, 497);
            this.PN_Main.TabIndex = 19;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BTN_New,
            this.BTN_Open,
            this.BTN_Save,
            this.BTN_Undo,
            this.toolStripSeparator1,
            this.BTN_Layer1,
            this.BTN_Layer2,
            this.BTN_Layer3,
            this.toolStripSeparator2,
            this.BTN_PEN_Draw,
            this.BTN_PEN_Multi,
            this.BTN_PEN_Eraser,
            this.BTN_PEN_Fill,
            this.BTN_PEN_Event,
            this.toolStripSeparator3,
            this.BTN_Refresh,
            this.BTN_TransparencyLayers,
            this.BTN_ScreenShot,
            this.BTN_Map,
            this.toolStripSeparator4,
            this.BTN_Script,
            this.BTN_Play});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(904, 25);
            this.toolStrip1.TabIndex = 20;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // BTN_New
            // 
            this.BTN_New.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_New.Enabled = false;
            this.BTN_New.Image = global::PixelLionWin.Properties.Resources.MENUICON_add;
            this.BTN_New.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_New.Margin = new System.Windows.Forms.Padding(5, 1, 5, 2);
            this.BTN_New.Name = "BTN_New";
            this.BTN_New.Size = new System.Drawing.Size(23, 22);
            this.BTN_New.Text = "Nouvelle carte";
            this.BTN_New.Click += new System.EventHandler(this.BTN_New_Click);
            // 
            // BTN_Open
            // 
            this.BTN_Open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Open.Enabled = false;
            this.BTN_Open.Image = global::PixelLionWin.Properties.Resources.MENUICON_Open;
            this.BTN_Open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Open.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_Open.Name = "BTN_Open";
            this.BTN_Open.Size = new System.Drawing.Size(23, 22);
            this.BTN_Open.Text = "Ouvrir une carte";
            this.BTN_Open.Click += new System.EventHandler(this.BTN_Open_Click);
            // 
            // BTN_Save
            // 
            this.BTN_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Save.Image = global::PixelLionWin.Properties.Resources.MENUICON_Save;
            this.BTN_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Save.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(23, 22);
            this.BTN_Save.Text = "Sauvegarder la carte";
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // BTN_Undo
            // 
            this.BTN_Undo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Undo.Enabled = false;
            this.BTN_Undo.Image = global::PixelLionWin.Properties.Resources.MENUICON_undo;
            this.BTN_Undo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Undo.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_Undo.Name = "BTN_Undo";
            this.BTN_Undo.Size = new System.Drawing.Size(23, 22);
            this.BTN_Undo.Text = "Annuler";
            this.BTN_Undo.Click += new System.EventHandler(this.BTN_Undo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BTN_Layer1
            // 
            this.BTN_Layer1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Layer1.Enabled = false;
            this.BTN_Layer1.Image = global::PixelLionWin.Properties.Resources.MENUICON_layer1;
            this.BTN_Layer1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Layer1.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_Layer1.Name = "BTN_Layer1";
            this.BTN_Layer1.Size = new System.Drawing.Size(23, 22);
            this.BTN_Layer1.Text = "Première couche";
            this.BTN_Layer1.Click += new System.EventHandler(this.BTN_Layer1_Click);
            // 
            // BTN_Layer2
            // 
            this.BTN_Layer2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Layer2.Image = global::PixelLionWin.Properties.Resources.MENUICON_layer2;
            this.BTN_Layer2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Layer2.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_Layer2.Name = "BTN_Layer2";
            this.BTN_Layer2.Size = new System.Drawing.Size(23, 22);
            this.BTN_Layer2.Text = "Deuxième couche";
            this.BTN_Layer2.Click += new System.EventHandler(this.BTN_Layer2_Click);
            // 
            // BTN_Layer3
            // 
            this.BTN_Layer3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Layer3.Image = global::PixelLionWin.Properties.Resources.MENUICON_layer3;
            this.BTN_Layer3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Layer3.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_Layer3.Name = "BTN_Layer3";
            this.BTN_Layer3.Size = new System.Drawing.Size(23, 22);
            this.BTN_Layer3.Text = "Troisième couche";
            this.BTN_Layer3.Click += new System.EventHandler(this.BTN_Layer3_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BTN_PEN_Draw
            // 
            this.BTN_PEN_Draw.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_PEN_Draw.Enabled = false;
            this.BTN_PEN_Draw.Image = global::PixelLionWin.Properties.Resources.MENUICON_pencil;
            this.BTN_PEN_Draw.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_PEN_Draw.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_PEN_Draw.Name = "BTN_PEN_Draw";
            this.BTN_PEN_Draw.Size = new System.Drawing.Size(23, 22);
            this.BTN_PEN_Draw.Text = "Crayon";
            this.BTN_PEN_Draw.Click += new System.EventHandler(this.BTN_PEN_Draw_Click);
            // 
            // BTN_PEN_Multi
            // 
            this.BTN_PEN_Multi.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_PEN_Multi.Image = global::PixelLionWin.Properties.Resources.MENUICON_Multi;
            this.BTN_PEN_Multi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_PEN_Multi.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_PEN_Multi.Name = "BTN_PEN_Multi";
            this.BTN_PEN_Multi.Size = new System.Drawing.Size(23, 22);
            this.BTN_PEN_Multi.Text = "Dessin multiple";
            this.BTN_PEN_Multi.Click += new System.EventHandler(this.BTN_PEN_Multi_Click);
            // 
            // BTN_PEN_Eraser
            // 
            this.BTN_PEN_Eraser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_PEN_Eraser.Image = global::PixelLionWin.Properties.Resources.MENUICON_Erase;
            this.BTN_PEN_Eraser.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_PEN_Eraser.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_PEN_Eraser.Name = "BTN_PEN_Eraser";
            this.BTN_PEN_Eraser.Size = new System.Drawing.Size(23, 22);
            this.BTN_PEN_Eraser.Text = "Efface";
            this.BTN_PEN_Eraser.Click += new System.EventHandler(this.BTN_PEN_Eraser_Click);
            // 
            // BTN_PEN_Fill
            // 
            this.BTN_PEN_Fill.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_PEN_Fill.Image = global::PixelLionWin.Properties.Resources.MENUICON_Fill;
            this.BTN_PEN_Fill.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_PEN_Fill.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_PEN_Fill.Name = "BTN_PEN_Fill";
            this.BTN_PEN_Fill.Size = new System.Drawing.Size(23, 22);
            this.BTN_PEN_Fill.Text = "Remplissage";
            this.BTN_PEN_Fill.Click += new System.EventHandler(this.BTN_PEN_Fill_Click);
            // 
            // BTN_PEN_Event
            // 
            this.BTN_PEN_Event.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_PEN_Event.Image = global::PixelLionWin.Properties.Resources.MENUICON_Pevent;
            this.BTN_PEN_Event.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_PEN_Event.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_PEN_Event.Name = "BTN_PEN_Event";
            this.BTN_PEN_Event.Size = new System.Drawing.Size(23, 22);
            this.BTN_PEN_Event.Text = "Événements";
            this.BTN_PEN_Event.Click += new System.EventHandler(this.BTN_PEN_Event_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Margin = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BTN_Refresh
            // 
            this.BTN_Refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Refresh.Image = global::PixelLionWin.Properties.Resources.MENUICON_refresh;
            this.BTN_Refresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Refresh.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_Refresh.Name = "BTN_Refresh";
            this.BTN_Refresh.Size = new System.Drawing.Size(23, 22);
            this.BTN_Refresh.Text = "Rafraichir";
            this.BTN_Refresh.Click += new System.EventHandler(this.BTN_Refresh_Click);
            // 
            // BTN_TransparencyLayers
            // 
            this.BTN_TransparencyLayers.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_TransparencyLayers.Image = global::PixelLionWin.Properties.Resources.MENUICON_Opacity;
            this.BTN_TransparencyLayers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_TransparencyLayers.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_TransparencyLayers.Name = "BTN_TransparencyLayers";
            this.BTN_TransparencyLayers.Size = new System.Drawing.Size(23, 22);
            this.BTN_TransparencyLayers.Text = "Opacité des couches";
            this.BTN_TransparencyLayers.Click += new System.EventHandler(this.BTN_TransparencyLayers_Click);
            // 
            // BTN_ScreenShot
            // 
            this.BTN_ScreenShot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_ScreenShot.Image = global::PixelLionWin.Properties.Resources.MENUICON_Screenshot;
            this.BTN_ScreenShot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_ScreenShot.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_ScreenShot.Name = "BTN_ScreenShot";
            this.BTN_ScreenShot.Size = new System.Drawing.Size(23, 22);
            this.BTN_ScreenShot.Text = "Capture d\'écran";
            this.BTN_ScreenShot.Click += new System.EventHandler(this.BTN_ScreenShot_Click);
            // 
            // BTN_Map
            // 
            this.BTN_Map.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Map.Image = global::PixelLionWin.Properties.Resources.MENUICON_map;
            this.BTN_Map.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Map.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_Map.Name = "BTN_Map";
            this.BTN_Map.Size = new System.Drawing.Size(23, 22);
            this.BTN_Map.Text = "Options de la carte";
            this.BTN_Map.Click += new System.EventHandler(this.BTN_Map_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Margin = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // BTN_Script
            // 
            this.BTN_Script.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Script.Image = global::PixelLionWin.Properties.Resources.MENUICON_Script;
            this.BTN_Script.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Script.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_Script.Name = "BTN_Script";
            this.BTN_Script.Size = new System.Drawing.Size(23, 22);
            this.BTN_Script.Text = "Interpréteur ";
            this.BTN_Script.Click += new System.EventHandler(this.BTN_Script_Click);
            // 
            // BTN_Play
            // 
            this.BTN_Play.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BTN_Play.Image = global::PixelLionWin.Properties.Resources.MENUICON_Play;
            this.BTN_Play.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BTN_Play.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BTN_Play.Name = "BTN_Play";
            this.BTN_Play.Size = new System.Drawing.Size(23, 22);
            this.BTN_Play.Text = "Pixel Link";
            this.BTN_Play.Click += new System.EventHandler(this.BTN_Play_Click);
            // 
            // WelcomeScreenPopup
            // 
            this.WelcomeScreenPopup.BackColor = System.Drawing.Color.WhiteSmoke;
            this.WelcomeScreenPopup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WelcomeScreenPopup.Location = new System.Drawing.Point(128, 107);
            this.WelcomeScreenPopup.Name = "WelcomeScreenPopup";
            this.WelcomeScreenPopup.Size = new System.Drawing.Size(633, 360);
            this.WelcomeScreenPopup.TabIndex = 21;
            this.WelcomeScreenPopup.Visible = false;
            this.WelcomeScreenPopup.NewMapClick += new System.EventHandler(this.WelcomeScreenPopup_NewMapClick);
            this.WelcomeScreenPopup.DBClick += new System.EventHandler(this.WelcomeScreenPopup_DBClick);
            this.WelcomeScreenPopup.InterpreterClick += new System.EventHandler(this.WelcomeScreenPopup_InterpreterClick);
            this.WelcomeScreenPopup.SpriteListClick += new System.EventHandler(this.WelcomeScreenPopup_SpriteListClick);
            this.WelcomeScreenPopup.NotesClick += new System.EventHandler(this.WelcomeScreenPopup_NotesClick);
            this.WelcomeScreenPopup.PixelLinkConfigClick += new System.EventHandler(this.WelcomeScreenPopup_PixelLinkConfigClick);
            this.WelcomeScreenPopup.StatsClick += new System.EventHandler(this.WelcomeScreenPopup_StatsClick);
            this.WelcomeScreenPopup.SongListClick += new System.EventHandler(this.WelcomeScreenPopup_SongListClick);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlPanel1.AutoSize = true;
            this.tabControlPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 0);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Size = new System.Drawing.Size(632, 21);
            this.tabControlPanel1.TabIndex = 18;
            this.tabControlPanel1.Visible = false;
            this.tabControlPanel1.TabChanged += new System.EventHandler(this.tabControlPanel1_TabChanged);
            // 
            // MainFormPixelLion
            // 
            this.AllowDrop = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(904, 568);
            this.Controls.Add(this.WelcomeScreenPopup);
            this.Controls.Add(this.PN_Main);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.MenuStripMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.MenuStripMain;
            this.MinimumSize = new System.Drawing.Size(612, 432);
            this.Name = "MainFormPixelLion";
            this.Text = "Pixel Lion";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormPixelLion_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormPixelLion_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFormPixelLion_KeyDown);
            this.Resize += new System.EventHandler(this.MainFormPixelLion_Resize);
            this.PN_MapContainer.ResumeLayout(false);
            this.PN_MapContainer.PerformLayout();
            this.pn_TilesetContainer.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.Tile_ContextMenu.ResumeLayout(false);
            this.MenuStripMain.ResumeLayout(false);
            this.MenuStripMain.PerformLayout();
            this.PN_Main.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuStripMain;
        private System.Windows.Forms.Panel pn_TilesetContainer;
        private System.Windows.Forms.Panel PN_Tileset;
        private System.Windows.Forms.ToolStripMenuItem mapsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouvelleMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem chargerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sauvegarderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fermerLaMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outilsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem détailsDeLaMapToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LB_STATUS_Position;
        private System.Windows.Forms.Panel PN_MapContainer;
        private System.Windows.Forms.ToolStripMenuItem éditeurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fondDécranToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brouillardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ouvrirUnFichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem supprimerLePaysageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ouvrirUnFichierToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem supprimerLeBrouillardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouveauProjetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ouvrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sauvegarderToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem détailsDuProjetToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton BTN_Size;
        private System.Windows.Forms.ToolStripMenuItem pleinÉcranToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grandeurInitialeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limitesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ressourcesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spritesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem musiqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem afficherLhistoriqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem baseDeDonnéesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iconesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classesDePEventToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sonsToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip Tile_ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem copierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem premierÉtageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deuxièmeÉtageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem troisièmeÉtageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem pEventToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tuileEntièreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem effacerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem insérerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pEventToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem Selectionner;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem remplirAvecCetteTuileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem étendreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoSurLaTuileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem animationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem préférencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem àProposToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem pixelLinkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem exécuterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem combatantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoneDeTéléportationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem préférencesUtilisateurToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem importerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exporterToolStripMenuItem;
        private System.Windows.Forms.Panel PN_Map;
        private System.Windows.Forms.ToolStripMenuItem pixelMonkeyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fichierMaîtreToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem navigateurDuContenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interpréteurToolStripMenuItem;
        private PixelLion_Win.TabControlPanel tabControlPanel1;
        private System.Windows.Forms.ToolStripStatusLabel LB_MemoryUsed;
        private System.Windows.Forms.Timer TIMER_MemoryUsage;
        private System.Windows.Forms.ToolStripMenuItem TS_PeventListe;
        private System.Windows.Forms.ToolStripMenuItem valeursParDéfautToolStripMenuItem;
        private System.Windows.Forms.Panel PN_Main;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton BTN_New;
        private System.Windows.Forms.ToolStripButton BTN_Open;
        private System.Windows.Forms.ToolStripButton BTN_Save;
        private System.Windows.Forms.ToolStripButton BTN_Undo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton BTN_Layer1;
        private System.Windows.Forms.ToolStripButton BTN_Layer2;
        private System.Windows.Forms.ToolStripButton BTN_Layer3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton BTN_PEN_Draw;
        private System.Windows.Forms.ToolStripButton BTN_PEN_Multi;
        private System.Windows.Forms.ToolStripButton BTN_PEN_Eraser;
        private System.Windows.Forms.ToolStripButton BTN_PEN_Fill;
        private System.Windows.Forms.ToolStripButton BTN_PEN_Event;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton BTN_Refresh;
        private System.Windows.Forms.ToolStripButton BTN_TransparencyLayers;
        private System.Windows.Forms.ToolStripButton BTN_ScreenShot;
        private System.Windows.Forms.ToolStripButton BTN_Script;
        private System.Windows.Forms.ToolStripButton BTN_Map;
        private System.Windows.Forms.ToolStripButton BTN_Play;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private WelcomeScreen WelcomeScreenPopup;
    }
}

