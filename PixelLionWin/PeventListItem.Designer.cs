﻿namespace PixelLionWin
{
    partial class PeventListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CB_Class = new System.Windows.Forms.ComboBox();
            this.CB_TriggerType = new System.Windows.Forms.ComboBox();
            this.CB_MoveType = new System.Windows.Forms.ComboBox();
            this.TB_Name = new System.Windows.Forms.TextBox();
            this.CHK_Through = new System.Windows.Forms.CheckBox();
            this.CHK_Disposable = new System.Windows.Forms.CheckBox();
            this.NUM_Speed = new System.Windows.Forms.NumericUpDown();
            this.NUM_PosX = new System.Windows.Forms.NumericUpDown();
            this.NUM_PosY = new System.Windows.Forms.NumericUpDown();
            this.CB_Direction = new System.Windows.Forms.ComboBox();
            this.NUM_Sight = new System.Windows.Forms.NumericUpDown();
            this.TabPanelEvent = new System.Windows.Forms.TableLayoutPanel();
            this.CHK_Flying = new System.Windows.Forms.CheckBox();
            this.BTN_Graphics = new System.Windows.Forms.Button();
            this.BTN_Script = new System.Windows.Forms.Button();
            this.CHK_Selected = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Speed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PosX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Sight)).BeginInit();
            this.TabPanelEvent.SuspendLayout();
            this.SuspendLayout();
            // 
            // CB_Class
            // 
            this.CB_Class.FormattingEnabled = true;
            this.CB_Class.Location = new System.Drawing.Point(135, 4);
            this.CB_Class.Name = "CB_Class";
            this.CB_Class.Size = new System.Drawing.Size(69, 21);
            this.CB_Class.TabIndex = 11;
            // 
            // CB_TriggerType
            // 
            this.CB_TriggerType.FormattingEnabled = true;
            this.CB_TriggerType.Items.AddRange(new object[] {
            "Proximity",
            "Step",
            "Press",
            "Distance",
            "Auto"});
            this.CB_TriggerType.Location = new System.Drawing.Point(211, 4);
            this.CB_TriggerType.Name = "CB_TriggerType";
            this.CB_TriggerType.Size = new System.Drawing.Size(70, 21);
            this.CB_TriggerType.TabIndex = 9;
            // 
            // CB_MoveType
            // 
            this.CB_MoveType.FormattingEnabled = true;
            this.CB_MoveType.Items.AddRange(new object[] {
            "None",
            "Magnet",
            "Away",
            "Random",
            "Custom"});
            this.CB_MoveType.Location = new System.Drawing.Point(288, 4);
            this.CB_MoveType.Name = "CB_MoveType";
            this.CB_MoveType.Size = new System.Drawing.Size(79, 21);
            this.CB_MoveType.TabIndex = 10;
            // 
            // TB_Name
            // 
            this.TB_Name.Location = new System.Drawing.Point(25, 4);
            this.TB_Name.Name = "TB_Name";
            this.TB_Name.Size = new System.Drawing.Size(103, 20);
            this.TB_Name.TabIndex = 1;
            // 
            // CHK_Through
            // 
            this.CHK_Through.AutoSize = true;
            this.CHK_Through.Location = new System.Drawing.Point(374, 4);
            this.CHK_Through.Name = "CHK_Through";
            this.CHK_Through.Size = new System.Drawing.Size(13, 14);
            this.CHK_Through.TabIndex = 13;
            this.CHK_Through.UseVisualStyleBackColor = true;
            // 
            // CHK_Disposable
            // 
            this.CHK_Disposable.AutoSize = true;
            this.CHK_Disposable.Location = new System.Drawing.Point(394, 4);
            this.CHK_Disposable.Name = "CHK_Disposable";
            this.CHK_Disposable.Size = new System.Drawing.Size(14, 14);
            this.CHK_Disposable.TabIndex = 14;
            this.CHK_Disposable.UseVisualStyleBackColor = true;
            // 
            // NUM_Speed
            // 
            this.NUM_Speed.Location = new System.Drawing.Point(436, 4);
            this.NUM_Speed.Name = "NUM_Speed";
            this.NUM_Speed.Size = new System.Drawing.Size(41, 20);
            this.NUM_Speed.TabIndex = 16;
            // 
            // NUM_PosX
            // 
            this.NUM_PosX.Location = new System.Drawing.Point(506, 4);
            this.NUM_PosX.Name = "NUM_PosX";
            this.NUM_PosX.Size = new System.Drawing.Size(34, 20);
            this.NUM_PosX.TabIndex = 17;
            // 
            // NUM_PosY
            // 
            this.NUM_PosY.Location = new System.Drawing.Point(547, 4);
            this.NUM_PosY.Name = "NUM_PosY";
            this.NUM_PosY.Size = new System.Drawing.Size(34, 20);
            this.NUM_PosY.TabIndex = 18;
            // 
            // CB_Direction
            // 
            this.CB_Direction.FormattingEnabled = true;
            this.CB_Direction.Items.AddRange(new object[] {
            "Left",
            "Right",
            "Up",
            "Down",
            "None",
            "Random"});
            this.CB_Direction.Location = new System.Drawing.Point(590, 4);
            this.CB_Direction.Name = "CB_Direction";
            this.CB_Direction.Size = new System.Drawing.Size(96, 21);
            this.CB_Direction.TabIndex = 19;
            // 
            // NUM_Sight
            // 
            this.NUM_Sight.Location = new System.Drawing.Point(693, 4);
            this.NUM_Sight.Name = "NUM_Sight";
            this.NUM_Sight.Size = new System.Drawing.Size(45, 20);
            this.NUM_Sight.TabIndex = 20;
            // 
            // TabPanelEvent
            // 
            this.TabPanelEvent.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.TabPanelEvent.ColumnCount = 15;
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 102F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 124F));
            this.TabPanelEvent.Controls.Add(this.CB_Class, 1, 0);
            this.TabPanelEvent.Controls.Add(this.CB_TriggerType, 2, 0);
            this.TabPanelEvent.Controls.Add(this.CB_MoveType, 2, 0);
            this.TabPanelEvent.Controls.Add(this.TB_Name, 1, 0);
            this.TabPanelEvent.Controls.Add(this.CHK_Through, 5, 0);
            this.TabPanelEvent.Controls.Add(this.CHK_Disposable, 6, 0);
            this.TabPanelEvent.Controls.Add(this.NUM_Speed, 8, 0);
            this.TabPanelEvent.Controls.Add(this.NUM_PosX, 9, 0);
            this.TabPanelEvent.Controls.Add(this.NUM_PosY, 10, 0);
            this.TabPanelEvent.Controls.Add(this.CB_Direction, 11, 0);
            this.TabPanelEvent.Controls.Add(this.NUM_Sight, 12, 0);
            this.TabPanelEvent.Controls.Add(this.CHK_Flying, 7, 0);
            this.TabPanelEvent.Controls.Add(this.BTN_Graphics, 13, 0);
            this.TabPanelEvent.Controls.Add(this.BTN_Script, 14, 0);
            this.TabPanelEvent.Controls.Add(this.CHK_Selected, 0, 0);
            this.TabPanelEvent.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.TabPanelEvent.Location = new System.Drawing.Point(0, 0);
            this.TabPanelEvent.Margin = new System.Windows.Forms.Padding(0);
            this.TabPanelEvent.Name = "TabPanelEvent";
            this.TabPanelEvent.RowCount = 1;
            this.TabPanelEvent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TabPanelEvent.Size = new System.Drawing.Size(915, 28);
            this.TabPanelEvent.TabIndex = 2;
            // 
            // CHK_Flying
            // 
            this.CHK_Flying.AutoSize = true;
            this.CHK_Flying.Location = new System.Drawing.Point(415, 4);
            this.CHK_Flying.Name = "CHK_Flying";
            this.CHK_Flying.Size = new System.Drawing.Size(14, 14);
            this.CHK_Flying.TabIndex = 15;
            this.CHK_Flying.UseVisualStyleBackColor = true;
            // 
            // BTN_Graphics
            // 
            this.BTN_Graphics.Location = new System.Drawing.Point(746, 4);
            this.BTN_Graphics.Name = "BTN_Graphics";
            this.BTN_Graphics.Size = new System.Drawing.Size(78, 20);
            this.BTN_Graphics.TabIndex = 21;
            this.BTN_Graphics.Text = "Graphics";
            this.BTN_Graphics.UseVisualStyleBackColor = true;
            this.BTN_Graphics.Click += new System.EventHandler(this.BTN_Graphics_Click);
            // 
            // BTN_Script
            // 
            this.BTN_Script.Location = new System.Drawing.Point(831, 4);
            this.BTN_Script.Name = "BTN_Script";
            this.BTN_Script.Size = new System.Drawing.Size(79, 20);
            this.BTN_Script.TabIndex = 22;
            this.BTN_Script.Text = "Script";
            this.BTN_Script.UseVisualStyleBackColor = true;
            this.BTN_Script.Click += new System.EventHandler(this.BTN_Script_Click);
            // 
            // CHK_Selected
            // 
            this.CHK_Selected.AutoSize = true;
            this.CHK_Selected.Location = new System.Drawing.Point(4, 4);
            this.CHK_Selected.Name = "CHK_Selected";
            this.CHK_Selected.Size = new System.Drawing.Size(14, 14);
            this.CHK_Selected.TabIndex = 23;
            this.CHK_Selected.UseVisualStyleBackColor = true;
            // 
            // PeventListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.TabPanelEvent);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "PeventListItem";
            this.Size = new System.Drawing.Size(916, 29);
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Speed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PosX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PosY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Sight)).EndInit();
            this.TabPanelEvent.ResumeLayout(false);
            this.TabPanelEvent.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TabPanelEvent;
        public System.Windows.Forms.CheckBox CHK_Selected;
        public System.Windows.Forms.ComboBox CB_Class;
        public System.Windows.Forms.ComboBox CB_TriggerType;
        public System.Windows.Forms.ComboBox CB_MoveType;
        public System.Windows.Forms.TextBox TB_Name;
        public System.Windows.Forms.CheckBox CHK_Through;
        public System.Windows.Forms.CheckBox CHK_Disposable;
        public System.Windows.Forms.NumericUpDown NUM_Speed;
        public System.Windows.Forms.NumericUpDown NUM_PosX;
        public System.Windows.Forms.NumericUpDown NUM_PosY;
        public System.Windows.Forms.ComboBox CB_Direction;
        public System.Windows.Forms.NumericUpDown NUM_Sight;
        public System.Windows.Forms.Button BTN_Graphics;
        public System.Windows.Forms.Button BTN_Script;
        public System.Windows.Forms.CheckBox CHK_Flying;
    }
}
