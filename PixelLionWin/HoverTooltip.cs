﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class HoverTooltip : UserControl
    {
        public HoverTooltip()
        {
            InitializeComponent();
        }

        public void SetInfos(Pevent e)
        {
            label1.Text = e.Name == String.Empty ? "Sans nom" : e.Name;
            label2.Text = e.Class != null ? "Hors-classe" : e.Class.ToString();
        }

        private void HoverTooltip_MouseEnter_1(object sender, EventArgs e)
        {
            BackColor = Color.Beige;
        }

        private void HoverTooltip_MouseLeave(object sender, EventArgs e)
        {
            BackColor = Color.AliceBlue;
        }

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            BackColor = Color.Beige;
        }

        private void label2_MouseEnter(object sender, EventArgs e)
        {
            BackColor = Color.Beige;
        }
    }
}
