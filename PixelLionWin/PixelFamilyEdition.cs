﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PixelLionWin
{
    public partial class PixelFamilyEdition : Form
    {
        Bitmap AnimationBuffer;
        Animation Anim;
        SpriteBatch Batch;
        ParticleEngine Engine;

        Int32 FrameCount;

        public PixelFamilyEdition(Animation animation)
        {
            InitializeComponent();

            Anim = animation;
            Init();
        }
        void Init()
        {
            //ATL_Temps.LoadAnimation(animation);
            ATL_Temps.Enabled = false;
            
            CB_ParticleShape.DataSource = Enum.GetValues(typeof(ParticleShape));
            CB_Behavior.DataSource = Enum.GetValues(typeof(ParticleBehaviour));

            NUM_VectorX.Minimum = -1 * (PN_Pixel.Width / 2);
            NUM_VectorX.Maximum = PN_Pixel.Width / 2;            
            NUM_VectorY.Minimum = -1 * (PN_Pixel.Height / 2);
            NUM_VectorY.Maximum = PN_Pixel.Height / 2;

            LB_FadeOutStart.Enabled = CHK_FadeOut.Checked;
            NUM_FadeOutStart.Enabled = CHK_FadeOut.Checked;
            LB_FadeInFrameCount.Enabled = CHK_FadeIn.Checked;
            NUM_FadeInFrameCount.Enabled = CHK_FadeIn.Checked;

            XNADevices.graphicsdevice = new GraphicsDevice(
                GraphicsAdapter.DefaultAdapter, GraphicsProfile.Reach, new PresentationParameters()
                    {
                        BackBufferHeight = PN_Pixel.Height,
                        BackBufferWidth = PN_Pixel.Width,
                        IsFullScreen = false,
                        DeviceWindowHandle = PN_Pixel.Handle
                    }
                );

            Batch = new SpriteBatch(XNADevices.graphicsdevice);
            Engine = new ParticleEngine(new Microsoft.Xna.Framework.Rectangle(0, 0, PN_Pixel.Width, PN_Pixel.Height));
            AnimationBuffer = new Bitmap(PN_Pixel.Width, PN_Pixel.Height);
        }

        void Save()
        {
        }
        private void BTN_Quit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BTN_Save_Click(object sender, EventArgs e)
        {
            Save();
        }
        private void BTN_SaveAndQuit_Click(object sender, EventArgs e)
        {
            Save();
            this.Close();
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }
        private void saveAndQuitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
            this.Close();
        }
        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void CHK_FadeOut_CheckedChanged(object sender, EventArgs e)
        {
            LB_FadeOutStart.Enabled = CHK_FadeOut.Checked;
            NUM_FadeOutStart.Enabled = CHK_FadeOut.Checked;
        }
        private void CHK_FadeIn_CheckedChanged(object sender, EventArgs e)
        {
            LB_FadeInFrameCount.Enabled = CHK_FadeIn.Checked;
            NUM_FadeInFrameCount.Enabled = CHK_FadeIn.Checked;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            DrawPanel();
        }
        private void Present()
        {
            XNADevices.graphicsdevice.Present();
            FrameCount++;
        }
        private void Clear()
        {
            XNADevices.graphicsdevice.Clear(Microsoft.Xna.Framework.Color.White);
        }
        private void Draw()
        {
            Engine.DrawParticles(Batch);
        }
        private void UpdateFamily()
        {
            Engine.UpdateParticles();
        }
        private void DrawPanel()
        {
            UpdateFamily();
            Clear();
            Draw();
            Present();
        }
        private void AddFamily()
        {
            Engine.AddParticules(
                800, 
                (ParticleBehaviour)Enum.Parse(typeof(ParticleBehaviour),CB_Behavior.SelectedItem.ToString()), 
                (ParticleShape)Enum.Parse(typeof(ParticleShape),CB_ParticleShape.SelectedItem.ToString()), 
                new Vector2(float.Parse(NUM_VectorX.Value.ToString()), float.Parse(NUM_VectorY.Value.ToString()))
                );
        }
        private void BTN_Advanced_Click(object sender, EventArgs e)
        {
            PixelMathFunctionEditor dlg = new PixelMathFunctionEditor();
            dlg.ShowDialog();
        }
        private void BTN_AddFamily_Click(object sender, EventArgs e)
        {
            AddFamily();
        }
    }
}
