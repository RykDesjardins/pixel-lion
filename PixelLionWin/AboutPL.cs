﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Media;
using System.IO;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class AboutPL : Form
    {
        // Main objects
        List<Bubble> Bubbles;
        Bitmap Buffer; 
        Image TitleImage;
        Random rand = new Random();
        ScrollingText sText;

        Int32 BubbleCount = 40;
        Int32 BubblePoped = 0;
        Int32 MaxBubbles = 500;

        public AboutPL()
        {
            InitializeComponent();

            // Initialize a table of new bubbles
            Bubbles = new Bubble[BubbleCount].Select(x => Bubble.Create(Size, rand)).ToList();

            // Creation of current buffer to be printed on window
            Buffer = new Bitmap(Width, Height);

            // Creation of window content
            TitleImage = Properties.Resources.PL_Splashscreen_Krypton;
            sText = new ScrollingText(this, Properties.Resources.AboutThanks);

            // Set window title
            Text = "À propos de " + Properties.Resources.ProjectName + " " + Properties.Resources.CurrentVersion;
        }

        public void RefreshWindow(Object sender, EventArgs e)
        {
            using (Graphics g = Graphics.FromImage(Buffer))
            using (Font font = new Font("Courrier New", 12))
            using (Font descfont = new Font("Calibri", 10))
            using (Font titlefont = new Font("Calibri", 16))
            using (Brush brush = new SolidBrush(Color.Black))
            {
                // Erase Buffer
                g.Clear(BackColor);

                // Draw bubbled
                foreach (Bubble b in Bubbles) b.Draw(g, rand);

                // Draw title image
                g.DrawImage(TitleImage, new Rectangle(new Point(0,0), new Size(640, 350)));

                // If bubbles were popped, show how many were
                if (BubblePoped != 0) g.DrawString("Bulles éclatées : " + BubblePoped, font, brush, new Point(3, 3));
             
                // Show scrolling text
                sText.Draw(g);

                // Show about Pixel Lion
                g.DrawString(Properties.Resources.ProjectName + " V." + Properties.Resources.CurrentVersion, titlefont, brush, new Point(3, TitleImage.Height + 10));
                g.DrawString(Properties.Resources.ProjectDescription, descfont, brush, new Rectangle(new Point(6, TitleImage.Height + 35), new Size(Width - 10, 400)));
            
                // Show about Pixel Monkey
                g.DrawString("Pixel Monkey V." + Properties.Resources.PixelMonkeyVersion, titlefont, brush, new Point(3, TitleImage.Height + 130));
                g.DrawString(Properties.Resources.PixelMonkey, descfont, brush, new Rectangle(new Point(6, TitleImage.Height + 155), new Size(Width - 10, 400)));

                // Show about Animation Studio
                g.DrawString(Properties.Resources.AnimationStudioName + " V." + Properties.Resources.AnimationStudioVersion, titlefont, brush, new Point(3, TitleImage.Height + 250));
                g.DrawString(Properties.Resources.AnimationStudioDescription, descfont, brush, new Rectangle(new Point(6, TitleImage.Height + 275), new Size(Width - 10, 300)));
            }

            using (Graphics g = CreateGraphics())
            {
                // Draw buffer on screen
                g.DrawImage(Buffer, new Point(0, 0));
            }
        }

        private void AboutPL_MouseDown(object sender, MouseEventArgs e)
        {
            // Detect if user clicked on a bubble
            Bubble ToPop = DetectCollision(new Point(e.X, e.Y));

            // If so, pop it!
            if (ToPop != null) ToPop.Pop(Properties.Resources.WormPop);
        }

        private Bubble DetectCollision(Point p)
        {
            // Value returned, will remain null if no bubble is found under cursor
            Bubble returnedvalue = null;

            foreach (Bubble b in Bubbles)
            {
                // If cursor is in corner of the bubble bounderies
                if (p.X <= b.Position.X + b.Diam && p.X > b.Position.X &&
                    p.Y <= b.Position.Y + b.Diam && p.Y > b.Position.Y &&
                    b.Opacity > 0)
                {
                    // Get half diameter and relative position of mouse
                    // according to bubble
                    Int32 semi = b.Diam / 2;
                    Point RelPos = new Point(p.X - b.Position.X, p.Y - b.Position.Y);

                    // Check if mouse is at "Half Diameter" distance from center of bubble
                    if (Math.Abs(Convert.ToDecimal(RelPos.X - b.Diam / 2)) <= b.Diam / 2 &&
                        Math.Abs(Convert.ToDecimal(RelPos.Y - b.Diam / 2)) <= b.Diam / 2)
                    {
                        // If so, increase bubble poped counter
                        BubblePoped++;

                        // Then set returned value with clicked bubble
                        returnedvalue = b;

                        break;
                    }
                }
            }

            return returnedvalue;
        }

        private void AboutPL_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Ensure that everything is disposed before closing form
            Refresh_Timer.Stop();

            TitleImage.Dispose();
            Buffer.Dispose();
        }

        class ScrollingText
        {
            const Int32 XVelocity = 2;
            readonly Point InitPos;
            Point Position;
            Int32 Length;

            String TextContent;

            public ScrollingText(Form form, String s)
            {
                // Set initial position, Y never changes
                InitPos = new Point(form.Width, Properties.Resources.PLSplash.Height - 15);
                Position = new Point(InitPos.X, InitPos.Y);

                // Ensure that text is not null
                if (s != null)
                {
                    using (Font font = new Font("Calibri", 12))
                    using (Graphics g = form.CreateGraphics())
                    {
                        // Set text and mesure string length
                        SetText(s);
                        Length = (Int32)g.MeasureString(s, font).Width;
                    }
                }
            }

            public void SetText(String s)
            {
                // Text mutator
                TextContent = s;
            }

            public void Scroll()
            {
                // Set new position
                //  X = Current X + Velocity X
                //  Y = Constent
                Position = new Point(Position.X - XVelocity, Position.Y);

                if (Position.X <= -Length) Position.X = InitPos.X;
            }

            public void Draw(Graphics g)
            {
                // Update position
                Scroll();

                using (Font font = new Font("Calibri", 12))
                using (Brush brush = new SolidBrush(Color.Black))
                {
                    // Draw text according to position, will simulate scroll effect
                    g.DrawString(TextContent, font, brush, Position);
                }
            }
        }

        

        private void AboutPL_KeyDown(object sender, KeyEventArgs e)
        {
            /*Commands
             * 
             * Up and Down to increase or decrease current bubbles size
             * Add or Substract to play with the current number of bubbles
             * Escape to close window
             * CTRL+X to pop all current bubbles
             */

            if (e.KeyCode == Keys.Up)
            {
                for (int i = 0; i < BubbleCount; i++)
                    Bubbles[i].Diam++;
            }
            else if (e.KeyCode == Keys.Down)
            {
                for (int i = 0; i < BubbleCount; i++)
                    Bubbles[i].Diam--;
            }
            else if (e.KeyCode == Keys.Add)
            {
                if (BubbleCount <= MaxBubbles)
                {
                    BubbleCount++;
                    Bubbles.Add(Bubble.Create(Size, rand));
                }
            }
            else if (e.KeyCode == Keys.Subtract)
            {
                if (BubbleCount != 0)
                {
                    BubbleCount--;
                    Bubbles.RemoveAt(BubbleCount);
                }
            }
            else if (e.KeyCode == Keys.X)
            {
                if ((e.Modifiers & Keys.Control) > 0)
                    foreach (Bubble b in Bubbles)
                    {
                        b.Pop(Properties.Resources.WormPop);
                        BubblePoped++;
                    }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }
    }
}
