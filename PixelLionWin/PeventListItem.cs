﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class PeventListItem : UserControl
    {
        Pevent pevent;
        PeventClassBundle cBundle;
        SizeI map;
        SpriteBundle sbundle;
        Map CurrentMap;

        public Sprite pSprite { get; set; }
        public string sScript { get; set; }

        public PeventListItem(Pevent Event, SpriteBundle bundle, PeventClassBundle bundlec, Map pmap)
        {
            sbundle = bundle;
            CurrentMap = pmap;
            map = pmap.MapSize;
            pevent = Event;
            cBundle = bundlec;

            System.Windows.Forms.ToolTip THEtooltip = new System.Windows.Forms.ToolTip();

            InitializeComponent();

            foreach (PeventClass pClass in cBundle.ListpEventClass)
                CB_Class.Items.Add(pClass);

            THEtooltip.SetToolTip(TB_Name, "Name");
            TB_Name.Text = pevent.Name;

            THEtooltip.SetToolTip(CB_Class, "Class");
            CB_Class.Text = pevent.Class.ToString();
            THEtooltip.SetToolTip(CB_Direction, "Direction");
            CB_Direction.SelectedItem = pevent.Direction.ToString();
            THEtooltip.SetToolTip(CB_MoveType, "Movement type");
            CB_MoveType.SelectedItem = pevent.MoveType.ToString();
            THEtooltip.SetToolTip(CB_TriggerType, "Trigger type");
            CB_TriggerType.SelectedItem = pevent.TriggerType.ToString();

            THEtooltip.SetToolTip(NUM_PosX, "Position X");
            NUM_PosX.Minimum = 0;
            NUM_PosX.Maximum = map.Width;
            NUM_PosX.Value = pevent.Position.X;
            THEtooltip.SetToolTip(NUM_PosY, "Position Y");
            NUM_PosY.Minimum = 0;
            NUM_PosY.Maximum = map.Height;
            NUM_PosY.Value = pevent.Position.Y;
            THEtooltip.SetToolTip(NUM_Sight, "Sight");
            NUM_Sight.Value = pevent.Sight;
            THEtooltip.SetToolTip(NUM_Speed, "Speed");
            NUM_Speed.Value = pevent.Velocity;

            THEtooltip.SetToolTip(CHK_Flying, "Flying");
            CHK_Flying.Checked = pevent.Flying;
            THEtooltip.SetToolTip(CHK_Through, "Trough");
            CHK_Through.Checked = pevent.Through;
            THEtooltip.SetToolTip(CHK_Selected, "Selected");

            THEtooltip.SetToolTip(CHK_Disposable, "Disposable");
            CHK_Disposable.Checked = pevent.Disposable;
            
            this.CB_MoveType.SelectedIndexChanged += new System.EventHandler(this.CB_MoveType_SelectedIndexChanged);
        }

        public void Save()
        {
            if ((CurrentMap.Tiles[0, int.Parse(NUM_PosX.Value.ToString()), int.Parse(NUM_PosY.Value.ToString())].pEvent != null) && (CurrentMap.Tiles[0, int.Parse(NUM_PosX.Value.ToString()), int.Parse(NUM_PosY.Value.ToString())].pEvent != pevent))
            {
                DialogResult dialogResult = MessageBox.Show("The tile at X: " + NUM_PosX.Value + " Y: " + NUM_PosY.Value + " already contains a pEvent do you want to replace it?", "Warning!", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    CurrentMap.Tiles[0, pevent.Position.X, pevent.Position.Y].pEvent = null;
                    CurrentMap.Tiles[0, int.Parse(NUM_PosX.Value.ToString()), int.Parse(NUM_PosY.Value.ToString())].pEvent = pevent;
                    pevent.Position = new Point(int.Parse(NUM_PosX.Value.ToString()), int.Parse(NUM_PosY.Value.ToString()));
                }
            }
            pevent.Direction = (PeventDirection)Enum.Parse(typeof(PeventDirection), CB_Direction.Text);
            pevent.Flying = CHK_Flying.Checked;
            pevent.MoveType = (PeventMoveType)Enum.Parse(typeof(PeventMoveType), CB_MoveType.Text);
            pevent.Name = TB_Name.Text;
            pevent.Sight = Convert.ToInt32(NUM_Sight.Value);
            pevent.Through = CHK_Through.Checked;
            pevent.Disposable = CHK_Disposable.Checked;
            pevent.TriggerType = (PeventTriggerType)Enum.Parse(typeof(PeventTriggerType), CB_TriggerType.Text);
            pevent.Velocity = Convert.ToInt32(NUM_Speed.Value);
            pevent.Class = (PeventClass)CB_Class.SelectedItem;

            if (sScript != null)
                pevent.Script = sScript;
            if (pSprite != null)
                pevent.pSprite = pSprite;
        }

        private void BTN_Script_Click(object sender, EventArgs e)
        {
            PopUpScript dlg;

            if (sScript == null)
                dlg = new PopUpScript(pevent);
            else
                dlg = new PopUpScript(sScript);

            dlg.ShowDialog();
            sScript = dlg.sScript;
        }

        private void BTN_Graphics_Click(object sender, EventArgs e)
        {
            PopUpGraphics dlg;

            if(pSprite == null)
                dlg = new PopUpGraphics(pevent, sbundle);
            else
                dlg = new PopUpGraphics(pSprite, sbundle);

            dlg.ShowDialog();

            pSprite = dlg.pSprite;
        }

        private void CB_MoveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CB_MoveType.SelectedIndex == 4)
            {
                CustomMouvement dlg = new CustomMouvement(pevent, NUM_PosX.Value.ToString() + " x " + NUM_PosY.Value.ToString(), map.Width, map.Height);
                dlg.ShowDialog();
            }
        }
    }
}
