﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class PixelLinkingForm : Form
    {
        PixelLink link;

        public PixelLinkingForm(PixelLink Link)
        {
            InitializeComponent();
            DialogResult = System.Windows.Forms.DialogResult.None;
            this.link = Link ?? new PixelLink();

            TB_Name.Text = link.Title;
            TB_Path.Text = link.Path;
            CB_CreateFolders.Checked = link.CreateFolders;
            CB_IncludeMaps.Checked = link.IncludeMapsInProject;
            CB_SingleFile.Checked = link.ProjectFileOnly;
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            link.Title = TB_Name.Text;
            link.Path = TB_Path.Text;
            link.CreateFolders = CB_CreateFolders.Checked;
            link.IncludeMapsInProject = CB_IncludeMaps.Checked;
            link.ProjectFileOnly = CB_SingleFile.Checked;

            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void BTN_Browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) TB_Path.Text = dlg.FileName;
        }

        public PixelLink GetLink()
        {
            return link;
        }
    }
}
