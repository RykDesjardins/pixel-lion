﻿namespace PixelLionWin
{
    partial class IconManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LIST_Icons = new System.Windows.Forms.ListBox();
            this.PN_Preview = new System.Windows.Forms.Panel();
            this.BTN_DeleteItemObject = new System.Windows.Forms.Button();
            this.BTN_AddItemObject = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LIST_Icons
            // 
            this.LIST_Icons.FormattingEnabled = true;
            this.LIST_Icons.Location = new System.Drawing.Point(12, 12);
            this.LIST_Icons.Name = "LIST_Icons";
            this.LIST_Icons.Size = new System.Drawing.Size(186, 264);
            this.LIST_Icons.TabIndex = 0;
            this.LIST_Icons.SelectedIndexChanged += new System.EventHandler(this.LIST_Icons_SelectedIndexChanged);
            // 
            // PN_Preview
            // 
            this.PN_Preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Preview.Location = new System.Drawing.Point(204, 12);
            this.PN_Preview.Name = "PN_Preview";
            this.PN_Preview.Size = new System.Drawing.Size(85, 81);
            this.PN_Preview.TabIndex = 1;
            // 
            // BTN_DeleteItemObject
            // 
            this.BTN_DeleteItemObject.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.BTN_DeleteItemObject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_DeleteItemObject.Location = new System.Drawing.Point(249, 99);
            this.BTN_DeleteItemObject.Name = "BTN_DeleteItemObject";
            this.BTN_DeleteItemObject.Size = new System.Drawing.Size(40, 40);
            this.BTN_DeleteItemObject.TabIndex = 3;
            this.BTN_DeleteItemObject.UseVisualStyleBackColor = true;
            this.BTN_DeleteItemObject.Click += new System.EventHandler(this.BTN_DeleteItemObject_Click);
            // 
            // BTN_AddItemObject
            // 
            this.BTN_AddItemObject.BackgroundImage = global::PixelLionWin.Properties.Resources.AddIcon;
            this.BTN_AddItemObject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_AddItemObject.Location = new System.Drawing.Point(204, 99);
            this.BTN_AddItemObject.Name = "BTN_AddItemObject";
            this.BTN_AddItemObject.Size = new System.Drawing.Size(40, 40);
            this.BTN_AddItemObject.TabIndex = 2;
            this.BTN_AddItemObject.UseVisualStyleBackColor = true;
            this.BTN_AddItemObject.Click += new System.EventHandler(this.BTN_AddItemObject_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Location = new System.Drawing.Point(235, 226);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 50);
            this.button1.TabIndex = 4;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // IconManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 286);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BTN_DeleteItemObject);
            this.Controls.Add(this.BTN_AddItemObject);
            this.Controls.Add(this.PN_Preview);
            this.Controls.Add(this.LIST_Icons);
            this.Name = "IconManager";
            this.Text = "IconManager";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LIST_Icons;
        private System.Windows.Forms.Panel PN_Preview;
        private System.Windows.Forms.Button BTN_DeleteItemObject;
        private System.Windows.Forms.Button BTN_AddItemObject;
        private System.Windows.Forms.Button button1;
    }
}