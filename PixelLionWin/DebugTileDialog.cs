﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class DebugTileDialog : Form
    {
        Tile[] tiles;
        ProjectRessources ressources;

        public DebugTileDialog(Tile[] tiles, ProjectRessources ress)
        {
            InitializeComponent();

            this.tiles = tiles;
            ressources = ress;

            SetInfos(tiles[0]);
        }

        private void r2_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked) SetInfos(tiles[Int32.Parse(((Control)sender).Name.Substring(1))]);
        }

        private void SetInfos(Tile tile)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                LB_Position.Text = tile.X + " X " + tile.Y;
                LB_TileSetIndex.Text = tile.TileIndexX < 0 ? "Tuile vide" : tile.TileIndexX + " X " + tile.TileIndexY;

                //PN_Preview.BackgroundImage = utils.ConvertToImage(tile.Background);
                BTN_pEvent.Enabled = tile.pEvent != null;
            }
        }

        private void BTN_pEvent_Click(object sender, EventArgs e)
        {
            Int32 Index = -1;

            if (r0.Checked) Index = 0;
            else if (r1.Checked) Index = 1;
            else if (r2.Checked) Index = 2;

            PeventEditor dlg = new PeventEditor(tiles[Index].pEvent, ressources.Sprites, ressources.Classes);
            dlg.ReadOnly();
            dlg.ShowDialog();
        }
    }
}
