﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace PixelLionWin
{
    public partial class ErrorHandler : Form
    {
        [DllImport("user32.dll")]
        static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        [DllImport("user32.dll")]
        static extern int SendMessage(IntPtr hWnd, int wMsg, int wParam, int lParam);

        const int EM_LINESCROLL = 0x00B6;
        const int SB_HORZ = 0;
        const int SB_VERT = 1;


        public ErrorHandler(Exception e)
        {
            InitializeComponent();
            
            label1.Text = (new String[] { "Hehh, oopsie!", "1001 0110 1002?", "Aie! Ça brule!", "Non mais!" ,"Encore--?!", "J'ai attrapé cela!", "*gasp* Je suis malade...", "Ça tourne pas rond par ici!", "«#@%&*$» dit Pixel Lion", "Au secours! Des insectes!" })[new Random().Next(10)];

            label2.Text = e.Message;

            TB_StackTrace.Text = e.StackTrace;

            var st = new StackTrace(e, true); 
            var query = st.GetFrames()         
                          .Select(frame => new
                          {                   
                              FileName = frame.GetFileName(),
                              LineNumber = frame.GetFileLineNumber(),
                              ColumnNumber = frame.GetFileColumnNumber(),
                              Method = frame.GetMethod(),
                              Class = frame.GetMethod().DeclaringType,
                          }).Where(x => x.FileName != null).FirstOrDefault();

            label3.Text = query.FileName + " à la ligne " + query.LineNumber + " dans la méthode " + query.Method + " de la classe " + query.Class;

            TB_Codefile.Text = "";

            String[] file = File.ReadAllLines(query.FileName);
            String line = file[query.LineNumber - 1].Trim();

            foreach (String str in file) TB_Codefile.AppendText(str + "\r\n");

            SetScrollPos(TB_Codefile.Handle, SB_VERT, query.LineNumber - 4, true);
            SendMessage(TB_Codefile.Handle, EM_LINESCROLL, 0, query.LineNumber - 4);

            ActiveControl = TB_Codefile;

            Int32 startindex = TB_Codefile.Text.IndexOf(line);
            TB_Codefile.SelectionStart = startindex;
            TB_Codefile.SelectionLength = line.Length;
        }

        private void BTN_Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


    }
}
