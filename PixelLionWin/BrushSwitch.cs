﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PixelLionWin
{
    public enum BrushType
    {
        Square, Circle
    }

    public partial class BrushSwitch : Form
    {
        public BrushType Brushtype;
        public Int32 Brushsize;

        public BrushSwitch(Int32 size, BrushType type)
        {
            InitializeComponent();

            Brushtype = type;
            Brushsize = size;
            TRACK_Size.Value = size;

            if (type == BrushType.Square)
                RB_Square.Checked = true;
            else
                RB_Circle.Checked = true;
            
            UpdatePreview();
        }

        private void UpdatePreview()
        {
            using (Brush p = new SolidBrush(Color.Black))
            using (Brush b = new SolidBrush(Color.White))
            using (Graphics g = PN_Preview.CreateGraphics())
            {
                g.FillRectangle(b, new Rectangle(new Point(0,0), PN_Preview.Size));

                if (Brushtype == BrushType.Circle)
                    g.FillEllipse(p, new Rectangle(PN_Preview.Width / 2 - Brushsize / 2, PN_Preview.Height / 2 - Brushsize / 2, Brushsize, Brushsize));
                else if (Brushtype == BrushType.Square)
                    g.FillRectangle(p, new Rectangle(PN_Preview.Width / 2 - Brushsize / 2, PN_Preview.Height / 2 - Brushsize / 2, Brushsize, Brushsize));
            
                
            }
        }

        private void TRACK_Size_Scroll(object sender, EventArgs e)
        {
            Brushsize = TRACK_Size.Value;
            UpdatePreview();
        }

        private void RB_Square_CheckedChanged(object sender, EventArgs e)
        {
            if (RB_Square.Checked) Brushtype = BrushType.Square;

            UpdatePreview();
        }

        private void RB_Circle_CheckedChanged(object sender, EventArgs e)
        {
            if (RB_Circle.Checked) Brushtype = BrushType.Circle;

            UpdatePreview();
        }

        private void BTN_Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BrushSwitch_Shown(object sender, EventArgs e)
        {
            UpdatePreview();
        }

        private void BrushSwitch_MouseClick(object sender, MouseEventArgs e)
        {
            Close();
        }
    }
}
