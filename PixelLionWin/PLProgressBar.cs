﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Media;
using System.Text;
using System.Windows.Forms;

namespace PixelLionWin
{
    public partial class PLProgressBar : UserControl
    {
        Int32 iMax;
        Int32 iValue;

        public PLProgressBar()
        {
            InitializeComponent();
            Max = 100;
            Value = 0;
        }

        public Int32 Max
        {
            get
            {
                return iMax;
            }
            set
            {
                if (value > 0) iMax = value;
                DrawBar();
            }
        }

        public Int32 Value
        {
            get
            {
                return iValue;
            }
            set
            {
                iValue = value > iMax ? iMax : value;
                DrawBar();
            }
        }

        public void Increment()
        {
            Value = Value == Max ? Value : Value + 1;
        }

        public void Decrement()
        {
            Value = Value == 0 ? 0 : Value - 1;
        }

        private void PN_Bar_Paint(object sender, PaintEventArgs e)
        {
            DrawBar();
        }

        private void DrawBar()
        {
            if (Value != 0)
            {
                using (Bitmap Buffer = new Bitmap(Width, Height))
                using (Graphics g = PN_Bar.CreateGraphics())
                using (Pen pen = new Pen(Color.Black, 1))
                using (Brush brush = new LinearGradientBrush(new Rectangle(0, 0, GetRatioLength(), Height), Color.Orange, Color.Cornsilk, 0f))
                {
                    // Buffer
                    Graphics gBuffer = Graphics.FromImage(Buffer);

                    // Draw on buffer
                    gBuffer.FillRectangle(brush, new Rectangle(0, 0, GetRatioLength(), Height));
                    gBuffer.DrawRectangle(pen, new Rectangle(new Point(0, 0), new Size(Width-1, Height-1)));

                    // Draw buffer on panel
                    g.DrawImage(Buffer, new Point(0, 0));
                    gBuffer.Dispose();
                }
            }
        }

        private Int32 GetRatioLength()
        {
            Single Percent = (Single)Value / (Single)Max;
            Single BarWidth = (Single)Width * Percent;

            return (Int32)BarWidth;
        }
    }
}
