﻿namespace PixelLionWin
{
    partial class PopUpScript
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TB_Script = new System.Windows.Forms.TextBox();
            this.BTN_Close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TB_Script
            // 
            this.TB_Script.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TB_Script.ForeColor = System.Drawing.Color.Lime;
            this.TB_Script.Location = new System.Drawing.Point(12, 13);
            this.TB_Script.Multiline = true;
            this.TB_Script.Name = "TB_Script";
            this.TB_Script.Size = new System.Drawing.Size(251, 253);
            this.TB_Script.TabIndex = 7;
            // 
            // BTN_Close
            // 
            this.BTN_Close.Location = new System.Drawing.Point(269, 243);
            this.BTN_Close.Name = "BTN_Close";
            this.BTN_Close.Size = new System.Drawing.Size(75, 23);
            this.BTN_Close.TabIndex = 9;
            this.BTN_Close.Text = "Close";
            this.BTN_Close.UseVisualStyleBackColor = true;
            this.BTN_Close.Click += new System.EventHandler(this.BTN_Cancel_Click);
            // 
            // PopUpScript
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 278);
            this.Controls.Add(this.BTN_Close);
            this.Controls.Add(this.TB_Script);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PopUpScript";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Script";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PopUpScript_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TB_Script;
        private System.Windows.Forms.Button BTN_Close;
    }
}