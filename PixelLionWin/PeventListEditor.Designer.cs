﻿namespace PixelLionWin
{
    partial class PeventListEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PeventListPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.BTN_Quit = new System.Windows.Forms.Button();
            this.BTN_SaveAndQuit = new System.Windows.Forms.Button();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.CB_Class = new System.Windows.Forms.ComboBox();
            this.CB_TriggerType = new System.Windows.Forms.ComboBox();
            this.CB_MoveType = new System.Windows.Forms.ComboBox();
            this.TabPanelEvent = new System.Windows.Forms.TableLayoutPanel();
            this.PN_Speed = new System.Windows.Forms.Panel();
            this.BTN_SpeedLower = new System.Windows.Forms.Button();
            this.BTN_SpeedAdd = new System.Windows.Forms.Button();
            this.PN_Sight = new System.Windows.Forms.Panel();
            this.BTN_SightLower = new System.Windows.Forms.Button();
            this.BTN_SightAdd = new System.Windows.Forms.Button();
            this.PN_PositionY = new System.Windows.Forms.Panel();
            this.BTN_PositionYLower = new System.Windows.Forms.Button();
            this.BTN_PositionYAdd = new System.Windows.Forms.Button();
            this.PN_PositionX = new System.Windows.Forms.Panel();
            this.BTN_PositionXLower = new System.Windows.Forms.Button();
            this.BTN_PositionXAdd = new System.Windows.Forms.Button();
            this.CHK_Through = new System.Windows.Forms.CheckBox();
            this.CHK_Disposable = new System.Windows.Forms.CheckBox();
            this.CB_Direction = new System.Windows.Forms.ComboBox();
            this.CHK_Flying = new System.Windows.Forms.CheckBox();
            this.BTN_Graphics = new System.Windows.Forms.Button();
            this.BTN_Script = new System.Windows.Forms.Button();
            this.TB_Name = new System.Windows.Forms.TextBox();
            this.GeneralModifier = new System.Windows.Forms.GroupBox();
            this.BTN_InvertSelection = new System.Windows.Forms.Button();
            this.BTN_DeselectAll = new System.Windows.Forms.Button();
            this.BTN_SelectAll = new System.Windows.Forms.Button();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAndQuitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deselectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TabPanelEvent.SuspendLayout();
            this.PN_Speed.SuspendLayout();
            this.PN_Sight.SuspendLayout();
            this.PN_PositionY.SuspendLayout();
            this.PN_PositionX.SuspendLayout();
            this.GeneralModifier.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // PeventListPanel
            // 
            this.PeventListPanel.AutoScroll = true;
            this.PeventListPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.PeventListPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PeventListPanel.Location = new System.Drawing.Point(12, 78);
            this.PeventListPanel.Margin = new System.Windows.Forms.Padding(0);
            this.PeventListPanel.Name = "PeventListPanel";
            this.PeventListPanel.Size = new System.Drawing.Size(935, 440);
            this.PeventListPanel.TabIndex = 1;
            // 
            // BTN_Quit
            // 
            this.BTN_Quit.Location = new System.Drawing.Point(950, 495);
            this.BTN_Quit.Name = "BTN_Quit";
            this.BTN_Quit.Size = new System.Drawing.Size(101, 23);
            this.BTN_Quit.TabIndex = 2;
            this.BTN_Quit.Text = "Quit";
            this.BTN_Quit.UseVisualStyleBackColor = true;
            this.BTN_Quit.Click += new System.EventHandler(this.BTN_Quit_Click);
            // 
            // BTN_SaveAndQuit
            // 
            this.BTN_SaveAndQuit.Location = new System.Drawing.Point(950, 466);
            this.BTN_SaveAndQuit.Name = "BTN_SaveAndQuit";
            this.BTN_SaveAndQuit.Size = new System.Drawing.Size(101, 23);
            this.BTN_SaveAndQuit.TabIndex = 3;
            this.BTN_SaveAndQuit.Text = "Save and Quit";
            this.BTN_SaveAndQuit.UseVisualStyleBackColor = true;
            this.BTN_SaveAndQuit.Click += new System.EventHandler(this.BTN_SaveAndQuit_Click);
            // 
            // BTN_Save
            // 
            this.BTN_Save.Location = new System.Drawing.Point(950, 437);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(101, 23);
            this.BTN_Save.TabIndex = 4;
            this.BTN_Save.Text = "Save";
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // CB_Class
            // 
            this.CB_Class.FormattingEnabled = true;
            this.CB_Class.Location = new System.Drawing.Point(133, 4);
            this.CB_Class.Name = "CB_Class";
            this.CB_Class.Size = new System.Drawing.Size(69, 21);
            this.CB_Class.TabIndex = 11;
            // 
            // CB_TriggerType
            // 
            this.CB_TriggerType.FormattingEnabled = true;
            this.CB_TriggerType.Items.AddRange(new object[] {
            "Proximity",
            "Step",
            "Press",
            "Distance",
            "Auto"});
            this.CB_TriggerType.Location = new System.Drawing.Point(209, 4);
            this.CB_TriggerType.Name = "CB_TriggerType";
            this.CB_TriggerType.Size = new System.Drawing.Size(69, 21);
            this.CB_TriggerType.TabIndex = 9;
            // 
            // CB_MoveType
            // 
            this.CB_MoveType.FormattingEnabled = true;
            this.CB_MoveType.Items.AddRange(new object[] {
            "None",
            "Magnet",
            "Away",
            "Random",
            "Custom"});
            this.CB_MoveType.Location = new System.Drawing.Point(285, 4);
            this.CB_MoveType.Name = "CB_MoveType";
            this.CB_MoveType.Size = new System.Drawing.Size(80, 21);
            this.CB_MoveType.TabIndex = 10;
            // 
            // TabPanelEvent
            // 
            this.TabPanelEvent.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.TabPanelEvent.ColumnCount = 13;
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.TabPanelEvent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.TabPanelEvent.Controls.Add(this.PN_Speed, 7, 0);
            this.TabPanelEvent.Controls.Add(this.PN_Sight, 11, 0);
            this.TabPanelEvent.Controls.Add(this.PN_PositionY, 9, 0);
            this.TabPanelEvent.Controls.Add(this.PN_PositionX, 8, 0);
            this.TabPanelEvent.Controls.Add(this.CHK_Through, 4, 0);
            this.TabPanelEvent.Controls.Add(this.CHK_Disposable, 5, 0);
            this.TabPanelEvent.Controls.Add(this.CB_Direction, 10, 0);
            this.TabPanelEvent.Controls.Add(this.CHK_Flying, 6, 0);
            this.TabPanelEvent.Controls.Add(this.BTN_Graphics, 12, 0);
            this.TabPanelEvent.Controls.Add(this.BTN_Script, 13, 0);
            this.TabPanelEvent.Controls.Add(this.CB_Class, 1, 0);
            this.TabPanelEvent.Controls.Add(this.TB_Name, 0, 0);
            this.TabPanelEvent.Controls.Add(this.CB_MoveType, 3, 0);
            this.TabPanelEvent.Controls.Add(this.CB_TriggerType, 2, 0);
            this.TabPanelEvent.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.TabPanelEvent.Location = new System.Drawing.Point(3, 16);
            this.TabPanelEvent.Margin = new System.Windows.Forms.Padding(0);
            this.TabPanelEvent.Name = "TabPanelEvent";
            this.TabPanelEvent.RowCount = 1;
            this.TabPanelEvent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TabPanelEvent.Size = new System.Drawing.Size(914, 28);
            this.TabPanelEvent.TabIndex = 5;
            // 
            // PN_Speed
            // 
            this.PN_Speed.Controls.Add(this.BTN_SpeedLower);
            this.PN_Speed.Controls.Add(this.BTN_SpeedAdd);
            this.PN_Speed.Location = new System.Drawing.Point(431, 1);
            this.PN_Speed.Margin = new System.Windows.Forms.Padding(0);
            this.PN_Speed.Name = "PN_Speed";
            this.PN_Speed.Size = new System.Drawing.Size(57, 22);
            this.PN_Speed.TabIndex = 7;
            // 
            // BTN_SpeedLower
            // 
            this.BTN_SpeedLower.Location = new System.Drawing.Point(33, -1);
            this.BTN_SpeedLower.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_SpeedLower.Name = "BTN_SpeedLower";
            this.BTN_SpeedLower.Size = new System.Drawing.Size(24, 20);
            this.BTN_SpeedLower.TabIndex = 27;
            this.BTN_SpeedLower.Text = "-";
            this.BTN_SpeedLower.UseVisualStyleBackColor = true;
            // 
            // BTN_SpeedAdd
            // 
            this.BTN_SpeedAdd.Location = new System.Drawing.Point(9, -1);
            this.BTN_SpeedAdd.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_SpeedAdd.Name = "BTN_SpeedAdd";
            this.BTN_SpeedAdd.Size = new System.Drawing.Size(24, 20);
            this.BTN_SpeedAdd.TabIndex = 26;
            this.BTN_SpeedAdd.Text = "+";
            this.BTN_SpeedAdd.UseVisualStyleBackColor = true;
            // 
            // PN_Sight
            // 
            this.PN_Sight.Controls.Add(this.BTN_SightLower);
            this.PN_Sight.Controls.Add(this.BTN_SightAdd);
            this.PN_Sight.Location = new System.Drawing.Point(687, 1);
            this.PN_Sight.Margin = new System.Windows.Forms.Padding(0);
            this.PN_Sight.Name = "PN_Sight";
            this.PN_Sight.Size = new System.Drawing.Size(56, 22);
            this.PN_Sight.TabIndex = 29;
            // 
            // BTN_SightLower
            // 
            this.BTN_SightLower.Location = new System.Drawing.Point(27, 0);
            this.BTN_SightLower.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_SightLower.Name = "BTN_SightLower";
            this.BTN_SightLower.Size = new System.Drawing.Size(24, 20);
            this.BTN_SightLower.TabIndex = 27;
            this.BTN_SightLower.Text = "-";
            this.BTN_SightLower.UseVisualStyleBackColor = true;
            // 
            // BTN_SightAdd
            // 
            this.BTN_SightAdd.Location = new System.Drawing.Point(3, 0);
            this.BTN_SightAdd.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_SightAdd.Name = "BTN_SightAdd";
            this.BTN_SightAdd.Size = new System.Drawing.Size(24, 20);
            this.BTN_SightAdd.TabIndex = 26;
            this.BTN_SightAdd.Text = "+";
            this.BTN_SightAdd.UseVisualStyleBackColor = true;
            // 
            // PN_PositionY
            // 
            this.PN_PositionY.Controls.Add(this.BTN_PositionYLower);
            this.PN_PositionY.Controls.Add(this.BTN_PositionYAdd);
            this.PN_PositionY.Location = new System.Drawing.Point(542, 1);
            this.PN_PositionY.Margin = new System.Windows.Forms.Padding(0);
            this.PN_PositionY.Name = "PN_PositionY";
            this.PN_PositionY.Size = new System.Drawing.Size(48, 22);
            this.PN_PositionY.TabIndex = 28;
            // 
            // BTN_PositionYLower
            // 
            this.BTN_PositionYLower.Location = new System.Drawing.Point(24, -1);
            this.BTN_PositionYLower.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_PositionYLower.Name = "BTN_PositionYLower";
            this.BTN_PositionYLower.Size = new System.Drawing.Size(24, 20);
            this.BTN_PositionYLower.TabIndex = 27;
            this.BTN_PositionYLower.Text = "-";
            this.BTN_PositionYLower.UseVisualStyleBackColor = true;
            // 
            // BTN_PositionYAdd
            // 
            this.BTN_PositionYAdd.Location = new System.Drawing.Point(0, -1);
            this.BTN_PositionYAdd.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_PositionYAdd.Name = "BTN_PositionYAdd";
            this.BTN_PositionYAdd.Size = new System.Drawing.Size(24, 20);
            this.BTN_PositionYAdd.TabIndex = 26;
            this.BTN_PositionYAdd.Text = "+";
            this.BTN_PositionYAdd.UseVisualStyleBackColor = true;
            // 
            // PN_PositionX
            // 
            this.PN_PositionX.Controls.Add(this.BTN_PositionXLower);
            this.PN_PositionX.Controls.Add(this.BTN_PositionXAdd);
            this.PN_PositionX.Location = new System.Drawing.Point(495, 1);
            this.PN_PositionX.Margin = new System.Windows.Forms.Padding(0);
            this.PN_PositionX.Name = "PN_PositionX";
            this.PN_PositionX.Size = new System.Drawing.Size(46, 22);
            this.PN_PositionX.TabIndex = 8;
            // 
            // BTN_PositionXLower
            // 
            this.BTN_PositionXLower.Location = new System.Drawing.Point(24, -1);
            this.BTN_PositionXLower.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_PositionXLower.Name = "BTN_PositionXLower";
            this.BTN_PositionXLower.Size = new System.Drawing.Size(24, 20);
            this.BTN_PositionXLower.TabIndex = 27;
            this.BTN_PositionXLower.Text = "-";
            this.BTN_PositionXLower.UseVisualStyleBackColor = true;
            // 
            // BTN_PositionXAdd
            // 
            this.BTN_PositionXAdd.Location = new System.Drawing.Point(0, -1);
            this.BTN_PositionXAdd.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_PositionXAdd.Name = "BTN_PositionXAdd";
            this.BTN_PositionXAdd.Size = new System.Drawing.Size(24, 20);
            this.BTN_PositionXAdd.TabIndex = 26;
            this.BTN_PositionXAdd.Text = "+";
            this.BTN_PositionXAdd.UseVisualStyleBackColor = true;
            // 
            // CHK_Through
            // 
            this.CHK_Through.AutoSize = true;
            this.CHK_Through.Location = new System.Drawing.Point(372, 4);
            this.CHK_Through.Name = "CHK_Through";
            this.CHK_Through.Size = new System.Drawing.Size(14, 14);
            this.CHK_Through.TabIndex = 13;
            this.CHK_Through.UseVisualStyleBackColor = true;
            // 
            // CHK_Disposable
            // 
            this.CHK_Disposable.AutoSize = true;
            this.CHK_Disposable.Location = new System.Drawing.Point(393, 4);
            this.CHK_Disposable.Name = "CHK_Disposable";
            this.CHK_Disposable.Size = new System.Drawing.Size(13, 14);
            this.CHK_Disposable.TabIndex = 14;
            this.CHK_Disposable.UseVisualStyleBackColor = true;
            // 
            // CB_Direction
            // 
            this.CB_Direction.FormattingEnabled = true;
            this.CB_Direction.Items.AddRange(new object[] {
            "Left",
            "Right",
            "Up",
            "Down",
            "None",
            "Random"});
            this.CB_Direction.Location = new System.Drawing.Point(595, 4);
            this.CB_Direction.Name = "CB_Direction";
            this.CB_Direction.Size = new System.Drawing.Size(88, 21);
            this.CB_Direction.TabIndex = 19;
            // 
            // CHK_Flying
            // 
            this.CHK_Flying.AutoSize = true;
            this.CHK_Flying.Location = new System.Drawing.Point(413, 4);
            this.CHK_Flying.Name = "CHK_Flying";
            this.CHK_Flying.Size = new System.Drawing.Size(14, 14);
            this.CHK_Flying.TabIndex = 15;
            this.CHK_Flying.UseVisualStyleBackColor = true;
            // 
            // BTN_Graphics
            // 
            this.BTN_Graphics.Location = new System.Drawing.Point(747, 4);
            this.BTN_Graphics.Name = "BTN_Graphics";
            this.BTN_Graphics.Size = new System.Drawing.Size(73, 20);
            this.BTN_Graphics.TabIndex = 21;
            this.BTN_Graphics.Text = "Graphics";
            this.BTN_Graphics.UseVisualStyleBackColor = true;
            // 
            // BTN_Script
            // 
            this.BTN_Script.Location = new System.Drawing.Point(829, 4);
            this.BTN_Script.Name = "BTN_Script";
            this.BTN_Script.Size = new System.Drawing.Size(79, 20);
            this.BTN_Script.TabIndex = 22;
            this.BTN_Script.Text = "Script";
            this.BTN_Script.UseVisualStyleBackColor = true;
            // 
            // TB_Name
            // 
            this.TB_Name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TB_Name.Location = new System.Drawing.Point(21, 4);
            this.TB_Name.Name = "TB_Name";
            this.TB_Name.Size = new System.Drawing.Size(105, 20);
            this.TB_Name.TabIndex = 1;
            this.TB_Name.TextChanged += new System.EventHandler(this.TB_Name_TextChanged);
            // 
            // GeneralModifier
            // 
            this.GeneralModifier.Controls.Add(this.TabPanelEvent);
            this.GeneralModifier.Location = new System.Drawing.Point(12, 27);
            this.GeneralModifier.Name = "GeneralModifier";
            this.GeneralModifier.Size = new System.Drawing.Size(935, 48);
            this.GeneralModifier.TabIndex = 6;
            this.GeneralModifier.TabStop = false;
            this.GeneralModifier.Text = "GeneralModifier";
            // 
            // BTN_InvertSelection
            // 
            this.BTN_InvertSelection.Location = new System.Drawing.Point(950, 98);
            this.BTN_InvertSelection.Name = "BTN_InvertSelection";
            this.BTN_InvertSelection.Size = new System.Drawing.Size(104, 23);
            this.BTN_InvertSelection.TabIndex = 9;
            this.BTN_InvertSelection.Text = "Invert Selection";
            this.BTN_InvertSelection.UseVisualStyleBackColor = true;
            this.BTN_InvertSelection.Click += new System.EventHandler(this.BTN_InvertSelection_Click);
            // 
            // BTN_DeselectAll
            // 
            this.BTN_DeselectAll.Location = new System.Drawing.Point(950, 68);
            this.BTN_DeselectAll.Name = "BTN_DeselectAll";
            this.BTN_DeselectAll.Size = new System.Drawing.Size(104, 23);
            this.BTN_DeselectAll.TabIndex = 8;
            this.BTN_DeselectAll.Text = "Deselect All";
            this.BTN_DeselectAll.UseVisualStyleBackColor = true;
            this.BTN_DeselectAll.Click += new System.EventHandler(this.BTN_DeselectAll_Click);
            // 
            // BTN_SelectAll
            // 
            this.BTN_SelectAll.Location = new System.Drawing.Point(950, 38);
            this.BTN_SelectAll.Name = "BTN_SelectAll";
            this.BTN_SelectAll.Size = new System.Drawing.Size(104, 23);
            this.BTN_SelectAll.TabIndex = 7;
            this.BTN_SelectAll.Text = "Select All";
            this.BTN_SelectAll.UseVisualStyleBackColor = true;
            this.BTN_SelectAll.Click += new System.EventHandler(this.BTN_SelectAll_Click);
            // 
            // Menu
            // 
            this.MainMenu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.MainMenu.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editionToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "Menu";
            this.MainMenu.Size = new System.Drawing.Size(1056, 24);
            this.MainMenu.TabIndex = 10;
            this.MainMenu.Text = "Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.saveAndQuitToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAndQuitToolStripMenuItem
            // 
            this.saveAndQuitToolStripMenuItem.Name = "saveAndQuitToolStripMenuItem";
            this.saveAndQuitToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.saveAndQuitToolStripMenuItem.Text = "Save and Quit";
            this.saveAndQuitToolStripMenuItem.Click += new System.EventHandler(this.saveAndQuitToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // editionToolStripMenuItem
            // 
            this.editionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllToolStripMenuItem,
            this.deselectAllToolStripMenuItem,
            this.invertSelectionToolStripMenuItem});
            this.editionToolStripMenuItem.Name = "editionToolStripMenuItem";
            this.editionToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.editionToolStripMenuItem.Text = "Edition";
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.selectAllToolStripMenuItem.Text = "Select All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // deselectAllToolStripMenuItem
            // 
            this.deselectAllToolStripMenuItem.Name = "deselectAllToolStripMenuItem";
            this.deselectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.deselectAllToolStripMenuItem.Text = "Deselect All";
            this.deselectAllToolStripMenuItem.Click += new System.EventHandler(this.deselectAllToolStripMenuItem_Click);
            // 
            // invertSelectionToolStripMenuItem
            // 
            this.invertSelectionToolStripMenuItem.Name = "invertSelectionToolStripMenuItem";
            this.invertSelectionToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.invertSelectionToolStripMenuItem.Text = "Invert Selection";
            this.invertSelectionToolStripMenuItem.Click += new System.EventHandler(this.invertSelectionToolStripMenuItem_Click);
            // 
            // PeventListEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 529);
            this.Controls.Add(this.BTN_InvertSelection);
            this.Controls.Add(this.BTN_DeselectAll);
            this.Controls.Add(this.BTN_SelectAll);
            this.Controls.Add(this.GeneralModifier);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.BTN_SaveAndQuit);
            this.Controls.Add(this.BTN_Quit);
            this.Controls.Add(this.PeventListPanel);
            this.Controls.Add(this.MainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "PeventListEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PeventListEditor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PeventListEditor_FormClosing);
            this.TabPanelEvent.ResumeLayout(false);
            this.TabPanelEvent.PerformLayout();
            this.PN_Speed.ResumeLayout(false);
            this.PN_Sight.ResumeLayout(false);
            this.PN_PositionY.ResumeLayout(false);
            this.PN_PositionX.ResumeLayout(false);
            this.GeneralModifier.ResumeLayout(false);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel PeventListPanel;
        private System.Windows.Forms.Button BTN_Quit;
        private System.Windows.Forms.Button BTN_SaveAndQuit;
        private System.Windows.Forms.Button BTN_Save;
        public System.Windows.Forms.ComboBox CB_Class;
        public System.Windows.Forms.ComboBox CB_TriggerType;
        public System.Windows.Forms.ComboBox CB_MoveType;
        private System.Windows.Forms.TableLayoutPanel TabPanelEvent;
        public System.Windows.Forms.TextBox TB_Name;
        public System.Windows.Forms.CheckBox CHK_Through;
        public System.Windows.Forms.CheckBox CHK_Disposable;
        public System.Windows.Forms.ComboBox CB_Direction;
        public System.Windows.Forms.CheckBox CHK_Flying;
        public System.Windows.Forms.Button BTN_Graphics;
        public System.Windows.Forms.Button BTN_Script;
        private System.Windows.Forms.GroupBox GeneralModifier;
        private System.Windows.Forms.Button BTN_SpeedAdd;
        private System.Windows.Forms.Panel PN_Speed;
        private System.Windows.Forms.Button BTN_SpeedLower;
        private System.Windows.Forms.Panel PN_PositionX;
        private System.Windows.Forms.Button BTN_PositionXLower;
        private System.Windows.Forms.Button BTN_PositionXAdd;
        private System.Windows.Forms.Panel PN_PositionY;
        private System.Windows.Forms.Button BTN_PositionYLower;
        private System.Windows.Forms.Button BTN_PositionYAdd;
        private System.Windows.Forms.Panel PN_Sight;
        private System.Windows.Forms.Button BTN_SightLower;
        private System.Windows.Forms.Button BTN_SightAdd;
        private System.Windows.Forms.Button BTN_InvertSelection;
        private System.Windows.Forms.Button BTN_DeselectAll;
        private System.Windows.Forms.Button BTN_SelectAll;
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAndQuitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deselectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertSelectionToolStripMenuItem;
    }
}