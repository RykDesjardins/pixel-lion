﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionWin
{
    public partial class SelectTeleportationForm : Form
    {
        Map map;
        Int32 TileSize = 32;
        System.Drawing.Point premier;
        int layer;
        Map firstMap;
        public bool ajouté = false;
        SpriteBatch batch;
        XNAUtils utils;

        public SelectTeleportationForm(Map map, System.Drawing.Point premier, int layer, Map firstMap, SpriteBatch Mapbatch)
        {
            InitializeComponent();
            utils = new XNAUtils();
            this.map = map;
            this.premier = premier;
            this.layer = layer;
            this.firstMap = firstMap;
            this.batch = Mapbatch;
            
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Size = Screen.PrimaryScreen.Bounds.Size;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new System.Drawing.Point(0, 0);

            PN_Map.Size = Screen.PrimaryScreen.Bounds.Size;
            PresentationParameters pp = new PresentationParameters()
            {
                BackBufferHeight = PN_Map.Height,
                BackBufferWidth = PN_Map.Width,
                DeviceWindowHandle = PN_Map.Handle,
                IsFullScreen = false
            };
            XNADevices.graphicsdevice.Reset(pp);
        }

        private void PN_Map_Paint(object sender, PaintEventArgs e)
        {
            batch.GraphicsDevice.Clear(Color.White);
            map.Draw(batch, utils);
            XNADevices.graphicsdevice.Present();
        }

        private void PN_Map_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.X / TileSize + 1 > map.MapSize.Width || e.Y / TileSize + 1 > map.MapSize.Height)
                return;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                System.Drawing.Point click = new System.Drawing.Point(e.X / TileSize, e.Y / TileSize);
                LayerSelector layerdlg = new LayerSelector();
                layerdlg.ShowDialog();
                if (layerdlg.confirmed)
                {
                    if (layerdlg.layer > -1 && layerdlg.layer < 3)
                    {
                        ConfirmationTeleportation confirmationdlg = new ConfirmationTeleportation(premier, layer, click, layerdlg.layer, firstMap, map);
                        confirmationdlg.ShowDialog();
                        if (confirmationdlg.pEventAjouté)
                        {
                            ajouté = true;
                            this.Close();
                        }
                    }
                }
            }
        }

        private void SelectTeleportationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            utils.Dispose();
        }
    }
}
