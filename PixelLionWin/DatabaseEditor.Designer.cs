﻿namespace PixelLionWin
{
    partial class DatabaseEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DatabaseEditor));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.GROUP_Item = new System.Windows.Forms.GroupBox();
            this.BTN_SaveItem = new System.Windows.Forms.Button();
            this.LBL_ITEM_Name = new System.Windows.Forms.LinkLabel();
            this.PN_Icon = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.DDL_ITEM_Target = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.CB_Unique = new System.Windows.Forms.CheckBox();
            this.CB_Consumable = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.NUM_Weight = new System.Windows.Forms.NumericUpDown();
            this.NUM_Price = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.CHK_LIST_StatusEffectItem = new System.Windows.Forms.CheckedListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.NUM_pMP = new System.Windows.Forms.NumericUpDown();
            this.NUM_pHP = new System.Windows.Forms.NumericUpDown();
            this.NUM_MP = new System.Windows.Forms.NumericUpDown();
            this.NUM_HP = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TB_Desc = new System.Windows.Forms.TextBox();
            this.BTN_DeleteItemObject = new System.Windows.Forms.Button();
            this.BTN_AddItemObject = new System.Windows.Forms.Button();
            this.LIST_ItemObjects = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.GROUP_Monsters = new System.Windows.Forms.GroupBox();
            this.BTN_SaveMonster = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.LBL_MonsterName = new System.Windows.Forms.LinkLabel();
            this.BTN_RemoveMonster = new System.Windows.Forms.Button();
            this.BTN_AddMonster = new System.Windows.Forms.Button();
            this.LIST_Monsters = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.GROUP_SpecialAttacks = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.DDL_ATK_AnimationSelf = new System.Windows.Forms.ComboBox();
            this.DDL_ATK_AnimationTarget = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.LB_ATK_Cible = new System.Windows.Forms.Label();
            this.DDL_Cible = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.DDL_ATK_Type = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.LB_ATK_Type = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.NUM_ATK_Radius = new System.Windows.Forms.NumericUpDown();
            this.NUM_ATK_Ratio = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.BTN_SaveAttack = new System.Windows.Forms.Button();
            this.LBL_ATK_Name = new System.Windows.Forms.LinkLabel();
            this.PN_Attaque = new System.Windows.Forms.Panel();
            this.BTN_DeleteAttack = new System.Windows.Forms.Button();
            this.BTN_AddAttack = new System.Windows.Forms.Button();
            this.LIST_SpecialAttacks = new System.Windows.Forms.ListBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.BTN_DeleteElement = new System.Windows.Forms.Button();
            this.BTN_AddElement = new System.Windows.Forms.Button();
            this.LIST_Elements = new System.Windows.Forms.ListBox();
            this.GROUP_Elements = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.TB_Description = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.PN_IconElement = new System.Windows.Forms.Panel();
            this.BTN_SaveElement = new System.Windows.Forms.Button();
            this.linkLBL_ELEM_Name = new System.Windows.Forms.LinkLabel();
            this.ES_MonstreFaiblesse = new PixelLionWin.ElementalSelector();
            this.ES_MonstrePrimaire = new PixelLionWin.ElementalSelector();
            this.MON_Statspicker = new PixelLionWin.StatsPicker();
            this.ES_AttaqueSpeciale = new PixelLionWin.ElementalSelector();
            this.ES_Faiblesses = new PixelLionWin.ElementalSelector();
            this.ES_Resistances = new PixelLionWin.ElementalSelector();
            this.ES_Immunites = new PixelLionWin.ElementalSelector();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.GROUP_Item.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Weight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Price)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_pMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_pHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_HP)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.GROUP_Monsters.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.GROUP_SpecialAttacks.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_ATK_Radius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_ATK_Ratio)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.GROUP_Elements.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1079, 560);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.GROUP_Item);
            this.tabPage1.Controls.Add(this.BTN_DeleteItemObject);
            this.tabPage1.Controls.Add(this.BTN_AddItemObject);
            this.tabPage1.Controls.Add(this.LIST_ItemObjects);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1071, 534);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Objets";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // GROUP_Item
            // 
            this.GROUP_Item.Controls.Add(this.BTN_SaveItem);
            this.GROUP_Item.Controls.Add(this.LBL_ITEM_Name);
            this.GROUP_Item.Controls.Add(this.PN_Icon);
            this.GROUP_Item.Controls.Add(this.groupBox3);
            this.GROUP_Item.Controls.Add(this.groupBox1);
            this.GROUP_Item.Controls.Add(this.groupBox2);
            this.GROUP_Item.Location = new System.Drawing.Point(238, 0);
            this.GROUP_Item.Name = "GROUP_Item";
            this.GROUP_Item.Size = new System.Drawing.Size(827, 531);
            this.GROUP_Item.TabIndex = 8;
            this.GROUP_Item.TabStop = false;
            this.GROUP_Item.Visible = false;
            // 
            // BTN_SaveItem
            // 
            this.BTN_SaveItem.BackgroundImage = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.BTN_SaveItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_SaveItem.Location = new System.Drawing.Point(754, 464);
            this.BTN_SaveItem.Name = "BTN_SaveItem";
            this.BTN_SaveItem.Size = new System.Drawing.Size(60, 60);
            this.BTN_SaveItem.TabIndex = 8;
            this.BTN_SaveItem.UseVisualStyleBackColor = true;
            this.BTN_SaveItem.Click += new System.EventHandler(this.BTN_SaveItem_Click);
            // 
            // LBL_ITEM_Name
            // 
            this.LBL_ITEM_Name.AutoSize = true;
            this.LBL_ITEM_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_ITEM_Name.Location = new System.Drawing.Point(6, 16);
            this.LBL_ITEM_Name.Name = "LBL_ITEM_Name";
            this.LBL_ITEM_Name.Size = new System.Drawing.Size(165, 31);
            this.LBL_ITEM_Name.TabIndex = 4;
            this.LBL_ITEM_Name.TabStop = true;
            this.LBL_ITEM_Name.Text = "Nouvel objet";
            this.LBL_ITEM_Name.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LBL_Name_LinkClicked);
            // 
            // PN_Icon
            // 
            this.PN_Icon.BackColor = System.Drawing.Color.AliceBlue;
            this.PN_Icon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PN_Icon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Icon.Location = new System.Drawing.Point(740, 19);
            this.PN_Icon.Name = "PN_Icon";
            this.PN_Icon.Size = new System.Drawing.Size(74, 72);
            this.PN_Icon.TabIndex = 3;
            this.PN_Icon.Click += new System.EventHandler(this.PN_Icon_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox7);
            this.groupBox3.Controls.Add(this.groupBox6);
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Location = new System.Drawing.Point(12, 243);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(722, 245);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informations";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.DDL_ITEM_Target);
            this.groupBox7.Location = new System.Drawing.Point(282, 146);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(184, 75);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Cible";
            // 
            // DDL_ITEM_Target
            // 
            this.DDL_ITEM_Target.FormattingEnabled = true;
            this.DDL_ITEM_Target.Location = new System.Drawing.Point(23, 30);
            this.DDL_ITEM_Target.Name = "DDL_ITEM_Target";
            this.DDL_ITEM_Target.Size = new System.Drawing.Size(141, 21);
            this.DDL_ITEM_Target.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.CB_Unique);
            this.groupBox6.Controls.Add(this.CB_Consumable);
            this.groupBox6.Location = new System.Drawing.Point(482, 31);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(216, 109);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Caractéristiques";
            // 
            // CB_Unique
            // 
            this.CB_Unique.AutoSize = true;
            this.CB_Unique.Location = new System.Drawing.Point(21, 65);
            this.CB_Unique.Name = "CB_Unique";
            this.CB_Unique.Size = new System.Drawing.Size(60, 17);
            this.CB_Unique.TabIndex = 0;
            this.CB_Unique.Text = "Unique";
            this.CB_Unique.UseVisualStyleBackColor = true;
            // 
            // CB_Consumable
            // 
            this.CB_Consumable.AutoSize = true;
            this.CB_Consumable.Location = new System.Drawing.Point(21, 33);
            this.CB_Consumable.Name = "CB_Consumable";
            this.CB_Consumable.Size = new System.Drawing.Size(74, 17);
            this.CB_Consumable.TabIndex = 0;
            this.CB_Consumable.Text = "Éphémère";
            this.CB_Consumable.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.NUM_Weight);
            this.groupBox5.Controls.Add(this.NUM_Price);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Location = new System.Drawing.Point(282, 31);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(184, 109);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Valeurs";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Poids";
            // 
            // NUM_Weight
            // 
            this.NUM_Weight.Location = new System.Drawing.Point(62, 63);
            this.NUM_Weight.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.NUM_Weight.Name = "NUM_Weight";
            this.NUM_Weight.Size = new System.Drawing.Size(85, 20);
            this.NUM_Weight.TabIndex = 2;
            // 
            // NUM_Price
            // 
            this.NUM_Price.Location = new System.Drawing.Point(62, 30);
            this.NUM_Price.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.NUM_Price.Name = "NUM_Price";
            this.NUM_Price.Size = new System.Drawing.Size(85, 20);
            this.NUM_Price.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Prix";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.CHK_LIST_StatusEffectItem);
            this.groupBox4.Location = new System.Drawing.Point(26, 31);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(234, 190);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Soin des status";
            // 
            // CHK_LIST_StatusEffectItem
            // 
            this.CHK_LIST_StatusEffectItem.FormattingEnabled = true;
            this.CHK_LIST_StatusEffectItem.Location = new System.Drawing.Point(13, 19);
            this.CHK_LIST_StatusEffectItem.Name = "CHK_LIST_StatusEffectItem";
            this.CHK_LIST_StatusEffectItem.Size = new System.Drawing.Size(204, 154);
            this.CHK_LIST_StatusEffectItem.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.NUM_pMP);
            this.groupBox1.Controls.Add(this.NUM_pHP);
            this.groupBox1.Controls.Add(this.NUM_MP);
            this.groupBox1.Controls.Add(this.NUM_HP);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(226, 176);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Propriétés bénéfiques";
            // 
            // NUM_pMP
            // 
            this.NUM_pMP.Location = new System.Drawing.Point(109, 129);
            this.NUM_pMP.Name = "NUM_pMP";
            this.NUM_pMP.Size = new System.Drawing.Size(61, 20);
            this.NUM_pMP.TabIndex = 1;
            // 
            // NUM_pHP
            // 
            this.NUM_pHP.Location = new System.Drawing.Point(109, 96);
            this.NUM_pHP.Name = "NUM_pHP";
            this.NUM_pHP.Size = new System.Drawing.Size(61, 20);
            this.NUM_pHP.TabIndex = 1;
            // 
            // NUM_MP
            // 
            this.NUM_MP.Location = new System.Drawing.Point(109, 64);
            this.NUM_MP.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.NUM_MP.Name = "NUM_MP";
            this.NUM_MP.Size = new System.Drawing.Size(87, 20);
            this.NUM_MP.TabIndex = 1;
            // 
            // NUM_HP
            // 
            this.NUM_HP.Location = new System.Drawing.Point(109, 31);
            this.NUM_HP.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.NUM_HP.Name = "NUM_HP";
            this.NUM_HP.Size = new System.Drawing.Size(87, 20);
            this.NUM_HP.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(181, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "%";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Pourcent MP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(181, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "%";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Pourcent HP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Soin MP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Soin HP";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TB_Desc);
            this.groupBox2.Location = new System.Drawing.Point(244, 61);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(490, 176);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Description";
            // 
            // TB_Desc
            // 
            this.TB_Desc.Location = new System.Drawing.Point(21, 26);
            this.TB_Desc.Multiline = true;
            this.TB_Desc.Name = "TB_Desc";
            this.TB_Desc.Size = new System.Drawing.Size(445, 123);
            this.TB_Desc.TabIndex = 0;
            // 
            // BTN_DeleteItemObject
            // 
            this.BTN_DeleteItemObject.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.BTN_DeleteItemObject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_DeleteItemObject.Location = new System.Drawing.Point(52, 484);
            this.BTN_DeleteItemObject.Name = "BTN_DeleteItemObject";
            this.BTN_DeleteItemObject.Size = new System.Drawing.Size(40, 40);
            this.BTN_DeleteItemObject.TabIndex = 1;
            this.BTN_DeleteItemObject.UseVisualStyleBackColor = true;
            this.BTN_DeleteItemObject.Click += new System.EventHandler(this.BTN_DeleteItemObject_Click);
            // 
            // BTN_AddItemObject
            // 
            this.BTN_AddItemObject.BackgroundImage = global::PixelLionWin.Properties.Resources.AddIcon;
            this.BTN_AddItemObject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_AddItemObject.Location = new System.Drawing.Point(6, 484);
            this.BTN_AddItemObject.Name = "BTN_AddItemObject";
            this.BTN_AddItemObject.Size = new System.Drawing.Size(40, 40);
            this.BTN_AddItemObject.TabIndex = 1;
            this.BTN_AddItemObject.UseVisualStyleBackColor = true;
            this.BTN_AddItemObject.Click += new System.EventHandler(this.BTN_AddItemObject_Click);
            // 
            // LIST_ItemObjects
            // 
            this.LIST_ItemObjects.FormattingEnabled = true;
            this.LIST_ItemObjects.Location = new System.Drawing.Point(6, 6);
            this.LIST_ItemObjects.Name = "LIST_ItemObjects";
            this.LIST_ItemObjects.Size = new System.Drawing.Size(226, 472);
            this.LIST_ItemObjects.TabIndex = 0;
            this.LIST_ItemObjects.SelectedIndexChanged += new System.EventHandler(this.LIST_ItemObjects_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.GROUP_Monsters);
            this.tabPage2.Controls.Add(this.BTN_RemoveMonster);
            this.tabPage2.Controls.Add(this.BTN_AddMonster);
            this.tabPage2.Controls.Add(this.LIST_Monsters);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1071, 534);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Monstres";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // GROUP_Monsters
            // 
            this.GROUP_Monsters.Controls.Add(this.BTN_SaveMonster);
            this.GROUP_Monsters.Controls.Add(this.panel2);
            this.GROUP_Monsters.Controls.Add(this.LBL_MonsterName);
            this.GROUP_Monsters.Location = new System.Drawing.Point(238, 0);
            this.GROUP_Monsters.Name = "GROUP_Monsters";
            this.GROUP_Monsters.Size = new System.Drawing.Size(827, 531);
            this.GROUP_Monsters.TabIndex = 8;
            this.GROUP_Monsters.TabStop = false;
            this.GROUP_Monsters.Visible = false;
            // 
            // BTN_SaveMonster
            // 
            this.BTN_SaveMonster.BackgroundImage = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.BTN_SaveMonster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_SaveMonster.Location = new System.Drawing.Point(761, 464);
            this.BTN_SaveMonster.Name = "BTN_SaveMonster";
            this.BTN_SaveMonster.Size = new System.Drawing.Size(60, 60);
            this.BTN_SaveMonster.TabIndex = 9;
            this.BTN_SaveMonster.UseVisualStyleBackColor = true;
            this.BTN_SaveMonster.Click += new System.EventHandler(this.BTN_SaveMonster_Click);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.groupBox11);
            this.panel2.Location = new System.Drawing.Point(12, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(809, 408);
            this.panel2.TabIndex = 8;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.ES_MonstreFaiblesse);
            this.groupBox11.Controls.Add(this.ES_MonstrePrimaire);
            this.groupBox11.Controls.Add(this.MON_Statspicker);
            this.groupBox11.Location = new System.Drawing.Point(3, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(473, 337);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Status";
            // 
            // LBL_MonsterName
            // 
            this.LBL_MonsterName.AutoSize = true;
            this.LBL_MonsterName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_MonsterName.Location = new System.Drawing.Point(6, 16);
            this.LBL_MonsterName.Name = "LBL_MonsterName";
            this.LBL_MonsterName.Size = new System.Drawing.Size(112, 31);
            this.LBL_MonsterName.TabIndex = 7;
            this.LBL_MonsterName.TabStop = true;
            this.LBL_MonsterName.Text = "Monstre";
            this.LBL_MonsterName.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LBL_MonsterName_LinkClicked);
            // 
            // BTN_RemoveMonster
            // 
            this.BTN_RemoveMonster.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.BTN_RemoveMonster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_RemoveMonster.Location = new System.Drawing.Point(52, 484);
            this.BTN_RemoveMonster.Name = "BTN_RemoveMonster";
            this.BTN_RemoveMonster.Size = new System.Drawing.Size(40, 40);
            this.BTN_RemoveMonster.TabIndex = 6;
            this.BTN_RemoveMonster.UseVisualStyleBackColor = true;
            // 
            // BTN_AddMonster
            // 
            this.BTN_AddMonster.BackgroundImage = global::PixelLionWin.Properties.Resources.AddIcon;
            this.BTN_AddMonster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_AddMonster.Location = new System.Drawing.Point(6, 484);
            this.BTN_AddMonster.Name = "BTN_AddMonster";
            this.BTN_AddMonster.Size = new System.Drawing.Size(40, 40);
            this.BTN_AddMonster.TabIndex = 7;
            this.BTN_AddMonster.UseVisualStyleBackColor = true;
            this.BTN_AddMonster.Click += new System.EventHandler(this.BTN_AddMonster_Click);
            // 
            // LIST_Monsters
            // 
            this.LIST_Monsters.FormattingEnabled = true;
            this.LIST_Monsters.Location = new System.Drawing.Point(6, 6);
            this.LIST_Monsters.Name = "LIST_Monsters";
            this.LIST_Monsters.Size = new System.Drawing.Size(226, 472);
            this.LIST_Monsters.TabIndex = 5;
            this.LIST_Monsters.SelectedIndexChanged += new System.EventHandler(this.LIST_Monsters_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.GROUP_SpecialAttacks);
            this.tabPage3.Controls.Add(this.BTN_DeleteAttack);
            this.tabPage3.Controls.Add(this.BTN_AddAttack);
            this.tabPage3.Controls.Add(this.LIST_SpecialAttacks);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1071, 534);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Attaques spéciales";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // GROUP_SpecialAttacks
            // 
            this.GROUP_SpecialAttacks.Controls.Add(this.groupBox10);
            this.GROUP_SpecialAttacks.Controls.Add(this.groupBox9);
            this.GROUP_SpecialAttacks.Controls.Add(this.groupBox8);
            this.GROUP_SpecialAttacks.Controls.Add(this.BTN_SaveAttack);
            this.GROUP_SpecialAttacks.Controls.Add(this.LBL_ATK_Name);
            this.GROUP_SpecialAttacks.Controls.Add(this.PN_Attaque);
            this.GROUP_SpecialAttacks.Location = new System.Drawing.Point(238, 0);
            this.GROUP_SpecialAttacks.Name = "GROUP_SpecialAttacks";
            this.GROUP_SpecialAttacks.Size = new System.Drawing.Size(830, 531);
            this.GROUP_SpecialAttacks.TabIndex = 5;
            this.GROUP_SpecialAttacks.TabStop = false;
            this.GROUP_SpecialAttacks.Visible = false;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.DDL_ATK_AnimationSelf);
            this.groupBox10.Controls.Add(this.DDL_ATK_AnimationTarget);
            this.groupBox10.Controls.Add(this.label15);
            this.groupBox10.Controls.Add(this.label14);
            this.groupBox10.Location = new System.Drawing.Point(464, 54);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(254, 164);
            this.groupBox10.TabIndex = 11;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Animation";
            // 
            // DDL_ATK_AnimationSelf
            // 
            this.DDL_ATK_AnimationSelf.FormattingEnabled = true;
            this.DDL_ATK_AnimationSelf.Location = new System.Drawing.Point(27, 103);
            this.DDL_ATK_AnimationSelf.Name = "DDL_ATK_AnimationSelf";
            this.DDL_ATK_AnimationSelf.Size = new System.Drawing.Size(185, 21);
            this.DDL_ATK_AnimationSelf.TabIndex = 0;
            // 
            // DDL_ATK_AnimationTarget
            // 
            this.DDL_ATK_AnimationTarget.FormattingEnabled = true;
            this.DDL_ATK_AnimationTarget.Location = new System.Drawing.Point(27, 32);
            this.DDL_ATK_AnimationTarget.Name = "DDL_ATK_AnimationTarget";
            this.DDL_ATK_AnimationTarget.Size = new System.Drawing.Size(185, 21);
            this.DDL_ATK_AnimationTarget.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label15.Location = new System.Drawing.Point(24, 130);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(210, 20);
            this.label15.TabIndex = 3;
            this.label15.Text = "L\'animation à être affichée sur l\'utilisateur";
            // 
            // label14
            // 
            this.label14.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label14.Location = new System.Drawing.Point(24, 59);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(188, 20);
            this.label14.TabIndex = 3;
            this.label14.Text = "L\'animation à être affichée sur la cible";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.ES_AttaqueSpeciale);
            this.groupBox9.Location = new System.Drawing.Point(292, 54);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(166, 339);
            this.groupBox9.TabIndex = 3;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Élément";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.LB_ATK_Cible);
            this.groupBox8.Controls.Add(this.DDL_Cible);
            this.groupBox8.Controls.Add(this.label20);
            this.groupBox8.Controls.Add(this.DDL_ATK_Type);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.LB_ATK_Type);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Controls.Add(this.label10);
            this.groupBox8.Controls.Add(this.NUM_ATK_Radius);
            this.groupBox8.Controls.Add(this.NUM_ATK_Ratio);
            this.groupBox8.Controls.Add(this.label9);
            this.groupBox8.Location = new System.Drawing.Point(12, 54);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(274, 470);
            this.groupBox8.TabIndex = 10;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Paramètres";
            // 
            // LB_ATK_Cible
            // 
            this.LB_ATK_Cible.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LB_ATK_Cible.Location = new System.Drawing.Point(15, 407);
            this.LB_ATK_Cible.Name = "LB_ATK_Cible";
            this.LB_ATK_Cible.Size = new System.Drawing.Size(248, 63);
            this.LB_ATK_Cible.TabIndex = 8;
            // 
            // DDL_Cible
            // 
            this.DDL_Cible.FormattingEnabled = true;
            this.DDL_Cible.Location = new System.Drawing.Point(70, 383);
            this.DDL_Cible.Name = "DDL_Cible";
            this.DDL_Cible.Size = new System.Drawing.Size(178, 21);
            this.DDL_Cible.TabIndex = 7;
            this.DDL_Cible.SelectedIndexChanged += new System.EventHandler(this.DDL_Cible_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(27, 386);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 13);
            this.label20.TabIndex = 6;
            this.label20.Text = "Cible";
            // 
            // DDL_ATK_Type
            // 
            this.DDL_ATK_Type.FormattingEnabled = true;
            this.DDL_ATK_Type.Location = new System.Drawing.Point(70, 284);
            this.DDL_ATK_Type.Name = "DDL_ATK_Type";
            this.DDL_ATK_Type.Size = new System.Drawing.Size(178, 21);
            this.DDL_ATK_Type.TabIndex = 5;
            this.DDL_ATK_Type.SelectedIndexChanged += new System.EventHandler(this.DDL_ATK_Target_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(27, 287);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Type";
            // 
            // label12
            // 
            this.label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label12.Location = new System.Drawing.Point(15, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(248, 76);
            this.label12.TabIndex = 3;
            this.label12.Text = "Le radius représente la distance à laquelle l\'attaque est effective. Plus le radi" +
    "us est grand, plus l\'attaque peut être utilisée de loin. \r\n\r\n1 radius = environ " +
    "32 pixels\r\n";
            // 
            // LB_ATK_Type
            // 
            this.LB_ATK_Type.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LB_ATK_Type.Location = new System.Drawing.Point(15, 311);
            this.LB_ATK_Type.Name = "LB_ATK_Type";
            this.LB_ATK_Type.Size = new System.Drawing.Size(248, 63);
            this.LB_ATK_Type.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label11.Location = new System.Drawing.Point(15, 194);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(248, 63);
            this.label11.TabIndex = 3;
            this.label11.Text = "Le ratio est un multiplicatif qui représente la puissance de l\'attaque. La formul" +
    "e est la suivante :\r\n\r\nIntelligence  x  Ratio  =  Dommage\r\n";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 168);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Ratio";
            // 
            // NUM_ATK_Radius
            // 
            this.NUM_ATK_Radius.Location = new System.Drawing.Point(70, 35);
            this.NUM_ATK_Radius.Name = "NUM_ATK_Radius";
            this.NUM_ATK_Radius.Size = new System.Drawing.Size(178, 20);
            this.NUM_ATK_Radius.TabIndex = 1;
            // 
            // NUM_ATK_Ratio
            // 
            this.NUM_ATK_Ratio.DecimalPlaces = 2;
            this.NUM_ATK_Ratio.Location = new System.Drawing.Point(70, 166);
            this.NUM_ATK_Ratio.Name = "NUM_ATK_Ratio";
            this.NUM_ATK_Ratio.Size = new System.Drawing.Size(178, 20);
            this.NUM_ATK_Ratio.TabIndex = 1;
            this.NUM_ATK_Ratio.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Radius";
            // 
            // BTN_SaveAttack
            // 
            this.BTN_SaveAttack.BackgroundImage = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.BTN_SaveAttack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_SaveAttack.Location = new System.Drawing.Point(754, 464);
            this.BTN_SaveAttack.Name = "BTN_SaveAttack";
            this.BTN_SaveAttack.Size = new System.Drawing.Size(60, 60);
            this.BTN_SaveAttack.TabIndex = 9;
            this.BTN_SaveAttack.UseVisualStyleBackColor = true;
            this.BTN_SaveAttack.Click += new System.EventHandler(this.BTN_SaveAttack_Click);
            // 
            // LBL_ATK_Name
            // 
            this.LBL_ATK_Name.AutoSize = true;
            this.LBL_ATK_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_ATK_Name.Location = new System.Drawing.Point(6, 16);
            this.LBL_ATK_Name.Name = "LBL_ATK_Name";
            this.LBL_ATK_Name.Size = new System.Drawing.Size(218, 31);
            this.LBL_ATK_Name.TabIndex = 6;
            this.LBL_ATK_Name.TabStop = true;
            this.LBL_ATK_Name.Text = "Nouvelle attaque";
            this.LBL_ATK_Name.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LBL_ATK_Name_LinkClicked);
            // 
            // PN_Attaque
            // 
            this.PN_Attaque.BackColor = System.Drawing.Color.AliceBlue;
            this.PN_Attaque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PN_Attaque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Attaque.Location = new System.Drawing.Point(740, 19);
            this.PN_Attaque.Name = "PN_Attaque";
            this.PN_Attaque.Size = new System.Drawing.Size(74, 72);
            this.PN_Attaque.TabIndex = 5;
            this.PN_Attaque.Click += new System.EventHandler(this.PNAttaque_Click);
            // 
            // BTN_DeleteAttack
            // 
            this.BTN_DeleteAttack.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.BTN_DeleteAttack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_DeleteAttack.Location = new System.Drawing.Point(52, 484);
            this.BTN_DeleteAttack.Name = "BTN_DeleteAttack";
            this.BTN_DeleteAttack.Size = new System.Drawing.Size(40, 40);
            this.BTN_DeleteAttack.TabIndex = 3;
            this.BTN_DeleteAttack.UseVisualStyleBackColor = true;
            // 
            // BTN_AddAttack
            // 
            this.BTN_AddAttack.BackgroundImage = global::PixelLionWin.Properties.Resources.AddIcon;
            this.BTN_AddAttack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_AddAttack.Location = new System.Drawing.Point(6, 484);
            this.BTN_AddAttack.Name = "BTN_AddAttack";
            this.BTN_AddAttack.Size = new System.Drawing.Size(40, 40);
            this.BTN_AddAttack.TabIndex = 4;
            this.BTN_AddAttack.UseVisualStyleBackColor = true;
            this.BTN_AddAttack.Click += new System.EventHandler(this.BTN_AddAttack_Click);
            // 
            // LIST_SpecialAttacks
            // 
            this.LIST_SpecialAttacks.FormattingEnabled = true;
            this.LIST_SpecialAttacks.Location = new System.Drawing.Point(6, 6);
            this.LIST_SpecialAttacks.Name = "LIST_SpecialAttacks";
            this.LIST_SpecialAttacks.Size = new System.Drawing.Size(226, 472);
            this.LIST_SpecialAttacks.TabIndex = 2;
            this.LIST_SpecialAttacks.SelectedIndexChanged += new System.EventHandler(this.LIST_SpecialAttacks_SelectedIndexChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.BTN_DeleteElement);
            this.tabPage4.Controls.Add(this.BTN_AddElement);
            this.tabPage4.Controls.Add(this.LIST_Elements);
            this.tabPage4.Controls.Add(this.GROUP_Elements);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1071, 534);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Éléments";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // BTN_DeleteElement
            // 
            this.BTN_DeleteElement.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.BTN_DeleteElement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_DeleteElement.Location = new System.Drawing.Point(52, 484);
            this.BTN_DeleteElement.Name = "BTN_DeleteElement";
            this.BTN_DeleteElement.Size = new System.Drawing.Size(40, 40);
            this.BTN_DeleteElement.TabIndex = 7;
            this.BTN_DeleteElement.UseVisualStyleBackColor = true;
            this.BTN_DeleteElement.Click += new System.EventHandler(this.BTN_DeleteElement_Click);
            // 
            // BTN_AddElement
            // 
            this.BTN_AddElement.BackgroundImage = global::PixelLionWin.Properties.Resources.AddIcon;
            this.BTN_AddElement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_AddElement.Location = new System.Drawing.Point(6, 484);
            this.BTN_AddElement.Name = "BTN_AddElement";
            this.BTN_AddElement.Size = new System.Drawing.Size(40, 40);
            this.BTN_AddElement.TabIndex = 8;
            this.BTN_AddElement.UseVisualStyleBackColor = true;
            this.BTN_AddElement.Click += new System.EventHandler(this.BTN_AddElement_Click);
            // 
            // LIST_Elements
            // 
            this.LIST_Elements.FormattingEnabled = true;
            this.LIST_Elements.Location = new System.Drawing.Point(6, 6);
            this.LIST_Elements.Name = "LIST_Elements";
            this.LIST_Elements.Size = new System.Drawing.Size(226, 472);
            this.LIST_Elements.TabIndex = 6;
            this.LIST_Elements.SelectedIndexChanged += new System.EventHandler(this.LIST_Elements_SelectedIndexChanged);
            // 
            // GROUP_Elements
            // 
            this.GROUP_Elements.Controls.Add(this.groupBox16);
            this.GROUP_Elements.Controls.Add(this.groupBox15);
            this.GROUP_Elements.Controls.Add(this.groupBox14);
            this.GROUP_Elements.Controls.Add(this.groupBox13);
            this.GROUP_Elements.Controls.Add(this.BTN_SaveElement);
            this.GROUP_Elements.Controls.Add(this.linkLBL_ELEM_Name);
            this.GROUP_Elements.Location = new System.Drawing.Point(238, 0);
            this.GROUP_Elements.Name = "GROUP_Elements";
            this.GROUP_Elements.Size = new System.Drawing.Size(830, 531);
            this.GROUP_Elements.TabIndex = 9;
            this.GROUP_Elements.TabStop = false;
            this.GROUP_Elements.Visible = false;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.ES_Faiblesses);
            this.groupBox16.Location = new System.Drawing.Point(636, 54);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(166, 339);
            this.groupBox16.TabIndex = 13;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Faiblesses";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.ES_Resistances);
            this.groupBox15.Location = new System.Drawing.Point(464, 54);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(166, 339);
            this.groupBox15.TabIndex = 13;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Résistances";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.ES_Immunites);
            this.groupBox14.Location = new System.Drawing.Point(292, 54);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(166, 339);
            this.groupBox14.TabIndex = 12;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Immunités";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label19);
            this.groupBox13.Controls.Add(this.label16);
            this.groupBox13.Controls.Add(this.TB_Description);
            this.groupBox13.Controls.Add(this.label17);
            this.groupBox13.Controls.Add(this.label18);
            this.groupBox13.Controls.Add(this.label21);
            this.groupBox13.Controls.Add(this.PN_IconElement);
            this.groupBox13.Location = new System.Drawing.Point(12, 54);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(274, 461);
            this.groupBox13.TabIndex = 11;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Spécifications";
            // 
            // label19
            // 
            this.label19.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label19.Location = new System.Drawing.Point(15, 277);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(248, 34);
            this.label19.TabIndex = 8;
            this.label19.Text = "L\'aide-mémoire idéal pour vos idées survenues à la création de votre nouvel éléme" +
    "nt!";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(24, 254);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 13);
            this.label16.TabIndex = 7;
            this.label16.Text = "Description de l\'élément";
            // 
            // TB_Description
            // 
            this.TB_Description.Location = new System.Drawing.Point(18, 321);
            this.TB_Description.Multiline = true;
            this.TB_Description.Name = "TB_Description";
            this.TB_Description.Size = new System.Drawing.Size(238, 123);
            this.TB_Description.TabIndex = 6;
            // 
            // label17
            // 
            this.label17.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label17.Location = new System.Drawing.Point(15, 60);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(248, 76);
            this.label17.TabIndex = 3;
            this.label17.Text = "L\'icône que vous choisirez ou créerez est utilisé a de multiples endroits (lors d" +
    "e la création de monstres ou d\'attaques), alors faites en sorte qu\'elle soit rep" +
    "résentative.";
            // 
            // label18
            // 
            this.label18.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label18.Location = new System.Drawing.Point(15, 311);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(248, 63);
            this.label18.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(24, 37);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(93, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Icône de l\'élément";
            // 
            // PN_IconElement
            // 
            this.PN_IconElement.BackColor = System.Drawing.Color.AliceBlue;
            this.PN_IconElement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PN_IconElement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_IconElement.Location = new System.Drawing.Point(18, 139);
            this.PN_IconElement.Name = "PN_IconElement";
            this.PN_IconElement.Size = new System.Drawing.Size(74, 72);
            this.PN_IconElement.TabIndex = 5;
            this.PN_IconElement.Click += new System.EventHandler(this.PN_IconElement_Click);
            // 
            // BTN_SaveElement
            // 
            this.BTN_SaveElement.BackgroundImage = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.BTN_SaveElement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_SaveElement.Location = new System.Drawing.Point(754, 464);
            this.BTN_SaveElement.Name = "BTN_SaveElement";
            this.BTN_SaveElement.Size = new System.Drawing.Size(60, 60);
            this.BTN_SaveElement.TabIndex = 9;
            this.BTN_SaveElement.UseVisualStyleBackColor = true;
            this.BTN_SaveElement.Click += new System.EventHandler(this.BTN_SaveElement_Click);
            // 
            // linkLBL_ELEM_Name
            // 
            this.linkLBL_ELEM_Name.AutoSize = true;
            this.linkLBL_ELEM_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLBL_ELEM_Name.Location = new System.Drawing.Point(6, 16);
            this.linkLBL_ELEM_Name.Name = "linkLBL_ELEM_Name";
            this.linkLBL_ELEM_Name.Size = new System.Drawing.Size(202, 31);
            this.linkLBL_ELEM_Name.TabIndex = 6;
            this.linkLBL_ELEM_Name.TabStop = true;
            this.linkLBL_ELEM_Name.Text = "Nouvel élément";
            this.linkLBL_ELEM_Name.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLBL_ELEM_Name_LinkClicked);
            // 
            // ES_MonstreFaiblesse
            // 
            this.ES_MonstreFaiblesse.AutoScroll = true;
            this.ES_MonstreFaiblesse.CurrentElement = null;
            this.ES_MonstreFaiblesse.Location = new System.Drawing.Point(332, 45);
            this.ES_MonstreFaiblesse.Name = "ES_MonstreFaiblesse";
            this.ES_MonstreFaiblesse.Size = new System.Drawing.Size(112, 257);
            this.ES_MonstreFaiblesse.TabIndex = 4;
            this.ES_MonstreFaiblesse.Type = PixelLionWin.ElementalSelector.SelectorType.Weaknesses;
            // 
            // ES_MonstrePrimaire
            // 
            this.ES_MonstrePrimaire.AutoScroll = true;
            this.ES_MonstrePrimaire.CurrentElement = null;
            this.ES_MonstrePrimaire.Location = new System.Drawing.Point(185, 45);
            this.ES_MonstrePrimaire.Name = "ES_MonstrePrimaire";
            this.ES_MonstrePrimaire.Size = new System.Drawing.Size(112, 257);
            this.ES_MonstrePrimaire.TabIndex = 3;
            this.ES_MonstrePrimaire.Type = PixelLionWin.ElementalSelector.SelectorType.General;
            // 
            // MON_Statspicker
            // 
            this.MON_Statspicker.Location = new System.Drawing.Point(6, 19);
            this.MON_Statspicker.Name = "MON_Statspicker";
            this.MON_Statspicker.Size = new System.Drawing.Size(464, 305);
            this.MON_Statspicker.TabIndex = 0;
            // 
            // ES_AttaqueSpeciale
            // 
            this.ES_AttaqueSpeciale.AutoScroll = true;
            this.ES_AttaqueSpeciale.CurrentElement = null;
            this.ES_AttaqueSpeciale.Location = new System.Drawing.Point(26, 43);
            this.ES_AttaqueSpeciale.Name = "ES_AttaqueSpeciale";
            this.ES_AttaqueSpeciale.Size = new System.Drawing.Size(112, 257);
            this.ES_AttaqueSpeciale.TabIndex = 2;
            this.ES_AttaqueSpeciale.Type = PixelLionWin.ElementalSelector.SelectorType.General;
            // 
            // ES_Faiblesses
            // 
            this.ES_Faiblesses.AutoScroll = true;
            this.ES_Faiblesses.CurrentElement = null;
            this.ES_Faiblesses.Dock = System.Windows.Forms.DockStyle.Top;
            this.ES_Faiblesses.Location = new System.Drawing.Point(3, 16);
            this.ES_Faiblesses.Name = "ES_Faiblesses";
            this.ES_Faiblesses.Size = new System.Drawing.Size(160, 257);
            this.ES_Faiblesses.TabIndex = 2;
            this.ES_Faiblesses.Type = PixelLionWin.ElementalSelector.SelectorType.Weaknesses;
            // 
            // ES_Resistances
            // 
            this.ES_Resistances.AutoScroll = true;
            this.ES_Resistances.CurrentElement = null;
            this.ES_Resistances.Dock = System.Windows.Forms.DockStyle.Top;
            this.ES_Resistances.Location = new System.Drawing.Point(3, 16);
            this.ES_Resistances.Name = "ES_Resistances";
            this.ES_Resistances.Size = new System.Drawing.Size(160, 257);
            this.ES_Resistances.TabIndex = 2;
            this.ES_Resistances.Type = PixelLionWin.ElementalSelector.SelectorType.Resistances;
            // 
            // ES_Immunites
            // 
            this.ES_Immunites.AutoScroll = true;
            this.ES_Immunites.CurrentElement = null;
            this.ES_Immunites.Dock = System.Windows.Forms.DockStyle.Top;
            this.ES_Immunites.Location = new System.Drawing.Point(3, 16);
            this.ES_Immunites.Name = "ES_Immunites";
            this.ES_Immunites.Size = new System.Drawing.Size(160, 257);
            this.ES_Immunites.TabIndex = 2;
            this.ES_Immunites.Type = PixelLionWin.ElementalSelector.SelectorType.Immunities;
            // 
            // DatabaseEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 584);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DatabaseEditor";
            this.Text = "Éditeur de la base de données";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DatabaseEditor_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.GROUP_Item.ResumeLayout(false);
            this.GROUP_Item.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Weight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Price)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_pMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_pHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_HP)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.GROUP_Monsters.ResumeLayout(false);
            this.GROUP_Monsters.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.GROUP_SpecialAttacks.ResumeLayout(false);
            this.GROUP_SpecialAttacks.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_ATK_Radius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_ATK_Ratio)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.GROUP_Elements.ResumeLayout(false);
            this.GROUP_Elements.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListBox LIST_ItemObjects;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button BTN_AddItemObject;
        private System.Windows.Forms.Button BTN_DeleteItemObject;
        private System.Windows.Forms.Panel PN_Icon;
        private System.Windows.Forms.LinkLabel LBL_ITEM_Name;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUM_pMP;
        private System.Windows.Forms.NumericUpDown NUM_pHP;
        private System.Windows.Forms.NumericUpDown NUM_MP;
        private System.Windows.Forms.NumericUpDown NUM_HP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TB_Desc;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckedListBox CHK_LIST_StatusEffectItem;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown NUM_Price;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox CB_Consumable;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox CB_Unique;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown NUM_Weight;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox DDL_ITEM_Target;
        private System.Windows.Forms.GroupBox GROUP_Item;
        private System.Windows.Forms.Button BTN_SaveItem;
        private System.Windows.Forms.GroupBox GROUP_SpecialAttacks;
        private System.Windows.Forms.Button BTN_DeleteAttack;
        private System.Windows.Forms.Button BTN_AddAttack;
        private System.Windows.Forms.ListBox LIST_SpecialAttacks;
        private System.Windows.Forms.LinkLabel LBL_ATK_Name;
        private System.Windows.Forms.Panel PN_Attaque;
        private System.Windows.Forms.Button BTN_SaveAttack;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.NumericUpDown NUM_ATK_Ratio;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown NUM_ATK_Radius;
        private System.Windows.Forms.GroupBox groupBox9;
        private ElementalSelector ES_AttaqueSpeciale;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox DDL_ATK_Type;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label LB_ATK_Type;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox DDL_ATK_AnimationTarget;
        private System.Windows.Forms.ComboBox DDL_ATK_AnimationSelf;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button BTN_RemoveMonster;
        private System.Windows.Forms.Button BTN_AddMonster;
        private System.Windows.Forms.ListBox LIST_Monsters;
        private System.Windows.Forms.GroupBox GROUP_Monsters;
        private System.Windows.Forms.GroupBox groupBox11;
        private StatsPicker MON_Statspicker;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.LinkLabel LBL_MonsterName;
        private System.Windows.Forms.Button BTN_SaveMonster;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button BTN_DeleteElement;
        private System.Windows.Forms.Button BTN_AddElement;
        private System.Windows.Forms.ListBox LIST_Elements;
        private System.Windows.Forms.GroupBox GROUP_Elements;
        private System.Windows.Forms.Button BTN_SaveElement;
        private System.Windows.Forms.LinkLabel linkLBL_ELEM_Name;
        private System.Windows.Forms.Panel PN_IconElement;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox15;
        private ElementalSelector ES_Resistances;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox TB_Description;
        private System.Windows.Forms.GroupBox groupBox16;
        private ElementalSelector ES_Faiblesses;
        private ElementalSelector ES_Immunites;
        private System.Windows.Forms.ComboBox DDL_Cible;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label LB_ATK_Cible;
        private ElementalSelector ES_MonstreFaiblesse;
        private ElementalSelector ES_MonstrePrimaire;
    }
}