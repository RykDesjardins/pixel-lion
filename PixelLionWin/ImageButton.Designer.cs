﻿namespace PixelLionWin
{
    partial class ImageButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PN_Icon = new System.Windows.Forms.Panel();
            this.LB_Text = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PN_Icon
            // 
            this.PN_Icon.BackColor = System.Drawing.Color.Transparent;
            this.PN_Icon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PN_Icon.Location = new System.Drawing.Point(0, 0);
            this.PN_Icon.Name = "PN_Icon";
            this.PN_Icon.Size = new System.Drawing.Size(24, 24);
            this.PN_Icon.TabIndex = 0;
            this.PN_Icon.Click += new System.EventHandler(this.PN_Icon_Click);
            this.PN_Icon.MouseEnter += new System.EventHandler(this.PN_Icon_MouseEnter);
            this.PN_Icon.MouseLeave += new System.EventHandler(this.PN_Icon_MouseLeave);
            // 
            // LB_Text
            // 
            this.LB_Text.AutoSize = true;
            this.LB_Text.BackColor = System.Drawing.Color.Transparent;
            this.LB_Text.Font = new System.Drawing.Font("Eras Light ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LB_Text.Location = new System.Drawing.Point(26, 1);
            this.LB_Text.Name = "LB_Text";
            this.LB_Text.Size = new System.Drawing.Size(94, 20);
            this.LB_Text.TabIndex = 1;
            this.LB_Text.Text = "[Button Text]";
            this.LB_Text.Click += new System.EventHandler(this.LB_Text_Click);
            this.LB_Text.MouseEnter += new System.EventHandler(this.LB_Text_MouseEnter);
            this.LB_Text.MouseLeave += new System.EventHandler(this.LB_Text_MouseLeave);
            // 
            // ImageButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.LB_Text);
            this.Controls.Add(this.PN_Icon);
            this.Name = "ImageButton";
            this.Size = new System.Drawing.Size(321, 24);
            this.Click += new System.EventHandler(this.ImageButton_Click);
            this.MouseEnter += new System.EventHandler(this.ImageButton_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.ImageButton_MouseLeave);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PN_Icon;
        private System.Windows.Forms.Label LB_Text;
    }
}
