﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class SpritePicker : Form
    {
        public SpriteBundle SpriteBundle { get; set; }
        public bool Choisi;
        public Sprite SpriteChoisi;

        public SpritePicker(SpriteBundle bundle)
        {
            InitializeComponent();
            SpriteBundle = bundle;
            foreach (Sprite item in bundle.Sprites)
            {
                LBSprites.Items.Add(item);
            }
        }

        private void LBSprites_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (XNAUtils utils = new XNAUtils())
            if (LBSprites.SelectedItem != null)
            {
                btnChoisir.Enabled = true;
                SpriteChoisi = (Sprite)LBSprites.SelectedItem;
                plPreview.BackgroundImage = utils.ConvertToImage(SpriteChoisi.SpriteImage);
            }
        }

        private void btnChoisir_Click(object sender, EventArgs e)
        {
            Choisir();
        }

        private void LBSprites_DoubleClick(object sender, EventArgs e)
        {
            if(btnChoisir.Enabled)
                Choisir();
        }

        private void Choisir()
        {
            Choisi = true;
            Close();
        }
    }
}
