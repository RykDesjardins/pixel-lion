﻿namespace PixelLionWin
{
    partial class WelcomeScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PN_Title = new System.Windows.Forms.Panel();
            this.LB_ProjectName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LB_Description = new System.Windows.Forms.Label();
            this.BTN_Close = new PixelLionWin.ImageButton();
            this.BTN_SongList = new PixelLionWin.ImageButton();
            this.BTN_Stats = new PixelLionWin.ImageButton();
            this.BTN_PixelLinkConfig = new PixelLionWin.ImageButton();
            this.BTN_Notes = new PixelLionWin.ImageButton();
            this.BTN_SpriteList = new PixelLionWin.ImageButton();
            this.BTN_Interpreter = new PixelLionWin.ImageButton();
            this.BTN_DB = new PixelLionWin.ImageButton();
            this.BTN_NewMap = new PixelLionWin.ImageButton();
            this.PN_Title.SuspendLayout();
            this.SuspendLayout();
            // 
            // PN_Title
            // 
            this.PN_Title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_Title.BackColor = System.Drawing.Color.Ivory;
            this.PN_Title.BackgroundImage = global::PixelLionWin.Properties.Resources.OpeningTitlebar;
            this.PN_Title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PN_Title.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Title.Controls.Add(this.LB_ProjectName);
            this.PN_Title.Controls.Add(this.label2);
            this.PN_Title.Controls.Add(this.label1);
            this.PN_Title.Controls.Add(this.panel1);
            this.PN_Title.Location = new System.Drawing.Point(-1, -1);
            this.PN_Title.Name = "PN_Title";
            this.PN_Title.Size = new System.Drawing.Size(635, 80);
            this.PN_Title.TabIndex = 0;
            // 
            // LB_ProjectName
            // 
            this.LB_ProjectName.AutoSize = true;
            this.LB_ProjectName.BackColor = System.Drawing.Color.Transparent;
            this.LB_ProjectName.Font = new System.Drawing.Font("Candara", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LB_ProjectName.Location = new System.Drawing.Point(143, 19);
            this.LB_ProjectName.Name = "LB_ProjectName";
            this.LB_ProjectName.Size = new System.Drawing.Size(142, 23);
            this.LB_ProjectName.TabIndex = 2;
            this.LB_ProjectName.Text = "[Nom du projet]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Candara", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(79, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Projet : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(76, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pixel Lion";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::PixelLionWin.Properties.Resources.PixelLion_HighRes_Logo;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(70, 72);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Location = new System.Drawing.Point(262, 94);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(2, 253);
            this.panel2.TabIndex = 2;
            // 
            // LB_Description
            // 
            this.LB_Description.Font = new System.Drawing.Font("Eras Medium ITC", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LB_Description.ForeColor = System.Drawing.Color.DimGray;
            this.LB_Description.Location = new System.Drawing.Point(287, 111);
            this.LB_Description.Name = "LB_Description";
            this.LB_Description.Size = new System.Drawing.Size(315, 208);
            this.LB_Description.TabIndex = 4;
            this.LB_Description.Text = "Petite description du projet.\r\n\r\nLe projet consiste en la création d\'un moteur de" +
    " jeu. Il est facile à utiliser.";
            // 
            // BTN_Close
            // 
            this.BTN_Close.BackColor = System.Drawing.Color.Transparent;
            this.BTN_Close.ButtonColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTN_Close.ButtonImage = global::PixelLionWin.Properties.Resources.MENUICON_X;
            this.BTN_Close.ButtonText = "";
            this.BTN_Close.ButtonTextColor = System.Drawing.SystemColors.ControlText;
            this.BTN_Close.Location = new System.Drawing.Point(606, 333);
            this.BTN_Close.Name = "BTN_Close";
            this.BTN_Close.Size = new System.Drawing.Size(24, 24);
            this.BTN_Close.TabIndex = 3;
            this.BTN_Close.ButtonClick += new System.EventHandler(this.BTN_Close_Click);
            // 
            // BTN_SongList
            // 
            this.BTN_SongList.BackColor = System.Drawing.Color.Transparent;
            this.BTN_SongList.ButtonColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTN_SongList.ButtonImage = global::PixelLionWin.Properties.Resources.MENUICON_Audio;
            this.BTN_SongList.ButtonText = "Liste des chansons";
            this.BTN_SongList.ButtonTextColor = System.Drawing.SystemColors.ControlText;
            this.BTN_SongList.Location = new System.Drawing.Point(19, 310);
            this.BTN_SongList.Name = "BTN_SongList";
            this.BTN_SongList.Size = new System.Drawing.Size(214, 24);
            this.BTN_SongList.TabIndex = 1;
            this.BTN_SongList.ButtonClick += new System.EventHandler(this.BTN_SongList_Click);
            // 
            // BTN_Stats
            // 
            this.BTN_Stats.BackColor = System.Drawing.Color.Transparent;
            this.BTN_Stats.ButtonColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTN_Stats.ButtonImage = global::PixelLionWin.Properties.Resources.MENUICON_Stats;
            this.BTN_Stats.ButtonText = "Statistiques du projet";
            this.BTN_Stats.ButtonTextColor = System.Drawing.SystemColors.ControlText;
            this.BTN_Stats.Location = new System.Drawing.Point(19, 280);
            this.BTN_Stats.Name = "BTN_Stats";
            this.BTN_Stats.Size = new System.Drawing.Size(214, 24);
            this.BTN_Stats.TabIndex = 1;
            this.BTN_Stats.ButtonClick += new System.EventHandler(this.BTN_Stats_Click);
            // 
            // BTN_PixelLinkConfig
            // 
            this.BTN_PixelLinkConfig.BackColor = System.Drawing.Color.Transparent;
            this.BTN_PixelLinkConfig.ButtonColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTN_PixelLinkConfig.ButtonImage = global::PixelLionWin.Properties.Resources.MENUICON_Link;
            this.BTN_PixelLinkConfig.ButtonText = "Configurations Pixel Link";
            this.BTN_PixelLinkConfig.ButtonTextColor = System.Drawing.SystemColors.ControlText;
            this.BTN_PixelLinkConfig.Location = new System.Drawing.Point(19, 250);
            this.BTN_PixelLinkConfig.Name = "BTN_PixelLinkConfig";
            this.BTN_PixelLinkConfig.Size = new System.Drawing.Size(214, 24);
            this.BTN_PixelLinkConfig.TabIndex = 1;
            this.BTN_PixelLinkConfig.ButtonClick += new System.EventHandler(this.BTN_PixelLinkConfig_Click);
            // 
            // BTN_Notes
            // 
            this.BTN_Notes.BackColor = System.Drawing.Color.Transparent;
            this.BTN_Notes.ButtonColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTN_Notes.ButtonImage = global::PixelLionWin.Properties.Resources.MENUICON_Note;
            this.BTN_Notes.ButtonText = "Notes du projet";
            this.BTN_Notes.ButtonTextColor = System.Drawing.SystemColors.ControlText;
            this.BTN_Notes.Location = new System.Drawing.Point(19, 220);
            this.BTN_Notes.Name = "BTN_Notes";
            this.BTN_Notes.Size = new System.Drawing.Size(214, 24);
            this.BTN_Notes.TabIndex = 1;
            this.BTN_Notes.ButtonClick += new System.EventHandler(this.BTN_Notes_Click);
            // 
            // BTN_SpriteList
            // 
            this.BTN_SpriteList.BackColor = System.Drawing.Color.Transparent;
            this.BTN_SpriteList.ButtonColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTN_SpriteList.ButtonImage = global::PixelLionWin.Properties.Resources.MENUICON_Sprites;
            this.BTN_SpriteList.ButtonText = "Liste des Sprites";
            this.BTN_SpriteList.ButtonTextColor = System.Drawing.SystemColors.ControlText;
            this.BTN_SpriteList.Location = new System.Drawing.Point(19, 190);
            this.BTN_SpriteList.Name = "BTN_SpriteList";
            this.BTN_SpriteList.Size = new System.Drawing.Size(214, 24);
            this.BTN_SpriteList.TabIndex = 1;
            this.BTN_SpriteList.ButtonClick += new System.EventHandler(this.BTN_SpriteList_Click);
            // 
            // BTN_Interpreter
            // 
            this.BTN_Interpreter.BackColor = System.Drawing.Color.Transparent;
            this.BTN_Interpreter.ButtonColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTN_Interpreter.ButtonImage = global::PixelLionWin.Properties.Resources.MENUICON_Console;
            this.BTN_Interpreter.ButtonText = "Interpreteur Pixel Monkey";
            this.BTN_Interpreter.ButtonTextColor = System.Drawing.SystemColors.ControlText;
            this.BTN_Interpreter.Location = new System.Drawing.Point(19, 160);
            this.BTN_Interpreter.Name = "BTN_Interpreter";
            this.BTN_Interpreter.Size = new System.Drawing.Size(214, 24);
            this.BTN_Interpreter.TabIndex = 1;
            this.BTN_Interpreter.ButtonClick += new System.EventHandler(this.BTN_Interpreter_Click);
            // 
            // BTN_DB
            // 
            this.BTN_DB.BackColor = System.Drawing.Color.Transparent;
            this.BTN_DB.ButtonColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTN_DB.ButtonImage = global::PixelLionWin.Properties.Resources.MENUICON_DB;
            this.BTN_DB.ButtonText = "Base de données";
            this.BTN_DB.ButtonTextColor = System.Drawing.SystemColors.ControlText;
            this.BTN_DB.Location = new System.Drawing.Point(19, 130);
            this.BTN_DB.Name = "BTN_DB";
            this.BTN_DB.Size = new System.Drawing.Size(214, 24);
            this.BTN_DB.TabIndex = 1;
            this.BTN_DB.ButtonClick += new System.EventHandler(this.BTN_DB_Click);
            // 
            // BTN_NewMap
            // 
            this.BTN_NewMap.BackColor = System.Drawing.Color.Transparent;
            this.BTN_NewMap.ButtonColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BTN_NewMap.ButtonImage = global::PixelLionWin.Properties.Resources.MENUICON_map;
            this.BTN_NewMap.ButtonText = "Créer une nouvelle carte";
            this.BTN_NewMap.ButtonTextColor = System.Drawing.SystemColors.ControlText;
            this.BTN_NewMap.Location = new System.Drawing.Point(19, 100);
            this.BTN_NewMap.Name = "BTN_NewMap";
            this.BTN_NewMap.Size = new System.Drawing.Size(214, 24);
            this.BTN_NewMap.TabIndex = 1;
            this.BTN_NewMap.ButtonClick += new System.EventHandler(this.BTN_NewMap_Click);
            // 
            // WelcomeScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.LB_Description);
            this.Controls.Add(this.BTN_Close);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.BTN_SongList);
            this.Controls.Add(this.BTN_Stats);
            this.Controls.Add(this.BTN_PixelLinkConfig);
            this.Controls.Add(this.BTN_Notes);
            this.Controls.Add(this.BTN_SpriteList);
            this.Controls.Add(this.BTN_Interpreter);
            this.Controls.Add(this.BTN_DB);
            this.Controls.Add(this.BTN_NewMap);
            this.Controls.Add(this.PN_Title);
            this.Name = "WelcomeScreen";
            this.Size = new System.Drawing.Size(633, 360);
            this.PN_Title.ResumeLayout(false);
            this.PN_Title.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN_Title;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LB_ProjectName;
        private System.Windows.Forms.Label label2;
        private ImageButton BTN_NewMap;
        private ImageButton BTN_DB;
        private ImageButton BTN_Interpreter;
        private ImageButton BTN_SpriteList;
        private ImageButton BTN_Notes;
        private ImageButton BTN_PixelLinkConfig;
        private ImageButton BTN_Stats;
        private ImageButton BTN_SongList;
        private System.Windows.Forms.Panel panel2;
        private ImageButton BTN_Close;
        private System.Windows.Forms.Label LB_Description;
    }
}
