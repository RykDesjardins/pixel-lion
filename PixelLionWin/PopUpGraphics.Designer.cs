﻿namespace PixelLionWin
{
    partial class PopUpGraphics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PN_Graphics = new System.Windows.Forms.Panel();
            this.BTN_Close = new System.Windows.Forms.Button();
            this.BTN_Edit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PN_Graphics
            // 
            this.PN_Graphics.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PN_Graphics.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PN_Graphics.Location = new System.Drawing.Point(12, 12);
            this.PN_Graphics.Name = "PN_Graphics";
            this.PN_Graphics.Size = new System.Drawing.Size(260, 229);
            this.PN_Graphics.TabIndex = 1;
            // 
            // BTN_Close
            // 
            this.BTN_Close.Location = new System.Drawing.Point(278, 217);
            this.BTN_Close.Name = "BTN_Close";
            this.BTN_Close.Size = new System.Drawing.Size(75, 23);
            this.BTN_Close.TabIndex = 11;
            this.BTN_Close.Text = "Close";
            this.BTN_Close.UseVisualStyleBackColor = true;
            this.BTN_Close.Click += new System.EventHandler(this.BTN_Quit_Click);
            // 
            // BTN_Edit
            // 
            this.BTN_Edit.Location = new System.Drawing.Point(278, 188);
            this.BTN_Edit.Name = "BTN_Edit";
            this.BTN_Edit.Size = new System.Drawing.Size(75, 23);
            this.BTN_Edit.TabIndex = 13;
            this.BTN_Edit.Text = "Edit";
            this.BTN_Edit.UseVisualStyleBackColor = true;
            this.BTN_Edit.Click += new System.EventHandler(this.BTN_Edit_Click);
            // 
            // PopUpGraphics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 252);
            this.Controls.Add(this.BTN_Edit);
            this.Controls.Add(this.BTN_Close);
            this.Controls.Add(this.PN_Graphics);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PopUpGraphics";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Graphics";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN_Graphics;
        private System.Windows.Forms.Button BTN_Close;
        private System.Windows.Forms.Button BTN_Edit;
    }
}