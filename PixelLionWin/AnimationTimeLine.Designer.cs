﻿namespace PixelLionWin
{
    partial class AnimationTimeLine
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PN_Timeline = new System.Windows.Forms.Panel();
            this.BTN_RemoveFrame = new System.Windows.Forms.Button();
            this.BTN_Insert = new System.Windows.Forms.Button();
            this.BTN_Play = new System.Windows.Forms.Button();
            this.BTN_AddFrame = new System.Windows.Forms.Button();
            this.BTN_FFW = new System.Windows.Forms.Button();
            this.BTN_RWN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PN_Timeline
            // 
            this.PN_Timeline.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_Timeline.BackColor = System.Drawing.Color.White;
            this.PN_Timeline.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Timeline.Cursor = System.Windows.Forms.Cursors.Cross;
            this.PN_Timeline.Location = new System.Drawing.Point(111, 3);
            this.PN_Timeline.Name = "PN_Timeline";
            this.PN_Timeline.Size = new System.Drawing.Size(800, 66);
            this.PN_Timeline.TabIndex = 1;
            this.PN_Timeline.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.PN_Timeline.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.PN_Timeline.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.PN_Timeline.Resize += new System.EventHandler(this.PN_Timeline_Resize);
            // 
            // BTN_RemoveFrame
            // 
            this.BTN_RemoveFrame.BackgroundImage = global::PixelLionWin.Properties.Resources.CloseIcon;
            this.BTN_RemoveFrame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_RemoveFrame.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonShadow;
            this.BTN_RemoveFrame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_RemoveFrame.Location = new System.Drawing.Point(39, 39);
            this.BTN_RemoveFrame.Name = "BTN_RemoveFrame";
            this.BTN_RemoveFrame.Size = new System.Drawing.Size(30, 30);
            this.BTN_RemoveFrame.TabIndex = 0;
            this.BTN_RemoveFrame.UseVisualStyleBackColor = true;
            this.BTN_RemoveFrame.Click += new System.EventHandler(this.BTN_RemoveFrame_Click);
            // 
            // BTN_Insert
            // 
            this.BTN_Insert.BackgroundImage = global::PixelLionWin.Properties.Resources.godownblue;
            this.BTN_Insert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Insert.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonShadow;
            this.BTN_Insert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_Insert.Location = new System.Drawing.Point(75, 40);
            this.BTN_Insert.Name = "BTN_Insert";
            this.BTN_Insert.Size = new System.Drawing.Size(30, 30);
            this.BTN_Insert.TabIndex = 0;
            this.BTN_Insert.UseVisualStyleBackColor = true;
            this.BTN_Insert.Click += new System.EventHandler(this.BTN_Insert_Click);
            // 
            // BTN_Play
            // 
            this.BTN_Play.BackgroundImage = global::PixelLionWin.Properties.Resources.MediaStartButton;
            this.BTN_Play.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Play.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonShadow;
            this.BTN_Play.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_Play.Location = new System.Drawing.Point(3, 39);
            this.BTN_Play.Name = "BTN_Play";
            this.BTN_Play.Size = new System.Drawing.Size(30, 30);
            this.BTN_Play.TabIndex = 0;
            this.BTN_Play.UseVisualStyleBackColor = true;
            this.BTN_Play.Click += new System.EventHandler(this.BTN_Play_Click);
            // 
            // BTN_AddFrame
            // 
            this.BTN_AddFrame.BackgroundImage = global::PixelLionWin.Properties.Resources.AddButton;
            this.BTN_AddFrame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_AddFrame.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonShadow;
            this.BTN_AddFrame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AddFrame.Location = new System.Drawing.Point(75, 3);
            this.BTN_AddFrame.Name = "BTN_AddFrame";
            this.BTN_AddFrame.Size = new System.Drawing.Size(30, 30);
            this.BTN_AddFrame.TabIndex = 0;
            this.BTN_AddFrame.UseVisualStyleBackColor = true;
            this.BTN_AddFrame.Click += new System.EventHandler(this.BTN_AddFrame_Click);
            // 
            // BTN_FFW
            // 
            this.BTN_FFW.BackgroundImage = global::PixelLionWin.Properties.Resources.NextButton;
            this.BTN_FFW.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_FFW.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonShadow;
            this.BTN_FFW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_FFW.Location = new System.Drawing.Point(39, 3);
            this.BTN_FFW.Name = "BTN_FFW";
            this.BTN_FFW.Size = new System.Drawing.Size(30, 30);
            this.BTN_FFW.TabIndex = 0;
            this.BTN_FFW.UseVisualStyleBackColor = true;
            this.BTN_FFW.Click += new System.EventHandler(this.BTN_FFW_Click);
            // 
            // BTN_RWN
            // 
            this.BTN_RWN.BackgroundImage = global::PixelLionWin.Properties.Resources.BackButton;
            this.BTN_RWN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_RWN.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonShadow;
            this.BTN_RWN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_RWN.Location = new System.Drawing.Point(3, 3);
            this.BTN_RWN.Name = "BTN_RWN";
            this.BTN_RWN.Size = new System.Drawing.Size(30, 30);
            this.BTN_RWN.TabIndex = 0;
            this.BTN_RWN.UseVisualStyleBackColor = true;
            this.BTN_RWN.Click += new System.EventHandler(this.BTN_RWN_Click);
            // 
            // AnimationTimeLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.BTN_Insert);
            this.Controls.Add(this.PN_Timeline);
            this.Controls.Add(this.BTN_RemoveFrame);
            this.Controls.Add(this.BTN_Play);
            this.Controls.Add(this.BTN_AddFrame);
            this.Controls.Add(this.BTN_FFW);
            this.Controls.Add(this.BTN_RWN);
            this.Name = "AnimationTimeLine";
            this.Size = new System.Drawing.Size(915, 73);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button BTN_RWN;
        public System.Windows.Forms.Button BTN_FFW;
        public System.Windows.Forms.Button BTN_Play;
        public System.Windows.Forms.Button BTN_Insert;
        public System.Windows.Forms.Panel PN_Timeline;
        public System.Windows.Forms.Button BTN_AddFrame;
        public System.Windows.Forms.Button BTN_RemoveFrame;

    }
}
