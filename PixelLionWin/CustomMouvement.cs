﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class CustomMouvement : Form
    {
        List<Point> ListePoints;
        public CustomMouvement(Pevent pvent, string Depart, int mapWidth, int mapHeight)
        {
            InitializeComponent();
            ListePoints = pvent.ArretMouvement;
            foreach (Point point in ListePoints)
                LBMouvements.Items.Add(point);
            lblPointDepart.Text = "Point de départ: " + Depart;
            NUM_PointX.Maximum = mapWidth;
            NUM_PointY.Maximum = mapHeight;
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            Point unMouvement = new Point((Int32)NUM_PointX.Value, (Int32)NUM_PointY.Value);
            ListePoints.Add(unMouvement);
            LBMouvements.Items.Add(unMouvement);
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            if (LBMouvements.SelectedItem == null)
                return;
            Point point = (Point)LBMouvements.SelectedItem;
            ListePoints.Remove(point);
            LBMouvements.Items.Remove(point);
        }

        private void CustomMouvement_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
