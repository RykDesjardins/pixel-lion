﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using System.IO;

namespace PixelLionWin
{
    public partial class SpritesForm : Form
    {
        public SpriteBundle SpriteBundle { get; set; }
        public Boolean HasSaved;
        
        public SpritesForm(SpriteBundle bundle)
        {
            InitializeComponent();
            SpriteBundle = bundle;
            foreach (Sprite item in bundle.Sprites)
            {
                LBSprites.Items.Add(item);
            }
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Multiselect = true;
            dlg.Filter = "Fichiers png (*.png)|*.png";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                WaitingForm form = new WaitingForm();
                form.Show();
                Application.DoEvents();

                using (XNAUtils utils = new XNAUtils())
                for (int i = 0; i < dlg.FileNames.Count(); i++)
                {
                    byte[] data = File.ReadAllBytes(dlg.FileNames[i]);
                    Image image = byteArrayToImage(data);
                    Sprite sprite = new Sprite() { SpriteImage = utils.ConvertToTexture(image), Name = dlg.SafeFileNames[i], TileCount = utils.xPoint(GetTileCount(image)) };
                    SpriteBundle.Sprites.Add(sprite);
                    LBSprites.Items.Add(sprite);
                }
                form.Close();
            }
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            if (LBSprites.SelectedItem == null)
                return;
            Sprite sprite = (Sprite)LBSprites.SelectedItem;
            SpriteBundle.Sprites.Remove(sprite);
            LBSprites.Items.Remove(sprite);
            plPreview.BackgroundImage = new Bitmap(1, 1);
            NUDLargeur.Value = 0;
            NUDHauteur.Value = 0;
            lblPixelsX.Text = "Pixels X: ";
            lblPixelsY.Text = "Pixels Y: ";
        }

        private Point GetTileCount(Image image)
        {
            Point TileCount = new Point();
            Single largeurUNEimage = 0;
            Single hauteurUNEimage = 0;
            Single largeurImage = image.Width;
            Single hauteurImage = image.Height;
            Single nbImageLarge = 0;
            Single nbImageHauteur = 0;
            Single nbImageLarge1 = 0;
            Single nbImageHauteur1 = 0;
            int tempDepart = 0;
            int tempFin = 0;
            bool commencé = false;
            bool trouverLargeur = false;
            bool trouverHauteur = false;
            Bitmap myBitmap = (Bitmap)image;

            // Largeur Image
            for (int i = 0; i < largeurImage && !trouverLargeur; i++)
            {
                Color color = myBitmap.GetPixel(i, 10);
                if (color.A != 0 && tempDepart == 0 && !commencé)
                {
                    tempDepart = i;
                    commencé = true;
                }
                else if (color.A == 0 && commencé)
                {
                    tempFin = i;
                    commencé = false;
                    nbImageLarge1 = largeurImage / (2 * tempDepart + (tempFin - tempDepart)); // nbImageLarge = largeurImage / tempFin;
                    nbImageLarge = Convert.ToInt32(Math.Round(nbImageLarge1));
                    largeurUNEimage = largeurImage / nbImageLarge;
                    trouverLargeur = true;
                }
            }

            // Hauteur Image
            tempDepart = 0;
            tempFin = 0;
            for (int i = 0; i < hauteurImage && !trouverHauteur; i++)
            {
                Color color = myBitmap.GetPixel(Convert.ToInt32(Math.Round(largeurUNEimage)) / 2, i);
                if (color.A != 0 && tempDepart == 0 && !commencé)
                {
                    tempDepart = i;
                    commencé = true;
                }
                else if (color.A == 0 && commencé)
                {
                    tempFin = i;
                    commencé = false;
                    nbImageHauteur1 = hauteurImage / (2 * tempDepart + (tempFin - tempDepart)); // nbImageHauteur = hauteurImage / tempFin;
                    nbImageHauteur = Convert.ToInt32(Math.Round(nbImageHauteur1));
                    hauteurUNEimage = hauteurImage / nbImageHauteur;
                    trouverHauteur = true;
                }
            }

            TileCount.X = Convert.ToInt32(Math.Round(nbImageLarge));
            TileCount.Y = Convert.ToInt32(Math.Round(nbImageHauteur));
            return TileCount;
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void LBSprites_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (XNAUtils utils = new XNAUtils())
            if (LBSprites.SelectedItem != null)
            {
                Sprite sprite = (Sprite)LBSprites.SelectedItem;
                plPreview.BackgroundImage = utils.ConvertToImage(sprite.SpriteImage);

                if (sprite.tempTileX != 0)
                {
                    NUDLargeur.Value = sprite.tempTileX;
                    lblPixelsX.Text = "Pixels X: " + sprite.SpriteImage.Width / sprite.tempTileX;
                }
                else
                {
                    NUDLargeur.Value = sprite.TileCount.X;
                    lblPixelsX.Text = "Pixels X: " + sprite.SpriteImage.Width / sprite.TileCount.X;
                }
                if (sprite.tempTileY != 0)
                {
                    NUDHauteur.Value = sprite.tempTileY;
                    lblPixelsY.Text = "Pixels Y: " + sprite.SpriteImage.Height / sprite.tempTileY;
                }
                else
                {
                    NUDHauteur.Value = sprite.TileCount.Y;
                    lblPixelsY.Text = "Pixels Y: " + sprite.SpriteImage.Height / sprite.TileCount.Y;
                }
            }
        }

        private void btnSauvegarder_Click(object sender, EventArgs e)
        {
            HasSaved = true;
            Close();
        }

        private void NUDLargeur_ValueChanged(object sender, EventArgs e)
        {
            Sprite sprite = (Sprite)LBSprites.SelectedItem;
            try
            {
                sprite.tempTileX = (int)NUDLargeur.Value;
                lblPixelsX.Text = "Pixels X: " + Math.Round(sprite.SpriteImage.Width / NUDLargeur.Value);
            }
            catch (NullReferenceException)
            {   }
        }

        private void NUDHauteur_ValueChanged(object sender, EventArgs e)
        {
            Sprite sprite = (Sprite)LBSprites.SelectedItem;
            try
            {
                sprite.tempTileY = (int)NUDHauteur.Value;
                lblPixelsY.Text = "Pixels Y: " + Math.Round(sprite.SpriteImage.Height / NUDHauteur.Value);
            }
            catch (NullReferenceException)
            {   }
        }
    }
}