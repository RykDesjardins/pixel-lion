﻿namespace PixelLionWin
{
    partial class ConfirmationTeleportation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTN_Accept = new System.Windows.Forms.Button();
            this.BTN_Refuse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LBL_Edit = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BTN_Accept
            // 
            this.BTN_Accept.BackgroundImage = global::PixelLionWin.Properties.Resources.AcceptIcon;
            this.BTN_Accept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Accept.Location = new System.Drawing.Point(12, 78);
            this.BTN_Accept.Name = "BTN_Accept";
            this.BTN_Accept.Size = new System.Drawing.Size(50, 50);
            this.BTN_Accept.TabIndex = 0;
            this.BTN_Accept.UseVisualStyleBackColor = true;
            this.BTN_Accept.Click += new System.EventHandler(this.BTN_Accept_Click);
            // 
            // BTN_Refuse
            // 
            this.BTN_Refuse.BackgroundImage = global::PixelLionWin.Properties.Resources.CloseIcon;
            this.BTN_Refuse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Refuse.Location = new System.Drawing.Point(322, 78);
            this.BTN_Refuse.Name = "BTN_Refuse";
            this.BTN_Refuse.Size = new System.Drawing.Size(50, 50);
            this.BTN_Refuse.TabIndex = 1;
            this.BTN_Refuse.UseVisualStyleBackColor = true;
            this.BTN_Refuse.Click += new System.EventHandler(this.BTN_Refuse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(365, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Êtes-vous certain de faire un point de téléportation";
            // 
            // LBL_Edit
            // 
            this.LBL_Edit.AutoSize = true;
            this.LBL_Edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Edit.Location = new System.Drawing.Point(12, 43);
            this.LBL_Edit.Name = "LBL_Edit";
            this.LBL_Edit.Size = new System.Drawing.Size(48, 20);
            this.LBL_Edit.TabIndex = 2;
            this.LBL_Edit.Text = "[de à]";
            // 
            // ConfirmationTeleportation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 140);
            this.Controls.Add(this.LBL_Edit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BTN_Refuse);
            this.Controls.Add(this.BTN_Accept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfirmationTeleportation";
            this.Text = "Confirmation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BTN_Accept;
        private System.Windows.Forms.Button BTN_Refuse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LBL_Edit;
    }
}