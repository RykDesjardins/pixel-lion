﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class DatabaseEditor : Form
    {
        public GameDatabase DB;
        public ProjectRessources ressources;
        public Boolean HasAccepted;

        public DatabaseEditor(GameDatabase DB, ProjectRessources ress)
        {
            InitializeComponent();

            this.DB = DB;
            this.ressources = ress;

            foreach (ItemObject item in DB.ItemObjects)
                LIST_ItemObjects.Items.Add(item);

            foreach (Monster item in DB.Monsters)
                LIST_Monsters.Items.Add(item);

            foreach (SpecialAttack item in DB.SpecialAttacks)
                LIST_SpecialAttacks.Items.Add(item);

            foreach (StatusEffect item in Enum.GetValues(typeof(StatusEffect)))
                CHK_LIST_StatusEffectItem.Items.Add(item);

            foreach (TargetType item in Enum.GetValues(typeof(TargetType)))
                DDL_ITEM_Target.Items.Add(item);

            foreach (AttackType item in Enum.GetValues(typeof(AttackType)))
                DDL_ATK_Type.Items.Add(item);

            foreach (TargetType item in Enum.GetValues(typeof(TargetType)))
                DDL_Cible.Items.Add(item);

            foreach (Animation item in ress.Animations)
            {
                DDL_ATK_AnimationSelf.Items.Add(item);
                DDL_ATK_AnimationTarget.Items.Add(item);
            }

            ES_Faiblesses.Init(LIST_Elements, "");
            ES_Resistances.Init(LIST_Elements, "");
            ES_Immunites.Init(LIST_Elements, "");
            ES_MonstreFaiblesse.Init(LIST_Elements, "");
            ES_MonstrePrimaire.Init(LIST_Elements, "");
            ES_AttaqueSpeciale.Init(LIST_Elements, "");

            DDL_ITEM_Target.SelectedIndex = 0;
            DDL_ATK_Type.SelectedIndex = 0;
            DDL_Cible.SelectedIndex = 0;

            if (DDL_ATK_AnimationTarget.Items.Count > 0)
            {
                DDL_ATK_Type.SelectedIndex = 0;
                DDL_Cible.SelectedIndex = 0;
            }
        }

        private void BTN_AddItemObject_Click(object sender, EventArgs e)
        {
            LIST_ItemObjects.Items.Add(new ItemObject());
        }

        private void LIST_ItemObjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemObject o = LIST_ItemObjects.SelectedItem as ItemObject;

            using (XNAUtils utils = new XNAUtils())
                if (o != null)
                {
                    GROUP_Item.Visible = true;

                    LBL_ITEM_Name.Text = o.Name;
                    CB_Consumable.Checked = o.Ephemeral;
                    TB_Desc.Text = o.Description;
                    NUM_pHP.Value = new decimal(o.HPPercent);
                    NUM_HP.Value = new decimal(o.HPRecovery);
                    if (o.Icon != null) PN_Icon.BackgroundImage = utils.ConvertToImage(o.Icon.GetImage());
                    else PN_Icon.BackgroundImage = null;
                    PN_Icon.Tag = o.Icon;
                    NUM_pMP.Value = new decimal(o.MPPercent);
                    NUM_MP.Value = new decimal(o.MPRecovery);
                    NUM_Price.Value = new decimal(o.Price);
                    CB_Unique.Checked = o.Unique;
                    NUM_Weight.Value = new decimal(o.Weight);
                    DDL_ITEM_Target.SelectedItem = o.Target;

                    for (int i = 0; i < CHK_LIST_StatusEffectItem.Items.Count; i++)
                        CHK_LIST_StatusEffectItem.SetItemChecked(i, false);

                    foreach (StatusEffect item in o.StatusClear)
                        CHK_LIST_StatusEffectItem.SetItemChecked(CHK_LIST_StatusEffectItem.Items.IndexOf(item), true);
                }
                else
                    GROUP_Item.Visible = false;
        }

        private void BTN_SaveItem_Click(object sender, EventArgs e)
        {
            if (LBL_ITEM_Name.Text == LIST_ItemObjects.SelectedItem.ToString() || !ContainsSameName(LIST_ItemObjects, LBL_ITEM_Name.Text))
            {
                ItemObject o = LIST_ItemObjects.SelectedItem as ItemObject;

                o.Name = LBL_ITEM_Name.Text;
                o.Ephemeral = CB_Consumable.Checked;
                o.Description = TB_Desc.Text;
                o.HPPercent = Convert.ToInt32(NUM_pHP.Value);
                o.HPRecovery = Convert.ToInt32(NUM_HP.Value);
                o.Icon = PN_Icon.Tag as pIcon;
                o.MPPercent = Convert.ToInt32(NUM_pMP.Value);
                o.MPRecovery = Convert.ToInt32(NUM_MP.Value);
                o.Price = Convert.ToInt32(NUM_Price.Value);
                o.Unique = CB_Unique.Checked;
                o.Weight = Convert.ToInt32(NUM_Weight.Value);
                o.Target = (TargetType)DDL_ITEM_Target.SelectedItem;

                o.StatusClear = new List<StatusEffect>();
                foreach (StatusEffect item in CHK_LIST_StatusEffectItem.CheckedItems)
                    o.StatusClear.Add(item);

                LIST_ItemObjects.Items[LIST_ItemObjects.SelectedIndex] = o;
            }
            else
                GenerateSaveWarningBox("objet", LBL_ITEM_Name.Text);
        }

        private void DatabaseEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            DB = new GameDatabase();

            foreach (ItemObject item in LIST_ItemObjects.Items)
                DB.ItemObjects.Add(item);

            foreach (Monster item in LIST_Monsters.Items)
                DB.Monsters.Add(item);

            foreach (SpecialAttack item in LIST_SpecialAttacks.Items)
                DB.SpecialAttacks.Add(item);
        }

        private void LBL_Name_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SingleStringInput dlg = new SingleStringInput("Nom de l'objet", LBL_ITEM_Name.Text);
            dlg.ShowDialog();

            LBL_ITEM_Name.Text = dlg.GetContent();
        }

        private void PN_Icon_Click(object sender, EventArgs e)
        {
            IconPicker dlg = new IconPicker(ressources.Icons);
            dlg.ShowDialog();

            using (XNAUtils utils = new XNAUtils())
                if (dlg.HasAccepted)
                {
                    PN_Icon.Tag = dlg.GetIcon();
                    PN_Icon.BackgroundImage = utils.ConvertToImage(dlg.GetIcon().GetImage());
                }
        }

        private void BTN_DeleteItemObject_Click(object sender, EventArgs e)
        {
            if (LIST_ItemObjects.SelectedItem != null) LIST_ItemObjects.Items.Remove(LIST_ItemObjects.SelectedItem);
        }

        private void BTN_AddAttack_Click(object sender, EventArgs e)
        {
            LIST_SpecialAttacks.Items.Add(new SpecialAttack());
        }

        private void LIST_SpecialAttacks_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (LIST_SpecialAttacks.SelectedItem != null)
            {
                GROUP_SpecialAttacks.Visible = true;

                SpecialAttack o = LIST_SpecialAttacks.SelectedItem as SpecialAttack;

                ES_AttaqueSpeciale.CurrentElement = o.Element;
            }
            else
                GROUP_SpecialAttacks.Visible = false;

            ES_AttaqueSpeciale.Init(LIST_Elements, "");
        }

        private void LBL_ATK_Name_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SingleStringInput dlg = new SingleStringInput("Nom de l'attaque spéciale", LBL_ATK_Name.Text);
            dlg.ShowDialog();

            LBL_ATK_Name.Text = dlg.GetContent();
        }

        private void DDL_ATK_Target_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((AttackType)DDL_ATK_Type.SelectedItem)
            {
                case AttackType.Absolute:
                    LB_ATK_Type.Text = "Une attaque de type absolue passe à travers n'importe quel obstacle. Elle est sûre de réussir mais est quand même affectée par le radius.";
                    break;

                case AttackType.Melee:
                    LB_ATK_Type.Text = "Une attaque de type rapprochée doit être éxécutée à proximité de la cible. Elle peut échouer si la cible est trop loin ou bouge lors de l'éxécution.";
                    break;

                case AttackType.Range:
                    LB_ATK_Type.Text = "Une attaque de type distante doit avoir un chemin sans obstacle pour réussir. Par exemple, il ne peut avoir une plateforme entre l'attaquant et la cible. Elle peut échouer si la cible l'esquive.";
                    break;

                case AttackType.Screen:
                    LB_ATK_Type.Text = "Une attaque de type écran a comme cible toutes les entités se trouvant sur l'écran présent. Autrement dit, tout ce qui peut être vu est attaqué. Elle ne peut échouer.";
                    break;

                default:
                    LB_ATK_Type.Text = "Cette attaque est inconnue.";
                    break;
            }
        }

        private void BTN_AddMonster_Click(object sender, EventArgs e)
        {
            LIST_Monsters.Items.Add(new Monster());
        }

        private void LIST_Monsters_SelectedIndexChanged(object sender, EventArgs e)
        {
            Monster monster = LIST_Monsters.SelectedItem as Monster;

            if (monster != null)
            {
                MON_Statspicker.SetStats(monster.Stats);
                LBL_MonsterName.Text = monster.Name;

                GROUP_Monsters.Visible = true;
            }
            else
            {
                GROUP_Monsters.Visible = false;
            }

            ES_MonstreFaiblesse.Init(LIST_Elements, "");
            ES_MonstrePrimaire.Init(LIST_Elements, "");
        }

        private void LBL_MonsterName_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SingleStringInput dlg = new SingleStringInput("Nom du monstre", LBL_MonsterName.Text);
            dlg.ShowDialog();

            LBL_MonsterName.Text = dlg.GetContent();
        }

        private void BTN_SaveMonster_Click(object sender, EventArgs e)
        {
            if (LBL_MonsterName.Text == LIST_Monsters.SelectedItem.ToString() || !ContainsSameName(LIST_Monsters, LBL_MonsterName.Text))
            {
                Monster monster = new Monster();
                monster.Stats = MON_Statspicker.GenerateStats();
                monster.Name = LBL_MonsterName.Text;
                LIST_Monsters.Items[LIST_Monsters.SelectedIndex] = monster;
            }

            else
                GenerateSaveWarningBox("monstre", LBL_MonsterName.Text);

            ES_MonstreFaiblesse.Init(LIST_Elements, "");
            ES_MonstrePrimaire.Init(LIST_Elements, "");
        }

        void GenerateSaveWarningBox(String Type, String Object)
        {
            MessageBox.Show("L'item '" + Object + "' du type '" + Type + "' existe déjà dans le contexte actuel, il doit avoir un nom unique et significatif.", Type + " existant",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void LIST_Elements_SelectedIndexChanged(object sender, EventArgs e)
        {
            Elemental elements = LIST_Elements.SelectedItem as Elemental;
            if (elements == null) return;
            
            using (XNAUtils utils = new XNAUtils())
                if (elements != null)
                {
                    linkLBL_ELEM_Name.Text = elements.GetName();
                    TB_Description.Text = elements.GetDescription();

                    if (elements.Icon != null) PN_IconElement.BackgroundImage = utils.ConvertToImage(elements.Icon.GetImage());
                    else PN_IconElement.BackgroundImage = null;

                    GROUP_Elements.Visible = true;
                }
                else
                {
                    GROUP_Elements.Visible = false;
                }

            ES_Faiblesses.Init(LIST_Elements, "");
            ES_Resistances.Init(LIST_Elements, "");
            ES_Immunites.Init(LIST_Elements, "");
        }

        private void BTN_SaveElement_Click(object sender, EventArgs e)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                if (linkLBL_ELEM_Name.Text == LIST_Elements.SelectedItem.ToString() || !ContainsSameName(LIST_Elements, linkLBL_ELEM_Name.Text))
                {
                    pIcon icon = new pIcon(utils.ConvertToTexture(PN_IconElement.BackgroundImage), linkLBL_ELEM_Name.Text);
                    Elemental elem = new Elemental(linkLBL_ELEM_Name.Text, icon);

                    elem.Immunities = new List<Elemental>();
                    elem.Resistances = new List<Elemental>();
                    elem.Weaknesses = new List<Elemental>();

                    elem.Immunities = ES_Immunites.UpdateList();
                    elem.Resistances = ES_Resistances.UpdateList();
                    elem.Weaknesses = ES_Faiblesses.UpdateList();

                    elem.SetDescription(TB_Description.Text);

                    LIST_Elements.Items[LIST_Elements.SelectedIndex] = elem;
                }

                else
                    GenerateSaveWarningBox("élément", linkLBL_ELEM_Name.Text);

                ES_Faiblesses.Init(LIST_Elements, linkLBL_ELEM_Name.Text);
                ES_Resistances.Init(LIST_Elements, linkLBL_ELEM_Name.Text);
                ES_Immunites.Init(LIST_Elements, linkLBL_ELEM_Name.Text);
            }
        }

        private void BTN_AddElement_Click(object sender, EventArgs e)
        {
            String Name = "Nouvel élément";

            LIST_Elements.Items.Add(new Elemental(GetNewName(Name)));
        }

        private Boolean ContainsSameName(ListBox List, String Name)
        {
            return List.GetEnumerable<Object>().Where(x => x.ToString() == Name).Count() > 0;
        }

        private String GetNewName(String Name)
        {
            Int32 cpt = 0;
            while (GetOccurences(LIST_Elements, Name + (cpt == 0 ? String.Empty : cpt.ToString())) != 0) cpt++;

            return Name + (cpt == 0 ? String.Empty : cpt.ToString());
        }

        private Int32 GetOccurences(ListBox List, String Name)
        {
            return List.GetEnumerable<Object>().Where(x => x.ToString() == Name).Count();
        }

        private void BTN_DeleteElement_Click(object sender, EventArgs e)
        {
            if (LIST_Elements.SelectedItem != null) LIST_Elements.Items.Remove(LIST_Elements.SelectedItem);

            ES_Faiblesses.Init(LIST_Elements, "");
            ES_Resistances.Init(LIST_Elements, "");
            ES_Immunites.Init(LIST_Elements, "");
        }

        private void linkLBL_ELEM_Name_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SingleStringInput dlg = new SingleStringInput("Nom de l'élément", linkLBL_ELEM_Name.Text);
            dlg.ShowDialog();

            linkLBL_ELEM_Name.Text = dlg.GetContent();
        }

        private void PN_IconElement_Click(object sender, EventArgs e)
        {
            IconPicker dlg = new IconPicker(ressources.Icons);
            dlg.ShowDialog();

            using (XNAUtils utils = new XNAUtils())
                if (dlg.HasAccepted)
                {
                    PN_IconElement.Tag = dlg.GetIcon();
                    PN_IconElement.BackgroundImage = utils.ConvertToImage(dlg.GetIcon().GetImage());
                }
        }

        private void PNAttaque_Click(object sender, EventArgs e)
        {
            IconPicker dlg = new IconPicker(ressources.Icons);
            dlg.ShowDialog();

            using (XNAUtils utils = new XNAUtils())
                if (dlg.HasAccepted)
                {
                    PN_Attaque.Tag = dlg.GetIcon();
                    PN_Attaque.BackgroundImage = utils.ConvertToImage(dlg.GetIcon().GetImage());
                }
        }

        private void BTN_SaveAttack_Click(object sender, EventArgs e)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                if (LBL_ATK_Name.Text == LIST_SpecialAttacks.SelectedItem.ToString() || !ContainsSameName(LIST_SpecialAttacks, LBL_ATK_Name.Text))
                {
                    SpecialAttack atk = new SpecialAttack();
                    pIcon icon = new pIcon(utils.ConvertToTexture(PN_Attaque.BackgroundImage), LBL_ATK_Name.Text);
                    atk.AtkIcon = icon;
                    atk.AtkType = (AttackType)DDL_ATK_Type.SelectedItem;
                    atk.DamageRatio = (float)NUM_ATK_Ratio.Value;
                    atk.Name = LBL_ATK_Name.Text;
                    atk.Radius = (int)NUM_ATK_Radius.Value;
                    atk.AnimTarget = (Animation)DDL_ATK_AnimationTarget.SelectedItem;
                    atk.AnimSelf = (Animation)DDL_ATK_AnimationSelf.SelectedItem;
                    atk.Target = (TargetType)DDL_Cible.SelectedItem;

                    LIST_SpecialAttacks.Items[LIST_SpecialAttacks.SelectedIndex] = atk;
                }
                else
                    GenerateSaveWarningBox("attaque spéciale", LBL_ATK_Name.Text);
            }
        }

        private void DDL_Cible_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((TargetType)DDL_Cible.SelectedItem)
            {
                case TargetType.Attaquant:
                    LB_ATK_Cible.Text = "Une cible de type attaquant représente l'entité qui effectue l'attaque.";
                    break;

                case TargetType.Adversaire:
                    LB_ATK_Cible.Text = "Une cible de type adversaire représente l'entité qui recoit l'attaque.";
                    break;   

                default:
                    LB_ATK_Cible.Text = "Cette cible est inconnue.";
                    break;
            }
        }
    }
}
