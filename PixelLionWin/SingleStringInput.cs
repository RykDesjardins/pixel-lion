﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PixelLionWin
{
    public partial class SingleStringInput : Form
    {
        public SingleStringInput(String title, String Content)
        {
            InitializeComponent();
            Text = title;
            TB_Content.Text = Content;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) Close();
        }

        public String GetContent()
        {
            return TB_Content.Text;
        }
    }
}
