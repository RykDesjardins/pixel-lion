﻿namespace PixelLionWin
{
    partial class IconPicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LIST_Icons = new System.Windows.Forms.ListBox();
            this.PN_Icon = new System.Windows.Forms.Panel();
            this.BTN_Choose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LIST_Icons
            // 
            this.LIST_Icons.FormattingEnabled = true;
            this.LIST_Icons.Location = new System.Drawing.Point(12, 12);
            this.LIST_Icons.Name = "LIST_Icons";
            this.LIST_Icons.Size = new System.Drawing.Size(201, 329);
            this.LIST_Icons.TabIndex = 0;
            this.LIST_Icons.SelectedIndexChanged += new System.EventHandler(this.LIST_Icons_SelectedIndexChanged);
            // 
            // PN_Icon
            // 
            this.PN_Icon.Location = new System.Drawing.Point(219, 12);
            this.PN_Icon.Name = "PN_Icon";
            this.PN_Icon.Size = new System.Drawing.Size(76, 74);
            this.PN_Icon.TabIndex = 1;
            // 
            // BTN_Choose
            // 
            this.BTN_Choose.BackgroundImage = global::PixelLionWin.Properties.Resources.PlayButton;
            this.BTN_Choose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Choose.Enabled = false;
            this.BTN_Choose.Location = new System.Drawing.Point(222, 266);
            this.BTN_Choose.Name = "BTN_Choose";
            this.BTN_Choose.Size = new System.Drawing.Size(75, 75);
            this.BTN_Choose.TabIndex = 2;
            this.BTN_Choose.UseVisualStyleBackColor = true;
            this.BTN_Choose.Click += new System.EventHandler(this.BTN_Choose_Click);
            // 
            // IconPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 353);
            this.Controls.Add(this.BTN_Choose);
            this.Controls.Add(this.PN_Icon);
            this.Controls.Add(this.LIST_Icons);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IconPicker";
            this.Text = "Choix de l\'icone";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LIST_Icons;
        private System.Windows.Forms.Panel PN_Icon;
        private System.Windows.Forms.Button BTN_Choose;
    }
}