﻿namespace PixelLionWin
{
    partial class PixelMathFunctionEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PN_Graph = new System.Windows.Forms.Panel();
            this.BTN_Show = new System.Windows.Forms.Button();
            this.LB_MathAnswer = new System.Windows.Forms.Label();
            this.TB_Yat = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.LB_Fx = new System.Windows.Forms.Label();
            this.TB_Function = new System.Windows.Forms.TextBox();
            this.Menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // Menu
            // 
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(375, 24);
            this.Menu.TabIndex = 3;
            this.Menu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // PN_Graph
            // 
            this.PN_Graph.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.PN_Graph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Graph.Location = new System.Drawing.Point(12, 105);
            this.PN_Graph.Name = "PN_Graph";
            this.PN_Graph.Size = new System.Drawing.Size(345, 345);
            this.PN_Graph.TabIndex = 7;
            // 
            // BTN_Show
            // 
            this.BTN_Show.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BTN_Show.Location = new System.Drawing.Point(272, 65);
            this.BTN_Show.Name = "BTN_Show";
            this.BTN_Show.Size = new System.Drawing.Size(85, 32);
            this.BTN_Show.TabIndex = 8;
            this.BTN_Show.Text = "Show";
            this.BTN_Show.UseVisualStyleBackColor = true;
            this.BTN_Show.Click += new System.EventHandler(this.BTN_Show_Click);
            // 
            // LB_MathAnswer
            // 
            this.LB_MathAnswer.AutoSize = true;
            this.LB_MathAnswer.ForeColor = System.Drawing.Color.Green;
            this.LB_MathAnswer.Location = new System.Drawing.Point(196, 64);
            this.LB_MathAnswer.Name = "LB_MathAnswer";
            this.LB_MathAnswer.Size = new System.Drawing.Size(0, 13);
            this.LB_MathAnswer.TabIndex = 13;
            // 
            // TB_Yat
            // 
            this.TB_Yat.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_Yat.Location = new System.Drawing.Point(88, 65);
            this.TB_Yat.Name = "TB_Yat";
            this.TB_Yat.Size = new System.Drawing.Size(100, 32);
            this.TB_Yat.TabIndex = 12;
            this.TB_Yat.TextChanged += new System.EventHandler(this.TB_Yat_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "f(x) for x =";
            // 
            // LB_Fx
            // 
            this.LB_Fx.AutoSize = true;
            this.LB_Fx.Location = new System.Drawing.Point(52, 36);
            this.LB_Fx.Name = "LB_Fx";
            this.LB_Fx.Size = new System.Drawing.Size(30, 13);
            this.LB_Fx.TabIndex = 10;
            this.LB_Fx.Text = "f(x) =";
            // 
            // TB_Function
            // 
            this.TB_Function.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_Function.Location = new System.Drawing.Point(88, 24);
            this.TB_Function.Name = "TB_Function";
            this.TB_Function.Size = new System.Drawing.Size(271, 32);
            this.TB_Function.TabIndex = 9;
            // 
            // PixelMathFunctionEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 462);
            this.Controls.Add(this.LB_MathAnswer);
            this.Controls.Add(this.TB_Yat);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LB_Fx);
            this.Controls.Add(this.TB_Function);
            this.Controls.Add(this.BTN_Show);
            this.Controls.Add(this.PN_Graph);
            this.Controls.Add(this.Menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.Menu;
            this.Name = "PixelMathFunctionEditor";
            this.Text = "PixelMathFunctionEditor";
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Panel PN_Graph;
        private System.Windows.Forms.Button BTN_Show;
        private System.Windows.Forms.Label LB_MathAnswer;
        private System.Windows.Forms.TextBox TB_Yat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LB_Fx;
        private System.Windows.Forms.TextBox TB_Function;

    }
}