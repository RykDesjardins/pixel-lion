﻿namespace PixelLionWin
{
    partial class PopupNotification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BTN_Close = new System.Windows.Forms.Button();
            this.PN_Icon = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "LB_Message";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(98, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(319, 70);
            this.label2.TabIndex = 2;
            this.label2.Text = "LB_Details";
            // 
            // BTN_Close
            // 
            this.BTN_Close.BackColor = System.Drawing.Color.Transparent;
            this.BTN_Close.BackgroundImage = global::PixelLionWin.Properties.Resources.CloseIcon;
            this.BTN_Close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Close.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown;
            this.BTN_Close.FlatAppearance.BorderSize = 0;
            this.BTN_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_Close.Location = new System.Drawing.Point(423, 6);
            this.BTN_Close.Name = "BTN_Close";
            this.BTN_Close.Size = new System.Drawing.Size(25, 25);
            this.BTN_Close.TabIndex = 2;
            this.BTN_Close.UseVisualStyleBackColor = false;
            this.BTN_Close.Click += new System.EventHandler(this.BTN_Close_Click);
            // 
            // PN_Icon
            // 
            this.PN_Icon.BackColor = System.Drawing.Color.Transparent;
            this.PN_Icon.BackgroundImage = global::PixelLionWin.Properties.Resources.AcceptIcon;
            this.PN_Icon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PN_Icon.Location = new System.Drawing.Point(5, 29);
            this.PN_Icon.Name = "PN_Icon";
            this.PN_Icon.Size = new System.Drawing.Size(87, 77);
            this.PN_Icon.TabIndex = 0;
            // 
            // PopupNotification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::PixelLionWin.Properties.Resources.PopupBackground;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(454, 112);
            this.ControlBox = false;
            this.Controls.Add(this.BTN_Close);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PN_Icon);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PopupNotification";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "PopupNotification";
            this.TopMost = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PopupNotification_FormClosed);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.PopupNotification_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PN_Icon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BTN_Close;
    }
}