﻿namespace PixelLionWin
{
    partial class LayerSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTN_Layer3 = new System.Windows.Forms.Button();
            this.BTN_Layer2 = new System.Windows.Forms.Button();
            this.BTN_Layer1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BTN_Layer3
            // 
            this.BTN_Layer3.BackgroundImage = global::PixelLionWin.Properties.Resources.Layer3Button;
            this.BTN_Layer3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Layer3.Location = new System.Drawing.Point(116, 12);
            this.BTN_Layer3.Name = "BTN_Layer3";
            this.BTN_Layer3.Size = new System.Drawing.Size(45, 45);
            this.BTN_Layer3.TabIndex = 3;
            this.BTN_Layer3.UseVisualStyleBackColor = true;
            this.BTN_Layer3.Click += new System.EventHandler(this.BTN_Layer3_Click);
            // 
            // BTN_Layer2
            // 
            this.BTN_Layer2.BackgroundImage = global::PixelLionWin.Properties.Resources.Layer2Button;
            this.BTN_Layer2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Layer2.Location = new System.Drawing.Point(65, 12);
            this.BTN_Layer2.Name = "BTN_Layer2";
            this.BTN_Layer2.Size = new System.Drawing.Size(45, 45);
            this.BTN_Layer2.TabIndex = 2;
            this.BTN_Layer2.UseVisualStyleBackColor = true;
            this.BTN_Layer2.Click += new System.EventHandler(this.BTN_Layer2_Click);
            // 
            // BTN_Layer1
            // 
            this.BTN_Layer1.BackgroundImage = global::PixelLionWin.Properties.Resources.Layer1Button;
            this.BTN_Layer1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Layer1.Location = new System.Drawing.Point(14, 12);
            this.BTN_Layer1.Name = "BTN_Layer1";
            this.BTN_Layer1.Size = new System.Drawing.Size(45, 45);
            this.BTN_Layer1.TabIndex = 1;
            this.BTN_Layer1.UseVisualStyleBackColor = true;
            this.BTN_Layer1.Click += new System.EventHandler(this.BTN_Layer1_Click);
            // 
            // LayerSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(172, 65);
            this.Controls.Add(this.BTN_Layer3);
            this.Controls.Add(this.BTN_Layer2);
            this.Controls.Add(this.BTN_Layer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LayerSelector";
            this.Text = "Quel layer?";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BTN_Layer3;
        private System.Windows.Forms.Button BTN_Layer2;
        private System.Windows.Forms.Button BTN_Layer1;
    }
}