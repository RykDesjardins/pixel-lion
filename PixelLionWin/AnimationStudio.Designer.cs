﻿namespace PixelLionWin
{
    partial class AnimationStudio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LIST_Animations = new System.Windows.Forms.ListBox();
            this.PN_Canvas = new System.Windows.Forms.Panel();
            this.LBL_AnimationName = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.BTN_AddAnimation = new System.Windows.Forms.Button();
            this.BTN_RemoveAnimation = new System.Windows.Forms.Button();
            this.GROUP_Animation = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DDL_Target = new System.Windows.Forms.ComboBox();
            this.LB_VersionTitle = new System.Windows.Forms.Label();
            this.BTN_PutEntirePattern = new System.Windows.Forms.Button();
            this.LB_MousePosition = new System.Windows.Forms.Label();
            this.GROUP_Pattern = new System.Windows.Forms.GroupBox();
            this.PN_Pattern = new System.Windows.Forms.Panel();
            this.BTN_Flash = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.BTN_LoadPattern = new System.Windows.Forms.Button();
            this.animationTimeLine1 = new PixelLionWin.AnimationTimeLine();
            this.groupBox1.SuspendLayout();
            this.GROUP_Animation.SuspendLayout();
            this.GROUP_Pattern.SuspendLayout();
            this.SuspendLayout();
            // 
            // LIST_Animations
            // 
            this.LIST_Animations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LIST_Animations.FormattingEnabled = true;
            this.LIST_Animations.Location = new System.Drawing.Point(15, 20);
            this.LIST_Animations.Name = "LIST_Animations";
            this.LIST_Animations.Size = new System.Drawing.Size(238, 758);
            this.LIST_Animations.TabIndex = 0;
            this.LIST_Animations.SelectedIndexChanged += new System.EventHandler(this.LIST_Animations_SelectedIndexChanged);
            // 
            // PN_Canvas
            // 
            this.PN_Canvas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_Canvas.BackColor = System.Drawing.Color.Black;
            this.PN_Canvas.Cursor = System.Windows.Forms.Cursors.Cross;
            this.PN_Canvas.Location = new System.Drawing.Point(72, 77);
            this.PN_Canvas.Name = "PN_Canvas";
            this.PN_Canvas.Size = new System.Drawing.Size(917, 375);
            this.PN_Canvas.TabIndex = 2;
            this.PN_Canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.PN_Canvas_Paint);
            this.PN_Canvas.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PN_Canvas_MouseClick);
            this.PN_Canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PN_Canvas_MouseMove);
            this.PN_Canvas.Resize += new System.EventHandler(this.PN_Canvas_Resize);
            // 
            // LBL_AnimationName
            // 
            this.LBL_AnimationName.AutoSize = true;
            this.LBL_AnimationName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_AnimationName.Location = new System.Drawing.Point(13, 20);
            this.LBL_AnimationName.Name = "LBL_AnimationName";
            this.LBL_AnimationName.Size = new System.Drawing.Size(259, 31);
            this.LBL_AnimationName.TabIndex = 3;
            this.LBL_AnimationName.TabStop = true;
            this.LBL_AnimationName.Text = "[Nom de l\'animation]";
            this.LBL_AnimationName.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LBL_AnimationName_LinkClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.BTN_Save);
            this.groupBox1.Controls.Add(this.LIST_Animations);
            this.groupBox1.Controls.Add(this.BTN_AddAnimation);
            this.groupBox1.Controls.Add(this.BTN_RemoveAnimation);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(267, 849);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Liste des animations";
            // 
            // BTN_Save
            // 
            this.BTN_Save.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BTN_Save.BackgroundImage = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.BTN_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Save.Location = new System.Drawing.Point(203, 784);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(50, 50);
            this.BTN_Save.TabIndex = 2;
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // BTN_AddAnimation
            // 
            this.BTN_AddAnimation.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BTN_AddAnimation.BackgroundImage = global::PixelLionWin.Properties.Resources.AddIcon;
            this.BTN_AddAnimation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_AddAnimation.Location = new System.Drawing.Point(15, 784);
            this.BTN_AddAnimation.Name = "BTN_AddAnimation";
            this.BTN_AddAnimation.Size = new System.Drawing.Size(50, 50);
            this.BTN_AddAnimation.TabIndex = 1;
            this.BTN_AddAnimation.UseVisualStyleBackColor = true;
            this.BTN_AddAnimation.Click += new System.EventHandler(this.BTN_AddAnimation_Click);
            // 
            // BTN_RemoveAnimation
            // 
            this.BTN_RemoveAnimation.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BTN_RemoveAnimation.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.BTN_RemoveAnimation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_RemoveAnimation.Location = new System.Drawing.Point(71, 784);
            this.BTN_RemoveAnimation.Name = "BTN_RemoveAnimation";
            this.BTN_RemoveAnimation.Size = new System.Drawing.Size(50, 50);
            this.BTN_RemoveAnimation.TabIndex = 1;
            this.BTN_RemoveAnimation.UseVisualStyleBackColor = true;
            this.BTN_RemoveAnimation.Click += new System.EventHandler(this.BTN_RemoveAnimation_Click);
            // 
            // GROUP_Animation
            // 
            this.GROUP_Animation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GROUP_Animation.Controls.Add(this.label1);
            this.GROUP_Animation.Controls.Add(this.DDL_Target);
            this.GROUP_Animation.Controls.Add(this.LB_VersionTitle);
            this.GROUP_Animation.Controls.Add(this.BTN_PutEntirePattern);
            this.GROUP_Animation.Controls.Add(this.LB_MousePosition);
            this.GROUP_Animation.Controls.Add(this.GROUP_Pattern);
            this.GROUP_Animation.Controls.Add(this.BTN_Flash);
            this.GROUP_Animation.Controls.Add(this.button2);
            this.GROUP_Animation.Controls.Add(this.BTN_LoadPattern);
            this.GROUP_Animation.Controls.Add(this.animationTimeLine1);
            this.GROUP_Animation.Controls.Add(this.LBL_AnimationName);
            this.GROUP_Animation.Controls.Add(this.PN_Canvas);
            this.GROUP_Animation.Location = new System.Drawing.Point(285, 12);
            this.GROUP_Animation.Name = "GROUP_Animation";
            this.GROUP_Animation.Size = new System.Drawing.Size(1127, 849);
            this.GROUP_Animation.TabIndex = 5;
            this.GROUP_Animation.TabStop = false;
            this.GROUP_Animation.Text = "Animation";
            this.GROUP_Animation.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(995, 415);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Cible";
            // 
            // DDL_Target
            // 
            this.DDL_Target.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DDL_Target.FormattingEnabled = true;
            this.DDL_Target.Location = new System.Drawing.Point(995, 431);
            this.DDL_Target.Name = "DDL_Target";
            this.DDL_Target.Size = new System.Drawing.Size(110, 21);
            this.DDL_Target.TabIndex = 12;
            this.DDL_Target.SelectedIndexChanged += new System.EventHandler(this.DDL_Target_SelectedIndexChanged);
            // 
            // LB_VersionTitle
            // 
            this.LB_VersionTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LB_VersionTitle.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.LB_VersionTitle.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.LB_VersionTitle.Location = new System.Drawing.Point(861, 16);
            this.LB_VersionTitle.Name = "LB_VersionTitle";
            this.LB_VersionTitle.Size = new System.Drawing.Size(260, 23);
            this.LB_VersionTitle.TabIndex = 11;
            this.LB_VersionTitle.Text = "[Form title + Version numbering]";
            this.LB_VersionTitle.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // BTN_PutEntirePattern
            // 
            this.BTN_PutEntirePattern.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BTN_PutEntirePattern.Location = new System.Drawing.Point(995, 196);
            this.BTN_PutEntirePattern.Name = "BTN_PutEntirePattern";
            this.BTN_PutEntirePattern.Size = new System.Drawing.Size(110, 26);
            this.BTN_PutEntirePattern.TabIndex = 10;
            this.BTN_PutEntirePattern.Text = "Motif en entier";
            this.BTN_PutEntirePattern.UseVisualStyleBackColor = true;
            this.BTN_PutEntirePattern.Click += new System.EventHandler(this.BTN_PutEntirePattern_Click);
            // 
            // LB_MousePosition
            // 
            this.LB_MousePosition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LB_MousePosition.Location = new System.Drawing.Point(733, 61);
            this.LB_MousePosition.Name = "LB_MousePosition";
            this.LB_MousePosition.Size = new System.Drawing.Size(256, 13);
            this.LB_MousePosition.TabIndex = 9;
            this.LB_MousePosition.Text = "0 x 0";
            this.LB_MousePosition.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // GROUP_Pattern
            // 
            this.GROUP_Pattern.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GROUP_Pattern.Controls.Add(this.PN_Pattern);
            this.GROUP_Pattern.Location = new System.Drawing.Point(19, 557);
            this.GROUP_Pattern.Name = "GROUP_Pattern";
            this.GROUP_Pattern.Size = new System.Drawing.Size(1086, 277);
            this.GROUP_Pattern.TabIndex = 8;
            this.GROUP_Pattern.TabStop = false;
            this.GROUP_Pattern.Text = "Motif";
            // 
            // PN_Pattern
            // 
            this.PN_Pattern.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PN_Pattern.AutoScroll = true;
            this.PN_Pattern.BackColor = System.Drawing.Color.LavenderBlush;
            this.PN_Pattern.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Pattern.Cursor = System.Windows.Forms.Cursors.Cross;
            this.PN_Pattern.ForeColor = System.Drawing.SystemColors.ControlText;
            this.PN_Pattern.Location = new System.Drawing.Point(6, 19);
            this.PN_Pattern.Name = "PN_Pattern";
            this.PN_Pattern.Size = new System.Drawing.Size(1074, 241);
            this.PN_Pattern.TabIndex = 0;
            // 
            // BTN_Flash
            // 
            this.BTN_Flash.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BTN_Flash.Location = new System.Drawing.Point(995, 109);
            this.BTN_Flash.Name = "BTN_Flash";
            this.BTN_Flash.Size = new System.Drawing.Size(110, 26);
            this.BTN_Flash.TabIndex = 7;
            this.BTN_Flash.Text = "Flash de couleur";
            this.BTN_Flash.UseVisualStyleBackColor = true;
            this.BTN_Flash.Click += new System.EventHandler(this.BTN_Flash_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(995, 77);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 26);
            this.button2.TabIndex = 6;
            this.button2.Text = "Famille de particule";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // BTN_LoadPattern
            // 
            this.BTN_LoadPattern.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BTN_LoadPattern.Location = new System.Drawing.Point(995, 164);
            this.BTN_LoadPattern.Name = "BTN_LoadPattern";
            this.BTN_LoadPattern.Size = new System.Drawing.Size(110, 26);
            this.BTN_LoadPattern.TabIndex = 5;
            this.BTN_LoadPattern.Text = "Charger un motif";
            this.BTN_LoadPattern.UseVisualStyleBackColor = true;
            this.BTN_LoadPattern.Click += new System.EventHandler(this.BTN_LoadPattern_Click);
            // 
            // animationTimeLine1
            // 
            this.animationTimeLine1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.animationTimeLine1.BackColor = System.Drawing.Color.FloralWhite;
            this.animationTimeLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.animationTimeLine1.Location = new System.Drawing.Point(72, 467);
            this.animationTimeLine1.Name = "animationTimeLine1";
            this.animationTimeLine1.Size = new System.Drawing.Size(917, 75);
            this.animationTimeLine1.TabIndex = 4;
            this.animationTimeLine1.FrameChanged += new PixelLionWin.AnimationFrameEventHandler(this.animationTimeLine1_FrameChanged);
            // 
            // AnimationStudio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(1430, 873);
            this.Controls.Add(this.GROUP_Animation);
            this.Controls.Add(this.groupBox1);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1446, 898);
            this.Name = "AnimationStudio";
            this.Text = "Animation Studio";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AnimationManager_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.GROUP_Animation.ResumeLayout(false);
            this.GROUP_Animation.PerformLayout();
            this.GROUP_Pattern.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LIST_Animations;
        private System.Windows.Forms.Button BTN_AddAnimation;
        private System.Windows.Forms.Button BTN_RemoveAnimation;
        private System.Windows.Forms.Panel PN_Canvas;
        private System.Windows.Forms.LinkLabel LBL_AnimationName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox GROUP_Animation;
        private AnimationTimeLine animationTimeLine1;
        private System.Windows.Forms.Button BTN_LoadPattern;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button BTN_Flash;
        private System.Windows.Forms.GroupBox GROUP_Pattern;
        private System.Windows.Forms.Panel PN_Pattern;
        private System.Windows.Forms.Label LB_MousePosition;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Button BTN_PutEntirePattern;
        private System.Windows.Forms.Label LB_VersionTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox DDL_Target;
    }
}