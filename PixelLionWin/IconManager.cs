﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class IconManager : Form
    {
        public Boolean HasAccepted = false;

        public IconManager(List<pIcon> icons)
        {
            InitializeComponent();

            if (icons != null) LIST_Icons.Items.AddRange(icons.ToArray());
        }

        private void BTN_AddItemObject_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Multiselect = true;
            dlg.Filter = "Images|*.jpg;*.jpeg;*.png;*.bmp";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (XNAUtils utils = new XNAUtils())
                foreach (String item in dlg.FileNames)
                {
                    LIST_Icons.Items.Add(new pIcon(utils.ConvertToTexture(Image.FromFile(item)), item.Substring(item.LastIndexOf("\\"))));
                }
            }
        }

        private void BTN_DeleteItemObject_Click(object sender, EventArgs e)
        {
            if (LIST_Icons.SelectedItem != null)
                LIST_Icons.Items.Remove(LIST_Icons.SelectedItem);            
        }

        private void LIST_Icons_SelectedIndexChanged(object sender, EventArgs e)
        {
            PN_Preview.BackgroundImageLayout = ImageLayout.Center;

            if (LIST_Icons.SelectedItem != null) using (XNAUtils utils = new XNAUtils()) 
                PN_Preview.BackgroundImage = utils.ConvertToImage(((pIcon)LIST_Icons.SelectedItem).GetImage());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HasAccepted = true;
            Close();
        }

        public List<pIcon> GetIcons()
        {
            List<pIcon> icons = new List<pIcon>();
            foreach (pIcon item in LIST_Icons.Items)
            {
                icons.Add(item);
            }

            return icons;
        }
    }
}
