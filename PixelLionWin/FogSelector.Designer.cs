﻿namespace PixelLionWin
{
    partial class FogSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FogSelector));
            this.PN_Preview = new System.Windows.Forms.Panel();
            this.BTN_Open = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TRACK_Opacity = new System.Windows.Forms.TrackBar();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.LB_Opacity = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TRACK_Opacity)).BeginInit();
            this.SuspendLayout();
            // 
            // PN_Preview
            // 
            this.PN_Preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Preview.Location = new System.Drawing.Point(12, 12);
            this.PN_Preview.Name = "PN_Preview";
            this.PN_Preview.Size = new System.Drawing.Size(541, 298);
            this.PN_Preview.TabIndex = 0;
            // 
            // BTN_Open
            // 
            this.BTN_Open.BackgroundImage = global::PixelLionWin.Properties.Resources.OpenIcon;
            this.BTN_Open.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Open.Location = new System.Drawing.Point(447, 312);
            this.BTN_Open.Name = "BTN_Open";
            this.BTN_Open.Size = new System.Drawing.Size(50, 50);
            this.BTN_Open.TabIndex = 1;
            this.BTN_Open.UseVisualStyleBackColor = true;
            this.BTN_Open.Click += new System.EventHandler(this.BTN_Open_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 316);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Opacité";
            // 
            // TRACK_Opacity
            // 
            this.TRACK_Opacity.LargeChange = 50;
            this.TRACK_Opacity.Location = new System.Drawing.Point(2, 332);
            this.TRACK_Opacity.Maximum = 255;
            this.TRACK_Opacity.Name = "TRACK_Opacity";
            this.TRACK_Opacity.Size = new System.Drawing.Size(439, 45);
            this.TRACK_Opacity.SmallChange = 5;
            this.TRACK_Opacity.TabIndex = 3;
            this.TRACK_Opacity.TickFrequency = 5;
            this.TRACK_Opacity.Scroll += new System.EventHandler(this.TRACK_Opacity_Scroll);
            // 
            // BTN_Save
            // 
            this.BTN_Save.BackgroundImage = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.BTN_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Save.Location = new System.Drawing.Point(503, 312);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(50, 50);
            this.BTN_Save.TabIndex = 1;
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // LB_Opacity
            // 
            this.LB_Opacity.AutoSize = true;
            this.LB_Opacity.Location = new System.Drawing.Point(62, 316);
            this.LB_Opacity.Name = "LB_Opacity";
            this.LB_Opacity.Size = new System.Drawing.Size(13, 13);
            this.LB_Opacity.TabIndex = 4;
            this.LB_Opacity.Text = "0";
            // 
            // FogSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 374);
            this.Controls.Add(this.LB_Opacity);
            this.Controls.Add(this.TRACK_Opacity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.BTN_Open);
            this.Controls.Add(this.PN_Preview);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FogSelector";
            this.Text = "Brouillard";
            ((System.ComponentModel.ISupportInitialize)(this.TRACK_Opacity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PN_Preview;
        private System.Windows.Forms.Button BTN_Open;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar TRACK_Opacity;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Label LB_Opacity;
    }
}