﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class StatsPicker : UserControl
    {
        public StatsPicker()
        {
            InitializeComponent();
        }

        public void SetStats(CharactStats stats)
        {
            NUM_HP.Value = stats.HP;
            NUM_MP.Value = stats.MP;
            NUM_STR.Value = stats.STR;
            NUM_DEF.Value = stats.DEF;
            NUM_INT.Value = stats.INT;
            NUM_RES.Value = stats.RES;

            Element_Select_Primary.CurrentElement = stats.PrimaryElement;
            Element_Select_Weakness.CurrentElement = stats.WeaknessElement;

            NUM_EXP.Value = stats.EXP;
            NUM_MONEY.Value = stats.MONEY;
        }

        public CharactStats GenerateStats()
        {
            CharactStats stats = new CharactStats();

            stats.HP = Convert.ToInt32(NUM_HP.Value);
            stats.MP = Convert.ToInt32(NUM_MP.Value);
            stats.STR = Convert.ToInt32(NUM_STR.Value);
            stats.DEF = Convert.ToInt32(NUM_DEF.Value);
            stats.INT = Convert.ToInt32(NUM_INT.Value);
            stats.RES = Convert.ToInt32(NUM_RES.Value);

            stats.PrimaryElement = Element_Select_Primary.CurrentElement;
            stats.WeaknessElement = Element_Select_Weakness.CurrentElement;

            stats.EXP = Convert.ToInt32(NUM_EXP.Value);
            stats.MONEY = Convert.ToInt32(NUM_MONEY.Value);

            return stats;
        }
    }
}
