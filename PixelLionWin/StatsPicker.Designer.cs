﻿namespace PixelLionWin
{
    partial class StatsPicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.NUM_HP = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.NUM_MP = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.NUM_STR = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.NUM_DEF = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.NUM_INT = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.NUM_RES = new System.Windows.Forms.NumericUpDown();
            this.Element_Select_Primary = new PixelLionWin.ElementalSelector();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Element_Select_Weakness = new PixelLionWin.ElementalSelector();
            this.label7 = new System.Windows.Forms.Label();
            this.NUM_EXP = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.NUM_MONEY = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_HP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_STR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_DEF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_INT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_RES)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_EXP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MONEY)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "HP";
            // 
            // NUM_HP
            // 
            this.NUM_HP.Location = new System.Drawing.Point(41, 15);
            this.NUM_HP.Name = "NUM_HP";
            this.NUM_HP.Size = new System.Drawing.Size(120, 20);
            this.NUM_HP.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "MP";
            // 
            // NUM_MP
            // 
            this.NUM_MP.Location = new System.Drawing.Point(41, 50);
            this.NUM_MP.Name = "NUM_MP";
            this.NUM_MP.Size = new System.Drawing.Size(120, 20);
            this.NUM_MP.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "STR";
            // 
            // NUM_STR
            // 
            this.NUM_STR.Location = new System.Drawing.Point(41, 106);
            this.NUM_STR.Name = "NUM_STR";
            this.NUM_STR.Size = new System.Drawing.Size(120, 20);
            this.NUM_STR.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "DEF";
            // 
            // NUM_DEF
            // 
            this.NUM_DEF.Location = new System.Drawing.Point(41, 132);
            this.NUM_DEF.Name = "NUM_DEF";
            this.NUM_DEF.Size = new System.Drawing.Size(120, 20);
            this.NUM_DEF.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "INT";
            // 
            // NUM_INT
            // 
            this.NUM_INT.Location = new System.Drawing.Point(41, 158);
            this.NUM_INT.Name = "NUM_INT";
            this.NUM_INT.Size = new System.Drawing.Size(120, 20);
            this.NUM_INT.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 186);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "RES";
            // 
            // NUM_RES
            // 
            this.NUM_RES.Location = new System.Drawing.Point(41, 184);
            this.NUM_RES.Name = "NUM_RES";
            this.NUM_RES.Size = new System.Drawing.Size(120, 20);
            this.NUM_RES.TabIndex = 1;
            // 
            // Element_Select_Primary
            // 
            this.Element_Select_Primary.Location = new System.Drawing.Point(8, 19);
            this.Element_Select_Primary.Name = "Element_Select_Primary";
            this.Element_Select_Primary.Size = new System.Drawing.Size(112, 259);
            this.Element_Select_Primary.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Element_Select_Primary);
            this.groupBox1.Location = new System.Drawing.Point(171, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(129, 283);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Élément primaire";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Element_Select_Weakness);
            this.groupBox2.Location = new System.Drawing.Point(317, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(129, 283);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Faiblesse";
            // 
            // Element_Select_Weakness
            // 
            this.Element_Select_Weakness.Location = new System.Drawing.Point(8, 19);
            this.Element_Select_Weakness.Name = "Element_Select_Weakness";
            this.Element_Select_Weakness.Size = new System.Drawing.Size(112, 259);
            this.Element_Select_Weakness.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 248);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "EXP";
            // 
            // NUM_EXP
            // 
            this.NUM_EXP.Location = new System.Drawing.Point(41, 246);
            this.NUM_EXP.Name = "NUM_EXP";
            this.NUM_EXP.Size = new System.Drawing.Size(120, 20);
            this.NUM_EXP.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 274);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "$$";
            // 
            // NUM_MONEY
            // 
            this.NUM_MONEY.Location = new System.Drawing.Point(41, 272);
            this.NUM_MONEY.Name = "NUM_MONEY";
            this.NUM_MONEY.Size = new System.Drawing.Size(120, 20);
            this.NUM_MONEY.TabIndex = 1;
            // 
            // StatsPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.NUM_MONEY);
            this.Controls.Add(this.NUM_RES);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.NUM_EXP);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.NUM_INT);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.NUM_DEF);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.NUM_STR);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.NUM_MP);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NUM_HP);
            this.Controls.Add(this.label1);
            this.Name = "StatsPicker";
            this.Size = new System.Drawing.Size(464, 305);
            ((System.ComponentModel.ISupportInitialize)(this.NUM_HP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_STR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_DEF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_INT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_RES)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NUM_EXP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_MONEY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUM_HP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown NUM_MP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown NUM_STR;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown NUM_DEF;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUM_INT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown NUM_RES;
        private ElementalSelector Element_Select_Primary;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private ElementalSelector Element_Select_Weakness;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown NUM_EXP;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown NUM_MONEY;
    }
}
