﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PixelLionWin
{
    public partial class SelectedTile : UserControl
    {
        public SelectedTile()
        {
            InitializeComponent();
        }

        private void SelectedTile_Paint(object sender, PaintEventArgs e)
        {
            this.BackColor = Color.FromArgb(128, 255, 255, 255);
            using (Graphics gr = CreateGraphics())
            {
                Pen pen = new Pen(Color.Black, 1);
                pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                gr.DrawRectangle(pen, 0, 0, this.Size.Width - 1, this.Size.Height - 1);
                pen.Dispose();
            }
        }

        // Method to click through the control
        protected override void WndProc(ref Message m)
        {
            const int WM_NCHITTEST = 0x0084;
            const int HTTRANSPARENT = (-1);

            if (m.Msg == WM_NCHITTEST)
                m.Result = (IntPtr)HTTRANSPARENT;
            else
                base.WndProc(ref m);
        }
    }
}
