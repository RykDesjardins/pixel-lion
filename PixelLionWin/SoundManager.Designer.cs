﻿namespace PixelLionWin
{
    partial class SoundManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SoundManager));
            this.BTN_Save = new System.Windows.Forms.Button();
            this.BTN_Stop = new System.Windows.Forms.Button();
            this.BTN_Play = new System.Windows.Forms.Button();
            this.BTN_Remove = new System.Windows.Forms.Button();
            this.BTN_Add = new System.Windows.Forms.Button();
            this.LIST_Sounds = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // BTN_Save
            // 
            this.BTN_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BTN_Save.BackgroundImage = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.BTN_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Save.Location = new System.Drawing.Point(201, 204);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(40, 40);
            this.BTN_Save.TabIndex = 4;
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // BTN_Stop
            // 
            this.BTN_Stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BTN_Stop.BackgroundImage = global::PixelLionWin.Properties.Resources.MediaStopButton;
            this.BTN_Stop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Stop.Location = new System.Drawing.Point(201, 58);
            this.BTN_Stop.Name = "BTN_Stop";
            this.BTN_Stop.Size = new System.Drawing.Size(40, 40);
            this.BTN_Stop.TabIndex = 6;
            this.BTN_Stop.UseVisualStyleBackColor = true;
            this.BTN_Stop.Click += new System.EventHandler(this.BTN_Stop_Click);
            // 
            // BTN_Play
            // 
            this.BTN_Play.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BTN_Play.BackgroundImage = global::PixelLionWin.Properties.Resources.PlayButton;
            this.BTN_Play.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Play.Location = new System.Drawing.Point(201, 12);
            this.BTN_Play.Name = "BTN_Play";
            this.BTN_Play.Size = new System.Drawing.Size(40, 40);
            this.BTN_Play.TabIndex = 7;
            this.BTN_Play.UseVisualStyleBackColor = true;
            this.BTN_Play.Click += new System.EventHandler(this.BTN_Play_Click);
            // 
            // BTN_Remove
            // 
            this.BTN_Remove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BTN_Remove.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.BTN_Remove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Remove.Location = new System.Drawing.Point(58, 204);
            this.BTN_Remove.Name = "BTN_Remove";
            this.BTN_Remove.Size = new System.Drawing.Size(40, 40);
            this.BTN_Remove.TabIndex = 5;
            this.BTN_Remove.UseVisualStyleBackColor = true;
            this.BTN_Remove.Click += new System.EventHandler(this.BTN_Remove_Click);
            // 
            // BTN_Add
            // 
            this.BTN_Add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BTN_Add.BackgroundImage = global::PixelLionWin.Properties.Resources.AddIcon;
            this.BTN_Add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Add.Location = new System.Drawing.Point(12, 204);
            this.BTN_Add.Name = "BTN_Add";
            this.BTN_Add.Size = new System.Drawing.Size(40, 40);
            this.BTN_Add.TabIndex = 3;
            this.BTN_Add.UseVisualStyleBackColor = true;
            this.BTN_Add.Click += new System.EventHandler(this.BTN_Add_Click);
            // 
            // LIST_Sounds
            // 
            this.LIST_Sounds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LIST_Sounds.FormattingEnabled = true;
            this.LIST_Sounds.Location = new System.Drawing.Point(12, 12);
            this.LIST_Sounds.MinimumSize = new System.Drawing.Size(183, 186);
            this.LIST_Sounds.Name = "LIST_Sounds";
            this.LIST_Sounds.Size = new System.Drawing.Size(183, 186);
            this.LIST_Sounds.TabIndex = 2;
            this.LIST_Sounds.DoubleClick += new System.EventHandler(this.LIST_Sounds_DoubleClick);
            // 
            // SoundManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 258);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.BTN_Stop);
            this.Controls.Add(this.BTN_Play);
            this.Controls.Add(this.BTN_Remove);
            this.Controls.Add(this.BTN_Add);
            this.Controls.Add(this.LIST_Sounds);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(269, 296);
            this.Name = "SoundManager";
            this.Text = "Gestion des sons";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SoundManager_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Button BTN_Stop;
        private System.Windows.Forms.Button BTN_Play;
        private System.Windows.Forms.Button BTN_Remove;
        private System.Windows.Forms.Button BTN_Add;
        private System.Windows.Forms.ListBox LIST_Sounds;
    }
}