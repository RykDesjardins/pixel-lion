﻿namespace PixelLionWin
{
    partial class SaveClosingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelText = new System.Windows.Forms.Label();
            this.BTN_DontSave = new System.Windows.Forms.Button();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelText
            // 
            this.LabelText.AutoSize = true;
            this.LabelText.BackColor = System.Drawing.Color.Transparent;
            this.LabelText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelText.Location = new System.Drawing.Point(12, 9);
            this.LabelText.Name = "LabelText";
            this.LabelText.Size = new System.Drawing.Size(329, 30);
            this.LabelText.TabIndex = 0;
            this.LabelText.Text = "Des changements ont été faits sans avoir été sauvegardés. \r\nVoulez-vous sauvegard" +
    "er le tout avant de fermer ";
            // 
            // BTN_DontSave
            // 
            this.BTN_DontSave.BackgroundImage = global::PixelLionWin.Properties.Resources.CloseIcon;
            this.BTN_DontSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_DontSave.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTN_DontSave.Location = new System.Drawing.Point(304, 57);
            this.BTN_DontSave.Name = "BTN_DontSave";
            this.BTN_DontSave.Size = new System.Drawing.Size(40, 40);
            this.BTN_DontSave.TabIndex = 1;
            this.BTN_DontSave.UseVisualStyleBackColor = true;
            this.BTN_DontSave.Click += new System.EventHandler(this.BTN_DontSave_Click);
            // 
            // BTN_Save
            // 
            this.BTN_Save.BackgroundImage = global::PixelLionWin.Properties.Resources.AcceptIcon;
            this.BTN_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Save.Location = new System.Drawing.Point(350, 57);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(48, 40);
            this.BTN_Save.TabIndex = 2;
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::PixelLionWin.Properties.Resources.AeroEffect;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.LabelText);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(412, 48);
            this.panel1.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.pictureBox1.Location = new System.Drawing.Point(12, 54);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 75);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // ClosingPLForm
            // 
            this.AcceptButton = this.BTN_Save;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MintCream;
            this.CancelButton = this.BTN_DontSave;
            this.ClientSize = new System.Drawing.Size(412, 109);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.BTN_DontSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ClosingPLForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sauvegarder les changements?";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LabelText;
        private System.Windows.Forms.Button BTN_DontSave;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}