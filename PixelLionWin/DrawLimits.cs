﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using System.Drawing.Imaging;

namespace PixelLionWin
{
    public partial class DrawLimits : Form
    {
        Map map;
        Int32 Brushsize;
        BrushType Brushtype;
        Color brushcolor;
        Boolean Erasing = false;
        public Boolean HasSaved = false;

        Bitmap Buffer;

        public DrawLimits(Map map)
        {
            InitializeComponent();
            this.map = map;

            using (XNAUtils utils = new XNAUtils())
            {
                PN_Map.BackgroundImage = utils.ConvertToImage(map.DrawMap());
                PN_Map.Size = new Size(map.MapSize.Width * 32, map.MapSize.Height * 32);

                Buffer = new Bitmap(PN_Map.Size.Width, PN_Map.Size.Height);
                using (Graphics g = Graphics.FromImage(Buffer))
                {
                    g.Clear(Color.White);
                    if (map.Limits != null) g.DrawImage(utils.ConvertToImage(map.Limits), new Point(0, 0));
                }
                Buffer.MakeTransparent(Color.White);

                Brushsize = 10;
                Brushtype = BrushType.Circle;
                brushcolor = Color.Black;
            }
        }

        private void PN_Map_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                using (Brush b = new SolidBrush(brushcolor))
                using (Graphics g = Graphics.FromImage(Buffer))
                using (Graphics gmap = PN_Map.CreateGraphics())
                {
                    if (Brushtype == BrushType.Circle)
                    {
                        g.FillEllipse(b, e.X - Brushsize / 2, e.Y - Brushsize / 2, Brushsize, Brushsize);
                        gmap.FillEllipse(b, e.X - Brushsize / 2, e.Y - Brushsize / 2, Brushsize, Brushsize);
                    }
                    else if (Brushtype == BrushType.Square)
                    {
                        g.FillRectangle(b, e.X - Brushsize / 2, e.Y - Brushsize / 2, Brushsize, Brushsize);
                        gmap.FillRectangle(b, e.X - Brushsize / 2, e.Y - Brushsize / 2, Brushsize, Brushsize);
                    }
                }


            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                BrushSwitch dlg = new BrushSwitch(Brushsize, Brushtype);
                dlg.StartPosition = FormStartPosition.Manual;
                dlg.Location = MousePosition;
                dlg.ShowDialog();

                Brushsize = dlg.Brushsize;
                Brushtype = dlg.Brushtype;
            }
        }

        private void PN_Map_MouseMove(object sender, MouseEventArgs e)
        {
            PN_Map_MouseDown(sender, e);
        }

        private void effacerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Erasing = true;
            brushcolor = Color.White;
        }

        private void PN_Map_Paint(object sender, PaintEventArgs e)
        {
            using (Graphics g = PN_Map.CreateGraphics())
            {
                g.DrawImage(Buffer, new Point(0,0));
            }
        }

        private void PN_Map_MouseUp(object sender, MouseEventArgs e)
        {
            if (Erasing)
            {
                Buffer.MakeTransparent(Color.White);
                PN_Map.Refresh();
            }
        }

        private void bloqierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Erasing = false;
            brushcolor = Color.Black;
        }

        private void supporterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Erasing = false;
            brushcolor = Color.FromArgb(255, 0, 0, 255);
        }

        private void zonerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Erasing = false;
            brushcolor = Color.FromArgb(255, 0, 255, 0);
        }

        private void sauvegarderEtQuitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HasSaved = true;
            Close();
        }

        public Bitmap GetLimits()
        {
            return Buffer;
        }
    }
}
