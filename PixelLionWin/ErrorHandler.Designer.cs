﻿namespace PixelLionWin
{
    partial class ErrorHandler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TB_Codefile = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TB_StackTrace = new System.Windows.Forms.TextBox();
            this.BTN_Close = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(712, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "[Funny title message]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(714, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "[Exception message]";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::PixelLionWin.Properties.Resources.CloseIcon;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Location = new System.Drawing.Point(656, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(50, 50);
            this.panel2.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::PixelLionWin.Properties.Resources.PLException;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(650, 361);
            this.panel1.TabIndex = 0;
            // 
            // TB_Codefile
            // 
            this.TB_Codefile.BackColor = System.Drawing.Color.Black;
            this.TB_Codefile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TB_Codefile.Location = new System.Drawing.Point(0, 382);
            this.TB_Codefile.MaxLength = 10000000;
            this.TB_Codefile.Multiline = true;
            this.TB_Codefile.Name = "TB_Codefile";
            this.TB_Codefile.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TB_Codefile.Size = new System.Drawing.Size(1234, 286);
            this.TB_Codefile.TabIndex = 5;
            this.TB_Codefile.WordWrap = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 366);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "[Filename]";
            // 
            // TB_StackTrace
            // 
            this.TB_StackTrace.Location = new System.Drawing.Point(656, 68);
            this.TB_StackTrace.Multiline = true;
            this.TB_StackTrace.Name = "TB_StackTrace";
            this.TB_StackTrace.ReadOnly = true;
            this.TB_StackTrace.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TB_StackTrace.Size = new System.Drawing.Size(565, 234);
            this.TB_StackTrace.TabIndex = 4;
            // 
            // BTN_Close
            // 
            this.BTN_Close.BackgroundImage = global::PixelLionWin.Properties.Resources.AcceptIcon;
            this.BTN_Close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Close.Location = new System.Drawing.Point(1141, 308);
            this.BTN_Close.Name = "BTN_Close";
            this.BTN_Close.Size = new System.Drawing.Size(80, 68);
            this.BTN_Close.TabIndex = 7;
            this.BTN_Close.UseVisualStyleBackColor = true;
            this.BTN_Close.Click += new System.EventHandler(this.BTN_Close_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::PixelLionWin.Properties.Resources.MediaStopButton;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Location = new System.Drawing.Point(1055, 308);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 68);
            this.button1.TabIndex = 7;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ErrorHandler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1233, 680);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BTN_Close);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TB_Codefile);
            this.Controls.Add(this.TB_StackTrace);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErrorHandler";
            this.Text = "Pixel Lion ~ Rapport d\'exception non-gérée";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox TB_Codefile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TB_StackTrace;
        private System.Windows.Forms.Button BTN_Close;
        private System.Windows.Forms.Button button1;
    }
}