﻿namespace PixelLionWin
{
    partial class CustomMouvement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomMouvement));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFermer = new System.Windows.Forms.Button();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.LBMouvements = new System.Windows.Forms.ListBox();
            this.NUM_PointY = new System.Windows.Forms.NumericUpDown();
            this.NUM_PointX = new System.Windows.Forms.NumericUpDown();
            this.lblPointDepart = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PointY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PointX)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btnFermer);
            this.groupBox1.Controls.Add(this.btnAjouter);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.LBMouvements);
            this.groupBox1.Controls.Add(this.NUM_PointY);
            this.groupBox1.Controls.Add(this.NUM_PointX);
            this.groupBox1.Controls.Add(this.lblPointDepart);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(237, 268);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mouvements Custom";
            // 
            // btnFermer
            // 
            this.btnFermer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFermer.Location = new System.Drawing.Point(99, 218);
            this.btnFermer.Name = "btnFermer";
            this.btnFermer.Size = new System.Drawing.Size(128, 40);
            this.btnFermer.TabIndex = 5;
            this.btnFermer.Text = "Fermer";
            this.btnFermer.UseVisualStyleBackColor = true;
            this.btnFermer.Click += new System.EventHandler(this.btnFermer_Click);
            // 
            // btnAjouter
            // 
            this.btnAjouter.BackgroundImage = global::PixelLionWin.Properties.Resources.AddIcon;
            this.btnAjouter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouter.Location = new System.Drawing.Point(143, 98);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(40, 40);
            this.btnAjouter.TabIndex = 4;
            this.btnAjouter.UseVisualStyleBackColor = true;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouter_Click);
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackgroundImage = global::PixelLionWin.Properties.Resources.EraseIcon;
            this.btnSupprimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSupprimer.Location = new System.Drawing.Point(99, 172);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(40, 40);
            this.btnSupprimer.TabIndex = 3;
            this.btnSupprimer.UseVisualStyleBackColor = true;
            this.btnSupprimer.Click += new System.EventHandler(this.btnSupprimer_Click);
            // 
            // LBMouvements
            // 
            this.LBMouvements.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBMouvements.FormattingEnabled = true;
            this.LBMouvements.Location = new System.Drawing.Point(10, 72);
            this.LBMouvements.Name = "LBMouvements";
            this.LBMouvements.Size = new System.Drawing.Size(82, 186);
            this.LBMouvements.TabIndex = 2;
            // 
            // NUM_PointY
            // 
            this.NUM_PointY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NUM_PointY.Location = new System.Drawing.Point(166, 72);
            this.NUM_PointY.Name = "NUM_PointY";
            this.NUM_PointY.Size = new System.Drawing.Size(61, 20);
            this.NUM_PointY.TabIndex = 1;
            // 
            // NUM_PointX
            // 
            this.NUM_PointX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NUM_PointX.Location = new System.Drawing.Point(99, 72);
            this.NUM_PointX.Name = "NUM_PointX";
            this.NUM_PointX.Size = new System.Drawing.Size(61, 20);
            this.NUM_PointX.TabIndex = 1;
            // 
            // lblPointDepart
            // 
            this.lblPointDepart.AutoSize = true;
            this.lblPointDepart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPointDepart.Location = new System.Drawing.Point(7, 45);
            this.lblPointDepart.Name = "lblPointDepart";
            this.lblPointDepart.Size = new System.Drawing.Size(85, 13);
            this.lblPointDepart.TabIndex = 0;
            this.lblPointDepart.Text = "Point de départ: ";
            // 
            // CustomMouvement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(258, 286);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "CustomMouvement";
            this.Text = "Custom Mouvement";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CustomMouvement_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PointY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_PointX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblPointDepart;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.ListBox LBMouvements;
        private System.Windows.Forms.NumericUpDown NUM_PointY;
        private System.Windows.Forms.NumericUpDown NUM_PointX;
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.Button btnFermer;
    }
}