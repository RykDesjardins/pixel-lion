﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PixelLionWin
{
    public partial class NewMap : Form
    {
        Image Tileset;

        public NewMap()
        {
            InitializeComponent();
        }

        private void BTN_Create_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        public Size GetSize()
        {
            return new Size(Convert.ToInt32(NUM_X.Value), Convert.ToInt32(NUM_Y.Value));
        }

        public String GetName()
        {
            return TB_Name.Text;
        }

        public Image GetImage()
        {
            return Tileset;
        }

        private void TB_Name_TextChanged(object sender, EventArgs e)
        {
            CheckForOK();
        }

        private void NewMap_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
            if (e.KeyCode == Keys.Enter)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
        }

        private void BTN_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BTN_Browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image PNG (*.png)|*.png";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TB_Tileset.Text = dlg.FileName;

                try        
                {
                    Tileset = Image.FromFile(dlg.FileName);
                }
                catch (Exception)       
                {
                    MessageBox.Show("L'image n'est pas valide. Veuillez choisir une image valide de type PNG.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    TB_Tileset.Text = String.Empty;
                }
            }

            CheckForOK();
        }

        private void CheckForOK()
        {
            BTN_Create.Enabled = TB_Name.Text != String.Empty && TB_Tileset.Text != String.Empty;
        }
    }
}
