﻿namespace PixelLionWin
{
    partial class BattlerManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BattlerManager));
            this.PN_Preview = new System.Windows.Forms.Panel();
            this.LIST_Battlers = new System.Windows.Forms.ListBox();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.BTN_Add = new System.Windows.Forms.Button();
            this.BTN_Delete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PN_Preview
            // 
            this.PN_Preview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PN_Preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Preview.Location = new System.Drawing.Point(12, 12);
            this.PN_Preview.Name = "PN_Preview";
            this.PN_Preview.Size = new System.Drawing.Size(425, 373);
            this.PN_Preview.TabIndex = 0;
            // 
            // LIST_Battlers
            // 
            this.LIST_Battlers.FormattingEnabled = true;
            this.LIST_Battlers.Location = new System.Drawing.Point(450, 12);
            this.LIST_Battlers.Name = "LIST_Battlers";
            this.LIST_Battlers.Size = new System.Drawing.Size(237, 329);
            this.LIST_Battlers.TabIndex = 1;
            this.LIST_Battlers.SelectedIndexChanged += new System.EventHandler(this.LIST_Battlers_SelectedIndexChanged);
            // 
            // BTN_Save
            // 
            this.BTN_Save.BackgroundImage = global::PixelLionWin.Properties.Resources.AcceptIcon;
            this.BTN_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Save.Location = new System.Drawing.Point(641, 347);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(46, 38);
            this.BTN_Save.TabIndex = 2;
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // BTN_Add
            // 
            this.BTN_Add.BackgroundImage = global::PixelLionWin.Properties.Resources.AddButton;
            this.BTN_Add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Add.Location = new System.Drawing.Point(450, 347);
            this.BTN_Add.Name = "BTN_Add";
            this.BTN_Add.Size = new System.Drawing.Size(38, 38);
            this.BTN_Add.TabIndex = 2;
            this.BTN_Add.UseVisualStyleBackColor = true;
            this.BTN_Add.Click += new System.EventHandler(this.BTN_Add_Click);
            // 
            // BTN_Delete
            // 
            this.BTN_Delete.BackgroundImage = global::PixelLionWin.Properties.Resources.CloseIcon;
            this.BTN_Delete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_Delete.Location = new System.Drawing.Point(494, 347);
            this.BTN_Delete.Name = "BTN_Delete";
            this.BTN_Delete.Size = new System.Drawing.Size(38, 38);
            this.BTN_Delete.TabIndex = 2;
            this.BTN_Delete.UseVisualStyleBackColor = true;
            this.BTN_Delete.Click += new System.EventHandler(this.BTN_Delete_Click);
            // 
            // BattlerManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 397);
            this.Controls.Add(this.BTN_Delete);
            this.Controls.Add(this.BTN_Add);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.LIST_Battlers);
            this.Controls.Add(this.PN_Preview);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BattlerManager";
            this.Text = "Gestion des combatants";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN_Preview;
        private System.Windows.Forms.ListBox LIST_Battlers;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Button BTN_Add;
        private System.Windows.Forms.Button BTN_Delete;
    }
}