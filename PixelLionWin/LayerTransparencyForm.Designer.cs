﻿namespace PixelLionWin
{
    partial class LayerTransparencyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrB_Layer1 = new System.Windows.Forms.GroupBox();
            this.Layer1Value = new System.Windows.Forms.Label();
            this.TB_Layer1 = new System.Windows.Forms.TrackBar();
            this.GrB_Layer2 = new System.Windows.Forms.GroupBox();
            this.Layer2Value = new System.Windows.Forms.Label();
            this.TB_Layer2 = new System.Windows.Forms.TrackBar();
            this.GrB_Layer3 = new System.Windows.Forms.GroupBox();
            this.Layer3Value = new System.Windows.Forms.Label();
            this.TB_Layer3 = new System.Windows.Forms.TrackBar();
            this.CB_Layer1 = new System.Windows.Forms.CheckBox();
            this.CB_Layer2 = new System.Windows.Forms.CheckBox();
            this.CB_Layer3 = new System.Windows.Forms.CheckBox();
            this.PN_Title = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GrB_Layer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TB_Layer1)).BeginInit();
            this.GrB_Layer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TB_Layer2)).BeginInit();
            this.GrB_Layer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TB_Layer3)).BeginInit();
            this.PN_Title.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrB_Layer1
            // 
            this.GrB_Layer1.BackColor = System.Drawing.Color.Transparent;
            this.GrB_Layer1.Controls.Add(this.Layer1Value);
            this.GrB_Layer1.Controls.Add(this.TB_Layer1);
            this.GrB_Layer1.Location = new System.Drawing.Point(32, 74);
            this.GrB_Layer1.Name = "GrB_Layer1";
            this.GrB_Layer1.Size = new System.Drawing.Size(416, 67);
            this.GrB_Layer1.TabIndex = 0;
            this.GrB_Layer1.TabStop = false;
            this.GrB_Layer1.Text = "Dessous (1)";
            // 
            // Layer1Value
            // 
            this.Layer1Value.AutoSize = true;
            this.Layer1Value.Location = new System.Drawing.Point(368, 20);
            this.Layer1Value.Name = "Layer1Value";
            this.Layer1Value.Size = new System.Drawing.Size(25, 13);
            this.Layer1Value.TabIndex = 1;
            this.Layer1Value.Text = "255";
            // 
            // TB_Layer1
            // 
            this.TB_Layer1.BackColor = System.Drawing.Color.White;
            this.TB_Layer1.LargeChange = 50;
            this.TB_Layer1.Location = new System.Drawing.Point(7, 20);
            this.TB_Layer1.Maximum = 255;
            this.TB_Layer1.Name = "TB_Layer1";
            this.TB_Layer1.Size = new System.Drawing.Size(355, 45);
            this.TB_Layer1.SmallChange = 5;
            this.TB_Layer1.TabIndex = 0;
            this.TB_Layer1.TickFrequency = 5;
            this.TB_Layer1.Scroll += new System.EventHandler(this.TB_Layer1_Scroll);
            this.TB_Layer1.ValueChanged += new System.EventHandler(this.TB_Layer1_ValueChanged);
            // 
            // GrB_Layer2
            // 
            this.GrB_Layer2.BackColor = System.Drawing.Color.Transparent;
            this.GrB_Layer2.Controls.Add(this.Layer2Value);
            this.GrB_Layer2.Controls.Add(this.TB_Layer2);
            this.GrB_Layer2.Location = new System.Drawing.Point(33, 147);
            this.GrB_Layer2.Name = "GrB_Layer2";
            this.GrB_Layer2.Size = new System.Drawing.Size(415, 67);
            this.GrB_Layer2.TabIndex = 0;
            this.GrB_Layer2.TabStop = false;
            this.GrB_Layer2.Text = "Centre (2)";
            // 
            // Layer2Value
            // 
            this.Layer2Value.AutoSize = true;
            this.Layer2Value.Location = new System.Drawing.Point(368, 20);
            this.Layer2Value.Name = "Layer2Value";
            this.Layer2Value.Size = new System.Drawing.Size(25, 13);
            this.Layer2Value.TabIndex = 1;
            this.Layer2Value.Text = "255";
            // 
            // TB_Layer2
            // 
            this.TB_Layer2.BackColor = System.Drawing.Color.White;
            this.TB_Layer2.LargeChange = 50;
            this.TB_Layer2.Location = new System.Drawing.Point(7, 20);
            this.TB_Layer2.Maximum = 255;
            this.TB_Layer2.Name = "TB_Layer2";
            this.TB_Layer2.Size = new System.Drawing.Size(355, 45);
            this.TB_Layer2.SmallChange = 5;
            this.TB_Layer2.TabIndex = 0;
            this.TB_Layer2.TickFrequency = 5;
            this.TB_Layer2.Scroll += new System.EventHandler(this.TB_Layer2_Scroll);
            this.TB_Layer2.ValueChanged += new System.EventHandler(this.TB_Layer2_ValueChanged);
            // 
            // GrB_Layer3
            // 
            this.GrB_Layer3.BackColor = System.Drawing.Color.Transparent;
            this.GrB_Layer3.Controls.Add(this.Layer3Value);
            this.GrB_Layer3.Controls.Add(this.TB_Layer3);
            this.GrB_Layer3.Location = new System.Drawing.Point(31, 220);
            this.GrB_Layer3.Name = "GrB_Layer3";
            this.GrB_Layer3.Size = new System.Drawing.Size(417, 67);
            this.GrB_Layer3.TabIndex = 0;
            this.GrB_Layer3.TabStop = false;
            this.GrB_Layer3.Text = "Dessus (3)";
            // 
            // Layer3Value
            // 
            this.Layer3Value.AutoSize = true;
            this.Layer3Value.Location = new System.Drawing.Point(368, 20);
            this.Layer3Value.Name = "Layer3Value";
            this.Layer3Value.Size = new System.Drawing.Size(25, 13);
            this.Layer3Value.TabIndex = 1;
            this.Layer3Value.Text = "255";
            // 
            // TB_Layer3
            // 
            this.TB_Layer3.BackColor = System.Drawing.Color.White;
            this.TB_Layer3.LargeChange = 50;
            this.TB_Layer3.Location = new System.Drawing.Point(7, 20);
            this.TB_Layer3.Maximum = 255;
            this.TB_Layer3.Name = "TB_Layer3";
            this.TB_Layer3.Size = new System.Drawing.Size(355, 45);
            this.TB_Layer3.SmallChange = 5;
            this.TB_Layer3.TabIndex = 0;
            this.TB_Layer3.TickFrequency = 5;
            this.TB_Layer3.Scroll += new System.EventHandler(this.TB_Layer3_Scroll);
            this.TB_Layer3.ValueChanged += new System.EventHandler(this.TB_Layer3_ValueChanged);
            // 
            // CB_Layer1
            // 
            this.CB_Layer1.AutoSize = true;
            this.CB_Layer1.BackColor = System.Drawing.Color.Transparent;
            this.CB_Layer1.Location = new System.Drawing.Point(10, 94);
            this.CB_Layer1.Name = "CB_Layer1";
            this.CB_Layer1.Size = new System.Drawing.Size(15, 14);
            this.CB_Layer1.TabIndex = 1;
            this.CB_Layer1.UseVisualStyleBackColor = false;
            this.CB_Layer1.CheckedChanged += new System.EventHandler(this.CB_Layer1_CheckedChanged);
            // 
            // CB_Layer2
            // 
            this.CB_Layer2.AutoSize = true;
            this.CB_Layer2.BackColor = System.Drawing.Color.Transparent;
            this.CB_Layer2.Location = new System.Drawing.Point(11, 167);
            this.CB_Layer2.Name = "CB_Layer2";
            this.CB_Layer2.Size = new System.Drawing.Size(15, 14);
            this.CB_Layer2.TabIndex = 1;
            this.CB_Layer2.UseVisualStyleBackColor = false;
            this.CB_Layer2.CheckedChanged += new System.EventHandler(this.CB_Layer2_CheckedChanged);
            // 
            // CB_Layer3
            // 
            this.CB_Layer3.AutoSize = true;
            this.CB_Layer3.BackColor = System.Drawing.Color.Transparent;
            this.CB_Layer3.Location = new System.Drawing.Point(10, 240);
            this.CB_Layer3.Name = "CB_Layer3";
            this.CB_Layer3.Size = new System.Drawing.Size(15, 14);
            this.CB_Layer3.TabIndex = 1;
            this.CB_Layer3.UseVisualStyleBackColor = false;
            this.CB_Layer3.CheckedChanged += new System.EventHandler(this.CB_Layer3_CheckedChanged);
            // 
            // PN_Title
            // 
            this.PN_Title.BackColor = System.Drawing.Color.MintCream;
            this.PN_Title.Controls.Add(this.label2);
            this.PN_Title.Controls.Add(this.label1);
            this.PN_Title.Controls.Add(this.panel1);
            this.PN_Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_Title.Location = new System.Drawing.Point(0, 0);
            this.PN_Title.Name = "PN_Title";
            this.PN_Title.Size = new System.Drawing.Size(460, 68);
            this.PN_Title.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::PixelLionWin.Properties.Resources.ICON_Opacity;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(65, 62);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(72, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(324, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Transparence de toutes les tuiles par groupe";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(74, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(308, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Les valeurs ci-dessous représentent l\'opacité de chaque \r\ncouche de tuiles sur un" +
    "e échelle de 0 (invisible) à 255 (opaque).";
            // 
            // LayerTransparencyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(460, 298);
            this.Controls.Add(this.PN_Title);
            this.Controls.Add(this.CB_Layer3);
            this.Controls.Add(this.CB_Layer2);
            this.Controls.Add(this.CB_Layer1);
            this.Controls.Add(this.GrB_Layer3);
            this.Controls.Add(this.GrB_Layer2);
            this.Controls.Add(this.GrB_Layer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LayerTransparencyForm";
            this.Text = "Transparence des couches";
            this.GrB_Layer1.ResumeLayout(false);
            this.GrB_Layer1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TB_Layer1)).EndInit();
            this.GrB_Layer2.ResumeLayout(false);
            this.GrB_Layer2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TB_Layer2)).EndInit();
            this.GrB_Layer3.ResumeLayout(false);
            this.GrB_Layer3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TB_Layer3)).EndInit();
            this.PN_Title.ResumeLayout(false);
            this.PN_Title.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GrB_Layer1;
        private System.Windows.Forms.TrackBar TB_Layer1;
        private System.Windows.Forms.Label Layer1Value;
        private System.Windows.Forms.GroupBox GrB_Layer2;
        private System.Windows.Forms.Label Layer2Value;
        private System.Windows.Forms.TrackBar TB_Layer2;
        private System.Windows.Forms.GroupBox GrB_Layer3;
        private System.Windows.Forms.Label Layer3Value;
        private System.Windows.Forms.TrackBar TB_Layer3;
        private System.Windows.Forms.CheckBox CB_Layer1;
        private System.Windows.Forms.CheckBox CB_Layer2;
        private System.Windows.Forms.CheckBox CB_Layer3;
        private System.Windows.Forms.Panel PN_Title;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
    }
}