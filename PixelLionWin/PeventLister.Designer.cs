﻿namespace PixelLionWin
{
    partial class PeventLister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTN_Edit = new System.Windows.Forms.Button();
            this.BTN_Close = new System.Windows.Forms.Button();
            this.BTN_SelectAll = new System.Windows.Forms.Button();
            this.BTN_DeselectAll = new System.Windows.Forms.Button();
            this.BTN_InvertSelection = new System.Windows.Forms.Button();
            this.Panel_pEvents = new System.Windows.Forms.FlowLayoutPanel();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deselectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // BTN_Edit
            // 
            this.BTN_Edit.Location = new System.Drawing.Point(215, 232);
            this.BTN_Edit.Name = "BTN_Edit";
            this.BTN_Edit.Size = new System.Drawing.Size(105, 23);
            this.BTN_Edit.TabIndex = 1;
            this.BTN_Edit.Text = "Edit";
            this.BTN_Edit.UseVisualStyleBackColor = true;
            this.BTN_Edit.Click += new System.EventHandler(this.BTN_Edit_Click);
            // 
            // BTN_Close
            // 
            this.BTN_Close.Location = new System.Drawing.Point(215, 261);
            this.BTN_Close.Name = "BTN_Close";
            this.BTN_Close.Size = new System.Drawing.Size(105, 23);
            this.BTN_Close.TabIndex = 2;
            this.BTN_Close.Text = "Close";
            this.BTN_Close.UseVisualStyleBackColor = true;
            this.BTN_Close.Click += new System.EventHandler(this.BTN_Close_Click);
            // 
            // BTN_SelectAll
            // 
            this.BTN_SelectAll.Location = new System.Drawing.Point(215, 40);
            this.BTN_SelectAll.Name = "BTN_SelectAll";
            this.BTN_SelectAll.Size = new System.Drawing.Size(104, 23);
            this.BTN_SelectAll.TabIndex = 4;
            this.BTN_SelectAll.Text = "Select All";
            this.BTN_SelectAll.UseVisualStyleBackColor = true;
            this.BTN_SelectAll.Click += new System.EventHandler(this.BTN_SelectAll_Click);
            // 
            // BTN_DeselectAll
            // 
            this.BTN_DeselectAll.Location = new System.Drawing.Point(215, 70);
            this.BTN_DeselectAll.Name = "BTN_DeselectAll";
            this.BTN_DeselectAll.Size = new System.Drawing.Size(104, 23);
            this.BTN_DeselectAll.TabIndex = 5;
            this.BTN_DeselectAll.Text = "Deselect All";
            this.BTN_DeselectAll.UseVisualStyleBackColor = true;
            this.BTN_DeselectAll.Click += new System.EventHandler(this.BTN_DeselectAll_Click);
            // 
            // BTN_InvertSelection
            // 
            this.BTN_InvertSelection.Location = new System.Drawing.Point(215, 100);
            this.BTN_InvertSelection.Name = "BTN_InvertSelection";
            this.BTN_InvertSelection.Size = new System.Drawing.Size(104, 23);
            this.BTN_InvertSelection.TabIndex = 6;
            this.BTN_InvertSelection.Text = "Invert Selection";
            this.BTN_InvertSelection.UseVisualStyleBackColor = true;
            this.BTN_InvertSelection.Click += new System.EventHandler(this.BTN_InvertSelection_Click);
            // 
            // Panel_pEvents
            // 
            this.Panel_pEvents.AutoScroll = true;
            this.Panel_pEvents.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Panel_pEvents.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_pEvents.Location = new System.Drawing.Point(9, 40);
            this.Panel_pEvents.Margin = new System.Windows.Forms.Padding(0);
            this.Panel_pEvents.Name = "Panel_pEvents";
            this.Panel_pEvents.Size = new System.Drawing.Size(200, 244);
            this.Panel_pEvents.TabIndex = 8;
            // 
            // Menu
            // 
            this.MainMenu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.MainMenu.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editionToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "Menu";
            this.MainMenu.Size = new System.Drawing.Size(335, 24);
            this.MainMenu.TabIndex = 11;
            this.MainMenu.Text = "Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // editionToolStripMenuItem
            // 
            this.editionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllToolStripMenuItem,
            this.deselectAllToolStripMenuItem,
            this.invertSelectionToolStripMenuItem});
            this.editionToolStripMenuItem.Name = "editionToolStripMenuItem";
            this.editionToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.editionToolStripMenuItem.Text = "Edition";
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.selectAllToolStripMenuItem.Text = "Select All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // deselectAllToolStripMenuItem
            // 
            this.deselectAllToolStripMenuItem.Name = "deselectAllToolStripMenuItem";
            this.deselectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.deselectAllToolStripMenuItem.Text = "Deselect All";
            this.deselectAllToolStripMenuItem.Click += new System.EventHandler(this.deselectAllToolStripMenuItem_Click);
            // 
            // invertSelectionToolStripMenuItem
            // 
            this.invertSelectionToolStripMenuItem.Name = "invertSelectionToolStripMenuItem";
            this.invertSelectionToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.invertSelectionToolStripMenuItem.Text = "Invert Selection";
            this.invertSelectionToolStripMenuItem.Click += new System.EventHandler(this.invertSelectionToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // PeventLister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 293);
            this.Controls.Add(this.MainMenu);
            this.Controls.Add(this.Panel_pEvents);
            this.Controls.Add(this.BTN_InvertSelection);
            this.Controls.Add(this.BTN_DeselectAll);
            this.Controls.Add(this.BTN_SelectAll);
            this.Controls.Add(this.BTN_Close);
            this.Controls.Add(this.BTN_Edit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "PeventLister";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PeventLister";
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BTN_Edit;
        private System.Windows.Forms.Button BTN_Close;
        private System.Windows.Forms.Button BTN_SelectAll;
        private System.Windows.Forms.Button BTN_DeselectAll;
        private System.Windows.Forms.Button BTN_InvertSelection;
        private System.Windows.Forms.FlowLayoutPanel Panel_pEvents;
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deselectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertSelectionToolStripMenuItem;
    }
}