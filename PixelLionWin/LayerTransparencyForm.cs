﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PixelLionWin
{
    public partial class LayerTransparencyForm : Form
    {
        MainFormPixelLion PLParent;

        public LayerTransparencyForm(Int32[] values, MainFormPixelLion parent)
        {
            InitializeComponent();

            CB_Layer1.Checked = values[0] != 0;
            CB_Layer2.Checked = values[1] != 0;
            CB_Layer3.Checked = values[2] != 0;

            TB_Layer1.Value = values[0];
            TB_Layer2.Value = values[1];
            TB_Layer3.Value = values[2];

            PLParent = parent;
        }

        public Int32[] GetTransparencyValues()
        {
            return new Int32[] { TB_Layer1.Value, TB_Layer2.Value, TB_Layer3.Value };
        }

        private void CB_Layer1_CheckedChanged(object sender, EventArgs e)
        {
            if (PLParent == null) return;

            if (CB_Layer1.Checked)
            {
                GrB_Layer1.Enabled = true;
                TB_Layer1.Value = 255;
            }
            else
            {
                GrB_Layer1.Enabled = false;
                TB_Layer1.Value = 0;
            }

            PLParent.CurrentMap.LayersOpacity = GetTransparencyValues();
            PLParent.DrawMap();
        }

        private void CB_Layer2_CheckedChanged(object sender, EventArgs e)
        {
            if (PLParent == null) return;

            if (CB_Layer2.Checked)
            {
                GrB_Layer2.Enabled = true;
                TB_Layer2.Value = 255;
            }
            else
            {
                GrB_Layer2.Enabled = false;
                TB_Layer2.Value = 0;
            }

            PLParent.CurrentMap.LayersOpacity = GetTransparencyValues();
            PLParent.DrawMap();
        }

        private void CB_Layer3_CheckedChanged(object sender, EventArgs e)
        {
            if (PLParent == null) return;

            if (CB_Layer3.Checked)
            {
                GrB_Layer3.Enabled = true;
                TB_Layer3.Value = 255;
            }
            else
            {
                GrB_Layer3.Enabled = false;
                TB_Layer3.Value = 0;
            }

            PLParent.CurrentMap.LayersOpacity = GetTransparencyValues();
            PLParent.DrawMap();
        }

        private void TB_Layer1_Scroll(object sender, EventArgs e)
        {
            Layer1Value.Text = TB_Layer1.Value.ToString();
        }

        private void TB_Layer2_Scroll(object sender, EventArgs e)
        {
            Layer2Value.Text = TB_Layer2.Value.ToString();
        }

        private void TB_Layer3_Scroll(object sender, EventArgs e)
        {
            Layer3Value.Text = TB_Layer3.Value.ToString();
        }

        private void TB_Layer1_ValueChanged(object sender, EventArgs e)
        {
            if (PLParent == null) return;

            Layer1Value.Text = TB_Layer1.Value.ToString();

            PLParent.CurrentMap.LayersOpacity = GetTransparencyValues();
            PLParent.DrawMap();
        }

        private void TB_Layer2_ValueChanged(object sender, EventArgs e)
        {
            if (PLParent == null) return;

            Layer2Value.Text = TB_Layer2.Value.ToString();

            PLParent.CurrentMap.LayersOpacity = GetTransparencyValues();
            PLParent.DrawMap();
        }

        private void TB_Layer3_ValueChanged(object sender, EventArgs e)
        {
            if (PLParent == null) return;

            Layer3Value.Text = TB_Layer3.Value.ToString();

            PLParent.CurrentMap.LayersOpacity = GetTransparencyValues();
            PLParent.DrawMap();
        }
    }
}