﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class PreferencesDialog : Form
    {
        MainFormPixelLion PLFormParent;

        public PreferencesDialog(UserPreferences prefs, MainFormPixelLion parent)
        {
            InitializeComponent();
            PLFormParent = parent;

            // Animation
            PN_AnimationBackgroundColor.BackColor = prefs.AnimationStudio.BackgroundColor;
            NUM_AnimationRulerSize.Value = prefs.AnimationStudio.RulerSize;
            CB_ShowSplashScreenAnimation.Checked = prefs.AnimationStudio.ShowSplashScreen;
            CB_UseXnaAnimation.Checked = prefs.AnimationStudio.UseXna;

            // Audio
            CB_LoopSong.Checked = prefs.Audio.LoopSongs;
            CB_PlayJingle.Checked = prefs.Audio.PlaySound;
            TB_JinglePath.Text = prefs.Audio.SoundPath;

            // Map
            PN_GridColor.BackColor = prefs.Map.GridColor;
            NUM_GridThickness.Value = prefs.Map.GridThickness;
            NUM_TileSizeX.Value = prefs.Map.TileSize.Width;
            NUM_TileSizeY.Value = prefs.Map.TileSize.Height;

            // Memory
            NUM_HistoryStateCount.Value = prefs.Memory.HistoryCount;
            CB_Hotkeys.Checked = prefs.Memory.Hotkeys;
            CB_PreloadMaps.Checked = prefs.Memory.PreloadMaps;
            CB_QuickMaps.Checked = prefs.Memory.QuickMaps;

            // Tileset
            PN_TileSetColorBackground.BackColor = prefs.TileSet.BackgroundColor;
            NUM_TileSetTileCount.Value = prefs.TileSet.TilesPerRow;

            // Video
            CB_DoubleBuffering.Checked = prefs.Video.DoubleBuffering;
            CB_ShowPopupsPrincipal.Checked = prefs.Video.ShowPopups;
            CB_ShowSplashScreenPrincipal.Checked = prefs.Video.ShowSplashScreen;
            CB_BubbleMainForm.Checked = prefs.Video.ShowBubbleMainScreen;

            //Tabs
            PN_TabColor1.BackColor = prefs.Tabs.FirstColorGradiant;
            PN_TabColor2.BackColor = prefs.Tabs.SecondColorGradiant;
        }

        public void ChangePanelColor(Object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                ((Panel)sender).BackColor = dlg.Color;
        }

        public UserPreferences GeneratePreferences()
        {
            return new UserPreferences()
            {
                AnimationStudio = new UserPreferences.AnimationStudioPreferences()
                {
                    BackgroundColor = PN_AnimationBackgroundColor.BackColor,
                    RulerSize = Convert.ToInt32(NUM_AnimationRulerSize.Value),
                    ShowSplashScreen = CB_ShowSplashScreenAnimation.Checked,
                    UseXna = CB_UseXnaAnimation.Checked
                },

                Audio = new UserPreferences.AudioPreferences()
                {
                    LoopSongs = CB_LoopSong.Checked,
                    PlaySound = CB_PlayJingle.Checked,
                    SoundPath = TB_JinglePath.Text
                },

                Map = new UserPreferences.MapPreferences()
                {
                    GridColor = PN_GridColor.BackColor,
                    GridThickness = Convert.ToInt32(NUM_GridThickness.Value),
                    TileSize = new Size(Convert.ToInt32(NUM_TileSizeX.Value), Convert.ToInt32(NUM_TileSizeY.Value))
                },

                Memory = new UserPreferences.MemoryPreferences()
                {
                    HistoryCount = Convert.ToInt32(NUM_HistoryStateCount.Value),
                    Hotkeys = CB_Hotkeys.Checked,
                    PreloadMaps = CB_PreloadMaps.Checked,
                    QuickMaps = CB_QuickMaps.Checked
                },

                TileSet = new UserPreferences.TileSetPreferences()
                {
                    BackgroundColor = PN_TileSetColorBackground.BackColor,
                    TilesPerRow = Convert.ToInt32(NUM_TileSetTileCount.Value)
                },

                Video = new UserPreferences.VideoPreferences()
                {
                    DoubleBuffering = CB_DoubleBuffering.Checked,
                    ShowPopups = CB_ShowPopupsPrincipal.Checked,
                    ShowSplashScreen = CB_ShowSplashScreenPrincipal.Checked,
                    ShowBubbleMainScreen = CB_BubbleMainForm.Checked
                },

                Tabs = new UserPreferences.TabsPreferences()
                {
                    FirstColorGradiant = PN_TabColor1.BackColor,
                    SecondColorGradiant = PN_TabColor2.BackColor
                }
            };
        }

        private void BTN_BrowseJingle_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) TB_JinglePath.Text = dlg.FileName;
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void BTN_ReloadProject_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez-vous vraiment recharger le projet? Pixel Lion redémarrera et les préférences seront sauvegardées.", "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                DialogResult = System.Windows.Forms.DialogResult.Retry;
                Close();
            }
        }

        private void BTN_EraseHistory_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Ignore;
            Close();
        }

        private void BTN_ImportPrefs_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Yes;
            Close();
        }

        private void BTN_ExportPrefs_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.No;
            Close();
        }

        private void NUM_GridThickness_ValueChanged(object sender, EventArgs e)
        {
            GlobalPreferences.Prefs.Map.GridThickness = Convert.ToInt32(NUM_GridThickness.Value);
            if (PLFormParent != null) (PLFormParent as MainFormPixelLion).DrawMap();
        }

        private void PN_GridColor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void PN_GridColor_Click(object sender, EventArgs e)
        {
            ChangePanelColor(sender, e);

            GlobalPreferences.Prefs.Map.GridColor = ((Panel)sender).BackColor;
            (PLFormParent as MainFormPixelLion).DrawMap();
        }

        private void ChangeTabColors(object sender, EventArgs e)
        {
            ChangePanelColor(sender, e);

            PN_TabPreviewGradiant.Refresh();
        }

        private void PN_TabPreviewGradiant_Paint(object sender, PaintEventArgs e)
        {
            using (System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush(new Point(0, 0), new Point(PN_TabPreviewGradiant.Width, PN_TabPreviewGradiant.Height), PN_TabColor1.BackColor, PN_TabColor2.BackColor))
            using (Graphics g = PN_TabPreviewGradiant.CreateGraphics())
            {
                g.FillRectangle(brush, new Rectangle(new Point(0, 0), PN_TabPreviewGradiant.Size));
            }
        }
    }
}
