﻿namespace PixelLionWin
{
    partial class PixelLinkingGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.LB_Progress = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BTN_Abort = new System.Windows.Forms.Button();
            this.BTN_Launch = new System.Windows.Forms.Button();
            this.plProgressBar1 = new PixelLionWin.PLProgressBar();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pixel Link";
            // 
            // LB_Progress
            // 
            this.LB_Progress.AutoSize = true;
            this.LB_Progress.Location = new System.Drawing.Point(12, 64);
            this.LB_Progress.Name = "LB_Progress";
            this.LB_Progress.Size = new System.Drawing.Size(73, 13);
            this.LB_Progress.TabIndex = 2;
            this.LB_Progress.Text = "[Avancement]";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(835, 52);
            this.panel1.TabIndex = 4;
            // 
            // BTN_Abort
            // 
            this.BTN_Abort.Location = new System.Drawing.Point(730, 117);
            this.BTN_Abort.Name = "BTN_Abort";
            this.BTN_Abort.Size = new System.Drawing.Size(93, 33);
            this.BTN_Abort.TabIndex = 5;
            this.BTN_Abort.Text = "Arrêter";
            this.BTN_Abort.UseVisualStyleBackColor = true;
            this.BTN_Abort.Click += new System.EventHandler(this.BTN_Abort_Click);
            // 
            // BTN_Launch
            // 
            this.BTN_Launch.Enabled = false;
            this.BTN_Launch.Location = new System.Drawing.Point(616, 117);
            this.BTN_Launch.Name = "BTN_Launch";
            this.BTN_Launch.Size = new System.Drawing.Size(96, 33);
            this.BTN_Launch.TabIndex = 6;
            this.BTN_Launch.Text = "Lancer";
            this.BTN_Launch.UseVisualStyleBackColor = true;
            this.BTN_Launch.Click += new System.EventHandler(this.BTN_Launch_Click);
            // 
            // plProgressBar1
            // 
            this.plProgressBar1.Location = new System.Drawing.Point(12, 80);
            this.plProgressBar1.Max = 100;
            this.plProgressBar1.Name = "plProgressBar1";
            this.plProgressBar1.Size = new System.Drawing.Size(811, 28);
            this.plProgressBar1.TabIndex = 0;
            this.plProgressBar1.Value = 100;
            // 
            // PixelLinkingGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 159);
            this.Controls.Add(this.BTN_Launch);
            this.Controls.Add(this.BTN_Abort);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LB_Progress);
            this.Controls.Add(this.plProgressBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PixelLinkingGame";
            this.Text = "Pixel Link ~ ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PixelLinkingGame_FormClosing);
            this.Shown += new System.EventHandler(this.PixelLinkingGame_Shown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private PLProgressBar plProgressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LB_Progress;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BTN_Abort;
        private System.Windows.Forms.Button BTN_Launch;
    }
}