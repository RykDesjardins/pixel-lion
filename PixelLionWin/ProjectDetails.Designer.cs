﻿namespace PixelLionWin
{
    partial class ProjectDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectDetails));
            this.lblMapNumber = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnChangePath = new System.Windows.Forms.Button();
            this.lblPath = new System.Windows.Forms.Label();
            this.lblNbSounds = new System.Windows.Forms.Label();
            this.lblNbSongs = new System.Windows.Forms.Label();
            this.lblNbSprites = new System.Windows.Forms.Label();
            this.lblProjectSize = new System.Windows.Forms.Label();
            this.lblProjectName = new System.Windows.Forms.LinkLabel();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMapNumber
            // 
            this.lblMapNumber.AutoSize = true;
            this.lblMapNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblMapNumber.Location = new System.Drawing.Point(6, 25);
            this.lblMapNumber.Name = "lblMapNumber";
            this.lblMapNumber.Size = new System.Drawing.Size(75, 13);
            this.lblMapNumber.TabIndex = 1;
            this.lblMapNumber.Text = "lblMapNumber";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btnChangePath);
            this.groupBox1.Controls.Add(this.lblPath);
            this.groupBox1.Controls.Add(this.lblNbSounds);
            this.groupBox1.Controls.Add(this.lblNbSongs);
            this.groupBox1.Controls.Add(this.lblNbSprites);
            this.groupBox1.Controls.Add(this.lblProjectSize);
            this.groupBox1.Controls.Add(this.lblMapNumber);
            this.groupBox1.Location = new System.Drawing.Point(12, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(406, 169);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informations";
            // 
            // btnChangePath
            // 
            this.btnChangePath.Location = new System.Drawing.Point(325, 140);
            this.btnChangePath.Name = "btnChangePath";
            this.btnChangePath.Size = new System.Drawing.Size(75, 23);
            this.btnChangePath.TabIndex = 7;
            this.btnChangePath.Text = "Changer...";
            this.btnChangePath.UseVisualStyleBackColor = true;
            this.btnChangePath.Click += new System.EventHandler(this.btnChangePath_Click);
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.BackColor = System.Drawing.Color.Transparent;
            this.lblPath.Location = new System.Drawing.Point(6, 145);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(39, 13);
            this.lblPath.TabIndex = 6;
            this.lblPath.Text = "lblPath";
            // 
            // lblNbSounds
            // 
            this.lblNbSounds.AutoSize = true;
            this.lblNbSounds.BackColor = System.Drawing.Color.Transparent;
            this.lblNbSounds.Location = new System.Drawing.Point(6, 122);
            this.lblNbSounds.Name = "lblNbSounds";
            this.lblNbSounds.Size = new System.Drawing.Size(67, 13);
            this.lblNbSounds.TabIndex = 4;
            this.lblNbSounds.Text = "lblNbSounds";
            // 
            // lblNbSongs
            // 
            this.lblNbSongs.AutoSize = true;
            this.lblNbSongs.BackColor = System.Drawing.Color.Transparent;
            this.lblNbSongs.Location = new System.Drawing.Point(6, 97);
            this.lblNbSongs.Name = "lblNbSongs";
            this.lblNbSongs.Size = new System.Drawing.Size(61, 13);
            this.lblNbSongs.TabIndex = 4;
            this.lblNbSongs.Text = "lblNbSongs";
            // 
            // lblNbSprites
            // 
            this.lblNbSprites.AutoSize = true;
            this.lblNbSprites.BackColor = System.Drawing.Color.Transparent;
            this.lblNbSprites.Location = new System.Drawing.Point(6, 72);
            this.lblNbSprites.Name = "lblNbSprites";
            this.lblNbSprites.Size = new System.Drawing.Size(63, 13);
            this.lblNbSprites.TabIndex = 3;
            this.lblNbSprites.Text = "lblNbSprites";
            // 
            // lblProjectSize
            // 
            this.lblProjectSize.AutoSize = true;
            this.lblProjectSize.BackColor = System.Drawing.Color.Transparent;
            this.lblProjectSize.Location = new System.Drawing.Point(6, 48);
            this.lblProjectSize.Name = "lblProjectSize";
            this.lblProjectSize.Size = new System.Drawing.Size(70, 13);
            this.lblProjectSize.TabIndex = 2;
            this.lblProjectSize.Text = "lblProjectSize";
            // 
            // lblProjectName
            // 
            this.lblProjectName.AutoSize = true;
            this.lblProjectName.BackColor = System.Drawing.Color.Transparent;
            this.lblProjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProjectName.Location = new System.Drawing.Point(12, 9);
            this.lblProjectName.Name = "lblProjectName";
            this.lblProjectName.Size = new System.Drawing.Size(198, 31);
            this.lblProjectName.TabIndex = 5;
            this.lblProjectName.TabStop = true;
            this.lblProjectName.Text = "lblProjectName";
            this.lblProjectName.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblProjectName_LinkClicked);
            // 
            // ProjectDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PixelLionWin.Properties.Resources.NewMapBack;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(429, 221);
            this.Controls.Add(this.lblProjectName);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProjectDetails";
            this.Text = "Détails du projet";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ProjectDetails_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMapNumber;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblProjectSize;
        private System.Windows.Forms.Label lblNbSprites;
        private System.Windows.Forms.Label lblNbSongs;
        private System.Windows.Forms.Label lblNbSounds;
        private System.Windows.Forms.LinkLabel lblProjectName;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.Button btnChangePath;
    }
}