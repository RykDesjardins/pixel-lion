﻿using PixelLionLib;

namespace PixelLionWin
{
    partial class PeventEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PN_Graphics = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_Name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CB_Class = new System.Windows.Forms.ComboBox();
            this.TB_Script = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CB_Direction = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.LB_Position = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.NUM_Sight = new System.Windows.Forms.NumericUpDown();
            this.NUM_Speed = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CHK_Flying = new System.Windows.Forms.CheckBox();
            this.CHK_Disposable = new System.Windows.Forms.CheckBox();
            this.CB_TriggerType = new System.Windows.Forms.ComboBox();
            this.CHK_Through = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CB_MoveType = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.BTN_Cancel = new System.Windows.Forms.Button();
            this.BTN_Delete = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Sight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Speed)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // PN_Graphics
            // 
            this.PN_Graphics.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PN_Graphics.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PN_Graphics.Location = new System.Drawing.Point(18, 162);
            this.PN_Graphics.Name = "PN_Graphics";
            this.PN_Graphics.Size = new System.Drawing.Size(260, 229);
            this.PN_Graphics.TabIndex = 0;
            this.PN_Graphics.Click += new System.EventHandler(this.PN_Graphics_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Graphics";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nom du Pevent";
            // 
            // TB_Name
            // 
            this.TB_Name.Location = new System.Drawing.Point(96, 6);
            this.TB_Name.Name = "TB_Name";
            this.TB_Name.Size = new System.Drawing.Size(273, 20);
            this.TB_Name.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(375, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Classe";
            // 
            // CB_Class
            // 
            this.CB_Class.FormattingEnabled = true;
            this.CB_Class.Location = new System.Drawing.Point(419, 6);
            this.CB_Class.Name = "CB_Class";
            this.CB_Class.Size = new System.Drawing.Size(192, 21);
            this.CB_Class.TabIndex = 5;
            this.CB_Class.SelectedIndexChanged += new System.EventHandler(this.CB_Class_SelectedIndexChanged);
            // 
            // TB_Script
            // 
            this.TB_Script.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TB_Script.ForeColor = System.Drawing.Color.Lime;
            this.TB_Script.Location = new System.Drawing.Point(15, 24);
            this.TB_Script.Multiline = true;
            this.TB_Script.Name = "TB_Script";
            this.TB_Script.Size = new System.Drawing.Size(251, 253);
            this.TB_Script.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Mouvement";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.CB_Direction);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.LB_Position);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.NUM_Sight);
            this.groupBox1.Controls.Add(this.NUM_Speed);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.PN_Graphics);
            this.groupBox1.Location = new System.Drawing.Point(311, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 405);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Attributs";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Vision";
            // 
            // CB_Direction
            // 
            this.CB_Direction.FormattingEnabled = true;
            this.CB_Direction.Items.AddRange(new object[] {
            "Left",
            "Right",
            "Up",
            "Down",
            "None",
            "Random"});
            this.CB_Direction.Location = new System.Drawing.Point(78, 84);
            this.CB_Direction.Name = "CB_Direction";
            this.CB_Direction.Size = new System.Drawing.Size(205, 21);
            this.CB_Direction.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Direction";
            // 
            // LB_Position
            // 
            this.LB_Position.AutoSize = true;
            this.LB_Position.Location = new System.Drawing.Point(74, 59);
            this.LB_Position.Name = "LB_Position";
            this.LB_Position.Size = new System.Drawing.Size(30, 13);
            this.LB_Position.TabIndex = 3;
            this.LB_Position.Text = "0 x 0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Position";
            // 
            // NUM_Sight
            // 
            this.NUM_Sight.Location = new System.Drawing.Point(77, 113);
            this.NUM_Sight.Name = "NUM_Sight";
            this.NUM_Sight.Size = new System.Drawing.Size(206, 20);
            this.NUM_Sight.TabIndex = 1;
            // 
            // NUM_Speed
            // 
            this.NUM_Speed.Location = new System.Drawing.Point(77, 27);
            this.NUM_Speed.Name = "NUM_Speed";
            this.NUM_Speed.Size = new System.Drawing.Size(206, 20);
            this.NUM_Speed.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Vitesse";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CHK_Flying);
            this.groupBox2.Controls.Add(this.CHK_Disposable);
            this.groupBox2.Controls.Add(this.CB_TriggerType);
            this.groupBox2.Controls.Add(this.CHK_Through);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.CB_MoveType);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(22, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(274, 128);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Interaction";
            // 
            // CHK_Flying
            // 
            this.CHK_Flying.AutoSize = true;
            this.CHK_Flying.Location = new System.Drawing.Point(191, 88);
            this.CHK_Flying.Name = "CHK_Flying";
            this.CHK_Flying.Size = new System.Drawing.Size(41, 17);
            this.CHK_Flying.TabIndex = 7;
            this.CHK_Flying.Text = "Vol";
            this.CHK_Flying.UseVisualStyleBackColor = true;
            // 
            // CHK_Disposable
            // 
            this.CHK_Disposable.AutoSize = true;
            this.CHK_Disposable.Location = new System.Drawing.Point(111, 88);
            this.CHK_Disposable.Name = "CHK_Disposable";
            this.CHK_Disposable.Size = new System.Drawing.Size(74, 17);
            this.CHK_Disposable.TabIndex = 9;
            this.CHK_Disposable.Text = "Éphémère";
            this.CHK_Disposable.UseVisualStyleBackColor = true;
            // 
            // CB_TriggerType
            // 
            this.CB_TriggerType.FormattingEnabled = true;
            this.CB_TriggerType.Items.AddRange(new object[] {
                PeventTriggerType.Proximity,
                PeventTriggerType.Step,
                PeventTriggerType.Press,
                PeventTriggerType.Distance,
                PeventTriggerType.Auto
            });
            this.CB_TriggerType.Location = new System.Drawing.Point(89, 56);
            this.CB_TriggerType.Name = "CB_TriggerType";
            this.CB_TriggerType.Size = new System.Drawing.Size(168, 21);
            this.CB_TriggerType.TabIndex = 10;
            // 
            // CHK_Through
            // 
            this.CHK_Through.AutoSize = true;
            this.CHK_Through.Location = new System.Drawing.Point(23, 88);
            this.CHK_Through.Name = "CHK_Through";
            this.CHK_Through.Size = new System.Drawing.Size(82, 17);
            this.CHK_Through.TabIndex = 8;
            this.CHK_Through.Text = "Traversable";
            this.CHK_Through.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Trigger";
            // 
            // CB_MoveType
            // 
            this.CB_MoveType.FormattingEnabled = true;
            this.CB_MoveType.Items.AddRange(new object[] {
            "None",
            "Magnet",
            "Away",
            "Random",
            "Custom"});
            this.CB_MoveType.Location = new System.Drawing.Point(89, 27);
            this.CB_MoveType.Name = "CB_MoveType";
            this.CB_MoveType.Size = new System.Drawing.Size(168, 21);
            this.CB_MoveType.TabIndex = 8;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TB_Script);
            this.groupBox3.Location = new System.Drawing.Point(22, 179);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(283, 292);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Script Pixel Monkey";
            // 
            // BTN_Save
            // 
            this.BTN_Save.Location = new System.Drawing.Point(523, 448);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(88, 23);
            this.BTN_Save.TabIndex = 11;
            this.BTN_Save.Text = "Enregistrer";
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // BTN_Cancel
            // 
            this.BTN_Cancel.Location = new System.Drawing.Point(442, 448);
            this.BTN_Cancel.Name = "BTN_Cancel";
            this.BTN_Cancel.Size = new System.Drawing.Size(75, 23);
            this.BTN_Cancel.TabIndex = 12;
            this.BTN_Cancel.Text = "Annuler";
            this.BTN_Cancel.UseVisualStyleBackColor = true;
            this.BTN_Cancel.Click += new System.EventHandler(this.BTN_Cancel_Click);
            // 
            // BTN_Delete
            // 
            this.BTN_Delete.Location = new System.Drawing.Point(311, 448);
            this.BTN_Delete.Name = "BTN_Delete";
            this.BTN_Delete.Size = new System.Drawing.Size(75, 23);
            this.BTN_Delete.TabIndex = 13;
            this.BTN_Delete.Text = "Supprimer";
            this.BTN_Delete.UseVisualStyleBackColor = true;
            this.BTN_Delete.Click += new System.EventHandler(this.BTN_Delete_Click);
            // 
            // PeventEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 481);
            this.Controls.Add(this.BTN_Delete);
            this.Controls.Add(this.BTN_Cancel);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CB_Class);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TB_Name);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PeventEditor";
            this.Text = "Pevent ~";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Sight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Speed)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PN_Graphics;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_Name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CB_Class;
        private System.Windows.Forms.TextBox TB_Script;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CB_MoveType;
        private System.Windows.Forms.ComboBox CB_TriggerType;
        private System.Windows.Forms.NumericUpDown NUM_Speed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LB_Position;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CB_Direction;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown NUM_Sight;
        private System.Windows.Forms.CheckBox CHK_Disposable;
        private System.Windows.Forms.CheckBox CHK_Through;
        private System.Windows.Forms.CheckBox CHK_Flying;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Button BTN_Cancel;
        private System.Windows.Forms.Button BTN_Delete;
    }
}