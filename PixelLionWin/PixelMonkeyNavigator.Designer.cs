﻿namespace PixelLionWin
{
    partial class PixelMonkeyNavigator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PixelMonkeyNavigator));
            this.TREE_Content = new System.Windows.Forms.TreeView();
            this.TREE_IconList = new System.Windows.Forms.ImageList(this.components);
            this.GROUP_Infos = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LB_Info1Type = new System.Windows.Forms.Label();
            this.LB_Info1 = new System.Windows.Forms.Label();
            this.LB_ListTitle = new System.Windows.Forms.Label();
            this.LIST_ParamsType = new System.Windows.Forms.ListBox();
            this.LB_Name = new System.Windows.Forms.Label();
            this.LB_ParentName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LB_Type = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PN_Icon = new System.Windows.Forms.Panel();
            this.GROUP_Infos.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TREE_Content
            // 
            this.TREE_Content.ImageIndex = 0;
            this.TREE_Content.ImageList = this.TREE_IconList;
            this.TREE_Content.Location = new System.Drawing.Point(12, 13);
            this.TREE_Content.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TREE_Content.Name = "TREE_Content";
            this.TREE_Content.SelectedImageIndex = 0;
            this.TREE_Content.Size = new System.Drawing.Size(256, 373);
            this.TREE_Content.TabIndex = 0;
            this.TREE_Content.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TREE_Content_AfterSelect);
            // 
            // TREE_IconList
            // 
            this.TREE_IconList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("TREE_IconList.ImageStream")));
            this.TREE_IconList.TransparentColor = System.Drawing.Color.Transparent;
            this.TREE_IconList.Images.SetKeyName(0, "TREEFunctionLogo.png");
            this.TREE_IconList.Images.SetKeyName(1, "TREEClassLogo.png");
            this.TREE_IconList.Images.SetKeyName(2, "TREEPropertyLogo.png");
            this.TREE_IconList.Images.SetKeyName(3, "TREEMethodLogo.png");
            // 
            // GROUP_Infos
            // 
            this.GROUP_Infos.Controls.Add(this.PN_Icon);
            this.GROUP_Infos.Controls.Add(this.groupBox1);
            this.GROUP_Infos.Controls.Add(this.LB_Name);
            this.GROUP_Infos.Controls.Add(this.LB_ParentName);
            this.GROUP_Infos.Controls.Add(this.label2);
            this.GROUP_Infos.Controls.Add(this.LB_Type);
            this.GROUP_Infos.Controls.Add(this.label1);
            this.GROUP_Infos.Location = new System.Drawing.Point(274, 13);
            this.GROUP_Infos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GROUP_Infos.Name = "GROUP_Infos";
            this.GROUP_Infos.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GROUP_Infos.Size = new System.Drawing.Size(434, 372);
            this.GROUP_Infos.TabIndex = 1;
            this.GROUP_Infos.TabStop = false;
            this.GROUP_Infos.Text = "Information sur le contenu";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LB_Info1Type);
            this.groupBox1.Controls.Add(this.LB_Info1);
            this.groupBox1.Controls.Add(this.LB_ListTitle);
            this.groupBox1.Controls.Add(this.LIST_ParamsType);
            this.groupBox1.Location = new System.Drawing.Point(27, 125);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(381, 220);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informations supplémentaires";
            // 
            // LB_Info1Type
            // 
            this.LB_Info1Type.AutoSize = true;
            this.LB_Info1Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LB_Info1Type.Location = new System.Drawing.Point(25, 34);
            this.LB_Info1Type.Name = "LB_Info1Type";
            this.LB_Info1Type.Size = new System.Drawing.Size(111, 13);
            this.LB_Info1Type.TabIndex = 6;
            this.LB_Info1Type.Text = "Type d\'information";
            // 
            // LB_Info1
            // 
            this.LB_Info1.AutoSize = true;
            this.LB_Info1.Location = new System.Drawing.Point(28, 58);
            this.LB_Info1.Name = "LB_Info1";
            this.LB_Info1.Size = new System.Drawing.Size(63, 14);
            this.LB_Info1.TabIndex = 7;
            this.LB_Info1.Text = "Information";
            // 
            // LB_ListTitle
            // 
            this.LB_ListTitle.AutoSize = true;
            this.LB_ListTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LB_ListTitle.Location = new System.Drawing.Point(205, 34);
            this.LB_ListTitle.Name = "LB_ListTitle";
            this.LB_ListTitle.Size = new System.Drawing.Size(119, 13);
            this.LB_ListTitle.TabIndex = 5;
            this.LB_ListTitle.Text = "Type de paramètres";
            // 
            // LIST_ParamsType
            // 
            this.LIST_ParamsType.FormattingEnabled = true;
            this.LIST_ParamsType.ItemHeight = 14;
            this.LIST_ParamsType.Location = new System.Drawing.Point(208, 50);
            this.LIST_ParamsType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LIST_ParamsType.Name = "LIST_ParamsType";
            this.LIST_ParamsType.Size = new System.Drawing.Size(147, 144);
            this.LIST_ParamsType.TabIndex = 4;
            // 
            // LB_Name
            // 
            this.LB_Name.AutoSize = true;
            this.LB_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LB_Name.Location = new System.Drawing.Point(23, 30);
            this.LB_Name.Name = "LB_Name";
            this.LB_Name.Size = new System.Drawing.Size(52, 24);
            this.LB_Name.TabIndex = 8;
            this.LB_Name.Text = "Titre";
            // 
            // LB_ParentName
            // 
            this.LB_ParentName.AutoSize = true;
            this.LB_ParentName.Location = new System.Drawing.Point(80, 92);
            this.LB_ParentName.Name = "LB_ParentName";
            this.LB_ParentName.Size = new System.Drawing.Size(72, 14);
            this.LB_ParentName.TabIndex = 3;
            this.LB_ParentName.Text = "Aucun parent";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Parent";
            // 
            // LB_Type
            // 
            this.LB_Type.AutoSize = true;
            this.LB_Type.Location = new System.Drawing.Point(99, 66);
            this.LB_Type.Name = "LB_Type";
            this.LB_Type.Size = new System.Drawing.Size(61, 14);
            this.LB_Type.TabIndex = 1;
            this.LB_Type.Text = "Aucun type";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type";
            // 
            // PN_Icon
            // 
            this.PN_Icon.Location = new System.Drawing.Point(80, 65);
            this.PN_Icon.Name = "PN_Icon";
            this.PN_Icon.Size = new System.Drawing.Size(16, 16);
            this.PN_Icon.TabIndex = 10;
            // 
            // PixelMonkeyNavigator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 399);
            this.Controls.Add(this.GROUP_Infos);
            this.Controls.Add(this.TREE_Content);
            this.Font = new System.Drawing.Font("Leelawadee", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PixelMonkeyNavigator";
            this.Text = "Navigateur Pixel Monkey";
            this.GROUP_Infos.ResumeLayout(false);
            this.GROUP_Infos.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView TREE_Content;
        private System.Windows.Forms.GroupBox GROUP_Infos;
        private System.Windows.Forms.ImageList TREE_IconList;
        private System.Windows.Forms.Label LB_Type;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LB_ParentName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LB_ListTitle;
        private System.Windows.Forms.ListBox LIST_ParamsType;
        private System.Windows.Forms.Label LB_Info1;
        private System.Windows.Forms.Label LB_Info1Type;
        private System.Windows.Forms.Label LB_Name;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel PN_Icon;
    }
}