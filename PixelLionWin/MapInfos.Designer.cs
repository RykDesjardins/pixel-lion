﻿namespace PixelLionWin
{
    partial class MapInfos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapInfos));
            this.PN_Title = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.NUM_Y = new System.Windows.Forms.NumericUpDown();
            this.NUM_X = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TB_Rename = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.LB_Filesize = new System.Windows.Forms.Label();
            this.LB_Filename = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PB_Tileset = new System.Windows.Forms.PictureBox();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.BTN_Close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_X)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Tileset)).BeginInit();
            this.SuspendLayout();
            // 
            // PN_Title
            // 
            this.PN_Title.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Title.Location = new System.Drawing.Point(0, 0);
            this.PN_Title.Name = "PN_Title";
            this.PN_Title.Size = new System.Drawing.Size(450, 200);
            this.PN_Title.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(83, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 26);
            this.label3.TabIndex = 8;
            this.label3.Text = "X";
            // 
            // NUM_Y
            // 
            this.NUM_Y.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NUM_Y.Location = new System.Drawing.Point(120, 29);
            this.NUM_Y.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.NUM_Y.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NUM_Y.Name = "NUM_Y";
            this.NUM_Y.Size = new System.Drawing.Size(53, 20);
            this.NUM_Y.TabIndex = 5;
            this.NUM_Y.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // NUM_X
            // 
            this.NUM_X.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NUM_X.Location = new System.Drawing.Point(17, 29);
            this.NUM_X.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.NUM_X.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NUM_X.Name = "NUM_X";
            this.NUM_X.Size = new System.Drawing.Size(53, 20);
            this.NUM_X.TabIndex = 6;
            this.NUM_X.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.NUM_X);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.NUM_Y);
            this.groupBox1.Location = new System.Drawing.Point(12, 206);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(190, 72);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Taille";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TB_Rename);
            this.groupBox2.Location = new System.Drawing.Point(208, 206);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(230, 72);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Renommer";
            // 
            // TB_Rename
            // 
            this.TB_Rename.Location = new System.Drawing.Point(20, 29);
            this.TB_Rename.Name = "TB_Rename";
            this.TB_Rename.Size = new System.Drawing.Size(191, 20);
            this.TB_Rename.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.LB_Filesize);
            this.groupBox3.Controls.Add(this.LB_Filename);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(208, 284);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(230, 88);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informations";
            // 
            // LB_Filesize
            // 
            this.LB_Filesize.AutoSize = true;
            this.LB_Filesize.Location = new System.Drawing.Point(64, 50);
            this.LB_Filesize.Name = "LB_Filesize";
            this.LB_Filesize.Size = new System.Drawing.Size(35, 13);
            this.LB_Filesize.TabIndex = 3;
            this.LB_Filesize.Text = "label5";
            // 
            // LB_Filename
            // 
            this.LB_Filename.AutoSize = true;
            this.LB_Filename.Location = new System.Drawing.Point(64, 26);
            this.LB_Filename.Name = "LB_Filename";
            this.LB_Filename.Size = new System.Drawing.Size(35, 13);
            this.LB_Filename.TabIndex = 2;
            this.LB_Filename.Text = "label4";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Taille";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fichier ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.panel1);
            this.groupBox4.Location = new System.Drawing.Point(12, 284);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(190, 134);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Tileset";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.PB_Tileset);
            this.panel1.Location = new System.Drawing.Point(13, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(160, 100);
            this.panel1.TabIndex = 0;
            // 
            // PB_Tileset
            // 
            this.PB_Tileset.Location = new System.Drawing.Point(0, 0);
            this.PB_Tileset.Name = "PB_Tileset";
            this.PB_Tileset.Size = new System.Drawing.Size(74, 63);
            this.PB_Tileset.TabIndex = 0;
            this.PB_Tileset.TabStop = false;
            this.PB_Tileset.Click += new System.EventHandler(this.PB_Tileset_Click);
            // 
            // BTN_Save
            // 
            this.BTN_Save.BackgroundImage = global::PixelLionWin.Properties.Resources.SaveIcon;
            this.BTN_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Save.Location = new System.Drawing.Point(398, 378);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(40, 40);
            this.BTN_Save.TabIndex = 13;
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // BTN_Close
            // 
            this.BTN_Close.BackgroundImage = global::PixelLionWin.Properties.Resources.MediaStopButton;
            this.BTN_Close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BTN_Close.Location = new System.Drawing.Point(352, 378);
            this.BTN_Close.Name = "BTN_Close";
            this.BTN_Close.Size = new System.Drawing.Size(40, 40);
            this.BTN_Close.TabIndex = 13;
            this.BTN_Close.UseVisualStyleBackColor = true;
            this.BTN_Close.Click += new System.EventHandler(this.BTN_Close_Click);
            // 
            // MapInfos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 426);
            this.Controls.Add(this.BTN_Close);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.PN_Title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MapInfos";
            this.Text = "Information sur la carte";
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_X)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PB_Tileset)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PN_Title;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown NUM_Y;
        private System.Windows.Forms.NumericUpDown NUM_X;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TB_Rename;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LB_Filesize;
        private System.Windows.Forms.Label LB_Filename;
        private System.Windows.Forms.PictureBox PB_Tileset;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Button BTN_Close;
    }
}