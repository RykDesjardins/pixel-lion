﻿namespace PixelLionWin
{
    partial class PixelFamilyEdition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CB_ParticleShape = new System.Windows.Forms.ComboBox();
            this.NUM_VectorX = new System.Windows.Forms.NumericUpDown();
            this.NUM_VectorY = new System.Windows.Forms.NumericUpDown();
            this.NUM_Opacity = new System.Windows.Forms.NumericUpDown();
            this.NUM_CurrentSize = new System.Windows.Forms.NumericUpDown();
            this.NUM_VelocityX = new System.Windows.Forms.NumericUpDown();
            this.NUM_VelocityY = new System.Windows.Forms.NumericUpDown();
            this.NUM_Lifespan = new System.Windows.Forms.NumericUpDown();
            this.NUM_SizeRatioOverTime = new System.Windows.Forms.NumericUpDown();
            this.CHK_FadeOut = new System.Windows.Forms.CheckBox();
            this.CHK_FadeIn = new System.Windows.Forms.CheckBox();
            this.NUM_FadeInFrameCount = new System.Windows.Forms.NumericUpDown();
            this.NUM_FadeOutStart = new System.Windows.Forms.NumericUpDown();
            this.PN_Pixel = new System.Windows.Forms.Panel();
            this.BTN_Advanced = new System.Windows.Forms.Button();
            this.GB_Fade = new System.Windows.Forms.GroupBox();
            this.LB_FadeOutStart = new System.Windows.Forms.Label();
            this.LB_FadeInFrameCount = new System.Windows.Forms.Label();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.BTN_SaveAndQuit = new System.Windows.Forms.Button();
            this.BTN_Quit = new System.Windows.Forms.Button();
            this.LB_VectorY = new System.Windows.Forms.Label();
            this.LB_Shape = new System.Windows.Forms.Label();
            this.LB_SizeRatioOverTime = new System.Windows.Forms.Label();
            this.LB_Size = new System.Windows.Forms.Label();
            this.LB_VectorX = new System.Windows.Forms.Label();
            this.GB_Vector = new System.Windows.Forms.GroupBox();
            this.LB_VelocityX = new System.Windows.Forms.Label();
            this.LB_VelocityY = new System.Windows.Forms.Label();
            this.GB_Velocity = new System.Windows.Forms.GroupBox();
            this.LB_Lifespan = new System.Windows.Forms.Label();
            this.LB_Opacity = new System.Windows.Forms.Label();
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAndQuitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NUM_Blue = new System.Windows.Forms.NumericUpDown();
            this.NUM_Green = new System.Windows.Forms.NumericUpDown();
            this.NUM_Red = new System.Windows.Forms.NumericUpDown();
            this.LB_Green = new System.Windows.Forms.Label();
            this.LB_Red = new System.Windows.Forms.Label();
            this.LB_Blue = new System.Windows.Forms.Label();
            this.GB_Color = new System.Windows.Forms.GroupBox();
            this.GB_Apperance = new System.Windows.Forms.GroupBox();
            this.CB_Behavior = new System.Windows.Forms.ComboBox();
            this.LB_Behavior = new System.Windows.Forms.Label();
            this.GB_Size = new System.Windows.Forms.GroupBox();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.BTN_AddFamily = new System.Windows.Forms.Button();
            this.ATL_Temps = new PixelLionWin.AnimationTimeLine();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_VectorX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_VectorY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Opacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_CurrentSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_VelocityX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_VelocityY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Lifespan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_SizeRatioOverTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_FadeInFrameCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_FadeOutStart)).BeginInit();
            this.GB_Fade.SuspendLayout();
            this.GB_Vector.SuspendLayout();
            this.GB_Velocity.SuspendLayout();
            this.Menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Blue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Green)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Red)).BeginInit();
            this.GB_Color.SuspendLayout();
            this.GB_Apperance.SuspendLayout();
            this.GB_Size.SuspendLayout();
            this.SuspendLayout();
            // 
            // CB_ParticleShape
            // 
            this.CB_ParticleShape.FormattingEnabled = true;
            this.CB_ParticleShape.Location = new System.Drawing.Point(67, 18);
            this.CB_ParticleShape.Name = "CB_ParticleShape";
            this.CB_ParticleShape.Size = new System.Drawing.Size(108, 21);
            this.CB_ParticleShape.TabIndex = 0;
            // 
            // NUM_VectorX
            // 
            this.NUM_VectorX.Location = new System.Drawing.Point(41, 19);
            this.NUM_VectorX.Name = "NUM_VectorX";
            this.NUM_VectorX.Size = new System.Drawing.Size(45, 20);
            this.NUM_VectorX.TabIndex = 4;
            // 
            // NUM_VectorY
            // 
            this.NUM_VectorY.Location = new System.Drawing.Point(128, 19);
            this.NUM_VectorY.Name = "NUM_VectorY";
            this.NUM_VectorY.Size = new System.Drawing.Size(45, 20);
            this.NUM_VectorY.TabIndex = 5;
            // 
            // NUM_Opacity
            // 
            this.NUM_Opacity.Location = new System.Drawing.Point(62, 92);
            this.NUM_Opacity.Name = "NUM_Opacity";
            this.NUM_Opacity.Size = new System.Drawing.Size(47, 20);
            this.NUM_Opacity.TabIndex = 6;
            // 
            // NUM_CurrentSize
            // 
            this.NUM_CurrentSize.Location = new System.Drawing.Point(114, 40);
            this.NUM_CurrentSize.Name = "NUM_CurrentSize";
            this.NUM_CurrentSize.Size = new System.Drawing.Size(45, 20);
            this.NUM_CurrentSize.TabIndex = 7;
            // 
            // NUM_VelocityX
            // 
            this.NUM_VelocityX.Location = new System.Drawing.Point(41, 19);
            this.NUM_VelocityX.Name = "NUM_VelocityX";
            this.NUM_VelocityX.Size = new System.Drawing.Size(45, 20);
            this.NUM_VelocityX.TabIndex = 8;
            // 
            // NUM_VelocityY
            // 
            this.NUM_VelocityY.Location = new System.Drawing.Point(128, 19);
            this.NUM_VelocityY.Name = "NUM_VelocityY";
            this.NUM_VelocityY.Size = new System.Drawing.Size(45, 20);
            this.NUM_VelocityY.TabIndex = 9;
            // 
            // NUM_Lifespan
            // 
            this.NUM_Lifespan.Location = new System.Drawing.Point(632, 141);
            this.NUM_Lifespan.Name = "NUM_Lifespan";
            this.NUM_Lifespan.Size = new System.Drawing.Size(120, 20);
            this.NUM_Lifespan.TabIndex = 10;
            // 
            // NUM_SizeRatioOverTime
            // 
            this.NUM_SizeRatioOverTime.Location = new System.Drawing.Point(114, 14);
            this.NUM_SizeRatioOverTime.Name = "NUM_SizeRatioOverTime";
            this.NUM_SizeRatioOverTime.Size = new System.Drawing.Size(45, 20);
            this.NUM_SizeRatioOverTime.TabIndex = 11;
            // 
            // CHK_FadeOut
            // 
            this.CHK_FadeOut.AutoSize = true;
            this.CHK_FadeOut.Location = new System.Drawing.Point(18, 19);
            this.CHK_FadeOut.Name = "CHK_FadeOut";
            this.CHK_FadeOut.Size = new System.Drawing.Size(70, 17);
            this.CHK_FadeOut.TabIndex = 12;
            this.CHK_FadeOut.Text = "Fade Out";
            this.CHK_FadeOut.UseVisualStyleBackColor = true;
            this.CHK_FadeOut.CheckedChanged += new System.EventHandler(this.CHK_FadeOut_CheckedChanged);
            // 
            // CHK_FadeIn
            // 
            this.CHK_FadeIn.AutoSize = true;
            this.CHK_FadeIn.Location = new System.Drawing.Point(18, 42);
            this.CHK_FadeIn.Name = "CHK_FadeIn";
            this.CHK_FadeIn.Size = new System.Drawing.Size(62, 17);
            this.CHK_FadeIn.TabIndex = 13;
            this.CHK_FadeIn.Text = "Fade In";
            this.CHK_FadeIn.UseVisualStyleBackColor = true;
            this.CHK_FadeIn.CheckedChanged += new System.EventHandler(this.CHK_FadeIn_CheckedChanged);
            // 
            // NUM_FadeInFrameCount
            // 
            this.NUM_FadeInFrameCount.Location = new System.Drawing.Point(206, 41);
            this.NUM_FadeInFrameCount.Name = "NUM_FadeInFrameCount";
            this.NUM_FadeInFrameCount.Size = new System.Drawing.Size(71, 20);
            this.NUM_FadeInFrameCount.TabIndex = 14;
            // 
            // NUM_FadeOutStart
            // 
            this.NUM_FadeOutStart.Location = new System.Drawing.Point(206, 18);
            this.NUM_FadeOutStart.Name = "NUM_FadeOutStart";
            this.NUM_FadeOutStart.Size = new System.Drawing.Size(71, 20);
            this.NUM_FadeOutStart.TabIndex = 15;
            // 
            // PN_Pixel
            // 
            this.PN_Pixel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.PN_Pixel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_Pixel.Location = new System.Drawing.Point(12, 27);
            this.PN_Pixel.Name = "PN_Pixel";
            this.PN_Pixel.Size = new System.Drawing.Size(226, 226);
            this.PN_Pixel.TabIndex = 16;
            // 
            // BTN_Advanced
            // 
            this.BTN_Advanced.Location = new System.Drawing.Point(678, 194);
            this.BTN_Advanced.Name = "BTN_Advanced";
            this.BTN_Advanced.Size = new System.Drawing.Size(74, 23);
            this.BTN_Advanced.TabIndex = 18;
            this.BTN_Advanced.Text = "Advanced";
            this.BTN_Advanced.UseVisualStyleBackColor = true;
            this.BTN_Advanced.Click += new System.EventHandler(this.BTN_Advanced_Click);
            // 
            // GB_Fade
            // 
            this.GB_Fade.Controls.Add(this.LB_FadeOutStart);
            this.GB_Fade.Controls.Add(this.LB_FadeInFrameCount);
            this.GB_Fade.Controls.Add(this.CHK_FadeOut);
            this.GB_Fade.Controls.Add(this.CHK_FadeIn);
            this.GB_Fade.Controls.Add(this.NUM_FadeInFrameCount);
            this.GB_Fade.Controls.Add(this.NUM_FadeOutStart);
            this.GB_Fade.Location = new System.Drawing.Point(244, 184);
            this.GB_Fade.Name = "GB_Fade";
            this.GB_Fade.Size = new System.Drawing.Size(290, 69);
            this.GB_Fade.TabIndex = 19;
            this.GB_Fade.TabStop = false;
            this.GB_Fade.Text = "Fade";
            // 
            // LB_FadeOutStart
            // 
            this.LB_FadeOutStart.AutoSize = true;
            this.LB_FadeOutStart.Location = new System.Drawing.Point(122, 20);
            this.LB_FadeOutStart.Name = "LB_FadeOutStart";
            this.LB_FadeOutStart.Size = new System.Drawing.Size(78, 13);
            this.LB_FadeOutStart.TabIndex = 34;
            this.LB_FadeOutStart.Text = "Fade out start :";
            // 
            // LB_FadeInFrameCount
            // 
            this.LB_FadeInFrameCount.AutoSize = true;
            this.LB_FadeInFrameCount.Location = new System.Drawing.Point(93, 43);
            this.LB_FadeInFrameCount.Name = "LB_FadeInFrameCount";
            this.LB_FadeInFrameCount.Size = new System.Drawing.Size(107, 13);
            this.LB_FadeInFrameCount.TabIndex = 35;
            this.LB_FadeInFrameCount.Text = "Fade in frame count :";
            // 
            // BTN_Save
            // 
            this.BTN_Save.Location = new System.Drawing.Point(749, 249);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(93, 23);
            this.BTN_Save.TabIndex = 20;
            this.BTN_Save.Text = "Save";
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // BTN_SaveAndQuit
            // 
            this.BTN_SaveAndQuit.Location = new System.Drawing.Point(749, 278);
            this.BTN_SaveAndQuit.Name = "BTN_SaveAndQuit";
            this.BTN_SaveAndQuit.Size = new System.Drawing.Size(93, 23);
            this.BTN_SaveAndQuit.TabIndex = 21;
            this.BTN_SaveAndQuit.Text = "Save and Quit";
            this.BTN_SaveAndQuit.UseVisualStyleBackColor = true;
            this.BTN_SaveAndQuit.Click += new System.EventHandler(this.BTN_SaveAndQuit_Click);
            // 
            // BTN_Quit
            // 
            this.BTN_Quit.Location = new System.Drawing.Point(749, 307);
            this.BTN_Quit.Name = "BTN_Quit";
            this.BTN_Quit.Size = new System.Drawing.Size(93, 23);
            this.BTN_Quit.TabIndex = 22;
            this.BTN_Quit.Text = "Quit";
            this.BTN_Quit.UseVisualStyleBackColor = true;
            this.BTN_Quit.Click += new System.EventHandler(this.BTN_Quit_Click);
            // 
            // LB_VectorY
            // 
            this.LB_VectorY.AutoSize = true;
            this.LB_VectorY.Location = new System.Drawing.Point(102, 21);
            this.LB_VectorY.Name = "LB_VectorY";
            this.LB_VectorY.Size = new System.Drawing.Size(20, 13);
            this.LB_VectorY.TabIndex = 23;
            this.LB_VectorY.Text = "Y :";
            // 
            // LB_Shape
            // 
            this.LB_Shape.AutoSize = true;
            this.LB_Shape.Location = new System.Drawing.Point(17, 21);
            this.LB_Shape.Name = "LB_Shape";
            this.LB_Shape.Size = new System.Drawing.Size(44, 13);
            this.LB_Shape.TabIndex = 24;
            this.LB_Shape.Text = "Shape :";
            // 
            // LB_SizeRatioOverTime
            // 
            this.LB_SizeRatioOverTime.AutoSize = true;
            this.LB_SizeRatioOverTime.Location = new System.Drawing.Point(6, 16);
            this.LB_SizeRatioOverTime.Name = "LB_SizeRatioOverTime";
            this.LB_SizeRatioOverTime.Size = new System.Drawing.Size(102, 13);
            this.LB_SizeRatioOverTime.TabIndex = 26;
            this.LB_SizeRatioOverTime.Text = "Size ratio over time :";
            // 
            // LB_Size
            // 
            this.LB_Size.AutoSize = true;
            this.LB_Size.Location = new System.Drawing.Point(75, 42);
            this.LB_Size.Name = "LB_Size";
            this.LB_Size.Size = new System.Drawing.Size(33, 13);
            this.LB_Size.TabIndex = 29;
            this.LB_Size.Text = "Size :";
            // 
            // LB_VectorX
            // 
            this.LB_VectorX.AutoSize = true;
            this.LB_VectorX.Location = new System.Drawing.Point(15, 22);
            this.LB_VectorX.Name = "LB_VectorX";
            this.LB_VectorX.Size = new System.Drawing.Size(20, 13);
            this.LB_VectorX.TabIndex = 30;
            this.LB_VectorX.Text = "X :";
            // 
            // GB_Vector
            // 
            this.GB_Vector.Controls.Add(this.NUM_VectorY);
            this.GB_Vector.Controls.Add(this.LB_VectorX);
            this.GB_Vector.Controls.Add(this.NUM_VectorX);
            this.GB_Vector.Controls.Add(this.LB_VectorY);
            this.GB_Vector.Location = new System.Drawing.Point(563, 27);
            this.GB_Vector.Name = "GB_Vector";
            this.GB_Vector.Size = new System.Drawing.Size(189, 50);
            this.GB_Vector.TabIndex = 31;
            this.GB_Vector.TabStop = false;
            this.GB_Vector.Text = "Vector";
            // 
            // LB_VelocityX
            // 
            this.LB_VelocityX.AutoSize = true;
            this.LB_VelocityX.Location = new System.Drawing.Point(15, 21);
            this.LB_VelocityX.Name = "LB_VelocityX";
            this.LB_VelocityX.Size = new System.Drawing.Size(20, 13);
            this.LB_VelocityX.TabIndex = 32;
            this.LB_VelocityX.Text = "X :";
            // 
            // LB_VelocityY
            // 
            this.LB_VelocityY.AutoSize = true;
            this.LB_VelocityY.Location = new System.Drawing.Point(102, 21);
            this.LB_VelocityY.Name = "LB_VelocityY";
            this.LB_VelocityY.Size = new System.Drawing.Size(20, 13);
            this.LB_VelocityY.TabIndex = 31;
            this.LB_VelocityY.Text = "Y :";
            // 
            // GB_Velocity
            // 
            this.GB_Velocity.Controls.Add(this.LB_VelocityY);
            this.GB_Velocity.Controls.Add(this.LB_VelocityX);
            this.GB_Velocity.Controls.Add(this.NUM_VelocityX);
            this.GB_Velocity.Controls.Add(this.NUM_VelocityY);
            this.GB_Velocity.Location = new System.Drawing.Point(563, 83);
            this.GB_Velocity.Name = "GB_Velocity";
            this.GB_Velocity.Size = new System.Drawing.Size(189, 48);
            this.GB_Velocity.TabIndex = 33;
            this.GB_Velocity.TabStop = false;
            this.GB_Velocity.Text = "Velocity";
            // 
            // LB_Lifespan
            // 
            this.LB_Lifespan.AutoSize = true;
            this.LB_Lifespan.Location = new System.Drawing.Point(573, 143);
            this.LB_Lifespan.Name = "LB_Lifespan";
            this.LB_Lifespan.Size = new System.Drawing.Size(53, 13);
            this.LB_Lifespan.TabIndex = 27;
            this.LB_Lifespan.Text = "Lifespan :";
            // 
            // LB_Opacity
            // 
            this.LB_Opacity.AutoSize = true;
            this.LB_Opacity.Location = new System.Drawing.Point(7, 94);
            this.LB_Opacity.Name = "LB_Opacity";
            this.LB_Opacity.Size = new System.Drawing.Size(49, 13);
            this.LB_Opacity.TabIndex = 28;
            this.LB_Opacity.Text = "Opacity :";
            // 
            // Menu
            // 
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(850, 24);
            this.Menu.TabIndex = 34;
            this.Menu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.saveAndQuitToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAndQuitToolStripMenuItem
            // 
            this.saveAndQuitToolStripMenuItem.Name = "saveAndQuitToolStripMenuItem";
            this.saveAndQuitToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.saveAndQuitToolStripMenuItem.Text = "Save and quit";
            this.saveAndQuitToolStripMenuItem.Click += new System.EventHandler(this.saveAndQuitToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // NUM_Blue
            // 
            this.NUM_Blue.Location = new System.Drawing.Point(62, 66);
            this.NUM_Blue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.NUM_Blue.Name = "NUM_Blue";
            this.NUM_Blue.Size = new System.Drawing.Size(47, 20);
            this.NUM_Blue.TabIndex = 35;
            // 
            // NUM_Green
            // 
            this.NUM_Green.Location = new System.Drawing.Point(61, 39);
            this.NUM_Green.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.NUM_Green.Name = "NUM_Green";
            this.NUM_Green.Size = new System.Drawing.Size(47, 20);
            this.NUM_Green.TabIndex = 36;
            // 
            // NUM_Red
            // 
            this.NUM_Red.Location = new System.Drawing.Point(61, 13);
            this.NUM_Red.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.NUM_Red.Name = "NUM_Red";
            this.NUM_Red.Size = new System.Drawing.Size(47, 20);
            this.NUM_Red.TabIndex = 37;
            // 
            // LB_Green
            // 
            this.LB_Green.AutoSize = true;
            this.LB_Green.Location = new System.Drawing.Point(13, 41);
            this.LB_Green.Name = "LB_Green";
            this.LB_Green.Size = new System.Drawing.Size(42, 13);
            this.LB_Green.TabIndex = 38;
            this.LB_Green.Text = "Green :";
            // 
            // LB_Red
            // 
            this.LB_Red.AutoSize = true;
            this.LB_Red.Location = new System.Drawing.Point(22, 16);
            this.LB_Red.Name = "LB_Red";
            this.LB_Red.Size = new System.Drawing.Size(33, 13);
            this.LB_Red.TabIndex = 39;
            this.LB_Red.Text = "Red :";
            // 
            // LB_Blue
            // 
            this.LB_Blue.AutoSize = true;
            this.LB_Blue.Location = new System.Drawing.Point(22, 68);
            this.LB_Blue.Name = "LB_Blue";
            this.LB_Blue.Size = new System.Drawing.Size(34, 13);
            this.LB_Blue.TabIndex = 40;
            this.LB_Blue.Text = "Blue :";
            // 
            // GB_Color
            // 
            this.GB_Color.Controls.Add(this.NUM_Red);
            this.GB_Color.Controls.Add(this.NUM_Opacity);
            this.GB_Color.Controls.Add(this.LB_Blue);
            this.GB_Color.Controls.Add(this.LB_Opacity);
            this.GB_Color.Controls.Add(this.LB_Red);
            this.GB_Color.Controls.Add(this.NUM_Blue);
            this.GB_Color.Controls.Add(this.LB_Green);
            this.GB_Color.Controls.Add(this.NUM_Green);
            this.GB_Color.Location = new System.Drawing.Point(181, 14);
            this.GB_Color.Name = "GB_Color";
            this.GB_Color.Size = new System.Drawing.Size(122, 119);
            this.GB_Color.TabIndex = 41;
            this.GB_Color.TabStop = false;
            this.GB_Color.Text = "Color";
            // 
            // GB_Apperance
            // 
            this.GB_Apperance.Controls.Add(this.CB_Behavior);
            this.GB_Apperance.Controls.Add(this.LB_Behavior);
            this.GB_Apperance.Controls.Add(this.GB_Size);
            this.GB_Apperance.Controls.Add(this.CB_ParticleShape);
            this.GB_Apperance.Controls.Add(this.LB_Shape);
            this.GB_Apperance.Controls.Add(this.GB_Color);
            this.GB_Apperance.Location = new System.Drawing.Point(244, 27);
            this.GB_Apperance.Name = "GB_Apperance";
            this.GB_Apperance.Size = new System.Drawing.Size(313, 151);
            this.GB_Apperance.TabIndex = 42;
            this.GB_Apperance.TabStop = false;
            this.GB_Apperance.Text = "Apperance";
            // 
            // CB_Behavior
            // 
            this.CB_Behavior.FormattingEnabled = true;
            this.CB_Behavior.Location = new System.Drawing.Point(67, 47);
            this.CB_Behavior.Name = "CB_Behavior";
            this.CB_Behavior.Size = new System.Drawing.Size(108, 21);
            this.CB_Behavior.TabIndex = 44;
            // 
            // LB_Behavior
            // 
            this.LB_Behavior.AutoSize = true;
            this.LB_Behavior.Location = new System.Drawing.Point(6, 50);
            this.LB_Behavior.Name = "LB_Behavior";
            this.LB_Behavior.Size = new System.Drawing.Size(55, 13);
            this.LB_Behavior.TabIndex = 45;
            this.LB_Behavior.Text = "Behavior :";
            // 
            // GB_Size
            // 
            this.GB_Size.Controls.Add(this.NUM_SizeRatioOverTime);
            this.GB_Size.Controls.Add(this.LB_SizeRatioOverTime);
            this.GB_Size.Controls.Add(this.NUM_CurrentSize);
            this.GB_Size.Controls.Add(this.LB_Size);
            this.GB_Size.Location = new System.Drawing.Point(6, 74);
            this.GB_Size.Name = "GB_Size";
            this.GB_Size.Size = new System.Drawing.Size(169, 70);
            this.GB_Size.TabIndex = 43;
            this.GB_Size.TabStop = false;
            this.GB_Size.Text = "Size";
            // 
            // Timer
            // 
            this.Timer.Enabled = true;
            this.Timer.Interval = 16;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // BTN_AddFamily
            // 
            this.BTN_AddFamily.Location = new System.Drawing.Point(678, 223);
            this.BTN_AddFamily.Name = "BTN_AddFamily";
            this.BTN_AddFamily.Size = new System.Drawing.Size(74, 23);
            this.BTN_AddFamily.TabIndex = 43;
            this.BTN_AddFamily.Text = "Add Family";
            this.BTN_AddFamily.UseVisualStyleBackColor = true;
            this.BTN_AddFamily.Click += new System.EventHandler(this.BTN_AddFamily_Click);
            // 
            // ATL_Temps
            // 
            this.ATL_Temps.BackColor = System.Drawing.Color.FloralWhite;
            this.ATL_Temps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ATL_Temps.Location = new System.Drawing.Point(12, 259);
            this.ATL_Temps.Name = "ATL_Temps";
            this.ATL_Temps.Size = new System.Drawing.Size(731, 73);
            this.ATL_Temps.TabIndex = 17;
            // 
            // PixelFamilyEdition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 341);
            this.Controls.Add(this.BTN_AddFamily);
            this.Controls.Add(this.GB_Apperance);
            this.Controls.Add(this.GB_Velocity);
            this.Controls.Add(this.GB_Vector);
            this.Controls.Add(this.LB_Lifespan);
            this.Controls.Add(this.BTN_Quit);
            this.Controls.Add(this.BTN_SaveAndQuit);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.GB_Fade);
            this.Controls.Add(this.BTN_Advanced);
            this.Controls.Add(this.ATL_Temps);
            this.Controls.Add(this.PN_Pixel);
            this.Controls.Add(this.NUM_Lifespan);
            this.Controls.Add(this.Menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.Menu;
            this.Name = "PixelFamilyEdition";
            this.Text = "PixelFamilyEdition";
            ((System.ComponentModel.ISupportInitialize)(this.NUM_VectorX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_VectorY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Opacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_CurrentSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_VelocityX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_VelocityY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Lifespan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_SizeRatioOverTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_FadeInFrameCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_FadeOutStart)).EndInit();
            this.GB_Fade.ResumeLayout(false);
            this.GB_Fade.PerformLayout();
            this.GB_Vector.ResumeLayout(false);
            this.GB_Vector.PerformLayout();
            this.GB_Velocity.ResumeLayout(false);
            this.GB_Velocity.PerformLayout();
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Blue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Green)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_Red)).EndInit();
            this.GB_Color.ResumeLayout(false);
            this.GB_Color.PerformLayout();
            this.GB_Apperance.ResumeLayout(false);
            this.GB_Apperance.PerformLayout();
            this.GB_Size.ResumeLayout(false);
            this.GB_Size.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CB_ParticleShape;
        private System.Windows.Forms.NumericUpDown NUM_VectorX;
        private System.Windows.Forms.NumericUpDown NUM_VectorY;
        private System.Windows.Forms.NumericUpDown NUM_Opacity;
        private System.Windows.Forms.NumericUpDown NUM_CurrentSize;
        private System.Windows.Forms.NumericUpDown NUM_VelocityX;
        private System.Windows.Forms.NumericUpDown NUM_VelocityY;
        private System.Windows.Forms.NumericUpDown NUM_Lifespan;
        private System.Windows.Forms.NumericUpDown NUM_SizeRatioOverTime;
        private System.Windows.Forms.CheckBox CHK_FadeOut;
        private System.Windows.Forms.CheckBox CHK_FadeIn;
        private System.Windows.Forms.NumericUpDown NUM_FadeInFrameCount;
        private System.Windows.Forms.NumericUpDown NUM_FadeOutStart;
        private System.Windows.Forms.Panel PN_Pixel;
        private AnimationTimeLine ATL_Temps;
        private System.Windows.Forms.Button BTN_Advanced;
        private System.Windows.Forms.GroupBox GB_Fade;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Button BTN_SaveAndQuit;
        private System.Windows.Forms.Button BTN_Quit;
        private System.Windows.Forms.Label LB_VectorY;
        private System.Windows.Forms.Label LB_Shape;
        private System.Windows.Forms.Label LB_SizeRatioOverTime;
        private System.Windows.Forms.Label LB_Size;
        private System.Windows.Forms.Label LB_VectorX;
        private System.Windows.Forms.GroupBox GB_Vector;
        private System.Windows.Forms.Label LB_FadeOutStart;
        private System.Windows.Forms.Label LB_FadeInFrameCount;
        private System.Windows.Forms.Label LB_VelocityX;
        private System.Windows.Forms.Label LB_VelocityY;
        private System.Windows.Forms.GroupBox GB_Velocity;
        private System.Windows.Forms.Label LB_Lifespan;
        private System.Windows.Forms.Label LB_Opacity;
        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAndQuitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown NUM_Blue;
        private System.Windows.Forms.NumericUpDown NUM_Green;
        private System.Windows.Forms.NumericUpDown NUM_Red;
        private System.Windows.Forms.Label LB_Green;
        private System.Windows.Forms.Label LB_Red;
        private System.Windows.Forms.Label LB_Blue;
        private System.Windows.Forms.GroupBox GB_Color;
        private System.Windows.Forms.GroupBox GB_Apperance;
        private System.Windows.Forms.GroupBox GB_Size;
        private System.Windows.Forms.ComboBox CB_Behavior;
        private System.Windows.Forms.Label LB_Behavior;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.Button BTN_AddFamily;
    }
}