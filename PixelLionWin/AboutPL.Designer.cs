﻿namespace PixelLionWin
{
    partial class AboutPL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutPL));
            this.Refresh_Timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // Refresh_Timer
            // 
            this.Refresh_Timer.Enabled = true;
            this.Refresh_Timer.Interval = 17;
            this.Refresh_Timer.Tick += new System.EventHandler(this.RefreshWindow);
            // 
            // AboutPL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(634, 694);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutPL";
            this.Text = "AboutPL";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AboutPL_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AboutPL_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AboutPL_MouseDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer Refresh_Timer;

    }
}