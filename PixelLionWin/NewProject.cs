﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PixelLionWin
{
    public partial class NewProject : Form
    {
        public NewProject()
        {
            InitializeComponent();
        }

        private void BTN_Create_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        public String GetName()
        {
            return TB_Name.Text;
        }

        public String GetPath()
        {
            return TB_FilePath.Text;
        }

        public String GetDescription()
        {
            return TB_Description.Text;
        }

        private void TB_Name_TextChanged(object sender, EventArgs e)
        {
            ToggleAcceptButton();
        }

        private void TB_Description_TextChanged(object sender, EventArgs e)
        {
            ToggleAcceptButton();
        }

        private void BTN_Browse_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Pixel Lion Projet|*.plproj";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TB_FilePath.Text = dlg.FileName;
            }

            ToggleAcceptButton();
        }

        private void ToggleAcceptButton()
        {
            BTN_Create.Enabled = (TB_Name.Text != String.Empty && TB_Description.Text != String.Empty && TB_FilePath.Text != String.Empty);
        }
    }
}
