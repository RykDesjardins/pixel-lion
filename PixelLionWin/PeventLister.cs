﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;

namespace PixelLionWin
{
    public partial class PeventLister : Form
    {
        List<Pevent> EventsToEdit;
        List<Pevent> ListePevent;
        SpriteBundle bundle;
        PeventClassBundle bundlec;
        Map map;

        public PeventLister(List<Pevent> pListePevent, SpriteBundle pbundle, PeventClassBundle pbundlec, Map pmap)
        {
            map = pmap;
            bundle = pbundle;
            bundlec = pbundlec;

            InitializeComponent();

            EventsToEdit = new List<Pevent>();

            int i = 0;
            ListePevent = pListePevent;
            foreach (Pevent item in ListePevent)
            {
                CheckBox c = new CheckBox();
                c.Width = Panel_pEvents.Width - SystemInformation.VerticalScrollBarWidth - 8;
                c.Name = i.ToString();
                c.Text = item.Name;
                c.CheckedChanged += new System.EventHandler(CB_CheckChanged);

                Panel_pEvents.Controls.Add(c);
                i++;
            }
        }

        private void CB_CheckChanged(object sender, EventArgs e)
        {
            CheckBox CB_Sender = (CheckBox)sender;
            if (CB_Sender.Checked)
                EventsToEdit.Add(ListePevent[int.Parse(CB_Sender.Name)]);
            else
                EventsToEdit.Remove(ListePevent[int.Parse(CB_Sender.Name)]);
        }

        private void BTN_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BTN_Edit_Click(object sender, EventArgs e)
        {
            Edit();
        }
        private void BTN_SelectAll_Click(object sender, EventArgs e)
        {
            SelectAll();
        }
        private void BTN_DeselectAll_Click(object sender, EventArgs e)
        {
            DeselectAll();
        }
        private void BTN_InvertSelection_Click(object sender, EventArgs e)
        {
            InvertSelection();
        }

        void Edit()
        {
            if (EventsToEdit.Count > 0)
            {
                PeventListEditor dlg = new PeventListEditor(EventsToEdit, bundle, bundlec, map);
                dlg.ShowDialog();
            }
            else
                MessageBox.Show("Aucun pEvent selectionner!");
        }
        void SelectAll()
        {
            foreach (CheckBox i in Panel_pEvents.Controls)
                i.Checked = true;
        }
        void DeselectAll()
        {
            foreach (CheckBox i in Panel_pEvents.Controls)
                i.Checked = false;
        }
        void InvertSelection()
        {
            foreach (CheckBox i in Panel_pEvents.Controls)
                i.Checked = !i.Checked;
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Edit();
        }
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectAll();
        }
        private void deselectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeselectAll();
        }
        private void invertSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InvertSelection();
        }
    }
}
