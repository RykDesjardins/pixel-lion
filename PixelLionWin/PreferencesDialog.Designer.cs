﻿namespace PixelLionWin
{
    partial class PreferencesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreferencesDialog));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.PN_TileSetColorBackground = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NUM_TileSetTileCount = new System.Windows.Forms.NumericUpDown();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.NUM_TileSizeY = new System.Windows.Forms.NumericUpDown();
            this.NUM_TileSizeX = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CB_QuickMaps = new System.Windows.Forms.CheckBox();
            this.BTN_ReloadProject = new System.Windows.Forms.Button();
            this.CB_Hotkeys = new System.Windows.Forms.CheckBox();
            this.CB_PreloadMaps = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BTN_EraseHistory = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.NUM_HistoryStateCount = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PN_GridColor = new System.Windows.Forms.Panel();
            this.NUM_GridThickness = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.CB_BubbleMainForm = new System.Windows.Forms.CheckBox();
            this.CB_ShowSplashScreenPrincipal = new System.Windows.Forms.CheckBox();
            this.CB_ShowPopupsPrincipal = new System.Windows.Forms.CheckBox();
            this.CB_DoubleBuffering = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.NUM_AnimationRulerSize = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.PN_AnimationBackgroundColor = new System.Windows.Forms.Panel();
            this.CB_ShowSplashScreenAnimation = new System.Windows.Forms.CheckBox();
            this.CB_UseXnaAnimation = new System.Windows.Forms.CheckBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.CB_LoopSong = new System.Windows.Forms.CheckBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.BTN_BrowseJingle = new System.Windows.Forms.Button();
            this.TB_JinglePath = new System.Windows.Forms.TextBox();
            this.CB_PlayJingle = new System.Windows.Forms.CheckBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.PN_TabPreviewGradiant = new System.Windows.Forms.Panel();
            this.PN_TabColor2 = new System.Windows.Forms.Panel();
            this.PN_TabColor1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.BTN_ImportPrefs = new System.Windows.Forms.Button();
            this.BTN_ExportPrefs = new System.Windows.Forms.Button();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_TileSetTileCount)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_TileSizeY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_TileSizeX)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_HistoryStateCount)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_GridThickness)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_AnimationRulerSize)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(519, 435);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(511, 409);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Editeur de cartes";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Location = new System.Drawing.Point(254, 17);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(239, 240);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Liste de tuiles";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.PN_TileSetColorBackground);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Location = new System.Drawing.Point(16, 155);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(206, 68);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Fond de la liste";
            // 
            // PN_TileSetColorBackground
            // 
            this.PN_TileSetColorBackground.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_TileSetColorBackground.Location = new System.Drawing.Point(137, 14);
            this.PN_TileSetColorBackground.Name = "PN_TileSetColorBackground";
            this.PN_TileSetColorBackground.Size = new System.Drawing.Size(45, 45);
            this.PN_TileSetColorBackground.TabIndex = 1;
            this.PN_TileSetColorBackground.Click += new System.EventHandler(this.ChangePanelColor);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Couleur de fond";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.NUM_TileSetTileCount);
            this.groupBox7.Location = new System.Drawing.Point(16, 87);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(206, 62);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Largeur de la liste";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Nombre de tuiles";
            // 
            // NUM_TileSetTileCount
            // 
            this.NUM_TileSetTileCount.Location = new System.Drawing.Point(122, 24);
            this.NUM_TileSetTileCount.Name = "NUM_TileSetTileCount";
            this.NUM_TileSetTileCount.Size = new System.Drawing.Size(64, 20);
            this.NUM_TileSetTileCount.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.NUM_TileSizeY);
            this.groupBox6.Controls.Add(this.NUM_TileSizeX);
            this.groupBox6.Location = new System.Drawing.Point(16, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(206, 62);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Grosseur d\'une tuile en pixels";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(93, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 26);
            this.label4.TabIndex = 1;
            this.label4.Text = "X";
            // 
            // NUM_TileSizeY
            // 
            this.NUM_TileSizeY.Location = new System.Drawing.Point(122, 25);
            this.NUM_TileSizeY.Name = "NUM_TileSizeY";
            this.NUM_TileSizeY.Size = new System.Drawing.Size(64, 20);
            this.NUM_TileSizeY.TabIndex = 0;
            // 
            // NUM_TileSizeX
            // 
            this.NUM_TileSizeX.Location = new System.Drawing.Point(25, 25);
            this.NUM_TileSizeX.Name = "NUM_TileSizeX";
            this.NUM_TileSizeX.Size = new System.Drawing.Size(64, 20);
            this.NUM_TileSizeX.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CB_QuickMaps);
            this.groupBox3.Controls.Add(this.BTN_ReloadProject);
            this.groupBox3.Controls.Add(this.CB_Hotkeys);
            this.groupBox3.Controls.Add(this.CB_PreloadMaps);
            this.groupBox3.Location = new System.Drawing.Point(17, 214);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(231, 177);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Mémoire";
            // 
            // CB_QuickMaps
            // 
            this.CB_QuickMaps.AutoSize = true;
            this.CB_QuickMaps.Location = new System.Drawing.Point(30, 81);
            this.CB_QuickMaps.Name = "CB_QuickMaps";
            this.CB_QuickMaps.Size = new System.Drawing.Size(93, 17);
            this.CB_QuickMaps.TabIndex = 4;
            this.CB_QuickMaps.Text = "Cartes rapides";
            this.CB_QuickMaps.UseVisualStyleBackColor = true;
            // 
            // BTN_ReloadProject
            // 
            this.BTN_ReloadProject.Location = new System.Drawing.Point(30, 116);
            this.BTN_ReloadProject.Name = "BTN_ReloadProject";
            this.BTN_ReloadProject.Size = new System.Drawing.Size(130, 36);
            this.BTN_ReloadProject.TabIndex = 3;
            this.BTN_ReloadProject.Text = "Recharger le projet";
            this.BTN_ReloadProject.UseVisualStyleBackColor = true;
            this.BTN_ReloadProject.Click += new System.EventHandler(this.BTN_ReloadProject_Click);
            // 
            // CB_Hotkeys
            // 
            this.CB_Hotkeys.AutoSize = true;
            this.CB_Hotkeys.Location = new System.Drawing.Point(30, 58);
            this.CB_Hotkeys.Name = "CB_Hotkeys";
            this.CB_Hotkeys.Size = new System.Drawing.Size(119, 17);
            this.CB_Hotkeys.TabIndex = 1;
            this.CB_Hotkeys.Text = "Raccourcis claviers";
            this.CB_Hotkeys.UseVisualStyleBackColor = true;
            // 
            // CB_PreloadMaps
            // 
            this.CB_PreloadMaps.AutoSize = true;
            this.CB_PreloadMaps.Location = new System.Drawing.Point(30, 35);
            this.CB_PreloadMaps.Name = "CB_PreloadMaps";
            this.CB_PreloadMaps.Size = new System.Drawing.Size(158, 17);
            this.CB_PreloadMaps.TabIndex = 0;
            this.CB_PreloadMaps.Text = "Précharger toutes les cartes";
            this.CB_PreloadMaps.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BTN_EraseHistory);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.NUM_HistoryStateCount);
            this.groupBox2.Location = new System.Drawing.Point(254, 263);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(239, 128);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Historique";
            // 
            // BTN_EraseHistory
            // 
            this.BTN_EraseHistory.Location = new System.Drawing.Point(62, 67);
            this.BTN_EraseHistory.Name = "BTN_EraseHistory";
            this.BTN_EraseHistory.Size = new System.Drawing.Size(114, 36);
            this.BTN_EraseHistory.TabIndex = 2;
            this.BTN_EraseHistory.Text = "Effacer l\'historique";
            this.BTN_EraseHistory.UseVisualStyleBackColor = true;
            this.BTN_EraseHistory.Click += new System.EventHandler(this.BTN_EraseHistory_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nombre d\'états";
            // 
            // NUM_HistoryStateCount
            // 
            this.NUM_HistoryStateCount.Location = new System.Drawing.Point(112, 31);
            this.NUM_HistoryStateCount.Name = "NUM_HistoryStateCount";
            this.NUM_HistoryStateCount.Size = new System.Drawing.Size(91, 20);
            this.NUM_HistoryStateCount.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.PN_GridColor);
            this.groupBox1.Controls.Add(this.NUM_GridThickness);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(17, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(231, 191);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Séparateurs";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Couleur";
            // 
            // PN_GridColor
            // 
            this.PN_GridColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_GridColor.Location = new System.Drawing.Point(87, 56);
            this.PN_GridColor.Name = "PN_GridColor";
            this.PN_GridColor.Size = new System.Drawing.Size(120, 110);
            this.PN_GridColor.TabIndex = 2;
            this.PN_GridColor.Click += new System.EventHandler(this.PN_GridColor_Click);
            this.PN_GridColor.Paint += new System.Windows.Forms.PaintEventHandler(this.PN_GridColor_Paint);
            // 
            // NUM_GridThickness
            // 
            this.NUM_GridThickness.Location = new System.Drawing.Point(87, 30);
            this.NUM_GridThickness.Name = "NUM_GridThickness";
            this.NUM_GridThickness.Size = new System.Drawing.Size(120, 20);
            this.NUM_GridThickness.TabIndex = 1;
            this.NUM_GridThickness.ValueChanged += new System.EventHandler(this.NUM_GridThickness_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Épaisseur";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(511, 409);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Vidéo";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label11);
            this.groupBox9.Controls.Add(this.label10);
            this.groupBox9.Controls.Add(this.CB_BubbleMainForm);
            this.groupBox9.Controls.Add(this.CB_ShowSplashScreenPrincipal);
            this.groupBox9.Controls.Add(this.CB_ShowPopupsPrincipal);
            this.groupBox9.Controls.Add(this.CB_DoubleBuffering);
            this.groupBox9.Location = new System.Drawing.Point(17, 17);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(475, 182);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Fenêtre principale";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.DarkGray;
            this.label11.Location = new System.Drawing.Point(25, 146);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(277, 26);
            this.label11.TabIndex = 3;
            this.label11.Text = "Version beta; peut causer une \r\ninstabilitée au niveau de l\'affichage [Présenteme" +
    "nt inactif]";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.DarkGray;
            this.label10.Location = new System.Drawing.Point(236, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(216, 39);
            this.label10.TabIndex = 3;
            this.label10.Text = "La mise en mémoire tampon double réduit le \r\nclignotement d\'affichage mais affich" +
    "e plus \r\nlentement.";
            // 
            // CB_BubbleMainForm
            // 
            this.CB_BubbleMainForm.AutoSize = true;
            this.CB_BubbleMainForm.Location = new System.Drawing.Point(27, 128);
            this.CB_BubbleMainForm.Name = "CB_BubbleMainForm";
            this.CB_BubbleMainForm.Size = new System.Drawing.Size(240, 17);
            this.CB_BubbleMainForm.TabIndex = 2;
            this.CB_BubbleMainForm.Text = "Afficher des bulles lorsqu\'il n\'y a aucune carte";
            this.CB_BubbleMainForm.UseVisualStyleBackColor = true;
            // 
            // CB_ShowSplashScreenPrincipal
            // 
            this.CB_ShowSplashScreenPrincipal.AutoSize = true;
            this.CB_ShowSplashScreenPrincipal.Location = new System.Drawing.Point(27, 96);
            this.CB_ShowSplashScreenPrincipal.Name = "CB_ShowSplashScreenPrincipal";
            this.CB_ShowSplashScreenPrincipal.Size = new System.Drawing.Size(138, 17);
            this.CB_ShowSplashScreenPrincipal.TabIndex = 2;
            this.CB_ShowSplashScreenPrincipal.Text = "Afficher le splashscreen";
            this.CB_ShowSplashScreenPrincipal.UseVisualStyleBackColor = true;
            // 
            // CB_ShowPopupsPrincipal
            // 
            this.CB_ShowPopupsPrincipal.AutoSize = true;
            this.CB_ShowPopupsPrincipal.Location = new System.Drawing.Point(27, 64);
            this.CB_ShowPopupsPrincipal.Name = "CB_ShowPopupsPrincipal";
            this.CB_ShowPopupsPrincipal.Size = new System.Drawing.Size(119, 17);
            this.CB_ShowPopupsPrincipal.TabIndex = 1;
            this.CB_ShowPopupsPrincipal.Text = "Afficher les pop-ups";
            this.CB_ShowPopupsPrincipal.UseVisualStyleBackColor = true;
            // 
            // CB_DoubleBuffering
            // 
            this.CB_DoubleBuffering.AutoSize = true;
            this.CB_DoubleBuffering.Location = new System.Drawing.Point(27, 31);
            this.CB_DoubleBuffering.Name = "CB_DoubleBuffering";
            this.CB_DoubleBuffering.Size = new System.Drawing.Size(186, 17);
            this.CB_DoubleBuffering.TabIndex = 0;
            this.CB_DoubleBuffering.Text = "Double tempon (Double Buffering)";
            this.CB_DoubleBuffering.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.NUM_AnimationRulerSize);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.PN_AnimationBackgroundColor);
            this.groupBox4.Controls.Add(this.CB_ShowSplashScreenAnimation);
            this.groupBox4.Controls.Add(this.CB_UseXnaAnimation);
            this.groupBox4.Location = new System.Drawing.Point(17, 205);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(475, 186);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Animation Studio";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(418, 101);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "pixels";
            // 
            // NUM_AnimationRulerSize
            // 
            this.NUM_AnimationRulerSize.Location = new System.Drawing.Point(350, 99);
            this.NUM_AnimationRulerSize.Name = "NUM_AnimationRulerSize";
            this.NUM_AnimationRulerSize.Size = new System.Drawing.Size(62, 20);
            this.NUM_AnimationRulerSize.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(261, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Taille des règles";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(93, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Couleur de fond";
            // 
            // PN_AnimationBackgroundColor
            // 
            this.PN_AnimationBackgroundColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_AnimationBackgroundColor.Location = new System.Drawing.Point(27, 101);
            this.PN_AnimationBackgroundColor.Name = "PN_AnimationBackgroundColor";
            this.PN_AnimationBackgroundColor.Size = new System.Drawing.Size(60, 60);
            this.PN_AnimationBackgroundColor.TabIndex = 3;
            this.PN_AnimationBackgroundColor.Click += new System.EventHandler(this.ChangePanelColor);
            // 
            // CB_ShowSplashScreenAnimation
            // 
            this.CB_ShowSplashScreenAnimation.AutoSize = true;
            this.CB_ShowSplashScreenAnimation.Location = new System.Drawing.Point(27, 64);
            this.CB_ShowSplashScreenAnimation.Name = "CB_ShowSplashScreenAnimation";
            this.CB_ShowSplashScreenAnimation.Size = new System.Drawing.Size(138, 17);
            this.CB_ShowSplashScreenAnimation.TabIndex = 2;
            this.CB_ShowSplashScreenAnimation.Text = "Afficher le splashscreen";
            this.CB_ShowSplashScreenAnimation.UseVisualStyleBackColor = true;
            // 
            // CB_UseXnaAnimation
            // 
            this.CB_UseXnaAnimation.AutoSize = true;
            this.CB_UseXnaAnimation.Location = new System.Drawing.Point(27, 32);
            this.CB_UseXnaAnimation.Name = "CB_UseXnaAnimation";
            this.CB_UseXnaAnimation.Size = new System.Drawing.Size(232, 17);
            this.CB_UseXnaAnimation.TabIndex = 0;
            this.CB_UseXnaAnimation.Text = "Utilisation de l\'accélération graphique (XNA)";
            this.CB_UseXnaAnimation.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox11);
            this.tabPage3.Controls.Add(this.groupBox10);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(511, 409);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Audio";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label15);
            this.groupBox11.Controls.Add(this.CB_LoopSong);
            this.groupBox11.Location = new System.Drawing.Point(17, 136);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(475, 110);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Musique";
            // 
            // CB_LoopSong
            // 
            this.CB_LoopSong.AutoSize = true;
            this.CB_LoopSong.Location = new System.Drawing.Point(29, 34);
            this.CB_LoopSong.Name = "CB_LoopSong";
            this.CB_LoopSong.Size = new System.Drawing.Size(155, 17);
            this.CB_LoopSong.TabIndex = 0;
            this.CB_LoopSong.Text = "Jouer la musique en boucle";
            this.CB_LoopSong.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.BTN_BrowseJingle);
            this.groupBox10.Controls.Add(this.TB_JinglePath);
            this.groupBox10.Controls.Add(this.CB_PlayJingle);
            this.groupBox10.Location = new System.Drawing.Point(17, 17);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(475, 113);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Accueil";
            // 
            // BTN_BrowseJingle
            // 
            this.BTN_BrowseJingle.Location = new System.Drawing.Point(414, 55);
            this.BTN_BrowseJingle.Name = "BTN_BrowseJingle";
            this.BTN_BrowseJingle.Size = new System.Drawing.Size(34, 23);
            this.BTN_BrowseJingle.TabIndex = 1;
            this.BTN_BrowseJingle.Text = "...";
            this.BTN_BrowseJingle.UseVisualStyleBackColor = true;
            this.BTN_BrowseJingle.Click += new System.EventHandler(this.BTN_BrowseJingle_Click);
            // 
            // TB_JinglePath
            // 
            this.TB_JinglePath.Location = new System.Drawing.Point(29, 57);
            this.TB_JinglePath.Name = "TB_JinglePath";
            this.TB_JinglePath.ReadOnly = true;
            this.TB_JinglePath.Size = new System.Drawing.Size(379, 20);
            this.TB_JinglePath.TabIndex = 1;
            // 
            // CB_PlayJingle
            // 
            this.CB_PlayJingle.AutoSize = true;
            this.CB_PlayJingle.Location = new System.Drawing.Point(29, 34);
            this.CB_PlayJingle.Name = "CB_PlayJingle";
            this.CB_PlayJingle.Size = new System.Drawing.Size(128, 17);
            this.CB_PlayJingle.TabIndex = 0;
            this.CB_PlayJingle.Text = "Jouer le son d\'accueil";
            this.CB_PlayJingle.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox13);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(511, 409);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Divers";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.groupBox14);
            this.groupBox13.Location = new System.Drawing.Point(17, 18);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(474, 278);
            this.groupBox13.TabIndex = 0;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Onglets des cartes";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.PN_TabPreviewGradiant);
            this.groupBox14.Controls.Add(this.PN_TabColor2);
            this.groupBox14.Controls.Add(this.PN_TabColor1);
            this.groupBox14.Controls.Add(this.label13);
            this.groupBox14.Controls.Add(this.label14);
            this.groupBox14.Controls.Add(this.label12);
            this.groupBox14.Location = new System.Drawing.Point(23, 28);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(428, 111);
            this.groupBox14.TabIndex = 2;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Couleurs du dégradé";
            // 
            // PN_TabPreviewGradiant
            // 
            this.PN_TabPreviewGradiant.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_TabPreviewGradiant.Location = new System.Drawing.Point(212, 53);
            this.PN_TabPreviewGradiant.Name = "PN_TabPreviewGradiant";
            this.PN_TabPreviewGradiant.Size = new System.Drawing.Size(200, 34);
            this.PN_TabPreviewGradiant.TabIndex = 2;
            this.PN_TabPreviewGradiant.Paint += new System.Windows.Forms.PaintEventHandler(this.PN_TabPreviewGradiant_Paint);
            // 
            // PN_TabColor2
            // 
            this.PN_TabColor2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_TabColor2.Location = new System.Drawing.Point(25, 62);
            this.PN_TabColor2.Name = "PN_TabColor2";
            this.PN_TabColor2.Size = new System.Drawing.Size(25, 25);
            this.PN_TabColor2.TabIndex = 0;
            this.PN_TabColor2.Click += new System.EventHandler(this.ChangeTabColors);
            // 
            // PN_TabColor1
            // 
            this.PN_TabColor1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PN_TabColor1.Location = new System.Drawing.Point(25, 31);
            this.PN_TabColor1.Name = "PN_TabColor1";
            this.PN_TabColor1.Size = new System.Drawing.Size(25, 25);
            this.PN_TabColor1.TabIndex = 0;
            this.PN_TabColor1.Click += new System.EventHandler(this.ChangeTabColors);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(56, 68);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Deuxième couleur";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(285, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Aperçu";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(56, 37);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Première couleur";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox12);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(511, 409);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Importer / Exporter";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.BTN_ImportPrefs);
            this.groupBox12.Controls.Add(this.BTN_ExportPrefs);
            this.groupBox12.Location = new System.Drawing.Point(63, 69);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(374, 270);
            this.groupBox12.TabIndex = 0;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Importation et Exportation";
            // 
            // BTN_ImportPrefs
            // 
            this.BTN_ImportPrefs.Location = new System.Drawing.Point(80, 166);
            this.BTN_ImportPrefs.Name = "BTN_ImportPrefs";
            this.BTN_ImportPrefs.Size = new System.Drawing.Size(223, 48);
            this.BTN_ImportPrefs.TabIndex = 0;
            this.BTN_ImportPrefs.Text = "Importer les préférences";
            this.BTN_ImportPrefs.UseVisualStyleBackColor = true;
            this.BTN_ImportPrefs.Click += new System.EventHandler(this.BTN_ImportPrefs_Click);
            // 
            // BTN_ExportPrefs
            // 
            this.BTN_ExportPrefs.Location = new System.Drawing.Point(80, 61);
            this.BTN_ExportPrefs.Name = "BTN_ExportPrefs";
            this.BTN_ExportPrefs.Size = new System.Drawing.Size(223, 48);
            this.BTN_ExportPrefs.TabIndex = 0;
            this.BTN_ExportPrefs.Text = "Exporter les préférences";
            this.BTN_ExportPrefs.UseVisualStyleBackColor = true;
            this.BTN_ExportPrefs.Click += new System.EventHandler(this.BTN_ExportPrefs_Click);
            // 
            // BTN_Save
            // 
            this.BTN_Save.Location = new System.Drawing.Point(412, 454);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(115, 43);
            this.BTN_Save.TabIndex = 1;
            this.BTN_Save.Text = "Sauvegarder";
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.DarkGray;
            this.label15.Location = new System.Drawing.Point(26, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(366, 26);
            this.label15.TabIndex = 4;
            this.label15.Text = "Lorsque l\'utilisateur écoute une chanson qu\'il a importé dans les ressources, \r\ns" +
    "i cette case est cochée, la chanson jouera en boucle.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.DarkGray;
            this.label16.Location = new System.Drawing.Point(261, 33);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "(Recommandé)";
            // 
            // PreferencesDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 509);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PreferencesDialog";
            this.Text = "Préférences";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_TileSetTileCount)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_TileSizeY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_TileSizeX)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_HistoryStateCount)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_GridThickness)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUM_AnimationRulerSize)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown NUM_GridThickness;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel PN_GridColor;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown NUM_HistoryStateCount;
        private System.Windows.Forms.Button BTN_EraseHistory;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox CB_PreloadMaps;
        private System.Windows.Forms.CheckBox CB_Hotkeys;
        private System.Windows.Forms.Button BTN_ReloadProject;
        private System.Windows.Forms.CheckBox CB_QuickMaps;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox CB_UseXnaAnimation;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown NUM_TileSizeY;
        private System.Windows.Forms.NumericUpDown NUM_TileSizeX;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUM_TileSetTileCount;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Panel PN_TileSetColorBackground;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox CB_DoubleBuffering;
        private System.Windows.Forms.CheckBox CB_ShowSplashScreenPrincipal;
        private System.Windows.Forms.CheckBox CB_ShowPopupsPrincipal;
        private System.Windows.Forms.CheckBox CB_ShowSplashScreenAnimation;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button BTN_BrowseJingle;
        private System.Windows.Forms.TextBox TB_JinglePath;
        private System.Windows.Forms.CheckBox CB_PlayJingle;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.CheckBox CB_LoopSong;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel PN_AnimationBackgroundColor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown NUM_AnimationRulerSize;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button BTN_ImportPrefs;
        private System.Windows.Forms.Button BTN_ExportPrefs;
        private System.Windows.Forms.CheckBox CB_BubbleMainForm;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Panel PN_TabPreviewGradiant;
        private System.Windows.Forms.Panel PN_TabColor2;
        private System.Windows.Forms.Panel PN_TabColor1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
    }
}