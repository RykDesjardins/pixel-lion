﻿namespace Test_Utilities
{
    partial class MapDrawings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PN_Map = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.TRACK_Ratio = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.TRACK_Ratio)).BeginInit();
            this.SuspendLayout();
            // 
            // PN_Map
            // 
            this.PN_Map.Location = new System.Drawing.Point(77, 120);
            this.PN_Map.Name = "PN_Map";
            this.PN_Map.Size = new System.Drawing.Size(787, 454);
            this.PN_Map.TabIndex = 0;
            this.PN_Map.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PN_Map_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Resizing Test";
            // 
            // TRACK_Ratio
            // 
            this.TRACK_Ratio.LargeChange = 20;
            this.TRACK_Ratio.Location = new System.Drawing.Point(77, 69);
            this.TRACK_Ratio.Maximum = 200;
            this.TRACK_Ratio.Name = "TRACK_Ratio";
            this.TRACK_Ratio.Size = new System.Drawing.Size(787, 45);
            this.TRACK_Ratio.SmallChange = 10;
            this.TRACK_Ratio.TabIndex = 2;
            this.TRACK_Ratio.Value = 100;
            this.TRACK_Ratio.Scroll += new System.EventHandler(this.TRACK_Ratio_Scroll);
            this.TRACK_Ratio.ValueChanged += new System.EventHandler(this.TRACK_Ratio_ValueChanged);
            // 
            // MapDrawings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 645);
            this.Controls.Add(this.TRACK_Ratio);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PN_Map);
            this.Name = "MapDrawings";
            this.Text = "MapDrawings";
            ((System.ComponentModel.ISupportInitialize)(this.TRACK_Ratio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PN_Map;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar TRACK_Ratio;
    }
}