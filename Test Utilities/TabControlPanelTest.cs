﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Test_Utilities
{
    public partial class TabControlPanelTest : Form
    {
        public TabControlPanelTest()
        {
            InitializeComponent();
        }

        private void BTN_AddTab_Click(object sender, EventArgs e)
        {
            if (TB_Tabname.Text.Trim() != String.Empty)
            {
                tabControlPanel1.AddTab(TB_Tabname.Text);
                TB_Tabname.Text = String.Empty;
            }
        }

        private void tabControlPanel1_TabChanged(object sender, EventArgs e)
        {
            LB_CurrentTab.Text = tabControlPanel1.GetCurrentTabItem().ToString();
        }
    }
}
