﻿namespace Test_Utilities
{
    partial class MainTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainTestForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.LIST_ResultContent = new System.Windows.Forms.ListBox();
            this.LB_ErrorCount = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LB_TimeToInterpret = new System.Windows.Forms.Label();
            this.BTN_InterpretCore = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BTN_LoadFile = new System.Windows.Forms.Button();
            this.TB_PMSLOutput = new System.Windows.Forms.RichTextBox();
            this.TB_PMSLInput = new System.Windows.Forms.TextBox();
            this.BTN_PMSLBrowseCoreFile = new System.Windows.Forms.Button();
            this.TB_CoreLocation = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.BTN_MapResize = new System.Windows.Forms.Button();
            this.BTN_Shapes = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.TB_Function = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.BTN_MathLoad = new System.Windows.Forms.Button();
            this.LB_MathAnswer = new System.Windows.Forms.Label();
            this.TB_Yat = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BTN_PixelFamily = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.BTN_InterpretCore);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.BTN_PMSLBrowseCoreFile);
            this.groupBox1.Controls.Add(this.TB_CoreLocation);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1074, 397);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pixel Monkey Scripting Language";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.LIST_ResultContent);
            this.groupBox3.Controls.Add(this.LB_ErrorCount);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.LB_TimeToInterpret);
            this.groupBox3.Location = new System.Drawing.Point(34, 127);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(412, 250);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Interpreter results";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Content";
            // 
            // LIST_ResultContent
            // 
            this.LIST_ResultContent.FormattingEnabled = true;
            this.LIST_ResultContent.Location = new System.Drawing.Point(168, 93);
            this.LIST_ResultContent.Name = "LIST_ResultContent";
            this.LIST_ResultContent.Size = new System.Drawing.Size(154, 134);
            this.LIST_ResultContent.TabIndex = 9;
            this.LIST_ResultContent.SelectedIndexChanged += new System.EventHandler(this.LIST_ResultContent_SelectedIndexChanged);
            // 
            // LB_ErrorCount
            // 
            this.LB_ErrorCount.AutoSize = true;
            this.LB_ErrorCount.ForeColor = System.Drawing.Color.Green;
            this.LB_ErrorCount.Location = new System.Drawing.Point(165, 66);
            this.LB_ErrorCount.Name = "LB_ErrorCount";
            this.LB_ErrorCount.Size = new System.Drawing.Size(45, 13);
            this.LB_ErrorCount.TabIndex = 8;
            this.LB_ErrorCount.Text = "No error";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Errors";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Time elapsed to interpret";
            // 
            // LB_TimeToInterpret
            // 
            this.LB_TimeToInterpret.AutoSize = true;
            this.LB_TimeToInterpret.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.LB_TimeToInterpret.Location = new System.Drawing.Point(165, 38);
            this.LB_TimeToInterpret.Name = "LB_TimeToInterpret";
            this.LB_TimeToInterpret.Size = new System.Drawing.Size(55, 13);
            this.LB_TimeToInterpret.TabIndex = 6;
            this.LB_TimeToInterpret.Text = "00:00:000";
            // 
            // BTN_InterpretCore
            // 
            this.BTN_InterpretCore.Location = new System.Drawing.Point(166, 68);
            this.BTN_InterpretCore.Name = "BTN_InterpretCore";
            this.BTN_InterpretCore.Size = new System.Drawing.Size(128, 38);
            this.BTN_InterpretCore.TabIndex = 8;
            this.BTN_InterpretCore.Text = "Interpret Core";
            this.BTN_InterpretCore.UseVisualStyleBackColor = true;
            this.BTN_InterpretCore.Click += new System.EventHandler(this.BTN_InterpretCore_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BTN_LoadFile);
            this.groupBox2.Controls.Add(this.TB_PMSLOutput);
            this.groupBox2.Controls.Add(this.TB_PMSLInput);
            this.groupBox2.Location = new System.Drawing.Point(467, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(587, 349);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Realtime Interpreter";
            // 
            // BTN_LoadFile
            // 
            this.BTN_LoadFile.Location = new System.Drawing.Point(24, 292);
            this.BTN_LoadFile.Name = "BTN_LoadFile";
            this.BTN_LoadFile.Size = new System.Drawing.Size(138, 34);
            this.BTN_LoadFile.TabIndex = 2;
            this.BTN_LoadFile.Text = "Load Script from file";
            this.BTN_LoadFile.UseVisualStyleBackColor = true;
            this.BTN_LoadFile.Click += new System.EventHandler(this.BTN_LoadFile_Click);
            // 
            // TB_PMSLOutput
            // 
            this.TB_PMSLOutput.BackColor = System.Drawing.Color.Black;
            this.TB_PMSLOutput.ForeColor = System.Drawing.Color.Lime;
            this.TB_PMSLOutput.Location = new System.Drawing.Point(24, 29);
            this.TB_PMSLOutput.Name = "TB_PMSLOutput";
            this.TB_PMSLOutput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.TB_PMSLOutput.Size = new System.Drawing.Size(538, 231);
            this.TB_PMSLOutput.TabIndex = 1;
            this.TB_PMSLOutput.Text = "";
            // 
            // TB_PMSLInput
            // 
            this.TB_PMSLInput.BackColor = System.Drawing.Color.Black;
            this.TB_PMSLInput.Enabled = false;
            this.TB_PMSLInput.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_PMSLInput.ForeColor = System.Drawing.Color.LawnGreen;
            this.TB_PMSLInput.Location = new System.Drawing.Point(24, 266);
            this.TB_PMSLInput.MaxLength = 400;
            this.TB_PMSLInput.Name = "TB_PMSLInput";
            this.TB_PMSLInput.Size = new System.Drawing.Size(538, 20);
            this.TB_PMSLInput.TabIndex = 0;
            this.TB_PMSLInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TB_PMSLInput_KeyDown);
            // 
            // BTN_PMSLBrowseCoreFile
            // 
            this.BTN_PMSLBrowseCoreFile.Location = new System.Drawing.Point(391, 28);
            this.BTN_PMSLBrowseCoreFile.Name = "BTN_PMSLBrowseCoreFile";
            this.BTN_PMSLBrowseCoreFile.Size = new System.Drawing.Size(55, 23);
            this.BTN_PMSLBrowseCoreFile.TabIndex = 2;
            this.BTN_PMSLBrowseCoreFile.Text = "...";
            this.BTN_PMSLBrowseCoreFile.UseVisualStyleBackColor = true;
            this.BTN_PMSLBrowseCoreFile.Click += new System.EventHandler(this.BTN_PMSLBrowseCoreFile_Click);
            // 
            // TB_CoreLocation
            // 
            this.TB_CoreLocation.Location = new System.Drawing.Point(85, 30);
            this.TB_CoreLocation.Name = "TB_CoreLocation";
            this.TB_CoreLocation.ReadOnly = true;
            this.TB_CoreLocation.Size = new System.Drawing.Size(300, 20);
            this.TB_CoreLocation.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Core File";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.BTN_PixelFamily);
            this.groupBox4.Controls.Add(this.BTN_MapResize);
            this.groupBox4.Controls.Add(this.BTN_Shapes);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Location = new System.Drawing.Point(12, 415);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(446, 165);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Other test forms";
            // 
            // BTN_MapResize
            // 
            this.BTN_MapResize.Location = new System.Drawing.Point(300, 36);
            this.BTN_MapResize.Name = "BTN_MapResize";
            this.BTN_MapResize.Size = new System.Drawing.Size(111, 38);
            this.BTN_MapResize.TabIndex = 1;
            this.BTN_MapResize.Text = "Map Resizing";
            this.BTN_MapResize.UseVisualStyleBackColor = true;
            this.BTN_MapResize.Click += new System.EventHandler(this.BTN_MapResize_Click);
            // 
            // BTN_Shapes
            // 
            this.BTN_Shapes.Location = new System.Drawing.Point(166, 36);
            this.BTN_Shapes.Name = "BTN_Shapes";
            this.BTN_Shapes.Size = new System.Drawing.Size(111, 38);
            this.BTN_Shapes.TabIndex = 1;
            this.BTN_Shapes.Text = "XNA Shapes";
            this.BTN_Shapes.UseVisualStyleBackColor = true;
            this.BTN_Shapes.Click += new System.EventHandler(this.BTN_Shapes_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(34, 36);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 38);
            this.button1.TabIndex = 0;
            this.button1.Text = "TabControl";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TB_Function
            // 
            this.TB_Function.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_Function.Location = new System.Drawing.Point(112, 36);
            this.TB_Function.Name = "TB_Function";
            this.TB_Function.Size = new System.Drawing.Size(394, 32);
            this.TB_Function.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.BTN_MathLoad);
            this.groupBox5.Controls.Add(this.LB_MathAnswer);
            this.groupBox5.Controls.Add(this.TB_Yat);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.TB_Function);
            this.groupBox5.Location = new System.Drawing.Point(479, 415);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(607, 165);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Math Tester";
            // 
            // BTN_MathLoad
            // 
            this.BTN_MathLoad.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BTN_MathLoad.Location = new System.Drawing.Point(512, 36);
            this.BTN_MathLoad.Name = "BTN_MathLoad";
            this.BTN_MathLoad.Size = new System.Drawing.Size(75, 32);
            this.BTN_MathLoad.TabIndex = 6;
            this.BTN_MathLoad.Text = "Load";
            this.BTN_MathLoad.UseVisualStyleBackColor = true;
            this.BTN_MathLoad.Click += new System.EventHandler(this.BTN_MathLoad_Click);
            // 
            // LB_MathAnswer
            // 
            this.LB_MathAnswer.AutoSize = true;
            this.LB_MathAnswer.ForeColor = System.Drawing.Color.Green;
            this.LB_MathAnswer.Location = new System.Drawing.Point(220, 76);
            this.LB_MathAnswer.Name = "LB_MathAnswer";
            this.LB_MathAnswer.Size = new System.Drawing.Size(0, 13);
            this.LB_MathAnswer.TabIndex = 5;
            // 
            // TB_Yat
            // 
            this.TB_Yat.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TB_Yat.Location = new System.Drawing.Point(112, 77);
            this.TB_Yat.Name = "TB_Yat";
            this.TB_Yat.Size = new System.Drawing.Size(100, 32);
            this.TB_Yat.TabIndex = 4;
            this.TB_Yat.TextChanged += new System.EventHandler(this.TB_Yat_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(51, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Y value at";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Function : y =";
            // 
            // BTN_PixelFamily
            // 
            this.BTN_PixelFamily.Location = new System.Drawing.Point(34, 80);
            this.BTN_PixelFamily.Name = "BTN_PixelFamily";
            this.BTN_PixelFamily.Size = new System.Drawing.Size(110, 38);
            this.BTN_PixelFamily.TabIndex = 2;
            this.BTN_PixelFamily.Text = "Pixel Family";
            this.BTN_PixelFamily.UseVisualStyleBackColor = true;
            this.BTN_PixelFamily.Click += new System.EventHandler(this.BTN_PixelFamily_Click);
            // 
            // MainTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1098, 592);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainTestForm";
            this.Text = "Pixel Lion ~ PMSL Workbench & Tests";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TB_CoreLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BTN_PMSLBrowseCoreFile;
        private System.Windows.Forms.TextBox TB_PMSLInput;
        private System.Windows.Forms.Label LB_TimeToInterpret;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button BTN_InterpretCore;
        private System.Windows.Forms.Label LB_ErrorCount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox LIST_ResultContent;
        private System.Windows.Forms.RichTextBox TB_PMSLOutput;
        private System.Windows.Forms.Button BTN_LoadFile;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox TB_Function;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label LB_MathAnswer;
        private System.Windows.Forms.TextBox TB_Yat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BTN_MathLoad;
        private System.Windows.Forms.Button BTN_Shapes;
        private System.Windows.Forms.Button BTN_MapResize;
        private System.Windows.Forms.Button BTN_PixelFamily;
    }
}

