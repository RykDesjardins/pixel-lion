﻿namespace Test_Utilities
{
    partial class TabControlPanelTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTN_AddTab = new System.Windows.Forms.Button();
            this.TB_Tabname = new System.Windows.Forms.TextBox();
            this.tabControlPanel1 = new PixelLion_Win.TabControlPanel();
            this.LB_CurrentTab = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BTN_AddTab
            // 
            this.BTN_AddTab.Location = new System.Drawing.Point(33, 55);
            this.BTN_AddTab.Name = "BTN_AddTab";
            this.BTN_AddTab.Size = new System.Drawing.Size(75, 23);
            this.BTN_AddTab.TabIndex = 1;
            this.BTN_AddTab.Text = "Add Tab";
            this.BTN_AddTab.UseVisualStyleBackColor = true;
            this.BTN_AddTab.Click += new System.EventHandler(this.BTN_AddTab_Click);
            // 
            // TB_Tabname
            // 
            this.TB_Tabname.Location = new System.Drawing.Point(114, 57);
            this.TB_Tabname.Name = "TB_Tabname";
            this.TB_Tabname.Size = new System.Drawing.Size(268, 20);
            this.TB_Tabname.TabIndex = 2;
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tabControlPanel1.Location = new System.Drawing.Point(12, 12);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Size = new System.Drawing.Size(788, 23);
            this.tabControlPanel1.TabIndex = 0;
            this.tabControlPanel1.TabChanged += new System.EventHandler(this.tabControlPanel1_TabChanged);
            // 
            // LB_CurrentTab
            // 
            this.LB_CurrentTab.AutoSize = true;
            this.LB_CurrentTab.Location = new System.Drawing.Point(411, 60);
            this.LB_CurrentTab.Name = "LB_CurrentTab";
            this.LB_CurrentTab.Size = new System.Drawing.Size(69, 13);
            this.LB_CurrentTab.TabIndex = 3;
            this.LB_CurrentTab.Text = "[Current Tab]";
            // 
            // TabControlPanelTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 355);
            this.Controls.Add(this.LB_CurrentTab);
            this.Controls.Add(this.TB_Tabname);
            this.Controls.Add(this.BTN_AddTab);
            this.Controls.Add(this.tabControlPanel1);
            this.Name = "TabControlPanelTest";
            this.Text = "TabControlPanel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PixelLion_Win.TabControlPanel tabControlPanel1;
        private System.Windows.Forms.Button BTN_AddTab;
        private System.Windows.Forms.TextBox TB_Tabname;
        private System.Windows.Forms.Label LB_CurrentTab;
    }
}