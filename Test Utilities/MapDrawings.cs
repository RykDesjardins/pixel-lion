﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelLionLib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PixelLionWin;

namespace Test_Utilities
{
    public partial class MapDrawings : Form
    {
        Map CurrentMap;
        SpriteBatch Batch;

        public MapDrawings()
        {
            InitializeComponent();

            InitDevice();

            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                CurrentMap = Map.LoadFromFile(dlg.FileName);
            }
        }


        private void InitDevice()
        {
            XNADevices.graphicsdevice = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, GraphicsProfile.Reach, new PresentationParameters()
            {
                BackBufferHeight = PN_Map.Height,
                BackBufferWidth = PN_Map.Width,
                IsFullScreen = false,
                DeviceWindowHandle = PN_Map.Handle
            }
            );

            Batch = new SpriteBatch(XNADevices.graphicsdevice);
            GlobalPreferences.Prefs = new UserPreferences();
        }

        private void DrawMap()
        {
            using (XNAUtils utils = new XNAUtils()) CurrentMap.Draw(Batch, utils, (Single)TRACK_Ratio.Value / 100);
            XNADevices.graphicsdevice.Present();
        }

        private void PN_Map_MouseClick(object sender, MouseEventArgs e)
        {
            DrawMap();
        }

        private void TRACK_Ratio_Scroll(object sender, EventArgs e)
        {
            
        }

        private void TRACK_Ratio_ValueChanged(object sender, EventArgs e)
        {
            DrawMap();
        }
    }
}
