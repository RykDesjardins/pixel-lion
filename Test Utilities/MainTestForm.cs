﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PixelMonkey;
using PixelLionLib;
using System.IO;
using VBMath;
using PixelLionWin;

namespace Test_Utilities
{
    public partial class MainTestForm : Form
    {
        PixelMonkeyScript Script;
        PixelMonkeyLanguage Language;
        String LastCommand;
        Int32 CommandScrollIndex;

        PixelMath PMath;

        public MainTestForm()
        {
            InitializeComponent();
            LastCommand = String.Empty;
        }

        private void BTN_PMSLBrowseCoreFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TB_CoreLocation.Text = dlg.FileName;
            }
        }

        private void BTN_InterpretCore_Click(object sender, EventArgs e)
        {
            InterpretFile(TB_CoreLocation.Text);
        }

        private void InterpretFile(String path)
        {
            try
            {
                DateTime starttime = DateTime.Now;

                Language = new PixelMonkeyLanguage();
                Language.LoadFile(path);

                LIST_ResultContent.Items.AddRange(Language.GetFunctionList().ToArray());
                LIST_ResultContent.Items.AddRange(Language.GetClassList().ToArray());

                LB_ErrorCount.Text = "No error";
                LB_ErrorCount.ForeColor = Color.Green;

                TimeSpan span = DateTime.Now.Subtract(starttime);
                LB_TimeToInterpret.Text = span.TotalMilliseconds + " milliseconds";

                Script = new PixelMonkeyScript(Language);

                TB_PMSLOutput.AppendText("Successfully loaded Core\r\n");
                TB_PMSLInput.Enabled = true;
                BTN_InterpretCore.Enabled = false;
            }
            catch (PixelMonkeyCoreException ex)
            {
                LB_ErrorCount.Text = "1";
                LB_ErrorCount.ForeColor = Color.Red;

                TB_PMSLOutput.AppendText("[CORE ERROR] " + ex.CustomMessage + "\r\n");
            }
        }

        private void TB_PMSLInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    TB_PMSLOutput.AppendText(">>> " + TB_PMSLInput.Text + "\r\n");
                    Script.AddLine(TB_PMSLInput.Text);
                    LastCommand = TB_PMSLInput.Text;
                    TB_PMSLInput.Text = String.Empty;

                    ExecuteNextCommand();
                }
                catch (PixelMonkeyInterpretationException ex)
                {
                    PrintException(ex);
                }
            }
            else if (e.KeyCode == Keys.Up)
            {
                TB_PMSLInput.Text = LastCommand;
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                TB_PMSLInput.Text = LIST_ResultContent.Items[CommandScrollIndex].ToString();
                CommandScrollIndex++;

                if (CommandScrollIndex == LIST_ResultContent.Items.Count) CommandScrollIndex = 0;
            }
        }

        private void PrintException(PixelMonkeyInterpretationException ex)
        {
            TB_PMSLOutput.SelectionStart = TB_PMSLOutput.TextLength;
            TB_PMSLOutput.SelectionLength = 0;
            TB_PMSLOutput.SelectionColor = Color.Red;
            TB_PMSLOutput.AppendText("[ERROR] " + ex.CustomMessage + (ex.Command == String.Empty ? ex.Command : " -> " + ex.Command) + "\r\n");
            TB_PMSLOutput.SelectionColor = TB_PMSLOutput.ForeColor;

            CommandScrollIndex = 0;
            TB_PMSLOutput.ScrollToCaret();
        }

        private void ExecuteNextCommand()
        {
            DateTime starttime = DateTime.Now;

            Object returned = Script.ExecuteNext();
            String returnedStr = returned == null ? String.Empty : returned.ToString();

            TimeSpan span = DateTime.Now.Subtract(starttime);

            TB_PMSLOutput.SelectionStart = TB_PMSLOutput.TextLength;
            TB_PMSLOutput.SelectionLength = 0;
            TB_PMSLOutput.SelectionColor = Color.White;
            if (returnedStr != String.Empty) TB_PMSLOutput.AppendText(">> " + returned + "\r\n");

            TB_PMSLOutput.SelectionStart = TB_PMSLOutput.TextLength;
            TB_PMSLOutput.SelectionLength = 0;
            TB_PMSLOutput.SelectionColor = Color.Green;
            TB_PMSLOutput.AppendText("> Command was executed successfully within " + span.TotalMilliseconds + " ms.\r\n");
            TB_PMSLOutput.SelectionColor = TB_PMSLOutput.ForeColor;

            CommandScrollIndex = 0;

            TB_PMSLOutput.ScrollToCaret();
        }

        private void BTN_LoadFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamReader reader = new StreamReader(File.OpenRead(dlg.FileName));
                String line;

                while ((line = reader.ReadLine()) != null) if (line.Trim() != String.Empty)
                {
                    try
                    {
                        Script.AddLine(line);
                    }
                    catch (PixelMonkeyInterpretationException ex)
                    {
                        PrintException(ex);
                    }
                }
                
                reader.Close();
            }

            while (Script.HasWorkToDo()) ExecuteNextCommand();
        }

        private void LIST_ResultContent_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (LIST_ResultContent.SelectedIndex != -1)
            {
                String str = Language.GetFunctions().Contains(LIST_ResultContent.SelectedValue) ? Language.GetFunctions().Where(x => x == LIST_ResultContent.SelectedValue).FirstOrDefault().Name : String.Empty;
                Language.GetFunctions().Where(x => x == LIST_ResultContent.SelectedValue).FirstOrDefault().ParamsType.ForEach(x => str += " " + x.ToString() + ", ");

                TB_PMSLInput.Text = str;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TabControlPanelTest dlg = new TabControlPanelTest();
            dlg.Show();
        }

        private void BTN_MathLoad_Click(object sender, EventArgs e)
        {
            PMath = new PixelMath(TB_Function.Text);
        }

        private void TB_Yat_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (TB_Yat.Text != String.Empty) LB_MathAnswer.Text = PMath.YAt(Single.Parse(TB_Yat.Text)).ToString();
            }
            catch (Exception)
            {
                LB_MathAnswer.Text = "Intrant invalide";
            }
        }

        private void BTN_Shapes_Click(object sender, EventArgs e)
        {
            XNADrawings dlg = new XNADrawings();
            dlg.ShowDialog();
        }

        private void BTN_MapResize_Click(object sender, EventArgs e)
        {
            MapDrawings dlg = new MapDrawings();
            dlg.ShowDialog();
        }

        private void BTN_PixelFamily_Click(object sender, EventArgs e)
        {
            PixelFamilyEdition dlg = new PixelFamilyEdition(new Animation());
            dlg.ShowDialog();
        }
    }
}
