﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using PixelLionLib;

namespace Test_Utilities
{
    public partial class XNADrawings : Form
    {
        SpriteBatch Batch;
        ParticleEngine PEngine;
        Random RandomFeed;
        Int32 FrameCount = 0;

        public XNADrawings()
        {
            InitializeComponent();

            InitDevice();
            InitObjects();
            AddFamily();
        }

        private void DrawPanel()
        {
            UpdateFamily();
            Clear();
            Draw();
            Present();
        }

        private void InitObjects()
        {
            RandomFeed = new Random();

            Batch = new SpriteBatch(XNADevices.graphicsdevice);
            PEngine = new ParticleEngine(new Rectangle(0, 0, panel1.Width, panel1.Height));
        }

        private void InitDevice()
        {
            XNADevices.graphicsdevice = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, GraphicsProfile.Reach, new PresentationParameters()
            {
                BackBufferHeight = panel1.Height,
                BackBufferWidth = panel1.Width,
                IsFullScreen = false,
                DeviceWindowHandle = panel1.Handle
            }
            );
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DrawPanel();

            LB_Memory.Text = "Mémoire vive allouée : " + (System.Diagnostics.Process.GetCurrentProcess().WorkingSet64 / 1024.0f / 1024.0f).ToString() + " Mb";
        }

        private void UpdateFireWorks()
        {
            if (FrameCount % 20 == 0) AddFamily();
        }

        private void AddFamily()
        {
            PEngine.AddParticules(800, ParticleBehaviour.Focus, ParticleShape.Circle, new Vector2(0, 0)); //PEngine.AddParticules(100, ParticleBehaviour.Focus, ParticleShape.Circle, new Vector2(RandomFeed.Next(Width + Width / 2) - Width, RandomFeed.Next(Height + Height / 2) - Height));
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            // ToggleTimer();
            AddFamily();
        }

        private void ToggleTimer()
        {
            if (timer1.Enabled) timer1.Stop();
            else timer1.Start();
        }

        private void Present()
        {
            XNADevices.graphicsdevice.Present();
            FrameCount++;
        }

        private void Clear()
        {
            XNADevices.graphicsdevice.Clear(Color.White);
        }

        private void Draw()
        {
            PEngine.DrawParticles(Batch);
        }

        private void UpdateFamily()
        {
            PEngine.UpdateParticles();
        }
    }
}
