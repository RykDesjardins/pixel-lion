﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionLib
{
    [Serializable()]
    public class Landscape : ISerializable
    {
        public Texture2D BackImage { get; set; }
        public Boolean Repeat { get; set; }
        public Int32 Opacity { get; set; }

        public Landscape()
        {
            Repeat = true;
        }
        ~Landscape()
        {
            if (BackImage != null) BackImage.Dispose();
        }

        public Landscape(SerializationInfo info, StreamingContext context)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                BackImage = utils.ConvertToTexture((System.Drawing.Image)info.GetValue("BackImage", typeof(System.Drawing.Image)));
                Repeat = (Boolean)info.GetValue("Repeat", typeof(Boolean));
                Opacity = (Int32)info.GetValue("Opacity", typeof(Int32));
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                info.AddValue("BackImage", utils.ConvertToImage(BackImage));
                info.AddValue("Repeat", Repeat);
                info.AddValue("Opacity", Opacity);
            }
        }
    }
}
