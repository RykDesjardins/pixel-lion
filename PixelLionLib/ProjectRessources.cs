﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class ProjectRessources :ISerializable
    {
        public List<Map> Maps;
        public SpriteBundle Sprites;
        public List<Pevent> CommunPevents;
        public SongBundle Songs;
        public PeventClassBundle Classes;
        public List<pIcon> Icons;
        public SoundBundle Sounds;
        public List<Animation> Animations;
        public List<Elemental> Elements;
        public List<Battler> Battlers;

        public ProjectRessources()
        {
            Maps = new List<Map>();
            Sprites = new SpriteBundle();
            CommunPevents = new List<Pevent>();
            Songs = new SongBundle();
            Classes = new PeventClassBundle();
            Icons = new List<pIcon>();
            Sounds = new SoundBundle();
            Animations = new List<Animation>();
            Elements = new List<Elemental>();
            Battlers = new List<Battler>();
        }

        public ProjectRessources(SerializationInfo info, StreamingContext context)
        {
            Maps = (List<Map>)info.GetValue("Maps", typeof(List<Map>));
            Sprites = (SpriteBundle)info.GetValue("Sprites", typeof(SpriteBundle));
            CommunPevents = (List<Pevent>)info.GetValue("CommunPevents", typeof(List<Pevent>));
            Songs = (SongBundle)info.GetValue("Songs", typeof(SongBundle));
            Classes = (PeventClassBundle)info.GetValue("PeventClassBundle", typeof(PeventClassBundle));
            Icons = (List<pIcon>)info.GetValue("Icons", typeof(List<pIcon>));
            Sounds = (SoundBundle)info.GetValue("Sounds", typeof(SoundBundle));
            Animations = (List<Animation>)info.GetValue("Animations", typeof(List<Animation>));
            Elements = (List<Elemental>)info.GetValue("Elements", typeof(List<Elemental>));
            Battlers = (List<Battler>)info.GetValue("Battlers", typeof(List<Battler>));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Maps", Maps);
            info.AddValue("Sprites", Sprites);
            info.AddValue("CommunPevents", CommunPevents);
            info.AddValue("Songs", Songs);
            info.AddValue("PeventClassBundle", Classes);
            info.AddValue("Icons", Icons);
            info.AddValue("Sounds", Sounds);
            info.AddValue("Animations", Animations);
            info.AddValue("Elements", Elements);
            info.AddValue("Battlers", Battlers);
        }
    }
}
