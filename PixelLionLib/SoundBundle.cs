﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using WMPLib;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class SoundBundle : ISerializable
    {
        public List<Sound> Sounds { get; set; }
        private Int32 PlayingIndex;
        static WindowsMediaPlayer player = new WindowsMediaPlayer();

        public SoundBundle()
        {
            Sounds = new List<Sound>();
            PlayingIndex = 0;
        }

        public SoundBundle(SerializationInfo info, StreamingContext context)
        {
            Sounds = (List<Sound>)info.GetValue("Sounds", typeof(List<Sound>));
            PlayingIndex = 0;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Sounds", Sounds);
        }

        public void Play(Int32 index)
        {
            File.WriteAllBytes("temp\\" + Sounds[index].Name, Sounds[index].Data);
            PlayingIndex = index;

            player.URL = "temp\\" + Sounds[index].Name;
            player.controls.play();
        }

        public void Play(Sound sound)
        {
            try
            {
                Stop();
                File.WriteAllBytes("temp\\" + sound.Name, sound.Data);
            }
            catch (IOException)
            {
                return;
            }

            PlayingIndex = Sounds.IndexOf(sound);

            player.URL = "temp\\" + sound.Name;
            player.controls.play();
        }

        public void Play(String sound)
        {
            Play(Sounds.Where(x => x.Name == sound).First());
        }

        public void Stop()
        {
            try
            {
                File.Delete("temp\\" + Sounds[PlayingIndex].Name);
            }
            catch (Exception)
            { }

            player.controls.stop();
            player.URL = "";
        }

        public static void StopActivity()
        {
            player.controls.stop();
            player.URL = "";
        }
    }
}
