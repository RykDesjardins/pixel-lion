﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionLib
{
    [Serializable()]
    public class Battler : ISerializable
    {
        public String Name;
        public Texture2D Img;

        public Battler(String name, Texture2D img)
        {
            Name = name;
            Img = img;
        }

        public Battler(SerializationInfo info, StreamingContext context)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                Name = info.GetString("Name");
                Img = utils.ConvertToTexture(info.GetValue("Img", typeof(System.Drawing.Image)) as System.Drawing.Image);
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                info.AddValue("Name", Name);
                info.AddValue("Img", utils.ConvertToImage(Img));
            }
        }

        public override String ToString()
        {
            return Name;
        }
    }
}
