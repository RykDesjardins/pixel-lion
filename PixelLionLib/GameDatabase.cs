﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class GameDatabase : ISerializable
    {
        public List<Monster> Monsters;
        public List<SpecialAttack> SpecialAttacks;
        public List<ItemObject> ItemObjects;
        public List<Elemental> Elements;

        public GameDatabase()
        {
            Monsters = new List<Monster>();
            SpecialAttacks = new List<SpecialAttack>();
            ItemObjects = new List<ItemObject>();
            Elements = new List<Elemental>();
        }

        public GameDatabase(SerializationInfo info, StreamingContext context)
        {
            Monsters = info.GetValue("Monsters", typeof(List<Monster>)) as List<Monster>;
            SpecialAttacks = info.GetValue("SpecialAttacks", typeof(List<SpecialAttack>)) as List<SpecialAttack>;
            ItemObjects = info.GetValue("ItemObjects", typeof(List<ItemObject>)) as List<ItemObject>;
            Elements = info.GetValue("Elements", typeof(List<Elemental>)) as List<Elemental>;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Monsters", Monsters);
            info.AddValue("SpecialAttacks", SpecialAttacks);
            info.AddValue("ItemObjects", ItemObjects);
            info.AddValue("Elements", Elements);
        }
    }
}
