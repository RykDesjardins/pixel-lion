﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    /*
    A class must be created by the name of "Elemental".
    This class must contain a name as a String, a pIcon and three arrays or list of its own class representing the followings :
    *Imunity
    *Resistance
    *Weakness
    Then, the ElementalSelector must receive an Elemental list which is going to be saved in the project ressources in order to load the existing elements into the user control.
    Finally, the Elemental class must override ToString method which will simply return this.Name;
     */

    [Serializable()]
    public class Elemental : ISerializable
    {
        String Name;
        public pIcon Icon;
        String Description;

        public List<Elemental> Immunities;
        public List<Elemental> Resistances;
        public List<Elemental> Weaknesses;

        public Elemental(String name)
        {
            SetName(name);
            Icon = null;
            Description = null;
        }

        public Elemental(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString("Name");
            Icon = info.GetValue("Icon", typeof(pIcon)) as pIcon;
            Description = info.GetString("Description");

            Immunities = (List<Elemental>)info.GetValue("Immunities", typeof(List<Elemental>));
            Resistances = (List<Elemental>)info.GetValue("Resistances", typeof(List<Elemental>));
            Weaknesses = (List<Elemental>)info.GetValue("Weaknesses", typeof(List<Elemental>));
        }

        public Elemental(String name, pIcon icon)
        {
            SetName(name);
            Icon = icon;
            Description = null;
        }

        public Elemental()
        {
            SetName(String.Empty);
            Icon = null;
            Description = null;
        }

        public override String ToString()
        {
            return this.Name;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("Icon", Icon);
            info.AddValue("Immunities", Immunities);
            info.AddValue("Resistances", Resistances);
            info.AddValue("Weaknesses", Weaknesses);
        }

        public void SetName(String name)
        {
            Name = name;
        }

        public String GetName()
        {
            return Name;
        }

        public void SetDescription(String desc)
        {
            Description = desc;
        }

        public String GetDescription()
        {
            return Description;
        }
    }
}
