﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionLib
{
    [Serializable()]
    public class Map : ISerializable, ICloneable
    {
        public Tile[, ,] Tiles;
        public Int32[] LayersOpacity;
        public Texture2D TileSet;
        public String Name;
        public Landscape LandScape;
        public Fog FogOverlay;
        public SizeI MapSize;
        public Boolean WasInit;
        public Texture2D Limits;
        public String FilePath;

        public Map()
        {
            MapSize = new SizeI();
            LayersOpacity = new Int32[] {255,255,255};
        }
        ~Map()
        {
            if (TileSet != null) TileSet.Dispose();
            if (Limits != null) Limits.Dispose();
        }

        public Tile[, ,] CloneTiles()
        {
            Tile[, ,] tiles = new Tile[3, MapSize.Width, MapSize.Height];
            for (int l = 0; l < 3; l++)
                for (int x = 0; x < MapSize.Width; x++)
                    for (int y = 0; y < MapSize.Height; y++)
                        tiles[l, x, y] = Tiles[l, x, y].Clone() as Tile;

            return tiles;
        }

        public Map(SizeI size, Int32 SquareLength)
        {
            Tiles = new Tile[3, size.Width, size.Height];

            for (int l = 0; l < 3; l++)
                for (int i = 0; i < size.Width; i++)
                    for (int j = 0; j < size.Height; j++)
                        Tiles[l, i, j] = new Tile(i, j, SquareLength);

            MapSize = size;
            LayersOpacity = new Int32[] { 255, 255, 255 };
        }

        public Map(Tile[, ,] tiles, Texture2D tileset)
        {
            Tiles = tiles;
            TileSet = tileset;

            MapSize = new SizeI(tiles.GetLength(1), tiles.GetLength(2));
            LayersOpacity = new Int32[] { 255, 255, 255 };
        }

        private Map(Map o)
        {
            this.Tiles = o.Tiles;
            this.TileSet = o.TileSet;
            this.Name = o.Name;
            this.LandScape = o.LandScape;
            this.FogOverlay = o.FogOverlay;
            this.MapSize = o.MapSize;
            this.WasInit = o.WasInit;
            this.Limits = o.Limits;
            this.FilePath = o.FilePath;
            this.LayersOpacity = o.LayersOpacity;
        }

        public void Resize(SizeI newSize)
        {
            Tile[, ,] newtiles = new Tile[3, newSize.Width, newSize.Height];

            for (int i = 0; i < 3; i++)
                for (int x = 0; x < newSize.Width; x++)
                    for (int y = 0; y < newSize.Height; y++)
                    {
                        try
                        {
                            newtiles[i, x, y] = Tiles[i, x, y];
                        }
                        catch (IndexOutOfRangeException)
                        {
                            newtiles[i, x, y] = new Tile(x, y, 32);
                        }
                    }

            MapSize = newSize;
            Tiles = newtiles;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                info.AddValue("Tiles", Tiles);
                info.AddValue("TileSet", utils.ConvertToImage(TileSet));
                info.AddValue("Name", Name);
                info.AddValue("Size", MapSize);
                info.AddValue("LandScape", LandScape);
                info.AddValue("Limits", utils.ConvertToImage(Limits));
                info.AddValue("Fog", FogOverlay);
                info.AddValue("FilePath", FilePath);
                info.AddValue("LayersOpacity", LayersOpacity);
            }
        }

        public Map(SerializationInfo info, StreamingContext context)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                Tiles = (Tile[, ,])info.GetValue("Tiles", typeof(Tile[, ,]));
                TileSet = utils.ConvertToTexture((System.Drawing.Image)info.GetValue("TileSet", typeof(System.Drawing.Image)));
                Name = (String)info.GetValue("Name", typeof(String));
                MapSize = (SizeI)info.GetValue("Size", typeof(SizeI));
                LandScape = (Landscape)info.GetValue("LandScape", typeof(Landscape));
                Limits = utils.ConvertToTexture((System.Drawing.Image)info.GetValue("Limits", typeof(System.Drawing.Image)));
                FogOverlay = (Fog)info.GetValue("Fog", typeof(Fog));
                WasInit = true;
                FilePath = info.GetString("FilePath");
                LayersOpacity = (Int32[])info.GetValue("LayersOpacity", typeof(Int32[]));
            }
        }

        public void SaveToFile(String path)
        {
            Stream stream = File.Open(path, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, this);
            stream.Close();
        }

        public void Save()
        {
            SaveToFile(this.FilePath);
        }

        public static Map LoadFromFile(String path)
        {
            Map objectToSerialize;
            Stream stream = File.Open(path, FileMode.Open);
            BinaryFormatter bFormatter = new BinaryFormatter();
            objectToSerialize = (Map)bFormatter.Deserialize(stream);
            stream.Close();

            objectToSerialize.FilePath = path;

            return objectToSerialize;
        }

        public static IEnumerable<Map> GetAllMaps(String DirPath)
        {
            // foreach (String str in Directory.GetFiles(DirPath)) if (str.EndsWith(".plm")) yield return Map.LoadFromFile(str);

            return Directory.GetFiles(DirPath).Where(x => x.EndsWith(".plm")).Select(x => Map.LoadFromFile(x));
        }

        public static Int32 GetMapCount(String DirPath)
        {
            // Int32 Count = 0;
            // foreach (String str in Directory.GetFiles(DirPath)) if (str.EndsWith(".plm")) Count++;
            // return Count;

            return Directory.GetFiles(DirPath).Where(x => x.EndsWith(".plm")).Count();
        }

        public void Draw(SpriteBatch MapBatch, XNAUtils utils, Single ratio = 1f)
        {
            DrawLandScape(MapBatch, ratio);
            DrawTiles(MapBatch, ratio);
            DrawPevents(MapBatch, ratio);
            DrawFog(MapBatch, ratio);
            DrawGrid(MapBatch, utils, ratio);
        }

        public void DrawThumbnail(SpriteBatch MapBatch, Single ratio)
        {
            DrawLandScape(MapBatch, ratio);
            DrawTiles(MapBatch, ratio);
            DrawPevents(MapBatch, ratio);
            DrawFog(MapBatch, ratio);
        }

        public void DrawGrid(Panel destination)
        {
            using (System.Drawing.Bitmap buffer = new System.Drawing.Bitmap(destination.Width, destination.Height))
            using (System.Drawing.Graphics graph = System.Drawing.Graphics.FromImage(buffer as System.Drawing.Image))
            using (System.Drawing.Pen pen = new System.Drawing.Pen(GlobalPreferences.Prefs.Map.GridColor, GlobalPreferences.Prefs.Map.GridThickness))
            {
                for (int x = 0; x < MapSize.Width; x++)
                    graph.DrawLine(pen, new System.Drawing.Point(x * GlobalPreferences.Prefs.Map.TileSize.Width, 0), new System.Drawing.Point(x * GlobalPreferences.Prefs.Map.TileSize.Width, MapSize.Height * GlobalPreferences.Prefs.Map.TileSize.Height));

                for (int y = 0; y < MapSize.Height; y++)
                    graph.DrawLine(pen, new System.Drawing.Point(0, y * GlobalPreferences.Prefs.Map.TileSize.Height), new System.Drawing.Point(MapSize.Width * GlobalPreferences.Prefs.Map.TileSize.Width, y * GlobalPreferences.Prefs.Map.TileSize.Height));

                graph.DrawRectangle(pen, new System.Drawing.Rectangle(1, 1, MapSize.Width * GlobalPreferences.Prefs.Map.TileSize.Width - 2, MapSize.Height * GlobalPreferences.Prefs.Map.TileSize.Height - 2));

                using (System.Drawing.Graphics g = destination.CreateGraphics())
                {
                    g.DrawImage(buffer, new System.Drawing.Point(0, 0));
                }
            }
        }

        public void DrawGrid(SpriteBatch MapBatch, XNAUtils utils, Single ratio)
        {
            MapBatch.Begin();

            for (int x = 0; x < MapSize.Width; x++)
                MapBatch.DrawLine(new Vector2((Int32)((Single)x * (Single)GlobalPreferences.Prefs.Map.TileSize.Width * ratio), 0), new Vector2((Int32)((Single)x * (Single)GlobalPreferences.Prefs.Map.TileSize.Width * ratio), (Int32)((Single)MapSize.Height * (Single)GlobalPreferences.Prefs.Map.TileSize.Height * ratio)), utils.xColor(GlobalPreferences.Prefs.Map.GridColor), GlobalPreferences.Prefs.Map.GridThickness);

            for (int y = 0; y < MapSize.Height; y++)
                MapBatch.DrawLine(new Vector2(0, (Int32)((Single)y * (Single)GlobalPreferences.Prefs.Map.TileSize.Height * ratio)), new Vector2((Int32)((Single)MapSize.Width * (Single)GlobalPreferences.Prefs.Map.TileSize.Width * ratio), (Int32)((Single)y * (Single)GlobalPreferences.Prefs.Map.TileSize.Height * ratio)), utils.xColor(GlobalPreferences.Prefs.Map.GridColor), GlobalPreferences.Prefs.Map.GridThickness);

            utils.DrawRectangle(MapBatch, GlobalPreferences.Prefs.Map.GridThickness, utils.xColor(GlobalPreferences.Prefs.Map.GridColor), new Rectangle(1, 1, (Int32)(((Single)MapSize.Width * (Single)GlobalPreferences.Prefs.Map.TileSize.Width - 2f) * ratio), (Int32)(((Single)MapSize.Height * (Single)GlobalPreferences.Prefs.Map.TileSize.Height - 2f) * ratio)));

            MapBatch.End();
        }

        public void DrawFog(SpriteBatch MapBatch, Single ratio)
        {
            if (FogOverlay != null && FogOverlay.BackImage != null)
            {
                MapBatch.Begin(SpriteSortMode.Texture, BlendState.Additive);

                for (int xCoef = 0; xCoef < MapSize.Width * GlobalPreferences.Prefs.Map.TileSize.Width; xCoef += FogOverlay.BackImage.Width)
                    for (int yCoef = 0; yCoef < MapSize.Height * GlobalPreferences.Prefs.Map.TileSize.Height; yCoef += FogOverlay.BackImage.Height)
                    {
                        MapBatch.Draw(FogOverlay.BackImage, new Rectangle((Int32)((Single)xCoef * ratio), (Int32)((Single)yCoef * ratio), (Int32)((Single)FogOverlay.BackImage.Width * ratio), (Int32)((Single)FogOverlay.BackImage.Height * ratio)), Color.FromNonPremultiplied(255, 255, 255, FogOverlay.Opacity));
                    }

                MapBatch.End();
            }
        }

        public void DrawLandScape(SpriteBatch MapBatch, Single ratio)
        {
            if (LandScape != null && LandScape.BackImage != null)
            {
                MapBatch.Begin();

                for (int xCoef = 0; xCoef < MapSize.Width * GlobalPreferences.Prefs.Map.TileSize.Width; xCoef += LandScape.BackImage.Width)
                    for (int yCoef = 0; yCoef < MapSize.Height * GlobalPreferences.Prefs.Map.TileSize.Height; yCoef += LandScape.BackImage.Height)
                    {
                        MapBatch.Draw(LandScape.BackImage, new Rectangle((Int32)((Single)xCoef * ratio), (Int32)((Single)yCoef * ratio), (Int32)((Single)LandScape.BackImage.Width * ratio), (Int32)((Single)LandScape.BackImage.Height * ratio)), Color.FromNonPremultiplied(255, 255, 255, LandScape.Opacity));
                    }

                MapBatch.End();
            }
        }

        public void DrawTiles(SpriteBatch MapBatch, Single ratio = 1f)
        {
            MapBatch.Begin();

            for (int l = 0; l < 3; l++)
                for (int x = 0; x < MapSize.Width; x++)
                    for (int y = 0; y < MapSize.Height; y++)
                        if (Tiles[l, x, y].WasDrawn)
                            MapBatch.Draw(TileSet, new Rectangle((Int32)((Single)x * (Single)GlobalPreferences.Prefs.Map.TileSize.Width * ratio), (Int32)((Single)y * (Single)GlobalPreferences.Prefs.Map.TileSize.Height * ratio), (Int32)((Single)GlobalPreferences.Prefs.Map.TileSize.Width * ratio), (Int32)((Single)GlobalPreferences.Prefs.Map.TileSize.Height * ratio)), Tiles[l, x, y].GetClipRectangle(), Color.FromNonPremultiplied(255, 255, 255, LayersOpacity[l]));

            MapBatch.End();
        }

        public void DrawPevents(SpriteBatch MapBatch, Single ratio = 1f)
        {
            MapBatch.Begin();

            // Draw every pEvent on map
            for (int l = 0; l < 3; l++)
                for (int x = 0; x < MapSize.Width; x++)
                    for (int y = 0; y < MapSize.Height; y++)
                        DisplayEv(MapBatch, l, x, y, ratio);

            MapBatch.End();
        }

        private void DisplayEv(SpriteBatch MapBatch, Int32 l, Int32 x, Int32 y, Single ratio = 1f)
        {
            if (Tiles[l, x, y].pEvent == null) return;

            if (Tiles[l, x, y].pEvent.pSprite == null)
            {
                MapBatch.Draw(XNADevices.EvMarkers[l], new Rectangle((Int32)((Single)x * (Single)GlobalPreferences.Prefs.Map.TileSize.Width * ratio), (Int32)((Single)y * (Single)GlobalPreferences.Prefs.Map.TileSize.Height * ratio), (Int32)((Single)XNADevices.EvMarkers[l].Width * ratio), (Int32)((Single)XNADevices.EvMarkers[l].Height * ratio)), Color.White);
            }
            else
            {
                Point pos = Tiles[l, x, y].pEvent.pSprite.ComputeMapPosition(new Point(x * GlobalPreferences.Prefs.Map.TileSize.Width, y * GlobalPreferences.Prefs.Map.TileSize.Height));

                Texture2D sprite = Tiles[l, x, y].pEvent.pSprite.SpriteImage;
                Rectangle crop = Tiles[l, x, y].pEvent.pSprite.GetClipRect(new Point(0, 0));

                MapBatch.Draw(sprite, new Rectangle((Int32)((Single)pos.X * ratio), (Int32)((Single)pos.Y * ratio), (Int32)((Single)crop.Width * ratio), (Int32)((Single)crop.Height * ratio)), crop, Color.White);
            }
        }

        public void DrawMap(SpriteBatch batch)
        {
            DrawAllLayers(batch, true);
        }

        public void DrawMap(SpriteBatch batch, Single ratio)
        {
            for (int l = 0; l < 3; l++) DrawScaledLayer(batch, ratio);
        }

        public void DrawAllLayers(SpriteBatch batch, Boolean drawPevent = false)
        {
            for (int l = 0; l < 3; l++) DrawLayer(batch, l, drawPevent);
        }

        public void DrawLayer(SpriteBatch batch, Int32 l, Boolean drawPevent = false)
        {
            System.Drawing.Size size = GlobalPreferences.Prefs.Map.TileSize;

            for (int i = 0; i < MapSize.Width; i++)
                for (int j = 0; j < MapSize.Height; j++)
                    if (Tiles[l, i, j].WasDrawn)
                        batch.Draw(TileSet, new Rectangle(i, j, size.Width, size.Height), Tiles[l, i, j].GetClipRectangle(), Color.White);

            if (drawPevent)
            {
                for (int x = 0; x < MapSize.Width; x++)
                    for (int y = 0; y < MapSize.Height; y++)
                        DisplayEv(batch, l, x, y, 1f);
            }
        }

        public void DrawScaledLayer(SpriteBatch batch, Single ratio)
        {
            for (int i = 0; i < MapSize.Width; i++)
                for (int j = 0; j < MapSize.Height; j++)
                    for (int l = 0; l < 3; l++)
                        if (Tiles[l, i, j].WasDrawn)
                            batch.Draw(TileSet, new Vector2(i, j), Tiles[l, i, j].GetClipRectangle(), Color.White, 0f, new Vector2(0, 0), ratio, SpriteEffects.None, 0f);
        }

        public Texture2D DrawMap()
        {
            return new Texture2D(XNADevices.graphicsdevice, 1, 1);
        }

        public override String ToString()
        {
            return Name;
        }

        public Tile[] MakeTileArray(Point p)
        {
            return new Tile[] { Tiles[0, p.X, p.Y], Tiles[1, p.X, p.Y], Tiles[2, p.X, p.Y] };
        }

        public object Clone()
        {
            return new Map(this);
        }

        public IEnumerable<Pevent> GetAllPevents()
        {
            for (int l = 0; l < 3; l++)
                for (int x = 0; x < MapSize.Width; x++)
                    for (int y = 0; y < MapSize.Height; y++)
                    {
                        if (Tiles[l, x, y].pEvent != null)
                        {
                            Tiles[l, x, y].pEvent.Position = new System.Drawing.Point(x, y);
                            yield return Tiles[l, x, y].pEvent;
                        }
                    }
        }

        public Pevent[] GetAllPeventsArray()
        {
            return GetAllPevents().ToArray();
        }

        public List<Pevent> GetAllPeventsList()
        {
            return GetAllPevents().ToList();
        }
    }
}
