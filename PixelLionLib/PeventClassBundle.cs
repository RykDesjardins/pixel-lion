﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PixelLionLib
{
     [Serializable()]
    public class PeventClassBundle : ISerializable
    {
        public List<PeventClass> ListpEventClass;

        public PeventClassBundle()
        {
            ListpEventClass = new List<PeventClass>();
            ListpEventClass.Add(new PeventClass(PeventType.pEvent, PeventTriggerType.Auto, PeventMoveType.Away, PeventDirection.Down, "None", 0, 0, false, false, false));
            ListpEventClass.Add(new PeventClass(PeventType.Monster, PeventTriggerType.Auto, PeventMoveType.Away, PeventDirection.Down, "Monster", 0, 0, false, false, false));
            ListpEventClass.Add(new PeventClass(PeventType.pEvent, PeventTriggerType.Auto, PeventMoveType.Away, PeventDirection.Down, "Lantern", 0, 0, false, false, false));
            ListpEventClass.Add(new PeventClass(PeventType.pEvent, PeventTriggerType.Auto, PeventMoveType.Away, PeventDirection.Down, "Chest", 0, 0, false, false, false));
            ListpEventClass.Add(new PeventClass(PeventType.pEvent, PeventTriggerType.Auto, PeventMoveType.Away, PeventDirection.Down, "Breakable", 0, 0, false, false, false));
            ListpEventClass.Add(new PeventClass(PeventType.pEvent, PeventTriggerType.Auto, PeventMoveType.Away, PeventDirection.Down, "Bombable", 0, 0, false, false, false));
            ListpEventClass.Add(new PeventClass(PeventType.pEvent, PeventTriggerType.Auto, PeventMoveType.Away, PeventDirection.Down, "Throwable", 0, 0, false, false, false));
            ListpEventClass.Add(new PeventClass(PeventType.pEvent, PeventTriggerType.Auto, PeventMoveType.Away, PeventDirection.Down, "Plateforme", 0, 0, false, false, false));
        }

        public PeventClassBundle(SerializationInfo info, StreamingContext context)
        {
            ListpEventClass = (List<PeventClass>)info.GetValue("PeventClass", typeof(List<PeventClass>));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("PeventClass", ListpEventClass);
        }
    }
}
