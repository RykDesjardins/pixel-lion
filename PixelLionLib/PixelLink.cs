﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace PixelLionLib
{
    [Serializable()]
    public class PixelLink : ISerializable
    {
        public String Path { get; set; }
        public String Title { get; set; }
        public Boolean IncludeMapsInProject { get; set; }
        public Boolean ProjectFileOnly { get; set; }
        public Boolean CreateFolders { get; set; }
        Process GameProcess;

        public PixelLink()
        {
            Path = "";
            Title = "";
            IncludeMapsInProject = true;
            CreateFolders = true;
        }

        public PixelLink(SerializationInfo info, StreamingContext context)
        {
            Path = info.GetString("Path");
            Title = info.GetString("Title");
            IncludeMapsInProject = info.GetBoolean("IncludeMapsInProject");
            ProjectFileOnly = info.GetBoolean("ProjectFileOnly");
            CreateFolders = info.GetBoolean("CreateFolders");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Path", Path);
            info.AddValue("Title", Title);
            info.AddValue("IncludeMapsInProject", IncludeMapsInProject);
            info.AddValue("ProjectFileOnly", ProjectFileOnly);
            info.AddValue("CreateFolders", CreateFolders);
        }

        public void StartGame()
        {
            ProcessStartInfo pInfo = new ProcessStartInfo();
            pInfo.FileName = Path;

            if (GameProcess != null)
            {
                if (!GameProcess.HasExited) GameProcess.Kill();
                GameProcess.Dispose();
            }

            GameProcess = new Process();
            GameProcess.StartInfo = pInfo;

            GameProcess.Start();
        }

        public void AbortGame()
        {
            if (GameProcess != null && !GameProcess.HasExited)
            {
                GameProcess.Kill();
            }
        }
    }
}
