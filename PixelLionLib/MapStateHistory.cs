﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PixelLionLib
{
    public class MapStateHistory
    {
        LinkedList<MapState> Buffer;
        Int32 Max;

        public MapStateHistory(Int32 BufferSize)
        {
            Buffer = new LinkedList<MapState>();
            Max = BufferSize;
        }

        public void PushHistory(MapState mapstate)
        {
            if (Buffer.Count() == Max) Buffer.RemoveFirst();

            Buffer.AddLast(mapstate);
        }

        public void Clear()
        {
            Buffer.Clear();
        }

        public MapState GetLastState()
        {
            if (Buffer.Count() > 0)
            {
                MapState state = Buffer.ElementAt(Buffer.Count() - 1);
                Buffer.RemoveLast();

                return state;
            }
            else
                return null;
        }

        public Boolean HasNodes()
        {
            return Buffer.Count() != 0;
        }

        public MapState[] GetAllStates()
        {
            return Buffer.Reverse().ToArray();
        }
    }
}
