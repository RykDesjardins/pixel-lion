﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class SizeS : ISerializable
    {
        public Single Width;
        public Single Height;

        public SizeS()
        {
            Width = 0f;
            Height = 0f;
        }

        public SizeS(Single width, Single height)
        {
            Width = width;
            Height = height;
        }

        public SizeS(SerializationInfo info, StreamingContext context)
        {
            Width = info.GetSingle("Width");
            Height = info.GetSingle("Height");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Width", Width);
            info.AddValue("Height", Height);
        }
    }

    [Serializable()]
    public class SizeI : ISerializable
    {
        public Int32 Width;
        public Int32 Height;

        public SizeI()
        {
            Width = 0;
            Height = 0;
        }

        public SizeI(System.Drawing.Size s)
        {
            Width = s.Width;
            Height = s.Height;
        }

        public SizeI(Int32 width, Int32 height)
        {
            Width = width;
            Height = height;
        }

        public SizeI(SerializationInfo info, StreamingContext context)
        {
            Width = info.GetInt32("Width");
            Height = info.GetInt32("Height");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Width", Width);
            info.AddValue("Height", Height);
        }
    }

    public class PointI
    {
        public Int32 X;
        public Int32 Y;

        public PointI()
        {
            X = 0;
            Y = 0;
        }

        public PointI(Int32 width, Int32 height)
        {
            X = width;
            Y = height;
        }
    }
}
