﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionLib
{
    [Serializable()]
    public class Tile : ISerializable, ICloneable
    {
        public Tile() { }
        public Tile(Int32 x, Int32 y, Int32 w) 
        { 
            X = x; 
            Y = y; 
            W = w;
            WasDrawn = false;
        }

        public Tile(Tile o)
        {
            this.pEvent = o.pEvent;
            this.TileIndexX = o.TileIndexX;
            this.TileIndexY = o.TileIndexY;
            this.W = o.W;
            this.WasDrawn = o.WasDrawn;
            this.X = o.X;
            this.Y = o.Y;
        }

        public object Clone()
        {
            return new Tile(this);
        }

        public Int32 X { get; set; }
        public Int32 Y { get; set; }
        public Int32 W { get; set; }

        public Int32 TileIndexX { get; set; }
        public Int32 TileIndexY { get; set; }
        public Boolean WasDrawn { get; set; }
        public Boolean ClipboardMode { get; set; }

        public Pevent pEvent { get; set; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("X", X);
            info.AddValue("Y", Y);
            info.AddValue("W", W);

            info.AddValue("TileIndexX", TileIndexX);
            info.AddValue("TileIndexY", TileIndexY);
            info.AddValue("WasDrawn", WasDrawn);

            info.AddValue("pEvent", pEvent);

            info.AddValue("ClipboardMode", ClipboardMode);
        }

        public Rectangle GetClipRectangle()
        {
            return new Rectangle(TileIndexX * GlobalPreferences.Prefs.Map.TileSize.Width, TileIndexY * GlobalPreferences.Prefs.Map.TileSize.Height, GlobalPreferences.Prefs.Map.TileSize.Width, GlobalPreferences.Prefs.Map.TileSize.Height);
        }

        public Boolean IsAt(Vector2 pos)
        {
            return TileIndexX == pos.X && TileIndexY == pos.Y;
        }

        public void SetIndex(Point p)
        {
            TileIndexX = p.X;
            TileIndexY = p.Y;
        }

        public void SetPosition(Point p)
        {
            X = p.X;
            Y = p.Y;
        }

        public Tile(SerializationInfo info, StreamingContext ctxt)
        {
            X = (Int32)info.GetValue("X", typeof(Int32));
            Y = (Int32)info.GetValue("Y", typeof(Int32));
            W = (Int32)info.GetValue("W", typeof(Int32));
            TileIndexX = (Int32)info.GetValue("TileIndexX", typeof(Int32));
            TileIndexY = (Int32)info.GetValue("TileIndexY", typeof(Int32));
            pEvent = (Pevent)info.GetValue("pEvent", typeof(Pevent));
            WasDrawn = (Boolean)info.GetValue("WasDrawn", typeof(Boolean));
            
            ClipboardMode = false;
        }

        public static Boolean operator ==(Tile original, Tile compared)
        {
            return (original.pEvent.Equals(compared.pEvent) && original.TileIndexX == compared.TileIndexX && original.TileIndexY == compared.TileIndexY);
        }

        public static Boolean operator !=(Tile original, Tile compared)
        {
            return !(original == compared);
        }

        public override Boolean Equals(Object o)
        {
            return this == (o as Tile);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
