﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Design;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System.IO;

namespace PixelLionLib
{
    public class XNAUtils : IDisposable
    {
        Texture2D blank;

        public XNAUtils()
        {
            blank = new Texture2D(XNADevices.graphicsdevice, 1, 1, false, SurfaceFormat.Color);
            blank.SetData<Color>(new[] { Color.White });
        }

        public Texture2D ConvertToTexture(System.Drawing.Image source)
        {
            if (source == null) return null;

            MemoryStream mem = new MemoryStream();
            source.Save(mem, System.Drawing.Imaging.ImageFormat.Png);
            return Texture2D.FromStream(XNADevices.graphicsdevice, mem);
        }

        public System.Drawing.Image ConvertToImage(Texture2D source)
        {
            if (source == null) return null; 

            MemoryStream mem = new MemoryStream();
            source.SaveAsPng(mem, source.Width, source.Height);
            return System.Drawing.Image.FromStream(mem);
        }

        public void DrawLine(SpriteBatch batch, float width, Color color, Vector2 point1, Vector2 point2)
        {
            float angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            float length = Microsoft.Xna.Framework.Vector2.Distance(point1, point2);

            batch.Draw(blank, point1, null, color, angle, Vect(0, 0), Vect(length, width), SpriteEffects.None, 0);
        }

        public void DrawRectangle(SpriteBatch batch, float width, Color color, Rectangle rect)
        {
            // Top, Bottom, Left, Right
            DrawLine(batch, width, color, Vect(rect.X, rect.Y), Vect(rect.X + rect.Width, rect.Y));
            DrawLine(batch, width, color, Vect(rect.X, rect.Y + rect.Height), Vect(rect.X + rect.Width, rect.Y + rect.Height));
            DrawLine(batch, width, color, Vect(rect.X, rect.Y), Vect(rect.X, rect.Y + rect.Height));
            DrawLine(batch, width, color, Vect(rect.X + rect.Width, rect.Y), Vect(rect.X + rect.Width, rect.Y + rect.Height));
        }

        public Microsoft.Xna.Framework.Point xPoint(System.Drawing.Point p)
        {
            return new Point(p.X, p.Y);
        }

        public System.Drawing.Point dPoint(Microsoft.Xna.Framework.Point p)
        {
            return new System.Drawing.Point(p.X, p.Y);
        }

        public Microsoft.Xna.Framework.Vector2 Vect(Single x, Single y)
        {
            return new Microsoft.Xna.Framework.Vector2(x, y);
        }

        public Microsoft.Xna.Framework.Color White()
        {
            return Microsoft.Xna.Framework.Color.White;
        }

        public Microsoft.Xna.Framework.Color xColor(Int32 r, Int32 g, Int32 b, Int32 a)
        {
            return Microsoft.Xna.Framework.Color.FromNonPremultiplied(r, g, b, a);
        }

        public Microsoft.Xna.Framework.Color xColor(System.Drawing.Color c)
        {
            return xColor(c.R, c.G, c.B, c.A);
        }

        public void Dispose()
        {
            blank.Dispose();
        }
    }

    public static class XNADevices
    {
        public static GraphicsDevice graphicsdevice;
        public static Texture2D[] EvMarkers;
    }
}
