﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionLib
{
    [Serializable()]
    public class Sprite : ISerializable
    {
        public Texture2D SpriteImage;
        public String Name;
        public Point TileCount;
        public int tempTileX;
        public int tempTileY;

        public Sprite()
        {
            tempTileX = 0;
            tempTileY = 0;
        }

        public Sprite(SerializationInfo info, StreamingContext context)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                SpriteImage = utils.ConvertToTexture((System.Drawing.Image)info.GetValue("Image", typeof(System.Drawing.Image)));
                Name = (String)info.GetValue("Name", typeof(String));
                TileCount = (Point)info.GetValue("TileCount", typeof(Point));
                tempTileX = 0;
                tempTileY = 0;
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                info.AddValue("Image", utils.ConvertToImage(SpriteImage));
                info.AddValue("Name", Name);
                info.AddValue("TileCount", TileCount);
            }
        }

        public SizeI GetSingleTileSize()
        {
            return new SizeI(SpriteImage.Width / TileCount.X, SpriteImage.Height / TileCount.Y);
        }

        public Point ComputeMapPosition(Point Pos)
        {
            SizeI tilesize = GetSingleTileSize();
            return new Point(Pos.X + 32 / 2 - tilesize.Width / 2, Pos.Y - tilesize.Height + 32);
        }

        public Rectangle GetClipRect(Point panelposition)
        {
            SizeI TileSize = GetSingleTileSize();
            return new Rectangle(panelposition.X * TileSize.Width, panelposition.Y * TileSize.Height, TileSize.Width, TileSize.Height);
        }

        public IEnumerable<Rectangle> GetAllStates()
        {
            for (int i = 0; i < TileCount.X; i++)
                for (int j = 0; j < TileCount.Y; j++)
                    yield return GetClipRect(new Point(i, j));
        }

        public override string ToString()
        {
            return Name;
        }
    }
}