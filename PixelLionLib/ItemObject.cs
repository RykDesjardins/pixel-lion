﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class ItemObject : ISerializable
    {
        public String Name;
        public String Description;
        public pIcon Icon;

        public Int32 HPRecovery;
        public Int32 MPRecovery;
        public Int32 HPPercent;
        public Int32 MPPercent;

        public Boolean Ephemeral;
        public Int32 Price;
        public Boolean Unique;
        public Int32 Weight;

        public TargetType Target;
        public List<StatusEffect> StatusClear;

        public CharactStats PermaChange;

        public ItemObject()
        {
            Name = "Nouvel Objet";
            Description = "Objet inconnu";

            HPPercent = 0;
            HPRecovery = 0;
            MPPercent = 0;
            MPRecovery = 0;

            Ephemeral = true;
            Price = 500;
            Unique = false;
            Weight = 5;

            Target = TargetType.Attaquant;
            StatusClear = new List<StatusEffect>();

            PermaChange = new CharactStats();
        }

        public ItemObject(SerializationInfo info, StreamingContext context)
        {
            Name = (String)info.GetValue("Name", typeof(String));
            Description = (String)info.GetValue("Desc", typeof(String));
            Icon = (pIcon)info.GetValue("Icon", typeof(pIcon));

            HPRecovery = (Int32)info.GetValue("HPRecovery", typeof(Int32));
            MPRecovery = (Int32)info.GetValue("MPRecovery", typeof(Int32));
            HPPercent = (Int32)info.GetValue("HPPercent", typeof(Int32));
            MPPercent = (Int32)info.GetValue("MPPercent", typeof(Int32));

            Ephemeral = (Boolean)info.GetValue("Ephemeral", typeof(Boolean));
            Price = (Int32)info.GetValue("Price", typeof(Int32));
            Unique = (Boolean)info.GetValue("Unique", typeof(Boolean));
            Weight = (Int32)info.GetValue("Weight", typeof(Int32));

            Target = (TargetType)info.GetValue("Target", typeof(TargetType));
            StatusClear = (List<StatusEffect>)info.GetValue("StatusClear", typeof(List<StatusEffect>));

            PermaChange = (CharactStats)info.GetValue("PermaChange", typeof(CharactStats));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("Desc", Description);
            info.AddValue("Icon", Icon);
            info.AddValue("HPRecovery", HPRecovery);
            info.AddValue("MPRecovery", MPRecovery);
            info.AddValue("HPPercent", HPPercent);
            info.AddValue("MPPercent", MPPercent);
            info.AddValue("Ephemeral", Ephemeral);
            info.AddValue("Price", Price);
            info.AddValue("Unique", Unique);
            info.AddValue("Weight", Weight);
            info.AddValue("Target", Target);
            info.AddValue("StatusClear", StatusClear);
            info.AddValue("PermaChange", PermaChange);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
