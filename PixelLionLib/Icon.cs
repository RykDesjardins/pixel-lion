﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionLib
{
    [Serializable()]
    public class pIcon : ISerializable
    {
        Texture2D ContentImage;
        public String Name;

        public pIcon(Texture2D img, String name)
        {
            ContentImage = img;
            Name = name;
        }

        public pIcon(SerializationInfo info, StreamingContext context)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                ContentImage = utils.ConvertToTexture(info.GetValue("ContentImage", typeof(System.Drawing.Image)) as System.Drawing.Image);
                Name = info.GetValue("Name", typeof(String)) as String;
            }
        }

        public Texture2D GetImage()
        {
            return ContentImage;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            using (XNAUtils utils = new XNAUtils())
            {
                info.AddValue("ContentImage", utils.ConvertToImage(ContentImage));
                info.AddValue("Name", Name);
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
