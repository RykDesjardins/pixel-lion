﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class PeventClass : ISerializable
    {
        public PeventType Type { get; set; } // Gneeee pas sur
        public PeventTriggerType TriggerType { get; set; }
        public PeventMoveType MoveType { get; set; }
        public PeventDirection Direction { get; set; }
        public String Name { get; set; }
        public Int32 Sight { get; set; }
        public Int32 Speed { get; set; }
        public Boolean Through { get; set; }
        public Boolean Flying { get; set; }
        public Boolean Disposable { get; set; }

        public PeventClass()
        {
        }

        public PeventClass(PeventType type, PeventTriggerType trigger, PeventMoveType move, PeventDirection direction, string name,
            int sight, int speed, bool through, bool flying, bool disposable)
        {
            Type = type;
            TriggerType = trigger;
            MoveType = move;
            Direction = direction;
            Name = name;
            Sight = sight;
            Speed = speed;
            Through = through;
            Flying = flying;
            Disposable = disposable;
        }

        public PeventClass(SerializationInfo info, StreamingContext context)
        {
            Type = (PeventType)info.GetValue("Type", typeof(PeventType));
            TriggerType = (PeventTriggerType)info.GetValue("TriggerType", typeof(PeventTriggerType));
            MoveType = (PeventMoveType)info.GetValue("MoveType", typeof(PeventMoveType));
            Direction = (PeventDirection)info.GetValue("Direction", typeof(PeventDirection));
            Name = (String)info.GetValue("Name", typeof(String));
            Sight = (Int32)info.GetValue("Sight", typeof(Int32));
            Speed = (Int32)info.GetValue("Speed", typeof(Int32));
            Through = (Boolean)info.GetValue("Through", typeof(Boolean));
            Flying = (Boolean)info.GetValue("Flying", typeof(Boolean));
            Disposable = (Boolean)info.GetValue("Disposable", typeof(Boolean));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Type", Type);
            info.AddValue("TriggerType", TriggerType);
            info.AddValue("MoveType", MoveType);
            info.AddValue("Direction", Direction);
            info.AddValue("Name", Name);
            info.AddValue("Sight", Sight);
            info.AddValue("Speed", Speed);
            info.AddValue("Through", Through);
            info.AddValue("Flying", Flying);
            info.AddValue("Disposable", Disposable);            
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
