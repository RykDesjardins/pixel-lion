﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionLib
{
    public class MapState
    {
        Tile[, ,] tiles;
        public readonly DateTime TimeStamp;

        public MapState(Tile[,,] currentState)
        {
            tiles = currentState;
            TimeStamp = DateTime.Now;
        }

        public Tile[,,] GetTiles()
        {
            return tiles;
        }
        
        public Texture2D GenerateMap(Texture2D tileset)
        {
            Map map = new Map(tiles, tileset);
            return map.DrawMap();
        }
    }
}
