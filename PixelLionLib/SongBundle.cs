﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using WMPLib;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class SongBundle : ISerializable
    {
        public List<Song> Songs { get; set; }
        private Int32 PlayingIndex;
        static WindowsMediaPlayer player = new WindowsMediaPlayer();

        public SongBundle()
        {
            Songs = new List<Song>();
            PlayingIndex = 0;
        }

        public SongBundle(SerializationInfo info, StreamingContext context)
        {
            Songs = (List<Song>)info.GetValue("Songs", typeof(List<Song>));
            PlayingIndex = 0;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Songs", Songs);
        }

        public void Play(Int32 index)
        {
            File.WriteAllBytes("temp\\" + Songs[index].Name, Songs[index].Data);
            PlayingIndex = index;

            player.URL = "temp\\" + Songs[index].Name;
            player.controls.play();
        }

        public void Play(Song song)
        {
            try
            {
                Stop();
                File.WriteAllBytes("temp\\" + song.Name, song.Data);
            }
            catch (IOException)
            {
                return;
            }

            PlayingIndex = Songs.IndexOf(song);

            player.URL = "temp\\" + song.Name;
            player.controls.play();
        }

        public void Play(String song)
        {
            Play(Songs.Where(x => x.Name == song).First());
        }

        public void Stop()
        {
            try
            {
                File.Delete("temp\\" + Songs[PlayingIndex].Name);
            }
            catch (Exception)
            { }

            player.controls.stop();
            player.URL = "";
        }

        public static void StopActivity()
        {
            player.controls.stop();
            player.URL = "";
        }
    }
}
