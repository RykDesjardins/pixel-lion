﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Media;
using System.IO;

namespace PixelLionLib
{
    // Throughly commented just for you Raph <3
    public class Bubble
    {
        // Constants
        const Int32 MinXVelocity = 40;
        const Int32 MaxXVelocity = 90;
        const Int32 MinDiam = 40;
        const Int32 MaxDiam = 100;
        const Int32 MinXCoef = 15;
        const Int32 MaxXCoef = 25;
        const Int32 MinOpacity = 80;
        const Int32 MaxOpacity = 150;

        // Variables
        Point InitPos { get; set; }
        Color color { get; set; }
        Int32 LifeSpan { get; set; }
        Int32 LifeTime { get; set; }
        Double XVelocity { get; set; }
        Double YVelocity { get; set; }
        Int32 XCoef { get; set; }

        // Properties
        public Int32 Opacity { get; set; }
        public Point Position { get; set; }
        public Int32 Diam { get; set; }

        public void Init(Size size, Random rand)
        {
            // Velocity with X = Random Number between 0 and 1 * Random Number between 40 and 90
            XVelocity = rand.NextDouble() * (Double)rand.Next(MinXVelocity, MaxXVelocity);

            // Velocity with Y = Random Number between 0 and 1 + 1
            YVelocity = rand.NextDouble() + 1d;

            // Diameter = Random Number between 40 and 100
            Diam = rand.Next(MinDiam, MaxDiam);

            // Coefficient or variation = Random Number between 15 and 25
            XCoef = rand.Next(MinXCoef, MaxXCoef);

            // Opacity = Random Number between 80 and 150
            Opacity = rand.Next(MinOpacity, MaxOpacity);

            // Color = Orange with specified Opacity
            color = Color.FromArgb(Opacity, 255, 165, 0);

            // Initial position -
            //  X = Random Number between 0 and the Width of the form - Half diameter
            //  Y = Height of the form + Random Number between 5 and the Height of the form
            InitPos = new Point(rand.Next(size.Width) - Diam / 2, size.Height + rand.Next(5, size.Height));

            // Current position = Initial position
            Position = new Point(InitPos.X, InitPos.Y);

            // Did not live yet
            LifeTime = 0;
        }

        public static Bubble Create(Size s, Random rand)
        {
            // Returns a newly created bubble
            Bubble b = new Bubble();
            b.Init(s, rand);

            return b;
        }

        public void Pop(Stream sound)
        {
            // Plays the pop sound from resources
            SoundPlayer player = new SoundPlayer();
            player.Stream = sound;
            player.Play();

            sound.Close();

            // Reinitialize position
            LifeTime = 0;
        }

        private void UpdatePosition()
        {
            // If bubble is out of window, reinit position
            LifeTime = Position.Y < -Diam ? 0 : LifeTime + 1;

            // Position X = Sin( Frame Count / Coefficient or variation ) * Velocity X + Initial Position
            Double DiffX = Math.Sin((Double)LifeTime / (Double)XCoef) * XVelocity + InitPos.X;

            // Position Y = Initial Position - Frame Count * Velocity Y
            Double DiffY = InitPos.Y - ((Double)LifeTime * YVelocity);

            Position = new Point((Int32)DiffX, (Int32)DiffY);
        }

        public void Draw(Graphics g, Random rand)
        {
            UpdatePosition();

            using (Pen pen = new Pen(Color.Orange, 1))
            using (Brush b = new SolidBrush(color))
            {
                // Draw center, then draw border
                g.DrawEllipse(pen, new Rectangle(Position, new Size(Diam, Diam)));
                g.FillEllipse(b, new Rectangle(Position, new Size(Diam, Diam)));
            }
        }
    }
}
