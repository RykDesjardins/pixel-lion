﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PixelLionLib
{
    [Serializable()]
    public class SpecialAttack : ISerializable
    {
        public String Name;
        public Int32 Radius;
        public Single DamageRatio;
        public Elemental Element;
        public AttackType AtkType;
        public TargetType Target;
        public pIcon AtkIcon;
        public Animation AnimTarget;
        public Animation AnimSelf;

        public SpecialAttack()
        {
            Name = "Nouvelle attaque";
            Radius = 1;
            DamageRatio = 1.00f;

            if (Element != null)
                Element = Element.Immunities[0];
            else
                Element = null;
           
            AtkType = AttackType.Melee;
            Target = TargetType.Attaquant;
            AnimTarget = null;
            AnimSelf = null;
        }

        public SpecialAttack(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString("Name");
            Radius = info.GetInt32("Radius");
            DamageRatio = (Single)info.GetValue("DamageRatio", typeof(Single));
            Element = (Elemental)info.GetValue("Element", typeof(Elemental));
            AtkType = (AttackType)info.GetValue("AtkType", typeof(AttackType));
            Target = (TargetType)info.GetValue("Target", typeof(TargetType));
            AtkIcon = (pIcon)info.GetValue("AtkIcon", typeof(pIcon));
            AnimTarget = (Animation)info.GetValue("AnimTarget", typeof(Animation));
            AnimSelf = (Animation)info.GetValue("AnimSelf", typeof(Animation));
        }

        public override String ToString()
        {
            return Name;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("Radius", Radius);
            info.AddValue("DamageRatio", DamageRatio);
            info.AddValue("Element", Element);
            info.AddValue("AtkType", AtkType);
            info.AddValue("Target", Target);
            info.AddValue("AtkIcon", AtkIcon);
            info.AddValue("AnimTarget", AnimTarget);
            info.AddValue("AnimSelf", AnimSelf);
        }
    }

    public enum AttackType
    {
        Melee, Range, Absolute, Screen
    }

    public enum TargetType
    {
        Attaquant, Adversaire
    }
}
