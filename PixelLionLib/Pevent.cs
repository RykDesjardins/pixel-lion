﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;


namespace PixelLionLib
{
    public enum PeventType
    {
        pEvent, Monster
    }

    public enum PeventTriggerType
    {
        Proximity, Step, Press, Distance, Auto
    }

    public enum PeventMoveType
    {
        None, Magnet, Away, Random, Custom
    }

    public enum PeventDirection
    {
        Down, Up, Right, Left, None, Random
    }

    [Serializable()]
    public class Pevent : ISerializable, ICloneable
    {
        public static readonly String NameMissingString = "[Sans nom]";

        public String Name { get; set; }
        public String Script { get; set; }

        public Point Position { get; set; }
        public Int32 Velocity { get; set; }
        public Int32 Sight { get; set; }

        public PeventDirection Direction { get; set; }
        public PeventTriggerType TriggerType { get; set; }
        public PeventMoveType MoveType { get; set; }
        public PeventType pType { get; set; }
        public PeventClass Class { get; set; }

        public List<Point> ArretMouvement { get; set; }

        public Boolean Through { get; set; }
        public Boolean Flying { get; set; }
        public Boolean Disposable { get; set; }

        public Sprite pSprite { get; set; }

        public Pevent()
        {

        }

        public static Pevent CreateNew()
        {
            return new Pevent()
            {
                ArretMouvement = new List<Point>(),
                Class = new PeventClass(),
                Position = new Point(0, 0),
                Direction = PeventDirection.None,
                Flying = false, 
                MoveType = PeventMoveType.None,
                Name = String.Empty,
                pType = PeventType.pEvent,
                Script = String.Empty,
                Sight = 0,
                Through = true,
                Disposable = false,
                TriggerType = PeventTriggerType.Step,
                Velocity = 0
            };
        }

        public Object Clone()
        {
            return new Pevent()
            {
                Name = this.Name,
                Script = this.Script,
                Position = this.Position,
                Velocity = this.Velocity,
                Sight = this.Sight,
                pType = this.pType,
                Direction = this.Direction,
                TriggerType = this.TriggerType,
                MoveType = this.MoveType,
                Class = this.Class,
                ArretMouvement = this.ArretMouvement,
                Through = this.Through,
                Disposable = this.Disposable,
                Flying = this.Flying,
                pSprite = this.pSprite
            };
        }


        public Pevent(SerializationInfo info, StreamingContext context)
        {
            Name = (String)info.GetValue("Name", typeof(String));
            Script = (String)info.GetValue("Script", typeof(String));
            Position = (Point)info.GetValue("Position", typeof(Point));
            Velocity = (Int32)info.GetValue("Velocity", typeof(Int32));
            Sight = (Int32)info.GetValue("Sight", typeof(Int32));
            pType = (PeventType)info.GetValue("pType", typeof(PeventType));
            Direction = (PeventDirection)info.GetValue("Direction", typeof(PeventDirection));
            TriggerType = (PeventTriggerType)info.GetValue("TriggerType", typeof(PeventTriggerType));
            MoveType = (PeventMoveType)info.GetValue("MoveType", typeof(PeventMoveType));
            Class = (PeventClass)info.GetValue("Class", typeof(PeventClass));
            ArretMouvement = (List<Point>)info.GetValue("ArretMouvement", typeof(List<Point>));
            Through = (Boolean)info.GetValue("Through", typeof(Boolean));
            Flying = (Boolean)info.GetValue("Flying", typeof(Boolean));
            pSprite = (Sprite)info.GetValue("Graphics", typeof(Sprite));
            Disposable = info.GetBoolean("Disposable");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("Script", Script);
            info.AddValue("Position", Position);
            info.AddValue("Velocity", Velocity);
            info.AddValue("Sight", Sight);
            info.AddValue("Direction", Direction);
            info.AddValue("TriggerType", TriggerType);
            info.AddValue("MoveType", MoveType);
            info.AddValue("Class", Class);
            info.AddValue("ArretMouvement", ArretMouvement);
            info.AddValue("Through", Through);
            info.AddValue("Flying", Flying);
            info.AddValue("Graphics", pSprite);
            info.AddValue("Disposable", Disposable);
            info.AddValue("pType", pType);
        }
    }
}
