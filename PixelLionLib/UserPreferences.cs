﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace PixelLionLib
{
    [Serializable()]
    public class UserPreferences : ISerializable
    {
        public const String DefaultFileName = "UserPrefs.plpref";

        public VideoPreferences Video;
        public AnimationStudioPreferences AnimationStudio;
        public AudioPreferences Audio;
        public MapPreferences Map;
        public TileSetPreferences TileSet;
        public MemoryPreferences Memory;
        public TabsPreferences Tabs;

        public UserPreferences()
        {
            Video = new VideoPreferences();
            AnimationStudio = new AnimationStudioPreferences();
            Audio = new AudioPreferences();
            Map = new MapPreferences();
            TileSet = new TileSetPreferences();
            Memory = new MemoryPreferences();
            Tabs = new TabsPreferences();
        }

        public UserPreferences(UserPreferences original)
        {
            Video = new VideoPreferences()
            {
                DoubleBuffering = original.Video.DoubleBuffering,
                ShowPopups = original.Video.ShowPopups,
                ShowSplashScreen = original.Video.ShowSplashScreen,
                ShowBubbleMainScreen = original.Video.ShowBubbleMainScreen
            };

            AnimationStudio = new AnimationStudioPreferences()
            {
                BackgroundColor = original.AnimationStudio.BackgroundColor,
                RulerSize = original.AnimationStudio.RulerSize,
                ShowSplashScreen = original.AnimationStudio.ShowSplashScreen,
                UseXna = original.AnimationStudio.UseXna
            };

            Audio = new AudioPreferences()
            {
                LoopSongs = original.Audio.LoopSongs,
                PlaySound = original.Audio.PlaySound,
                SoundPath = original.Audio.SoundPath
            };

            Map = new MapPreferences()
            {
                GridColor = original.Map.GridColor,
                GridThickness = original.Map.GridThickness,
                TileSize = original.Map.TileSize
            };

            TileSet = new TileSetPreferences()
            {
                BackgroundColor = original.TileSet.BackgroundColor,
                TilesPerRow = original.TileSet.TilesPerRow
            };

            Memory = new MemoryPreferences()
            {
                HistoryCount = original.Memory.HistoryCount,
                Hotkeys = original.Memory.Hotkeys,
                PreloadMaps = original.Memory.PreloadMaps,
                QuickMaps = original.Memory.QuickMaps
            };
            
            Tabs = new TabsPreferences()
            {
                FirstColorGradiant = original.Tabs.FirstColorGradiant,
                SecondColorGradiant = original.Tabs.SecondColorGradiant
            };
        }

        public UserPreferences(SerializationInfo info, StreamingContext context)
        {
            Video = new VideoPreferences()
            {
                DoubleBuffering = info.GetBoolean("VideoDoubleBuffering"),
                ShowPopups = info.GetBoolean("VideoShowPopups"),
                ShowSplashScreen = info.GetBoolean("VideoSplashScreen"),
                ShowBubbleMainScreen = info.GetBoolean("VideoShowBubbleMainScreen")
            };

            AnimationStudio = new AnimationStudioPreferences()
            {
                BackgroundColor = (Color)info.GetValue("AnimationStudioBackgroundColor", typeof(Color)),
                RulerSize = info.GetInt32("AnimationStudioRulerSize"),
                ShowSplashScreen = info.GetBoolean("AnimationStudioShowSplashScreen"),
                UseXna = info.GetBoolean("AnimationStudioUseXna")
            };

            Audio = new AudioPreferences()
            {
                LoopSongs = info.GetBoolean("AudioLoopSongs"),
                PlaySound = info.GetBoolean("AudioPlaySound"),
                SoundPath = info.GetString("AudioSoundPath")
            };

            Map = new MapPreferences()
            {
                GridColor = (Color)info.GetValue("MapGridColor", typeof(Color)),
                GridThickness = info.GetInt32("MapGridThickness"),
                TileSize = (Size)info.GetValue("MapTileSize", typeof(Size))
            };

            TileSet = new TileSetPreferences()
            {
                BackgroundColor = (Color)info.GetValue("TileSetBackgroundColor", typeof(Color)),
                TilesPerRow = info.GetInt32("TileSetTilesPerRow")
            };

            Memory = new MemoryPreferences()
            {
                HistoryCount = info.GetInt32("MemoryHistoryCount"),
                Hotkeys = info.GetBoolean("MemoryHotkeys"),
                PreloadMaps = info.GetBoolean("MemoryPreloadMaps"),
                QuickMaps = info.GetBoolean("MemoryQuickMaps")
            };

            Tabs = new TabsPreferences()
            {
                FirstColorGradiant = (Color)info.GetValue("FirstColorGradiant", typeof(Color)),
                SecondColorGradiant = (Color)info.GetValue("SecondColorGradiant", typeof(Color))
            };
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            // Video
            info.AddValue("VideoDoubleBuffering", Video.DoubleBuffering);
            info.AddValue("VideoShowPopups", Video.ShowPopups);
            info.AddValue("VideoSplashScreen", Video.ShowSplashScreen);
            info.AddValue("VideoShowBubbleMainScreen", Video.ShowBubbleMainScreen);

            // Animation
            info.AddValue("AnimationStudioBackgroundColor", AnimationStudio.BackgroundColor);
            info.AddValue("AnimationStudioRulerSize", AnimationStudio.RulerSize);
            info.AddValue("AnimationStudioShowSplashScreen", AnimationStudio.ShowSplashScreen);
            info.AddValue("AnimationStudioUseXna", AnimationStudio.UseXna);

            // Audio
            info.AddValue("AudioLoopSongs", Audio.LoopSongs);
            info.AddValue("AudioPlaySound", Audio.PlaySound);
            info.AddValue("AudioSoundPath", Audio.SoundPath);

            // Map
            info.AddValue("MapGridColor", Map.GridColor);
            info.AddValue("MapGridThickness", Map.GridThickness);
            info.AddValue("MapTileSize", Map.TileSize);

            // Tileset
            info.AddValue("TileSetBackgroundColor", TileSet.BackgroundColor);
            info.AddValue("TileSetTilesPerRow", TileSet.TilesPerRow);

            // Memory
            info.AddValue("MemoryHistoryCount", Memory.HistoryCount);
            info.AddValue("MemoryHotkeys", Memory.Hotkeys);
            info.AddValue("MemoryPreloadMaps", Memory.PreloadMaps);
            info.AddValue("MemoryQuickMaps", Memory.QuickMaps);

            // Tabs
            info.AddValue("FirstColorGradiant", Tabs.FirstColorGradiant);
            info.AddValue("SecondColorGradiant", Tabs.SecondColorGradiant);
        }

        public void SaveToFile(String path)
        {
            Stream stream = File.Open(path, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, this);
            stream.Close();
        }

        public static UserPreferences LoadFromFile(String path)
        {
            if (!File.Exists(path))
            {
                UserPreferences prefs = new UserPreferences();
                prefs.SaveToFile(DefaultFileName);

                return prefs;
            }
            else
            {
                UserPreferences objectToSerialize;

                try
                {
                    Stream stream = File.Open(path, FileMode.Open);
                    BinaryFormatter bFormatter = new BinaryFormatter();
                    objectToSerialize = (UserPreferences)bFormatter.Deserialize(stream);
                    stream.Close();
                }
                catch (Exception)
                {
                    objectToSerialize = new UserPreferences();
                }

                return objectToSerialize;
            }
        }

        public class VideoPreferences
        {
            public Boolean DoubleBuffering = true;
            public Boolean ShowPopups = true;
            public Boolean ShowSplashScreen = true;
            public Boolean ShowBubbleMainScreen = false;
        }

        public class AnimationStudioPreferences
        {
            public Boolean UseXna = true;
            public Boolean ShowSplashScreen = true;
            public Color BackgroundColor = Color.Black;
            public Int32 RulerSize = 20;
        }

        public class AudioPreferences
        {
            public Boolean PlaySound = false;
            public String SoundPath = String.Empty;
            public Boolean LoopSongs = false;
        }

        public class MapPreferences
        {
            public Int32 GridThickness = 1;
            public Color GridColor = Color.Black;
            public Size TileSize = new Size(32, 32);
        }

        public class TileSetPreferences
        {
            public Int32 TilesPerRow = 8;
            public Color BackgroundColor = Color.Azure;
        }

        public class MemoryPreferences
        {
            public Boolean PreloadMaps = false;
            public Boolean Hotkeys = true;
            public Boolean QuickMaps = true;
            public Int32 HistoryCount = 10;
        }

        public class TabsPreferences
        {
            public Color FirstColorGradiant = Color.FromArgb(255, 80, 180, 230);
            public Color SecondColorGradiant = Color.FromArgb(255, 120, 220, 255);
        }
    }

    public static class GlobalPreferences
    {
        public static UserPreferences Prefs;
    }
}
