﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VBMath;

namespace PixelLionLib
{
    public class PixelMath
    {
        Single xVal;
        mcCalc Calculator;
        String function;
        Boolean ContainsX;

        public PixelMath(String function)
        {
            Calculator = new mcCalc();
            this.function = function;

            ContainsX = function.Contains('x');
        }

        public Single GetNextValue()
        {
            return ContainsX ? YAt(++xVal) : Evaluate();
        }

        public Single YAt(Single x)
        {
            return ContainsX ? (Single)Calculator.evaluate(function.Replace("x", x.ToString())) : Evaluate();
        }

        public Single Evaluate()
        {
            return (Single)Calculator.evaluate(function);
        }
    }
}
