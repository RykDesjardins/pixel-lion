﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PixelLionLib
{
    public static class Extensions
    {
        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (T item in source)
            {
                action(item);
                yield return item;
            }
        }

        public static void EncapsulateLoop<T>(this IEnumerable<T> source, IEnumerable<T> left, Action<T> action)
        {
            if (source.Count() != left.Count()) throw new ArgumentOutOfRangeException("Two enumerables have not the same number of arguments");

            foreach (T item in source) action(item);
            foreach (T item in left) action(item);
        }

        public static void EncapsulateLoop<L, R>(this IEnumerable<L> left, IEnumerable<R> right, Action<L, R> action)
        {
            L[] lefts = left.ToArray();
            R[] rights = right.ToArray();

            Int32 Max = left.Count() > right.Count() ? right.Count() : left.Count();

            for (int i = 0; i < Max; i++) action(lefts[i], rights[i]);
        }


        public static void EncapsulateLoop<L, R>(this IEnumerable<L> left, IEnumerable<R> right, Action action)
        {
            L[] lefts = left.ToArray();
            R[] rights = right.ToArray();

            Int32 Max = left.Count() > right.Count() ? right.Count() : left.Count();

            for (int i = 0; i < Max; i++) action();
        }

        public static void EncapsulateLoop<T, H>(this IEnumerable<T> source, IEnumerable<H> left, Action<T> actionSource, Action<H> actionLeft)
        {
            foreach (T item in source) actionSource(item);
            foreach (H item in left) actionLeft(item);
        }

        public static IEnumerable<T> ExceptNulls<T>(this IEnumerable<T> source)
        {
            // foreach (T item in source) if (item != null) yield return item;
            return source.Where(x => x != null);
        }

        public static void TimesDo(this Int32 num, Action action)
        {
            for (int i = 0; i < num; i++) action();
        }

        public static void TimesDo(this Int32 num, Delegate del, Object[] paramarr)
        {
            for (int i = 0; i < num; i++) del.DynamicInvoke(paramarr);
        }

        public static void AppendInTextBox<T>(this IEnumerable<T> source, System.Windows.Forms.TextBoxBase tb, String separator)
        {
            source.ForEach(item => tb.AppendText(item.ToString() + separator));
            if (separator != String.Empty) tb.Text.Remove(tb.Text.Length - separator.Length);
        }

        public static String MergeStrings(this IEnumerable<String> source, String separator = "")
        {
            String str = String.Empty;
            source.ForEach(x => { str += x + separator; });
            str.Remove(str.Length - separator.Length);

            return str;
        }

        public static System.Collections.ArrayList ToArrayList<T>(this IEnumerable<T> source)
        {
            System.Collections.ArrayList al = new System.Collections.ArrayList();
            source.ForEach(x => al.Add(x));
            return al;
        }

        public static System.Collections.Hashtable CreateHashTableWithValues<T>(this IEnumerable<T> source, IEnumerable<Object> keys)
        {
            System.Collections.Hashtable table = new System.Collections.Hashtable();
            var hashvals = source.Zip(keys, (x, y) => new { Value = x, Key = y });
            foreach (var val in hashvals) table.Add(val.Key, val.Value);

            return table;
        }

        public static System.Collections.Hashtable CreateHashTableWithKeys<T>(this IEnumerable<T> source, IEnumerable<Object> values)
        {
            System.Collections.Hashtable table = new System.Collections.Hashtable();
            var hashvals = source.Zip(values, (x, y) => new { Key = x, Value = y });
            foreach (var val in hashvals) table.Add(val.Key, val.Value);

            return table;
        }

        public static void AppendInTextBox(this String source, System.Windows.Forms.TextBoxBase tb)
        {
            tb.AppendText(source);
        }

        public static void SetInLabel(this String source, System.Windows.Forms.Label lb)
        {
            lb.Text = source;
        }

        public static Boolean IsInside(this System.Drawing.Rectangle source, System.Drawing.Point point)
        {
            return point.X >= source.X && point.X <= source.X + source.Width && point.Y >= source.Y && point.Y <= source.Y + source.Height;
        }

        public static IEnumerable<T> GetEnumerable<T>(this System.Windows.Forms.ListBox source)
        {
            foreach (T item in source.Items)
            {
                yield return item;
            }
        }

        public static IEnumerable<T> ToEnumerable<T>(this System.Windows.Forms.ListBox source)
        {
            foreach (T i in source.Items) yield return i;
        }

        public static IEnumerable<T> ForEach<T>(this System.Windows.Forms.ListBox source, Action<T> action)
        {
            foreach (T item in source.Items)
            {
                action(item);
                yield return item;
            }
        }

        public static Boolean IsNearBlack(this Microsoft.Xna.Framework.Color color, Int32 Range)
        {
            return new Boolean[] { color.R < Range, color.G < Range, color.B < Range }.Count() >= 3;
        }

        public static Boolean IsNearWhite(this Microsoft.Xna.Framework.Color color, Int32 Range)
        {
            return new Boolean[] { 255 - Range > color.R, 255 - Range > color.G, 255 - Range > color.B }.Count() >= 3;
        }

        public static Boolean IsNearBlack(this System.Drawing.Color color, Int32 Range)
        {
            return new Boolean[] { color.R < Range, color.G < Range, color.B < Range }.Count() >= 3;
        }

        public static Boolean IsNearWhite(this System.Drawing.Color color, Int32 Range)
        {
            return new Boolean[] { 255 - Range > color.R, 255 - Range > color.G, 255 - Range > color.B }.Count() >= 3;
        }

        public static Microsoft.Xna.Framework.Color ToXnaColor(this System.Drawing.Color o)
        {
            return Microsoft.Xna.Framework.Color.FromNonPremultiplied(o.R, o.G, o.B, o.A);
        }
    }
}
