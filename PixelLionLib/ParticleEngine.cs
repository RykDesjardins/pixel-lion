﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using PixelLionLib;

namespace PixelLionLib
{
    public class ParticleEngine : IDisposable
    {
        List<Particle> Particles;
        Rectangle ClipTarget;
        Random RandomFeed;
        List<Texture2D> Textures;

        public ParticleEngine()
        {
            InitializeObjects();
        }

        public ParticleEngine(Rectangle target)
        {
            InitializeObjects();
            SetTarget(target);
        }

        private void InitializeObjects()
        {
            Particles = new List<Particle>();
            RandomFeed = new Random();
            Textures = new List<Texture2D>();
        }

        public void UpdateParticles()
        {
            Particles.RemoveAll(x => x.Dead());
            Particles.ForEach(x => x.Update());
        }

        public void DrawParticles(SpriteBatch batch)
        {
            batch.Begin();
            Particles.ForEach(x => x.Draw(batch, null));
            batch.End();
        }

        public void SetTarget(Rectangle target)
        {
            ClipTarget = target;
        }

        public void AddParticules(Int32 Number, ParticleBehaviour Behaviour, ParticleShape Shape, Vector2 PositionModifier)
        {
            Number.TimesDo(() => Particles.Add(Particle.CreateParticle(Behaviour, Shape, ClipTarget, PositionModifier, RandomFeed)));
        }

        public void Dispose()
        {
            Textures.ForEach(x => x.Dispose());
            Textures.Clear();
            Particles.Clear();
        }
    }

    public class Particle
    {
        // Appearence
        ParticleShape Shape;
        Single CurrentSize;
        Int32 Opacity;
        Color BackColor;

        // Movement
        Vector2 Position;
        Single VelocityX;
        Single VelocityY;
        PixelMath PMathX;
        PixelMath PMathY;

        // Behaviour
        Int32 Lifespan;
        Single SizeRatioOverTime;

        // Fade In / Out
        Boolean FadeOut;
        Boolean FadeIn;
        Int32 FadeOutStartAt;
        Int32 FadeInFrameCount;

        // Particle Life variables
        public Int32 FrameCount;

        private Particle()
        {
            FrameCount = 0;
        }

        public void Update()
        {
            FrameCount++;
            CurrentSize *= SizeRatioOverTime;
            Position.X += PMathX.YAt(FrameCount) * VelocityX;
            Position.Y += PMathY.YAt(FrameCount) * VelocityY;

            if (FadeIn && FadeInFrameCount > FrameCount)
                Opacity = (Int32)(255f * (Single)FrameCount / (Single)FadeInFrameCount);
            else if (FadeOut && FadeOutStartAt < FrameCount)
                Opacity = (Int32)(255f - (255f / (Single)(Lifespan - FadeOutStartAt) * (Single)(FrameCount - FadeOutStartAt)));
        }

        public void SetAbsolutePosition(Vector2 pos)
        {
            Position = pos;
        }

        public Boolean Dead()
        {
            return FrameCount >= Lifespan;
        }

        public void SetMathAlgo(String xAlgo, String yAlgo)
        {
            PMathX = new PixelMath(xAlgo);
            PMathY = new PixelMath(yAlgo);
        }

        public void Draw(SpriteBatch batch, Texture2D Graphic)
        {
            if (Opacity <= 0) return;

            switch (Shape)
            {
                case ParticleShape.Circle:
                    batch.DrawCircle(Position, CurrentSize, 20, Color.FromNonPremultiplied(BackColor.R, BackColor.G, BackColor.B, Opacity), CurrentSize);
                    break;

                case ParticleShape.Square:
                    batch.DrawRectangle(new Rectangle((Int32)Position.X, (Int32)Position.Y, (Int32)CurrentSize, (Int32)CurrentSize), Color.FromNonPremultiplied(BackColor.R, BackColor.G, BackColor.B, Opacity), CurrentSize);
                    break;

                case ParticleShape.Texture:
                    batch.Draw(Graphic, Position, Graphic.Bounds, Color.FromNonPremultiplied(255, 255, 255, Opacity));
                    break;

                default:
                    break;
            }
        }

        public static Particle CreateParticle(ParticleBehaviour Behaviour, ParticleShape Shape, Rectangle Clip, Vector2 PositionModifier, Random RandomFeed, Int32 Opacity, Int32 Lifespan, Single SizeRatio, Vector2 VelocityModifier)
        {
            Particle p = new Particle();
            p.Shape = Shape;

            switch (Behaviour)
            {
                case ParticleBehaviour.Explode:
                    {
                        p.Opacity = Opacity;
                        p.BackColor = Color.FromNonPremultiplied(RandomFeed.Next(255), 0, 0, 255);
                        p.Position = new Vector2((Single)Clip.Width / 2f + PositionModifier.X, (Single)Clip.Height / 2f + PositionModifier.Y);
                        p.Lifespan = Lifespan;
                        p.SizeRatioOverTime = SizeRatio;
                        p.VelocityX = ((Single)RandomFeed.NextDouble() - 0.5f) * ((Single)RandomFeed.NextDouble() * VelocityModifier.X);
                        p.VelocityY = ((Single)RandomFeed.NextDouble() - 0.5f) * ((Single)RandomFeed.NextDouble() * VelocityModifier.Y);

                        p.FadeOut = true;
                        p.FadeOutStartAt = 40;

                        p.FadeIn = false;
                        p.FadeInFrameCount = 0;
                        p.CurrentSize = RandomFeed.Next(5) + 5;

                        p.PMathX = new PixelMath("x * 2");
                        p.PMathY = new PixelMath("x * 2");
                    }
                    break;

                case ParticleBehaviour.Focus:
                    {
                        p.Opacity = Opacity;
                        p.BackColor = Color.FromNonPremultiplied(RandomFeed.Next(255), 0, 0, 255);
                        p.Position = new Vector2(RandomFeed.Next(Clip.Width), RandomFeed.Next(Clip.Height));
                        p.Lifespan = Lifespan;
                        p.SizeRatioOverTime = SizeRatio;
                        p.VelocityX = (Single)RandomFeed.NextDouble();
                        p.VelocityY = (Single)RandomFeed.NextDouble();

                        p.FadeOut = true;
                        p.FadeOutStartAt = 40;

                        p.FadeIn = true;
                        p.FadeInFrameCount = 20;
                        p.CurrentSize = RandomFeed.Next(5) + 5;

                        Single xVariation = ((Single)Clip.Width / 2f - p.Position.X) / (Single)p.Lifespan;
                        Single yVariation = ((Single)Clip.Height / 2f - p.Position.Y) / (Single)p.Lifespan;
                        p.PMathX = new PixelMath(xVariation.ToString());
                        p.PMathY = new PixelMath(yVariation.ToString());
                    }
                    break;

                case ParticleBehaviour.Trail:
                    {
                        p.Opacity = Opacity;
                        p.BackColor = Color.FromNonPremultiplied(RandomFeed.Next(255), 0, 0, 255);
                        p.Position = new Vector2((Single)Clip.Width / 2f + PositionModifier.X, (Single)Clip.Height / 2f + PositionModifier.Y);
                        p.Lifespan = Lifespan;
                        p.SizeRatioOverTime = SizeRatio;
                        p.VelocityX = ((Single)RandomFeed.NextDouble() - 0.5f) * ((Single)RandomFeed.NextDouble() * VelocityModifier.X);
                        p.VelocityY = ((Single)RandomFeed.NextDouble() - 0.5f) * ((Single)RandomFeed.NextDouble() * VelocityModifier.Y);

                        p.FadeOut = true;
                        p.FadeOutStartAt = 40;

                        p.FadeIn = false;
                        p.FadeInFrameCount = 0;
                        p.CurrentSize = RandomFeed.Next(5) + 2;

                        p.PMathX = new PixelMath("x");
                        p.PMathY = new PixelMath("x");
                    }
                    break;
            }



            return p;
        }

        public static Particle CreateParticle(ParticleBehaviour Behaviour, ParticleShape Shape, Rectangle Clip, Vector2 PositionModifier, Random RandomFeed)
        {
            return Particle.CreateParticle(Behaviour, Shape, Clip, PositionModifier, RandomFeed, 255, 60, 0.99f, new Vector2(3f, 3f));
        }

        public static Particle CreateParticle(ParticleBehaviour Behaviour, Rectangle Clip, Random RandomFeed)
        {
            return Particle.CreateParticle(Behaviour, ParticleShape.Circle, Clip, new Vector2(0, 0), RandomFeed, 255, 60, 0.99f, new Vector2(3f, 3f));
        }
    }

    public enum ParticleBehaviour
    {
        Explode, Focus, Trail, Inward, Upward
    }

    public enum ParticleShape
    {
        Circle, Square, Texture
    }
}
