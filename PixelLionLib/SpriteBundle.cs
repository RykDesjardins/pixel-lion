﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class SpriteBundle : ISerializable
    {
        public List<Sprite> Sprites { get; set; }
        public SpriteBundle()
        {
            Sprites = new List<Sprite>();
        }

        public SpriteBundle(SerializationInfo info, StreamingContext context)
        {
            Sprites = (List<Sprite>)info.GetValue("Sprite", typeof(List<Sprite>));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Sprite", Sprites);
        }
    }
}
