﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class CharactStats : ISerializable
    {
        public Int32 HP;
        public Int32 MP;
        public Int32 STR;
        public Int32 DEF;
        public Int32 INT;
        public Int32 RES;
        public Elemental PrimaryElement;
        public Elemental WeaknessElement;
        public Int32 EXP;
        public Int32 MONEY;

        public CharactStats()
        {

        }

        public CharactStats(SerializationInfo info, StreamingContext context)
        {
            HP = (Int32)info.GetValue("HP", typeof(Int32));
            MP = (Int32)info.GetValue("MP", typeof(Int32));
            STR = (Int32)info.GetValue("STR", typeof(Int32));
            DEF = (Int32)info.GetValue("DEF", typeof(Int32));
            INT = info.GetInt32("INT");
            RES = info.GetInt32("RES");
            PrimaryElement = (Elemental)info.GetValue("PrimaryElement", typeof(Elemental));
            WeaknessElement = (Elemental)info.GetValue("WeaknessElement", typeof(Elemental));
            EXP = (Int32)info.GetValue("EXP", typeof(Int32));
            MONEY = (Int32)info.GetValue("MONEY", typeof(Int32));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("HP", HP);
            info.AddValue("MP", MP);
            info.AddValue("STR", STR);
            info.AddValue("DEF", DEF);
            info.AddValue("INT", INT);
            info.AddValue("RES", RES);
            info.AddValue("PrimaryElement", PrimaryElement);
            info.AddValue("WaeknessElement", WeaknessElement);
            info.AddValue("EXP", EXP);
            info.AddValue("MONEY", MONEY);
        }
    }
}
