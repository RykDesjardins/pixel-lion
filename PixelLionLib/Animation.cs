﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class Animation : ISerializable, ICloneable, IDisposable
    {
        List<AnimationFrame> Frames;
        AnimationPattern Pattern;
        public String Name;

        public Animation()
        {
            Frames = new AnimationFrame[20].Select(x => x = new AnimationFrame()).ToList();

            Name = "Nouvelle animation";
        }

        public Animation(SerializationInfo info, StreamingContext context)
        {
            Frames = info.GetValue("Frames", typeof(List<AnimationFrame>)) as List<AnimationFrame>;
            Pattern = info.GetValue("Pattern", typeof(AnimationPattern)) as AnimationPattern;
            Name = info.GetString("Name");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Frames", Frames);
            info.AddValue("Pattern", Pattern);
            info.AddValue("Name", Name);
        }

        public Int32 GetFrameCount()
        {
            return Frames.Count();
        }

        public override String ToString()
        {
            return Name;
        }

        public void AddFrame()
        {
            if (Frames.Count() < 100) Frames.Add(new AnimationFrame());
        }

        public void AddFrameAt(Int32 Index)
        {
            if (Frames.Count() < 100) Frames.Insert(Index, new AnimationFrame());
        }

        public void RemoveFrame()
        {
            if (Frames.Count() != 1) Frames.RemoveAt(Frames.Count() - 1);
        }

        public void AlterFrameAt(Int32 index, AnimationFrame frame)
        {
            Frames[index] = frame;
        }

        public void RemoveFrameAt(Int32 index)
        {
            Frames.RemoveAt(index);
        }

        public AnimationFrame FrameAt(Int32 index)
        {
            return Frames[index];
        }

        public void SetPattern(Image img)
        {
            Pattern = new AnimationPattern();

            // TODO : Change default value into parameters
            Pattern.SetPattern(img, new Size(192, 192));
        }

        public AnimationPattern GetPattern()
        {
            return Pattern;
        }

        public Image CropPatternByIndex(Int32 Index)
        {
            return Pattern == null ? null : Pattern.CropPatternByIndex(Index);
        }

        public Image[] CropPattern()
        {
            return Pattern.CropPattern().ToArray();
        }

        public object Clone()
        {
            Animation anim = new Animation();
            anim.Name = this.Name;
            anim.Frames = this.Frames.Select(x => x.Clone() as AnimationFrame).ToList();
            if (this.Pattern != null) anim.Pattern = this.Pattern.Clone() as AnimationPattern;

            return anim;
        }

        public void Dispose()
        {
            if (Pattern != null) Pattern.Dispose();
            foreach (AnimationFrame frame in Frames) frame.Dispose();
        }
    }

    [Serializable()]
    public class AnimationPattern : ISerializable, ICloneable, IDisposable
    {
        Image Pattern;
        Size ImageSize;
        Size FrameSize;
        Int32 FrameCount;
        Int32 FramesPerLine;

        public AnimationPattern()
        {

        }
        public AnimationPattern(SerializationInfo info, StreamingContext context)
        {
            Pattern = (Image)info.GetValue("Pattern", typeof(Image));
            ImageSize = (Size)info.GetValue("ImageSize", typeof(Size));
            FrameSize = (Size)info.GetValue("FrameSize", typeof(Size));
            FrameCount = (Int32)info.GetValue("FrameCount", typeof(Int32));
            FramesPerLine = (Int32)info.GetValue("FramesPerLine", typeof(Int32));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Pattern", Pattern);
            info.AddValue("ImageSize", ImageSize);
            info.AddValue("FrameSize", FrameSize);
            info.AddValue("FrameCount", FrameCount);
            info.AddValue("FramesPerLine", FramesPerLine);
        }

        public void SetPattern(Image img, Size FrameSize)
        {
            if (this.Pattern != null) Pattern.Dispose();
            this.Pattern = img;
            this.ImageSize = img.Size;
            this.FrameSize = FrameSize;

            FramesPerLine = ImageSize.Width / FrameSize.Width;
            FrameCount = FramesPerLine * (ImageSize.Height / FrameSize.Height);
        }

        public Image GetImage()
        {
            return Pattern;
        }

        public Size GetFrameSize()
        {
            return FrameSize;
        }

        public Int32 GetFrameCount()
        {
            return FrameCount;
        }

        public Image CropPatternByIndex(Int32 Index)
        {
            Int32 XPoint = Index % FramesPerLine * FrameSize.Width;
            Int32 YPoint = Index / FramesPerLine * FrameSize.Height;

            return (Pattern as Bitmap).Clone(new Rectangle(new Point(XPoint, YPoint), FrameSize), Pattern.PixelFormat) as Image;
        }

        public IEnumerable<Image> CropPattern()
        {
            for (int i = 0; i < FrameCount; i++) yield return CropPatternByIndex(i);
        }

        public object Clone()
        {
            AnimationPattern ap = new AnimationPattern();
            ap.FrameCount = this.FrameCount;
            ap.FrameSize = this.FrameSize;
            ap.FramesPerLine = this.FramesPerLine;
            ap.ImageSize = this.ImageSize;
            ap.Pattern = this.Pattern;

            return ap;
        }

        public void Dispose()
        {
            if (Pattern != null) Pattern.Dispose();
        }
    }

    [Serializable()]
    public class AnimationFrame : ISerializable, ICloneable, IDisposable
    {
        public AnimationParticules[] Particules;
        public List<AnimationPatternFrame> Images;
        public AnimationPixels[] Pixels;
        public Color Overlay;
        public Color Upperlay;
        public Sound BGS;

        public AnimationFrame()
        {
            Images = new List<AnimationPatternFrame>();
            Particules = new AnimationParticules[0];
            Pixels = new AnimationPixels[0];
            Upperlay = Color.White;
            Overlay = Color.Transparent;
        }

        public AnimationFrame(SerializationInfo info, StreamingContext context)
        {
            Particules = info.GetValue("Particules", typeof(AnimationParticules[])) as AnimationParticules[];
            Images = info.GetValue("Images", typeof(List<AnimationPatternFrame>)) as List<AnimationPatternFrame>;
            Pixels = info.GetValue("Pixels", typeof(AnimationPixels[])) as AnimationPixels[];
            Overlay = (Color)info.GetValue("Overlay", typeof(Color));
            Upperlay = (Color)info.GetValue("Upperlay", typeof(Color));
            BGS = info.GetValue("BGS", typeof(Sound)) as Sound;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Particules", Particules);
            info.AddValue("Images", Images);
            info.AddValue("Pixels", Pixels);
            info.AddValue("Overlay", Overlay);
            info.AddValue("Upperlay", Upperlay);
            info.AddValue("BGS", BGS);
        }

        public object Clone()
        {
            AnimationFrame frame = new AnimationFrame();
            frame.BGS = this.BGS;
            frame.Images = this.Images.Select(x => x.Clone() as AnimationPatternFrame).ToList();
            frame.Overlay = this.Overlay;
            frame.Upperlay = this.Upperlay;
            frame.Particules = this.Particules.Select(x => x.Clone() as AnimationParticules).ToArray();
            frame.Pixels = new AnimationPixels[0];

            return frame;
        }

        public void Dispose()
        {
            foreach (AnimationParticules pt in Particules) if (pt != null) pt.Dispose();
        }
    }

    [Serializable()]
    public class AnimationParticules : ISerializable, ICloneable, IDisposable
    {
        public Image texture;
        public Point Position;
        public Single SizeRation;
        public Single Rotation;

        public AnimationParticules() { }
        public AnimationParticules(SerializationInfo info, StreamingContext context)
        {
            texture = info.GetValue("texture", typeof(Image)) as Image;
            Position = (Point)info.GetValue("Position", typeof(Point));
            SizeRation = (Single)info.GetValue("SizeRation", typeof(Single));
            Rotation = (Single)info.GetValue("Rotation", typeof(Single));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("texture", texture);
            info.AddValue("Position", Position);
            info.AddValue("SizeRation", SizeRation);
            info.AddValue("Rotation", Rotation);
        }

        public object Clone()
        {
            AnimationParticules ap = new AnimationParticules();
            ap.texture = this.texture;
            ap.SizeRation = this.SizeRation;
            ap.Rotation = this.Rotation;
            ap.Position = this.Position;

            return ap;
        }

        public void Dispose()
        {
            if (texture != null) texture.Dispose();
        }
    }

    [Serializable()]
    public class AnimationPixels : ISerializable
    {
        public Point LineStart;
        public Point LineEnd;
        public Color color;

        public AnimationPixels() { }
        public AnimationPixels(SerializationInfo info, StreamingContext context)
        {
            LineStart = (Point)info.GetValue("LineStart", typeof(Point));
            LineEnd = (Point)info.GetValue("LineEnd", typeof(Point));
            color = (Color)info.GetValue("color", typeof(Color));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("LineStart", LineStart);
            info.AddValue("LineEnd", LineEnd);
            info.AddValue("color", color);
        }
    }

    [Serializable()]
    public class AnimationPatternFrame : ISerializable, ICloneable
    {
        public Int32 ImageIndex;
        public Single Ratio;
        public Single Rotation;
        public Point Position;
        public Int32 Blending;
        public Single Opacity;
        public Boolean Fliped;
        public Single[] RGBratio;
        public Single[] RGBadd;

        public AnimationPatternFrame() 
        {
            RGBadd = new Single[] { 0, 0, 0 };
            RGBratio = new Single[] { 1, 1, 1 };
            Opacity = 1f;
        }
        public AnimationPatternFrame(SerializationInfo info, StreamingContext context)
        {
            ImageIndex = (Int32)info.GetValue("ImageIndex", typeof(Int32));
            Ratio = (Single)info.GetValue("Ratio", typeof(Single));
            Rotation = (Single)info.GetValue("Rotation", typeof(Single));
            Position = (Point)info.GetValue("Position", typeof(Point));
            Blending = (Int32)info.GetValue("Blending", typeof(Int32));
            Fliped = (Boolean)info.GetValue("Fliped", typeof(Boolean));
            Opacity = info.GetSingle("Opacity");
            RGBratio = (Single[])info.GetValue("RGBflip", typeof(Single[]));
            RGBadd = (Single[])info.GetValue("RGBadd", typeof(Single[]));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("ImageIndex", ImageIndex);
            info.AddValue("Ratio", Ratio);
            info.AddValue("Rotation", Rotation);
            info.AddValue("Position", Position);
            info.AddValue("Blending", Blending);
            info.AddValue("Fliped", Fliped);
            info.AddValue("Opacity", Opacity);
            info.AddValue("RGBflip", RGBratio);
            info.AddValue("RGBadd", RGBadd);
        }

        public object Clone()
        {
            AnimationPatternFrame apf = new AnimationPatternFrame();
            apf.Blending = this.Blending;
            apf.Fliped = this.Fliped;
            apf.ImageIndex = this.ImageIndex;
            apf.Position = this.Position;
            apf.Ratio = this.Ratio;
            apf.Rotation = this.Rotation;
            apf.Opacity = this.Opacity;
            apf.RGBratio = new Single[] { this.RGBratio[0], this.RGBratio[1], this.RGBratio[2] };
            apf.RGBadd = new Single[] { this.RGBadd[0], this.RGBadd[1], this.RGBadd[2] };

            return apf;
        }
    }
}
