﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace PixelLionLib
{
    [Serializable()]
    public class PLProject : ISerializable
    {
        public String Name;
        public DateTime DateCreated;
        public String Dir;

        public ProjectRessources Ressources;
        public GameDatabase Database;
        public PixelLink GameLink;
        public String PixelMonkeyCore;

        public String Description;

        public static String CurrentDir;

        public PLProject()
        {
            Ressources = new ProjectRessources();
            Database = new GameDatabase();
            GameLink = new PixelLink();
            PixelMonkeyCore = String.Empty;

            Description = String.Empty;
        }

        public PLProject(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString("Name");
            DateCreated = (DateTime)info.GetValue("DateCreated", typeof(DateTime));
            Ressources = (ProjectRessources)info.GetValue("Ressources", typeof(ProjectRessources));
            Database = (GameDatabase)info.GetValue("Database", typeof(GameDatabase));
            GameLink = (PixelLink)info.GetValue("GameLink", typeof(PixelLink));
            PixelMonkeyCore = info.GetString("PixelMonkeyCore");
            Description = info.GetString("Description");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("DateCreated", DateCreated);
            info.AddValue("Ressources", Ressources);
            info.AddValue("Database", Database);
            info.AddValue("GameLink", GameLink);
            info.AddValue("PixelMonkeyCore", PixelMonkeyCore);
            info.AddValue("Description", Description);
        }

        public String Save()
        {
            SaveFileDialog dlg = new SaveFileDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Stream stream = File.Open(dlg.FileName, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();
                bFormatter.Serialize(stream, this);
                stream.Close();

                return dlg.FileName;
            }
            else
                return "";
        }

        public void Save(String path)
        {
            this.Dir = path.Substring(0, path.LastIndexOf("\\"));

            Stream stream = File.Open(path, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, this);
            stream.Close();
        }

        public void SaveExport(String GamePath, String GameName)
        {
            String AbsolutePath = GamePath.Substring(0, GamePath.LastIndexOf("\\")) + "\\" + GameName + ".plproj";

            Stream stream = File.Open(AbsolutePath, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, this);
            stream.Close();
        }

        public static PLProject Load(String Path)
        {
            try
            {
                PLProject objectToSerialize;
                Stream stream = File.Open(Path, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();
                objectToSerialize = (PLProject)bFormatter.Deserialize(stream);
                stream.Close();

                objectToSerialize.Dir = Path.Substring(0, Path.LastIndexOf("\\"));

                CurrentDir = Path;

                return objectToSerialize;
            }
            catch (SerializationException)
            {
                return null;
            }
        }
    }
}
