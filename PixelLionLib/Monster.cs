﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class Monster : ISerializable
    {
        public String Name;
        public Sprite mSprite;
        public CharactStats Stats;
        public List<SpecialAttack> Attacks;

        public ItemObject Drop;
        public Int32 DropRate;

        public Monster()
        {
            Name = "Monstre";
            mSprite = new Sprite();
            Stats = new CharactStats();
            Attacks = new List<SpecialAttack>();
            Drop = new ItemObject();
            DropRate = 0;
        }

        public Monster(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetValue("Name", typeof(String)) as String;
            mSprite = info.GetValue("mSprite", typeof(Sprite)) as Sprite;
            Stats = info.GetValue("Stats", typeof(CharactStats)) as CharactStats;
            Attacks = info.GetValue("Attacks", typeof(List<SpecialAttack>)) as List<SpecialAttack>;
            Drop = info.GetValue("Drop", typeof(ItemObject)) as ItemObject;
            DropRate = (Int32)info.GetValue("DropRate", typeof(Int32));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("mSprite", mSprite);
            info.AddValue("Stats", Stats);
            info.AddValue("Attacks", Attacks);
            info.AddValue("Drop", Drop);
            info.AddValue("DropRate", DropRate);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
