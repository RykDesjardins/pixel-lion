﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PixelLionLib
{
    public enum StatusEffect
    {
        Dizzy, Rushed, Disabled, Weight, Blind, Ablazed, Frozen, Poisoned
    }
}
