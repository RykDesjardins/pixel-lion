﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PixelLionLib
{
    [Serializable()]
    public class Song : ISerializable
    {
        public Byte[] Data { get; set; }
        public String Name { get; set; }

        public Song()
        {

        }

        public Song(SerializationInfo info, StreamingContext context)
        {
            Data = (Byte[])info.GetValue("Data", typeof(Byte[]));
            Name = (String)info.GetValue("Name", typeof(String));
        }

        public override string ToString()
        {
            return Name;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Data", Data);
            info.AddValue("Name", Name);
        }
    }
}
