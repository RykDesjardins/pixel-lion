<?php require_once("../PHP/SCRIPT_dbaccess.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- TemplateBeginEditable name="doctitle" -->
<title>Tricking Heroes</title>
<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
<style type="text/css">
<!--

body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #4E5869;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl
{
	padding: 0;
	margin: 0;
}

h1, h2, h3, h4, h5, h6, p 
{
	margin-top: 0;
	padding-right: 15px;
	padding-left: 15px;
}

a img 
{ 
	border: none;
}


a:link 
{
	color:#414958;
	text-decoration: underline; 
}

a:visited 
{
	color: #4E5869;
	text-decoration: underline;
}

a:hover, a:active, a:focus 
{ 
	text-decoration: none;
}

/* ~~ this container surrounds all other divs giving them their percentage-based width ~~ */
.container 
{
	width: 80%;
	max-width: 1000px;
	min-width: 1000px;
	background: #FFF;
	margin: 0 auto;
	
}

/* ~~the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo~~ */
.header 
{
	background: #6F7D94;
}

/* ~~ This is the layout information. ~~ */
.content 
{
	padding: 10px 0;
	background: eaffe9;
}

/* ~~ This grouped selector gives the lists in the .content area space ~~ */
.content ul, .content ol 
{ 
	padding: 0 15px 15px 40px;
}

/* ~~ The footer ~~ */
.footer 
{
	padding: 10px 0;
	background:url(../TemplateImages/BottomBanner.jpg);
	text-align:right;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt 
{
	float: right;
	margin-left: 8px;
}
.fltlft 
{ 
	float: left;
	margin-right: 8px;
}

.clearfloat 
{ 
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

#sddm
{	margin: 0;
	padding: 0;
	z-index: 30}

#sddm li
{	margin: 0;
	padding: 0;
	list-style: none;
	float: left;
}

#sddm li a
{	
	height: 40px;
	background:url(../TemplateImages/BannerButton.png);
	text-decoration: none;
}

#sddm li a:hover
{	
	height: 40px;
	background: url(../TemplateImages/BannerButtonHover.png);
	text-decoration: none;
}

.informationdiv
{
	position: absolute;
	visibility: hidden;
	margin: 0;
	padding: 0;
	background: #FFF;
	border: 1px solid #5970B2;
	width: 300px;	
}

#sddm div
{
	position: absolute;
	visibility: hidden;
	margin: 0;
	padding: 0;
	background: #FFF;
	border: 1px solid #5970B2;
}

#sddm div a
{	
	position: relative;
	display: block;
	margin: 0;
	padding: 5px 10px;
	width: auto;
	white-space: nowrap;
	text-align: left;
	text-decoration: none;
	background: #FFF;
	color: #000;
	font: 11px arial
}

#sddm div a:hover
{	
	background:#CCC;
}

-->
</style>

<script type="text/javascript">
var timeout	= 600;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}
</script>

</head>

<body>

<div class="container">
  <div class="header" style="height:300px; background:url(../TemplateImages/TitleBanner.jpg)"><!-- end .header --></div>
  <div class="header" style="vertical-align:middle; height:40px; background:url(../TemplateImages/ActionBarBackground.jpg)">
  
  <table cellpadding="0" cellspacing="0" border="0" style="font-size:12px;">
  <tr>
  <td width="30">
  
  </td>

    


    <td  width="120" height="40" align="center" style="vertical-align:central">
    <a href="../News.php" style="text-decoration:none;">
    <img src="../TemplateImages/Icons/HomePNG.png" width="30" alt="HomePNG" align="center"> 
    Home 
    </a>
    </td>
    
    <td  width="120" height="40" align="center" style="vertical-align:central;  color:#000">
    
 <ul id="sddm">
    <li><a href="#" 
        onmouseover="mopen('forumdiv')" 
        onmouseout="mclosetime()"
        style=" color:#000"><img src="../TemplateImages/Icons/ForumsPNG.png" width="30" alt="HomePNG" align="center"> 
    Forums<img src="../TemplateImages/Icons/view-sort-descending.png" width="30" alt="Dropdown" align="center"></a>
        <div id="forumdiv" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()"
            style="width: 550px;">
            
        <?php
		
			$result = ExeSQL("SELECT * FROM Topics ORDER BY 8 DESC LIMIT 10");
			$continue = true;
			
			while ($continue)
			{
				$row = mysql_fetch_row($result);
				
				if ($row[3] != "")
				{
					?>
					<a href="#"><span style="color:#06F"><?=$row[7]?></span> - <?=$row[3]?></a>
                    <?php
				}
				else
				{
					$continue = false;	
				}					
			}
			
		?>
        </div>
    </li>
 </ul>    
 
    </td> 
    
    <td width="120" height="40" align="center" style="vertical-align:central">

 <ul id="sddm">
    <li><a href="#" 
        onmouseover="mopen('videodiv')" 
        onmouseout="mclosetime()"><img src="../TemplateImages/Icons/VideoPNG.png" width="30" alt="HomePNG" align="center"> 
    Videos<img src="../TemplateImages/Icons/view-sort-descending.png" width="30" alt="Dropdown" align="center"></a>
        <div id="videodiv" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()"
            style=" color:#000; width: 508px;">
            
        <?php
		
			$result = ExeSQL("SELECT * FROM Videos ORDER BY 5 DESC LIMIT 10");
			$continue = true;
			
			while ($continue)
			{
				$row = mysql_fetch_row($result);
				
				if ($row[3] != "")
				{
					?>
					<a href="#"><span style="color:#06F"><?=$row[1]?></span> - <?=$row[1]?></a>
                    <?php
				}
				else
				{
					$continue = false;	
				}					
			}
			
		?>
        </div>
    </li>
 </ul>        
    
    

    </td>     
    
    
 
 
<?php  if ($_SESSION['MM_Username'] == ""){ ?>  
	
    <td width="310"></td>
    
    <td width="120" height="40" align="center" style="vertical-align:central">
    <a href="../Login.php" style="text-decoration:none; color:#000">
    <img src="../TemplateImages/Icons/LoginPNG.png" width="30" alt="LoginPNG" align="center">
    
    Login
    </a>
    </td> 
    
    <td width="20"></td>
    
    <td width="120" height="40" align="center" style="vertical-align:central"><img src="../TemplateImages/Icons/RegisterPNG.png" width="30" alt="RegisterPNG" align="center">
    Register
    </td>     
   
<?php }else{ ?>
   
    <td width="190"></td>
    
    <td width="140" height="40" align="center" style="vertical-align:central;  color:#000">
    <ul id="sddm">
            <li><a href="#" 
                onmouseover="mopen('maildiv')" 
                onmouseout="mclosetime()"><img src="../TemplateImages/Icons/MailPNG.png" width="30" alt="HomePNG" align="center"> 
            Inbox(<?php $result = ExeSQLFirstRow("SELECT COUNT(*) FROM Mail WHERE 5=0 AND 2='"  . $_SESSION['MM_Username']  . "'");  echo($result[0]);?>)<img src="../TemplateImages/Icons/view-sort-descending.png" width="30" alt="Dropdown" align="center"></a>
                <div id="maildiv" 
                    onmouseover="mcancelclosetime()" 
                    onmouseout="mclosetime()"
                    style="width: 308px;">
                    
                <?php
                
                    $result = ExeSQL("SELECT * FROM Mail WHERE 2='" . $_SESSION['MM_Username']  . "' ORDER BY 6 DESC LIMIT 10");
                    $continue = true;
                    
                    while ($continue)
                    {
                        $row = mysql_fetch_row($result);
                        
                        if ($row[3] != "")
                        {
                            ?>
                            <a href="#"><span style="color:#06F"><?=$row[1]?></span> - <?=$row[1]?></a>
                            <?php
                        }
                        else
                        {
                            $continue = false;	
                        }					
                    }
                    
                ?>
                </div>
            </li>
         </ul>   
    </td> 
    
    <td width="10"></td>
    
    <td width="120" height="40" align="center" style="vertical-align:central"><img src="../TemplateImages/Icons/ProfilePNG.png" width="30" alt="RegisterPNG" align="center">
    Profile
    </td>  
    
	<td width="10"></td>       
  
    <td width="120" height="40" align="center" style="vertical-align:central">
    <a href="../Logout.php" style="text-decoration:none; color:#000">
    <img src="../TemplateImages/Icons/LogoutPNG.png" width="30" alt="RegisterPNG" align="center">
    Logout
    </a>
    </td>    
  
<?php } ?>
  
  </tr>
  </table>  
  
  <!-- end .header -->
  </div>  
  
  <table align="center">
  <tr>
  <td  width="200" valign="top">
  
  <!-- MAIN MENU -->
  	
  	<table cellpadding="0" cellspacing="0">
    <tr  style="max-height:22px;"><td style="max-height:22px; height:22px; background-image:url(../MenusImage/TopMenuBackground.png); background-repeat:no-repeat;"></td></tr>
    <tr style="background-image:url(../MenusImage/CenterMenuBackground.png)">
    <td>
		
    </td>
    </tr>
    <tr><td  style="max-height:22px;" valign="top">
    	<img src="../MenusImage/BottomMenuBackground.png" />
    </td></tr>
    
    
    </table>
  
  <!-- MAIN MENU END -->
  
  </td>
  
  <td  width="620">
  <div class="content" align="center">
    <!-- end .content -->
    <!-- TemplateBeginEditable name="MainContent" -->
    
	[HARDCORE CONTENT OF THE PAGE MOFOS!] 
	
	<!-- TemplateEndEditable --></div>
    </td>
    
    <td  width="180">
    
    </td>
    </tr>
    </table>
    
    
  <div class="footer">
    <p style="font-size:10px;">Tricking Heroes v0.1 BETA - 2011 ~ Optimized for <a href="http://www.mozilla.org/">Mozilla Firefox</a> &amp; <a href="http://www.google.com/chrome">Google Chrome</a></p>
    <!-- end .footer --></div>
  <!-- end .container --></div>
</body>
</html>
