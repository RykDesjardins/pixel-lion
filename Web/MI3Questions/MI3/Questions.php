<?php
    include_once("../PHP/functions.php");

	session_start();
	$username = $_SESSION['MM_Username'];
	if ($username == null || $username == '') header('location: Intro.php');
	
    // Database connection, must be closed at the end of script
	$db = DBConnect();
    $dbresult = ExeQuery($db, "SELECT * FROM `utils` WHERE U_NAME = '$username'");
    $user = mysql_fetch_row($dbresult);
    $questionID = $user[11];
    
	// If page is releaded after a question was answered
    if (isset($_POST['BTNNext']))
    {
        $originalID = $questionID;
        $qID = 1 + (int)$questionID;
        $questionID = (string)($qID);
        ExeQuery($db, "UPDATE utils SET Q_index = $questionID WHERE U_NAME='$username'");
        
        $dbquestion = ExeQuery($db, "SELECT * FROM questions WHERE Q_ID=" . $originalID);
        $question = mysql_fetch_row($dbquestion);
        
		// If answer is good, increment score of user
        if ($_POST['AnsValue'] == (string)$question[5])
        {
            $score = 1 + (int)$user[12];
            ExeQuery($db, "UPDATE utils SET Score=" . (string)$score);
        }
        
		// If last question was answered, exit quiz
        if ($qID > 100)
        {
            echo("TERMINÉ");
            $questionID = "1";
        }
    }
    
    // Select next question and put informations into question variable
    $dbquestion = ExeQuery($db, "SELECT * FROM questions WHERE Q_ID=" . (string)$questionID);
    $question = mysql_fetch_row($dbquestion);	
	
	// Closure of database connection
    ENDSql($db);
?><head>>


<title>Quiz - #<?=(string)$question[0]?></title>
<script language="javascript" type="text/javascript"> 
 
upImage = new Image();
upImage.src = "Images/BTNCheckH.png";
downImage = new Image();
downImage.src = "Images/BTNCheckD.png"
normalImage = new Image();
normalImage.src = "Images/BTNCheckN.png";

$pressedindex = 'BTN0';

function BTNPress(id)
{
	document.images[id].src = "Images/BTNCheckD.png";
	$pressedindex = id;
	
	document.getElementById('BTNNext').style.visibility = 'visible';
	document.getElementById('AnsValue').value = id.substring(3);
	
	if (id != 'BTN1') BTNRestore('BTN1');
	if (id != 'BTN2') BTNRestore('BTN2');
	if (id != 'BTN3') BTNRestore('BTN3');
}
function BTNHover(id)
{
	if ($pressedindex != id) document.images[id].src = "Images/BTNCheckH.png";
}
function BTNRestore(id)
{
	if (document.images[id] != null)
		document.images[id].src = "Images/BTNCheckN.png";
}
function BTNLeave(id)
{
	if ($pressedindex != id) BTNRestore(id);	
}

</script>

<style type="text/css">
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #444;
	margin: 0;
	padding: 0;
	color: #000;
	text-align:center;
}

p {
	margin-top: 0;	 
	padding-right: 15px;
	padding-left: 15px;
}
a img { 
	border: none;
}

a:link {
	color: #42413C;
	text-decoration: underline;
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus {
	text-decoration: none;
}

.container {
	width: 800px;
	background: #FFF;
	margin: 0 auto;
}

.header {
	background: #444;
	background-image:url(Images/_PageTop.png);
	height:80px;
	max-height:80px;
	min-height:80px;
}

.content {

	padding: 10px 0;
	background-image:url(Images/_PageBG.jpg);
	height:800px;
	max-height:800px;
	min-height:800px;
	background-color: #000;
	background-repeat:no-repeat;
}

.footer {
	padding: 10px 0;
	background: #000;
}

.fltrt {  
	float: right;
	margin-left: 8px;
}
.fltlft {
	float: left;
	margin-right: 8px;
}
.clearfloat {
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

</style></head>

<body>


<div class="container">
  <div class="header">
  
  	<p style="font-size:12px;">
    
	<br /><br />
    <table style="width:auto;font-size:12px;">
    <tr>
    <td valign="bottom" style="width:400px; text-align:left; font-size:18px;">
    	<b>&nbsp;&nbsp;Question # <?=(string)$question[0]?> / 100</b>
    </td>
    <td valign="bottom" style="width:400px; text-align:right;  font-size:18px;">
    	<?=$user[2]?> <?=$user[3]?>&nbsp;&nbsp; 
    </td>
    </tr>
    </table>
</p>
    	
  <!-- end .header -->
  </div>
  
  <div class="content">
    <br />
    <br />
    <br />
    
    <center>
    <table align="center" cellpadding="0" cellspacing="0">
    	<tr>
        	<td style="background-image:url(Images/Container/TopLeft.png); width:24px;"></td>
    	
        	<td style="background-image:url(Images/Container/Top.png); height:28px;">
        	</td>
        
        	<td style="background-image:url(Images/Container/TopRight.png); width:24px;"></td>
        </tr>
        
        <tr>
        	<td style="background-image:url(Images/Container/Left.png); width:24px;"></td>
    	
        	<td style="max-width:600px; background-image:url(Images/Container/Content.png);">
    			<span style="color:#000; font-size:36px; text-align:center; font-family:Calibri;">
    			<?=$question[1]?>
    			</span>
            </td>
        
        	<td style="background-image:url(Images/Container/Right.png); width:24px;"></td>
        </tr>
        
        <tr>
        	<td style="background-image:url(Images/Container/BottomLeft.png); width:24px;"></td>
        
        	<td style="background-image:url(Images/Container/Bottom.png); height:28px;">
            </td>
        
        	<td style="background-image:url(Images/Container/BottomRight.png); width:24px;"></td>
        </tr>
        <tr>
        <td colspan="3" height="70">
        </td>
        </tr>
     </table>
     
     <table border="0" width="600" cellpadding="0" cellspacing="0" style="font-size:24px;">
     	<tr>
        	<td background="Images/AnswerContainer/Left.png" width="25" style="max-width:25px; min-width:25px;"></td>
     		<td width="120" height="97" background="Images/AnswerContainer/Content.png" align="left">
     	<img src="Images/BTNCheckN.png" width="80" height="80" id="BTN1" name="BTN1" onClick="BTNPress('BTN1');" onMouseOver="BTNHover('BTN1');" onMouseOut="BTNLeave('BTN1')" />
        	</td>
            <td height="97" background="Images/AnswerContainer/Content.png">
            	<?=(string)$question[2]?>
            </td>
            <td background="Images/AnswerContainer/Right.png" width="25"></td>
        </tr>
        <tr>
        	<td colspan="4" height="20">
            
            </td>
        </tr>
        <tr>
        	<td background="Images/AnswerContainer/Left.png" width="25"></td>
     		<td width="120" height="97" background="Images/AnswerContainer/Content.png" align="left">
        <img src="Images/BTNCheckN.png" width="80" height="80" id="BTN2" name="BTN2" onClick="BTNPress('BTN2');" onMouseOver="BTNHover('BTN2');" onMouseOut="BTNLeave('BTN2')" />
        	</td>
            <td height="97" background="Images/AnswerContainer/Content.png">
            	<?=(string)$question[3]?>
            </td>
            <td background="Images/AnswerContainer/Right.png" width="25"></td>
        </tr>
        <tr>
        	<td colspan="4" height="20">
            
            </td>
        </tr>
        <?php if ((string)$question[4] != "-") { ?>
        <tr>
        	<td background="Images/AnswerContainer/Left.png" width="25"></td>
            <td width="120" height="97" background="Images/AnswerContainer/Content.png" align="left">
            <img src="Images/BTNCheckN.png" width="80" height="80" id="BTN3" name="BTN3" onClick="BTNPress('BTN3');" onMouseOver="BTNHover('BTN3');" onMouseOut="BTNLeave('BTN3')" />
            </td>
            <td height="97" background="Images/AnswerContainer/Content.png">
            	<?=(string)$question[4]?>
            </td>
            <td background="Images/AnswerContainer/Right.png" width="25"></td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="3" align="right" height="120">
            	<form method="post" id="fNext">
                	<input type="submit" style="width:250px; height:50px; font-size:24px; visibility:hidden;" value="Next question" name="BTNNext" id="BTNNext" />
                    <input type="hidden" name="AnsValue" id="AnsValue" value="0" />
                </form>
            </td>
            <td>
            </td>
        </tr>
    </table>
    </center>
  </div>
  <div class="footer">
  	
    <!-- end .footer -->
  </div>
<!-- end .container -->
</div>
</body>
</html>