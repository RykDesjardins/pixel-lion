<?php

	require_once('PHP/functions.php');
	session_start();
	
	if (SessionIsActive())
	{
		$db = DBConnect();
		PrepareSQL($db);
		
		$username = $_SESSION['MM_Username'];
		$userinfos = mysql_fetch_row(EXEQuery($db, "SELECT * FROM `utils` WHERE U_NAME = '$username'"));
		if ($username == "admin") $quizURL = "ViewResults.php";
		else $quizURL = "Questions.php";
	}
	else
	{
		$userinfos = array('', '', '', '');	
		$quizURL = "Questions.php";
	}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #4E5869;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}

/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color:#414958;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #4E5869;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

/* ~~ this container surrounds all other divs giving them their percentage-based width ~~ */
.container {
	width: 60%;
	max-width: 1260px;/* a max-width may be desirable to keep this layout from getting too wide on a large monitor. This keeps line length more readable. IE6 does not respect this declaration. */
	min-width: 780px;/* a min-width may be desirable to keep this layout from getting too narrow. This keeps line length more readable in the side columns. IE6 does not respect this declaration. */
	background: #FFF;
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout. It is not needed if you set the .container's width to 100%. */
}

/* ~~the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo~~ */
.header {
	background: #6F7D94;
}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/
.content {
	padding: 10px 0;
}

/* ~~ This grouped selector gives the lists in the .content area space ~~ */
.content ul, .content ol { 
	padding: 0 15px 15px 40px; /* this padding mirrors the right padding in the headings and paragraph rule above. Padding was placed on the bottom for space between other elements on the lists and on the left to create the indention. These may be adjusted as you wish. */
}

/* ~~ The footer ~~ */
.footer {
	padding: 10px 0;
	background: #6F7D94;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}
.titlebar {
	background: #002152;
	background-image:url(Images/TitleBG.png);
	background-position:center;
	background-repeat:no-repeat;
	width:100%;
}
-->
</style></head>

<body>

<div style="" class="titlebar">
	<img src="Images/logo.png" width="268" height="52" />
</div>

<div class="container">
  <div class="header"><!-- end .header --></div>
  <div class="content">
    <div style="width:100%; background-color:#EEE; text-align:right; font-size:12px; border-color:#AAA; border-width:1px; border-style:ridge; border-left-width:0px; border-right:0px; height:25px;">
    	<table align="right" width="100%">
        	<tr>
            	<td style="text-align:left">
                	&nbsp;<?=$userinfos[2]?>&nbsp;<?=$userinfos[3]?>
                </td>
            	<td width="100" style="text-align:left">
                	<a href="Register.php">Register</a>
                </td>
            	<td width="100" style="text-align:left">
                	<a href="Login.php">Login</a>
                </td>
            </tr>                     
        </table> 
        
    </div>
    <br />
    <div style="background-color:#EEE; width:90%; border-color:#CCC; border-width:1px; margin: 0px auto; font-size:12px;
">
   	  <h1>Welcome</h1>
    	<p>In order to start the quiz, you first need to register. Note that it is highly suggested to provide
        valid informations so that we can easily contact you if you win one of the fabulous prizes for scoring the
        highest score in the quiz. </p>
      <p>
        Once registered, you'll be automaticaly redirected to the quiz page. A hundread questions will follow; fifty 
        'multiple choices' and fifty 'true or false'.
      </p>
      <p>
        Once you are done with the quiz, you will be able to fill a more detailed form where you will be able to 
        talk a little more about yourself, what you like, your field of studies, etc.
        </p>
        <p>
        Concider choosing a Username and a Password easy for you to remember. If for some reason the device
        you are using freezes or stop working, you will be able to continue the quiz from the exact question where
        you had a problem with your device.
        </p>
        
        <?php if ($userinfos[2] == ''){ ?>
        <div align="center">
        	<table align="center" style="font-size:20px;">
            <tr>
            <td>
        	<a href="Register.php"><img src="Images/ICON_CREATEUSER.png" /></a>
            </td>
            <td rowspan="2" width="50">
            
            </td>
            <td>
            <a href="Login.php"><img src="Images/ICON_LOGIN.png" /></a>
            </td>
            </tr>
            <tr>
            <td align="center">
            Register
            </td>
            <td align="center">
            Login
            </td>
            </tr>
            </table>
        </div>
        <?php }else{ ?>
         <div align="center">
        	<table align="center" style="font-size:20px;">
            <tr>
            <td>
        	<a href="<?=$quizURL?>"><img src="Images/ICON_Startquiz.png" /></a>
            </td>
            </tr>
            <tr>
            <td align="center">
            Go to quiz
            </td>
            </tr>
            </table>
        </div>       
        <?php } ?>
    </div>
    
    <!-- end .content --></div>
  <div class="footer">
    <p>&nbsp;</p>
    <!-- end .footer --></div>
  <!-- end .container --></div>
</body>
</html>