<?php
require_once("CLASS_User.php");
require_once("SCRIPT_Points.php");
require_once("SCRIPT_MainDBConn.php");

function PrepareSQL()
{
	$db = DBconnect();
	//if (!$db) die("Oops! Looks like you could not access main database for unknown reasons. Please try again later or contact admin.");
	mysql_select_db("tricking_dbMain", $db);
	mysql_query("SET NAMES UTF8"); 	
	
	return "tricking_dbMain"; //$MainDBConn;
}

function CloseSQL($conn)
{
	mysql_close($conn);
}

function IsLogged()
{
	return (isset($_SESSION['MM_Username']));	
}

function ExeSQL($sql)
{
	return mysql_db_query(PrepareSQL(), $sql); 
}

function ExeSQLFirstRow($sql)
{
	$result = mysql_db_query(PrepareSQL(), $sql);
	$row = mysql_fetch_row($result);
	return $row;
}

function GetMailCount($username)
{
	$row = ExeSQLFirstRow("SELECT COUNT(*) FROM Mail WHERE Receiver='" . $username . "'");
	return $row[0];		
}

function GetUser($username)
{
	return new User(ExeSQL("SELECT * FROM Users WHERE Username='" . $username . "'"));
}

function UserExists($username)
{
	$request = ExeSQL("SELECT Username FROM Users WHERE Username LIKE binary '" . $username . "'");
	return mysql_num_rows($request);		
}

function UserAccessCheck($username, $password)
{
	$request = ExeSQL("SELECT Username, Password FROM Users WHERE Username LIKE binary '" . $loginUsername . "' AND Password LIKE binary '" . $password . "'");
	return mysql_num_rows($request);
}

function AddPointsToAuthor($topicid)
{
	$topic = ExeSQLFirstRow("SELECT * FROM Topics WHERE INDEXED=" . $topicid);
	$user = GetUser($topic[4]);
	$user->AddExp(Points::$getpost);
	$user->CommitUpdate();
}

function PostToForum($topicid, $username, $content)
{
	ExeSQL("INSERT INTO Posts VALUES(NULL, " . $topicid . ", '" . $username . "', now(), '" . $content . "')");
	$user = GetUser($username);
	$user->AddExp(Points::$post);
	$user->CommitUpdate();
	
	AddPointsToAuthor($topicid);
	
	$sql = ExeSQLFirstRow("SELECT MAX(INDEXED) FROM Topics");
	ExeSQL("UPDATE Topics SET LASTPOSTID=" . $sql[0] . ", Timeupdated=now()");	
}

function DeleteTopic($topicid, $username)
{
	ExeSQL("DELETE FROM Topics WHERE Topicauthor='" . $username . "' AND INDEXED=" . $topicid);
	ExeSQL("DELETE FROM Posts WHERE TOPICID=" . $topicid);
}

function CreateTopic($username, $topicname, $content, $sectionid, $topictype)
{
	$row = ExeSQLFirstRow("SELECT * FROM Topics WHERE Topicname='" . $topicname . "'");
	$okay = true;
	
	if ($row[0] == "")
	{
		ExeSQL("INSERT INTO Topics VALUES(NULL, " . $sectionid . ", -1, '" . $topicname . "', '" . $username . "', '" . $topictype . "', 0, now())") or exit;
		$row = ExeSQLFirstRow("SELECT * FROM Topics WHERE Topicname='" . $topicname . "'");
		
		ExeSQL("INSERT INTO Posts VALUES(NULL, " . $row[0] . ", '" . $username . "', now(), '" . $content . "')");
		$row = ExeSQLFirstRow("SELECT * FROM Posts WHERE TOPICID=" . $row[0]);
		
		ExeSQL("UPDATE ForumSection SET LASTPOSTID=" . $row[0] . ", Topiccount=Topiccount+1 WHERE INDEXID=" . $sectionid);
		$user = GetUser($username);
		$user->AddExp(Points::$createtopic);
		$user->CommitUpdate();
	}
	else
	{
		$okay = false;
	}
	
	return $okay;
}


?>