<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/Tranzoid_Template.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Tranzoid ~ De qui parle-t-on?</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background:#CCC;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}
/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color: #42413C;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

/* ~~ this fixed width container surrounds the other divs ~~ */
.container {
	width: 1024px;
	max-width:1024px;
	min-width:1024px;
	background: #FFF;
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout */
}

/* ~~ the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo ~~ */
.header {
	background: #ADB96E;
}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/

.content {

	padding: 10px 0;
}

/* ~~ The footer ~~ */
.footer {
	padding: 10px 0;
	background: #CCC49F;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

div.content a:visited
{
	color:#93F;
}

div.content a:active
{
	text-decoration:none;	
	font-size:12px;
	text-transform:none;
	color:#90F;		
}

div.content a
{
	text-decoration:none;
	text-transform:none;
	color:#90F;		
}

div.content a:hover
{
	text-decoration:underline;
	color:#93F;
}

@font-face {
    font-family:"Hand Of Sean";
	src: url("../../Fonts/handsean.ttf");
}
-->
</style>
<script language="javascript" type="text/javascript">
function togglevisible(elemname)
{
	var tdrow = document.getElementById(elemname);
	if (tdrow.style.display == "none") tdrow.style.display = "block";	
	else tdrow.style.display = "none";	
}

</script>

</head>

<body>

<div class="container">
  <div class="header"><a href="../index.php"><img src="../IMGS/Tz/TzTitle.jpg" alt="Insert Logo Here" name="Insert_logo" /></a> 
  <img src="../IMGS/Tz/TzHr.jpg" /></div>
  <div class="content" style="background-image:url(../IMGS/Tz/TzBG.jpg); background-repeat:no-repeat;">
  	
  <!-- end .content -->
  <table>
  <tr>
  <td width="200" valign="top">
  	
    <table>
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzNavigation.png" onclick="togglevisible('navtd');" />
    </td>
    </tr>
    
    <tr>
    <td style="font-family:'calibri'; display:block;" id="navtd">
            &nbsp;&nbsp;&nbsp;          <a href="../index.php">Accueil               </a><br />
            &nbsp;&nbsp;&nbsp;    <a href="../Application.php">Application           </a><br /> 
            &nbsp;&nbsp;&nbsp; <a href="../ServeurWindows.php">Serveur Windows       </a><br />
            &nbsp;&nbsp;&nbsp;         <a href="../Images.php">En images             </a><br />
            &nbsp;&nbsp;&nbsp;   <a href="../Technilisees.php">Matériel              </a><br />
            &nbsp;&nbsp;&nbsp;            <a href="../Qui.php">De qui parle-t-on?    </a><br />
    </td>
    </tr>
    
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzVersion.png" onclick="togglevisible('vertd');" />
    </td>
    </tr>
    <tr>
    <td style="display:none;" id="vertd">
    	<center>
    	Version 1.1.2 -
    	</center>
        <span style="font-size:12px; color:#555">
        Derrnière mise à jour le premier décembre 2011.
        </span>
    </td>
    </tr>
    
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzObtenir.png" onclick="togglevisible('obttd');" />
    </td>
    </tr>
    <tr>
    <td style="display:none;" id="obttd">
    	<center>
    	Télécharger -
    	</center>
        <span style="font-size:12px; color:#555">
        <a href="../Downloads/Tranzoid.apk">Application Android</a><br />
        <a href="../Downloads/Serveur Tranzoid 1.1.2.zip">Serveur Windows</a><br />
        <a href="../Tranzoid - Document Analyse.pdf">Document d'analyse</a><br />
        </span>
    </td>
    </tr>
    
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzContacts.png" onclick="togglevisible('contd');" />
    </td>
    </tr>
    <tr>
    <td style="display:none;" id="contd">
    	<center>
    	Références -
    	</center>
        <span style="font-size:12px; color:#555">
        <a href="http://developer.android.com/index.html">Développeur Android</a><br />
        <a href="http://www.clg.qc.ca/">Collège Lionel-Groulx</a><br />
        <a href="http://www.multiservicessd.com/">Multiservices SD</a><br />
        <a href="http://www.mozilla.org/fr/firefox/fx/">Mozilla Firefox</a><br />
        <a href="http://www.kubuntu.org/">Kubuntu</a><br />
        <a href="http://www.eclipse.org/downloads/">Eclipse</a><br />
        </span>
    </td>
    </tr>           
    </table>
    
    
  </td>
  <td valign="top" width="800" style="font-family:Calibri">
  	<!-- InstanceBeginEditable name="EditableBody" -->
            <center><span style="font-family:'Hand Of Sean'; font-size:24px;">De qui parle-t-on?</span><br />
        <img src="../IMGS/Tz/TzHr.jpg" width="700" height="5" /><br /></center>
        <span style="text-align:right; right:auto; color:#666; font-size:12px;">est une question que vous vous posez si vous êtes sur cette page.</span><br />
        <span style="font-family:'Hand Of Sean'; font-size:14px; text-align:center">
        <center>
        Nous sommes une équipe gigantesque! Une seule personne. Un humain dois-je préciser.
        </center>
        </span>
        <br />
        <table align="center">
          <tr>
        <td  style="background-color:#ECE">
        	<span style="font-family:'Hand Of Sean'; font-size:24px;">Érik Desjardins</span><br />
        </td>
        </tr>
        <tr>
        <td style="font-size:12px; background-color:#EEE">
            &nbsp;&nbsp;&nbsp;Informatique de Gestion<br />
            &nbsp;&nbsp;&nbsp;Collège Lionel-Groulx<br />
            &nbsp;&nbsp;&nbsp;erik@multiservicessd.com<br />
            <br />
            Développement, analyse, conception<br />
            <center>Chargé de projet
            </center>
        </td>
        </tr>
      </table>
		<br />
        Le projet a été supervisé par monsieur <b>François Jean</b> de la technique d'informatique du Collège Lionel-Groulx.
        <br />
        <br />
        
        <center><span style="font-family:'Hand Of Sean'; font-size:24px;">Document d'analyse du projet Tranzoid</span><br />
        <img src="../IMGS/ChecklistIcon.png" width="40"/><a href="Tranzoid - Document Analyse.pdf" style="font-size:20px;">Tranzoid - Document Analyse.pdf</a></center>
	
	<!-- InstanceEndEditable --></td>
  </tr>
  </table></div>
  <div class="footer">
    <p style="font-size:10px;">
    <a href="http://www.multiservicessd.com/Tranzoid/">Tranzoid - 2011</a> | 
    <a href="http://www.clg.qc.ca/">Collège Lionel-Groulx</a> | 
    <a href="mailto:erik@multiservicessd.com">Érik Desjardins</a> | 
    <a href="http://developer.android.com/index.html">Android - Google</a> | 
    <a href="http://www.mozilla.org/fr/firefox/fx/">Mozilla Firefox</a> | 
    <a href="http://www.kubuntu.org/">Kubuntu</a> | 
    <a href="http://www.eclipse.org/downloads/">Eclipse</a> |
     <a href="http://www.microsoft.com/visualstudio/fr-ca">Visual Studio</a>
     </p>
    <!-- end .footer --></div>
  <!-- end .container --></div>
</body>
<!-- InstanceEnd --></html>
