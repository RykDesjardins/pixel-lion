<?php

if (($handle = fopen("users.csv", "r")) !== FALSE)
{
	$foundentry = false;
	$linenumber = 0;
	$user = $_POST['tbusername'];
	$pass = $_POST['tbpassword'];
	session_start();
	
    while (($data = fgetcsv($handle, 1000, ":")) !== FALSE and !$foundentry) 
	{
		$linenumber++;
		
		if ($user == $data[0] and $pass == $data[1])
		{
			$foundentry = true;
			
			$_SESSION['UserLogged'] = $user;
			$_SESSION['CompleteName'] = $data[2];
			$_SESSION['UserIndex'] = $linenumber;
		}
    }	
	
	if ($foundentry) header('Location: profile.php?login=success');	
	else header('Location: index.php?failed=true');
	
	exit;
}

?>