<?php

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

// Récupération des objets session
// session_start();

// Objets de base
$valide = true;
$reason = "";

// Le mot de passe
$newpassword = $_POST['tbpassword'];

// Validation
if (strlen($newpassword) < 8) $valide = false;
if (!preg_match('/[A-Z]/', $newpassword)) $valide = false;
if (!preg_match('/[a-z]/', $newpassword)) $valide = false;
if (!preg_match('/[0-9]/', $newpassword)) $valide = false;

try
{
	if ($valide) 
	{
		// Création des objets
		$source = 'users.txt';
		$target = 'temp_users.txt';
		$username = $_POST['husername'];
		$userlength = strlen($username);
		$cpt = $_POST['hindex'];
		
		// Copier les valeurs
		$sh = fopen($source, 'r');
		$th = fopen($target, 'a+');
		
		while (!feof($sh)) 
		{
			$cpt++;
			$line = fgets($sh);
			if ($cpt == $_SESSION['UserIndex']) $line = substr($line, 0, $userlength) . ":" . $newpassword . ":" . $_SESSION['CompleteName'] . PHP_EOL;
			fwrite($th, $line);
		}
		fclose($sh);
		fclose($th);
		
		// Supprimer le fichier source
		unlink($source);
		
		// Renommer le fichier temporaire
		rename($target, $source);
	
		// Redirection
		header('Location: profile.php?change=success&validation=success');
	}
	else
	{
		// Redirection
		header('Location: profile.php?change=failed&reason=validation');
	}
}
catch (Exception $e)
{
	header('Location: profile.php?change=failed&reason=exception');
}

?>