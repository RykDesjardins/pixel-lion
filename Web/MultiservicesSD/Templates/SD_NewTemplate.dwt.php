<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Multiservices SD</title>

<style type="text/css">

@font-face {
	font-family: Eras Light;
	src: url('..Fonts/ERASLGHT.TTF');
}

h2 
{
	color:#FFFFFF;
	font-size:30px;
	font-family:Eras Light;
	text-align:center;	
	vertical-align:middle;
	margin: 0 0 0 0;
}

.menutd
{
	height:90px;	
	text-align:center;
	vertical-align:middle;
	color:#BBBBBB;
	font-size:12px;
	font-weight:bold;
	width:100px;
}

.outer {
	position:absolute;
	top: 50%;
	left: 50%;
	
	width:600px;
	height:540px;
    margin-top: -270px;
	margin-left:  -300px;
}

.fullscreen
{
	display:block;
	
	position:absolute;
	top:0;
	left:0;
	
	width:100%;
	height:100%;
	
	background-image:url(../IMGS/BackTechno.png); 
	background-position:center; 
	background-repeat:no-repeat; 
	
	z-index:-1000;
}

.infocontent
{
	width:100%;
	height:100%;	
}

</style>
</head>

<body style="background-color:#113366; width:100%; height:100%; ">

<div class="fullscreen"></div>

    <div class="outer">
        <table background="../IMGS/TBackBG.png" width="100%">
            <tr>
                <td>
                    <img src="../IMGS/SD_transp.png" height="150" />
                </td>
                <td rowspan="5" height="100%" width="2">
                	<img src="../IMGS/MenuSeparatorVertical.png" />
                </td>
                <td rowspan="5" height="100%">
                	<div class="infocontent">
                    	
                    </div>
                </td>
            </tr>
            <tr>
                <td class="menutd">
                    <h2>À propos</h2>
                    En savoir plus sur nous
                </td>
            </tr>
            <tr>
                <td class="menutd">
                    <h2>Services</h2>
                    Ce dont vous avez besoin
                </td>
            </tr>
            <tr>
                <td class="menutd">
                    <h2>Réseaux</h2>
                    Nous suivre de près
                </td>
            </tr>
            <tr>
                <td class="menutd">
                    <h2>Contact</h2>
                    Nous joindre simplement
                </td>
            </tr>                
        </table>
	</div>
</body>
</html>
