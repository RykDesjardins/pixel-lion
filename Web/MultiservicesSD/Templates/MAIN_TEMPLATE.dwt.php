<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- TemplateBeginEditable name="doctitle" -->
<title>MAIN TEMPLATE</title>
<link rel="SHORTCUT ICON" href="../IMGS/ICON.bmp" />

<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background:url(../IMGS/Background.jpg) ;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}

/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color:#414958;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #4E5869;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

/* ~~ this container surrounds all other divs giving them their percentage-based width ~~ */
.container {
	width: 70%;
	max-width: 1260px;/* a max-width may be desirable to keep this layout from getting too wide on a large monitor. This keeps line length more readable. IE6 does not respect this declaration. */
	min-width: 900px;/* a min-width may be desirable to keep this layout from getting too narrow. This keeps line length more readable in the side columns. IE6 does not respect this declaration. */
	background: #FFF;
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout. It is not needed if you set the .container's width to 100%. */
}

/* ~~the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo~~ */
.header {
	background: url(../IMGS/TopBanner.png);
	width: 100%;
}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/
.content {
	padding: 10px 0;
}

/* ~~ This grouped selector gives the lists in the .content area space ~~ */
.content ul, .content ol { 
	padding: 0 15px 15px 40px; /* this padding mirrors the right padding in the headings and paragraph rule above. Padding was placed on the bottom for space between other elements on the lists and on the left to create the indention. These may be adjusted as you wish. */
}

/* ~~ The footer ~~ */
.footer {
	padding: 10px 0;
	background: #6F7D94;
	font-size:9px;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

#sddm
{	margin: 0;
	padding: 0;
	z-index: 30}

#sddm li
{	margin: 0;
	padding: 0;
	list-style: none;
	float: left;
	font: bold 18px arial}

#sddm li a
{	display: block;
	margin: 0 1px 0 0;
	padding: 4px 10px;
	width: 180px;
	height: 32px;
	background:url(../IMGS/BTN/Top_BTN.jpg);
	color: #FFC;
	text-align: center;
	text-decoration: none}

#sddm li a:hover
{	background: url(../IMGS/BTN/Top_BTN_H.jpg)}

.informationdiv
{
	position: absolute;
	visibility: hidden;
	margin: 0;
	padding: 0;
	background: #222;
	border: 1px solid #5970B2;
	width: 300px;	
}

#sddm div
{
	position: absolute;
	visibility: hidden;
	margin: 0;
	padding: 0;
	background: #222;
	border: 1px solid #5970B2;
	width: 219px;
}

			#sddm div a
			{	
				position: relative;
				display: block;
				margin: 0;
				padding: 5px 10px;
				width: auto;
				white-space: nowrap;
				text-align: left;
				text-decoration: none;
				background: #FFC;
				color: #000;
				font: 16px "Comic Sans MS", cursive;
			}
		
			#sddm div a:hover
			{	background: #FC6;
			}

-->
</style>

<script type="text/javascript">
var timeout	= 600;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}
</script>

</head>

<body>
<div class="container">
  <div class="header">
  <!-- end .header -->
  
  <table>
  <tr>
  <td rowspan="2">
  <img src="../IMGS/SD_transp.png" width="140" height="140" alt="logo" /> 
  </td>
  <td height="96">
  <img src="../IMGS/Title_MultiserviceSD.png" width="413" height="55" alt="Title_Text" />
  </td>
  </tr>
  <tr>
  <td><img src="../IMGS/Catchline.png" width="403" height="30" alt="Catchline" />
  </td>
  </tr>
  </table>
  
  </div>
  

  <div class="content">
  <table>
  <tr>
  <td>
<ul id="sddm">
    <li><a href="#" 
        onmouseover="mopen('m1')" 
        onmouseout="mclosetime()">Accueil</a>
        <div id="m1" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
            <a href="../O_index.php">Retour à l'accueil</a>
        </div>
    </li>
    <li><a href="#" 
        onmouseover="mopen('m2')" 
        onmouseout="mclosetime()">L'entreprise</a>    
         <div id="m2" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
            <a href="#">Qui nous sommes</a>
            <a href="#">Notre mission</a>
            <a href="#">Territoire desservi</a>
        </div>
    </li>       
    <li><a href="#" 
        onmouseover="mopen('m3')" 
        onmouseout="mclosetime()">Services</a>    
         <div id="m3" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()"
            style="width:320px;">
            <a href="#">Réseautage</a>
            <a href="#">Logiciels de gestion</a>
            <a href="#">Développement web</a>
            <a href="#">Installation d'équipement technologique</a>
            <a href="#">Assistance</a>
        </div>
    </li>      
    <li><a href="#" 
        onmouseover="mopen('m4')" 
        onmouseout="mclosetime()">Rencontrez-nous</a>    
         <div id="m4" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
            <a href="#">Laissez vos infos</a>
            <a href="#">Nous contacter</a>
        </div>
    </li>     
</ul>  	
  </td>
  </tr>
  </table>


<br />
  <!-- TemplateBeginEditable name="Main" -->
  
  [Main]
  
  <!-- TemplateEndEditable -->
  </div>
  
  <div class="footer">
    <p>MultiServices SD - 2011</p>
    <!-- end .footer --></div>
  <!-- end .container --></div>
</body>
</html>
