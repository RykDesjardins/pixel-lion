<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/Site_Template.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Multiservices SD ~ Bienvenue!</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<script type="text/javascript"> 
<!--
function goHome()
{
	window.location = "http://www.multiservicessd.com";
}
//-->
</script>

<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background-color:#000;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}

/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color:#414958;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #4E5869;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

/* ~~ this container surrounds all other divs giving them their percentage-based width ~~ */
.container {
	width: 70%;
	max-width: 1260px;/* a max-width may be desirable to keep this layout from getting too wide on a large monitor. This keeps line length more readable. IE6 does not respect this declaration. */
	min-width: 1260px;/* a min-width may be desirable to keep this layout from getting too narrow. This keeps line length more readable in the side columns. IE6 does not respect this declaration. */
	max-height: 990px;
	background: #000;
	background-image:url(IMGS/SiteBG.jpg);
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout. It is not needed if you set the .container's width to 100%. */
}

/* ~~the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo~~ */
.header {
	background: url(IMGS/TopBanner.png);
	width: 100%;
}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/
.content {
	padding: 10px 0;
}

/* ~~ This grouped selector gives the lists in the .content area space ~~ */
.content ul, .content ol { 
	padding: 0 15px 15px 40px; /* this padding mirrors the right padding in the headings and paragraph rule above. Padding was placed on the bottom for space between other elements on the lists and on the left to create the indention. These may be adjusted as you wish. */
}

/* ~~ The footer ~~ */
.footer {
	padding: 10px 0;
	font-size:9px;
	min-height:100px;
	max-heigth:100px;
	max-width: 1260px;
	min-width: 1260px;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

#sddm
{	margin: 0;
	padding: 0;
	z-index: 30}

#sddm li
{	margin: 0;
	padding: 0;
	list-style: none;
	float: left;
	font: bold 18px arial}

#sddm li a
{	display: block;
	margin: 0 1px 0 0;
	padding: 4px 10px;
	width: 180px;
	height: 32px;
	background:url(IMGS/BTN/Top_BTN.jpg);
	color: #FFC;
	text-align: center;
	text-decoration: none}

#sddm li a:hover
{	background: url(IMGS/BTN/Top_BTN_H.jpg)}

.linkbutton
{
	background-image:url(IMGS/BTN/GoToLink.jpg);
	width: 20px;
	height: 20px;	
	display: block;
	text-indent: -9999px;	
}

.linkbutton:hover
{
	background-position: 0 -20px;
}

.linkbutton:active
{
	background-position: 0 -40px;
}

.informationdiv
{
	position: absolute;
	visibility: hidden;
	margin: 0;
	padding: 0;
	background: #222;
	border: 1px solid #5970B2;
	width: 300px;	
}

#sddm div
{
	position: absolute;
	visibility: hidden;
	margin: 0;
	padding: 0;
	background: #222;
	border: 1px solid #5970B2;
	width: 219px;
}

			#sddm div a
			{	
				position: relative;
				display: block;
				margin: 0;
				padding: 5px 10px;
				width: auto;
				white-space: nowrap;
				text-align: left;
				text-decoration: none;
				background: #FFC;
				color: #000;
				font: 16px "Comic Sans MS", cursive;
			}
		
			#sddm div a:hover
			{	background: #FC6;
			}

-->
</style>

</head>

<body>

<div class="container">
  <div class="content"> 
    
    <div style="background-image:url(IMGS/TitleBar.png); height:150px; margin-top:50px;" >
     <table width="1260" style="color:#FC3; font-family:'Comic Sans MS', cursive">
        <tr><td width="593" height="145" rowspan="6" onclick="document.location='index.php'"></td>
        <td width="20" valign="top"></td></tr>
        
        <tr><td valign="top" height="23"></td></tr>
        
        <tr>
        <td valign="top">
        <a href="#" class="linkbutton"></a>
        </td><td width="631" height="27" valign="top">
        <img src="IMGS/BTN/QuiSommesNous.png" width="600" height="25" />
        </td></tr>

        <tr>
        <td valign="top">
        <a href="#" class="linkbutton"></a>
        </td><td height="29" valign="top">
        <img src="IMGS/BTN/NosServices.png" width="600" height="25" />
        </td></tr>
        
        <tr>
          <td valign="top">
          <a href="nouscontacter.php" class="linkbutton"></a>
          </td><td valign="top">
          <img src="IMGS/BTN/NousContacter.png" width="600" height="25" />
          </td></tr>
        
        <tr><td valign="top"></td></tr>
                             
 
     </table>   
    
    </div>
    
    <table style="color:#FFF">
    <tr>
    <td height="150" width="1260">
    
      <!-- InstanceBeginEditable name="TOP" --><!-- InstanceEndEditable --></td>
    </tr>
    
    <tr>
    <td><!-- InstanceBeginEditable name="MAINBODY" -->
    <img src="IMGS/DroidCorner.png" style="left:0px; top:0px; position:fixed;" />
    
     <table style="vertical-align:top;">
     <tr>
     <td width="400" valign="top"><p><span style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:24px; margin-left:10px;">L'indice SD</span><br />
       <span style=" font-family:'Comic Sans MS', cursive; font-size:14px;  margin-left:25px;">Qu'est-ce que l'indice SD? Il s'agit d'un outil de classement mesurant le niveau technologique de votre entreprise. </span></p>
       <p><span style=" font-family:'Comic Sans MS', cursive; font-size:14px;  margin-left:25px;"><a href="indiceSD.php">En savoir plus</a></span></p></td>
   <td>
   <img src="IMGS/SeparateurVertical.png" />
   </td>
   
   <td width="400" valign="top"><p><span style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:24px; margin-left:10px;">Nos forces</span><br />
     <span style=" font-family:'Comic Sans MS', cursive; font-size:14px;  margin-left:25px;">
       Nous sommes spécialisés dans le développment de systèmes de gestion informatique et dans la création de sites web. </span></p>
     <p><span style=" font-family:'Comic Sans MS', cursive; font-size:14px;  margin-left:25px;">Nous avons aussi divers champs de forces passant par l'installation et   la mise en marche de votre équipement technologique jusqu'au service après vente qui fera en sorte de vous rassurer sur le bon fonctionnement de votre entreprise.</span>
       
     </p>
     <p>&nbsp;</p></td>

   <td>
   <img src="IMGS/SeparateurVertical.png" />
   </td>   
   
   <td width="400" valign="top"><p><span style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:24px; margin-left:10px; ">Site en construction</span><br />
     <span style=" font-family:'Comic Sans MS', cursive; font-size:14px;  margin-left:25px;">Le site est présentement en construction.</span></p>
     <p>&nbsp;</p></td>


<tr>
<td colspan="5" height="150">

    <table cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td width="13">
        	<img src="IMGS/MENUS/OrangeLeftT.png" />
        </td>
        <td style="background:url(IMGS/MENUS/OrangeBodyT.png); background-repeat:repeat-x; max-height:114;">
        	<img src="IMGS/AndroidBugDroid.png" width="100" />
        </td>
        <td style="background:url(IMGS/MENUS/OrangeBodyT.png); background-repeat:repeat-x; max-height:114;">
        	<span style="font-family:'Comic Sans MS', cursive; font-size:12px; color:#000">
        	 Multiservices SD sont des fiers utilisateurs des systèmes Android autant <br />
             sur les téléphones mobiles que sur les tablettes.<br />
             Le développement d'applications propre à votre entreprise peut être une<br />
             solution efficace à l'optimisation de vos systèmes informatiques.
             </span>
        </td>
        <td width="13">
        	<img src="IMGS/MENUS/OrangeRightT.png" />
        </td>
    </tr>
    </table>

    
</td>
</tr>
</table>

	<!-- InstanceEndEditable -->
    
    </td>
    </tr>
    </table>
    <div class="footer">
    <img src="IMGS/SeparateurHorizontal.png" /><br />
    </div>
    
    
    <!-- end .content --></div>
  <!-- end .container --></div>
</body>
<!-- InstanceEnd --></html>
