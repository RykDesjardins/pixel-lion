<?php

if (($handle = fopen("users.txt", "r")) !== FALSE)
{
	session_start();
	$_SESSION['UserLogged'] = null;
	$_SESSION['CompleteName'] = null;

	header('Location: index.php?logout=success');	
	session_destroy();
	exit;
}

?>