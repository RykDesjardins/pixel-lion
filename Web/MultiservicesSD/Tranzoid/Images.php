<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/Tranzoid_Template.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Tranzoid ~ Les Images</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #42413C;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}
/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color: #42413C;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

/* ~~ this fixed width container surrounds the other divs ~~ */
.container {
	width: 1024px;
	max-width:1024px;
	min-width:1024px;
	background: #FFF;
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout */
}

/* ~~ the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo ~~ */
.header {
	background: #ADB96E;
}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/

.content {

	padding: 10px 0;
}

/* ~~ The footer ~~ */
.footer {
	padding: 10px 0;
	background: #CCC49F;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

div.content a:visited
{
	color:#93F;
}

div.content a:active
{
	text-decoration:none;	
	font-size:12px;
	text-transform:none;
	color:#90F;		
}

div.content a
{
	text-decoration:none;
	text-transform:none;
	color:#90F;		
}

div.content a:hover
{
	text-decoration:underline;
	color:#93F;
}

@font-face {
    font-family:"Hand Of Sean";
	src: url("../Fonts/handsean.ttf");
}
-->
</style>
<script language="javascript" type="text/javascript">
function togglevisible(elemname)
{
	var tdrow = document.getElementById(elemname);
	if (tdrow.style.display == "none") tdrow.style.display = "block";	
	else tdrow.style.display = "none";	
}

</script>

</head>

<body>

<div class="container">
  <div class="header"><a href="index.php"><img src="../IMGS/Tz/TzTitle.jpg" alt="Insert Logo Here" name="Insert_logo" /></a> 
  <img src="../IMGS/Tz/TzHr.jpg" /></div>
  <div class="content" style="background-image:url(../IMGS/Tz/TzBG.jpg); background-repeat:no-repeat;">
  	
  <!-- end .content -->
  <table>
  <tr>
  <td width="200" valign="top">
  	
    <table>
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzNavigation.png" onclick="togglevisible('navtd');" />
    </td>
    </tr>
    
    <tr>
    <td style="font-family:'calibri'; display:block;" id="navtd">
            &nbsp;&nbsp;&nbsp;          <a href="index.php">Accueil               </a><br />
            &nbsp;&nbsp;&nbsp;    <a href="Application.php">Application           </a><br /> 
            &nbsp;&nbsp;&nbsp; <a href="ServeurWindows.php">Serveur Windows       </a><br />
            &nbsp;&nbsp;&nbsp;         <a href="Images.php">En images             </a><br />
            &nbsp;&nbsp;&nbsp;   <a href="Technilisees.php">Matériel              </a><br />
            &nbsp;&nbsp;&nbsp;            <a href="Qui.php">De qui parle-t-on?    </a><br />
    </td>
    </tr>
    
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzVersion.png" onclick="togglevisible('vertd');" />
    </td>
    </tr>
    <tr>
    <td style="display:none;" id="vertd">
    	<center>Version 0.8 Bêta -</center>
        <span style="font-size:12px; color:#555">
        Derrnière mise à jour le premier novembre 2011.
        </span>
    </td>
    </tr>
    
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzObtenir.png" onclick="togglevisible('obttd');" />
    </td>
    </tr>
    <tr>
    <td style="display:none;" id="obttd">
    	<center>
    	Télécharger -
    	</center>
        <span style="font-size:12px; color:#555">
        <a href="#">Application Android</a><br />
        <a href="#">Serveur Windows</a><br />
        <a href="Tranzoid - Document Analyse.pdf">Document d'analyse</a><br />
        </span>
    </td>
    </tr>
    
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzContacts.png" onclick="togglevisible('contd');" />
    </td>
    </tr>
    <tr>
    <td style="display:none;" id="contd">
    	<center>
    	Références -
    	</center>
        <span style="font-size:12px; color:#555">
        <a href="http://developer.android.com/index.html">Développeur Android</a><br />
        <a href="http://www.clg.qc.ca/">Collège Lionel-Groulx</a><br />
        <a href="http://www.multiservicessd.com/">Multiservices SD</a><br />
        <a href="http://www.mozilla.org/fr/firefox/fx/">Mozilla Firefox</a><br />
        <a href="http://www.kubuntu.org/">Kubuntu</a><br />
        <a href="http://www.eclipse.org/downloads/">Eclipse</a><br />
        </span>
    </td>
    </tr>           
    </table>
    
    
  </td>
  <td valign="top" width="800" style="font-family:Calibri">
  	<!-- InstanceBeginEditable name="EditableBody" -->
    <center><span style="font-family:'Hand Of Sean'; font-size:24px;">Une photo vaut mille mots</span><br />
    <img src="../IMGS/Tz/TzHr.jpg" width="700" height="5" /><br /></center>
    <b style="text-align:right">La première section porte sur le serveur Windows.</b><br />
    <table>
    <tr>
    <td width="258">
    	<a href="../IMGS/Tz/Server SS1.jpg"><img src="../IMGS/Tz/Server SS1.jpg" width="250" /></a>
    </td>
    <td width="530">
    	Cette première image ilustre le serveur à sa première ouverture. À gauche, il y a le status de celui-ci ainsi que votre adresse IP. C'est celle-ci qu'il faudra écrire dans l'application sur votre cellulaire lors de la première connexion. À droite, on peut voir les utilisateurs qui ont été ajoutés par la personne qui gère le serveur. Oh mon Dieu, mais on parle de vous sur internet!
    </td>
    </tr>

    <tr>
    <td>
    	<a href="../IMGS/Tz/Server SS2.jpg"><img src="../IMGS/Tz/Server SS2.jpg" width="250" /></a>
    </td>
    <td>
    	Ici, on peut observer la fenêtre d'ajout et de modification d'un utilisateur. Comme vous pouvez le voir, elle est très simple et le tout est facile à comprendre au premier balayage d'yeux. Évidemment, il est préferable de balayer avec un balai.
    </td>
    </tr>    

    <tr>
    <td>
    	<a href="../IMGS/Tz/Server SS3.jpg"><img src="../IMGS/Tz/Server SS3.jpg" width="250" /></a>
    </td>
    <td>
    	Voici de quoi le serveur à l'air quand un utilisateur est connecté au serveur. Une petite fenêtre apparait vous avertissant de l'événement ainsi qu'une petite tonalité qui peut être enlevé à votre aise. Remarqué le status du serveur à gauche et l'icon de l'utilisateur à droite. Dans la vraie vie, il n'y a pas d'ondes rouges qui sortent de l'antenne de votre téléphone. Garde-à-vous.
    </td>
    </tr>  
    
    </table>
	<center>
    <span style="font-family:'Hand Of Sean'; font-size:14px;">
    <br />
    Le tout est bien entendu paramètrisable de sens que vous pouvez changer la couleur de pratiquement tout ce qui se trouve dans cette fenêtre.<br />
    De plus, vous pouvez changer quelques trucs de l'interface qui pourait vous déranger.<br />
    Tranzoid comprend un serveur qui peut prendre la forme que vous voulez.<br />
    La puissance de Tranzoid provient au départ de vos idées.<br /><br /></span>
    <img src="../IMGS/Tz/TzHr.jpg" alt="" width="700" height="5" /><br />
    </center>
	
    <b style="text-align:right">Cette deuxième section porte sur l'application Android.</b><br />
        <table>
    <tr>
    <td width="112">
    	<a href="../IMGS/Tz/Accueil.jpg"><img src="../IMGS/Tz/Accueil.jpg" height="190" /></a>
    </td>
    <td width="676">
    	Voici la première chose que vous allez voir en ouvrant l'application pour la première fois sur votre supertéléphone ou téléphone intelligent Android. À vous de choisir si vous avez le courage de quelqu'un qui contrôle à distance son matériel ou si vous préférez la solution lâche et obèse.
    </td>
    </tr>
    
    <tr>
    <td>
    	<a href="../IMGS/Tz/Menu Principal.jpg"><img src="../IMGS/Tz/Menu Principal.jpg" height="190" /></a>
    </td>
    <td>
    	Vous y êtes! Le menu principal! C'est ici que tout se passe. La dernière mise à jour comprend maintenant les commandes vocales qui peuvent être utilisées directement du menu. Pas mal ça non? De toute façon, notre génération a tellement une grande "gueule" que aussi bien DIRE à notre téléphone quoi faire que de lui faire faire des affair-- Ah. Menu principal, voilà!
    </td>
    </tr>    

    <tr>
    <td>
    	<a href="../IMGS/Tz/Connexion.jpg"><img src="../IMGS/Tz/Connexion.jpg" height="190" /></a>
    </td>
    <td>
    	<i>"C bcp tro compliké! Je compren po komen sa march!"</i><br />
        Si vous avez trouvez ce qui précède moindrement comique, vous avez le cerveau nécessaire pour utiliser le tout! Félicitation capitaine, vous pouvez maintenant vous connecter à votre serveur Windows grâce à une simple adresse afficher sur la première page du serveur, votre utilisateur et bien entendu votre mot de passe.
    </td>
    </tr>

    <tr>
    <td>
    	<a href="../IMGS/Tz/Download.jpg"><img src="../IMGS/Tz/Download.jpg" height="190" /></a>
    </td>
    <td>
    	Ça ressemble à Windows ça! Pas autant que Windows lui-même mais c'est toujours mieux que rien. Ici vous avez la chance de naviguer dans vos fichiers tout en... Ah et faites ce que vous voulez, je ne vous direz pas quoi faire! Sauf bien entendu noter cette application cinq étoiles sur le market. Sur cette page, il vous est possible de télécharger des fichiers de votre ordi à votre cellulaire ou encore de les exécuter à distance. Et si ça c'est pas du confort!
    </td>
    </tr>
    
    </table>
	
	<!-- InstanceEndEditable --></td>
  </tr>
  </table></div>
  <div class="footer">
    <p style="font-size:10px;">
    <a href="http://www.multiservicessd.com/Tranzoid/">Tranzoid - 2011</a> | 
    <a href="http://www.clg.qc.ca/">Collège Lionel-Groulx</a> | 
    <a href="mailto:erik@multiservicessd.com">Érik Desjardins</a> | 
    <a href="http://developer.android.com/index.html">Android - Google</a> | 
    <a href="http://www.mozilla.org/fr/firefox/fx/">Mozilla Firefox</a> | 
    <a href="http://www.kubuntu.org/">Kubuntu</a> | 
    <a href="http://www.eclipse.org/downloads/">Eclipse</a> |
     <a href="http://www.microsoft.com/visualstudio/fr-ca">Visual Studio</a>
     </p>
    <!-- end .footer --></div>
  <!-- end .container --></div>
</body>
<!-- InstanceEnd --></html>
