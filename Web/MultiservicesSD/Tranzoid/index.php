<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/Tranzoid_Template.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Tranzoid ~ Accueil</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #42413C;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}
/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color: #42413C;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

/* ~~ this fixed width container surrounds the other divs ~~ */
.container {
	width: 1024px;
	max-width:1024px;
	min-width:1024px;
	background: #FFF;
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout */
}

/* ~~ the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo ~~ */
.header {
	background: #ADB96E;
}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/

.content {

	padding: 10px 0;
}

/* ~~ The footer ~~ */
.footer {
	padding: 10px 0;
	background: #CCC49F;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

div.content a:visited
{
	color:#93F;
}

div.content a:active
{
	text-decoration:none;	
	font-size:12px;
	text-transform:none;
	color:#90F;		
}

div.content a
{
	text-decoration:none;
	text-transform:none;
	color:#90F;		
}

div.content a:hover
{
	text-decoration:underline;
	color:#93F;
}

@font-face {
    font-family:"Hand Of Sean";
	src: url("../Fonts/handsean.ttf");
}
-->
</style>
<script language="javascript" type="text/javascript">
function togglevisible(elemname)
{
	var tdrow = document.getElementById(elemname);
	if (tdrow.style.display == "none") tdrow.style.display = "block";	
	else tdrow.style.display = "none";	
}

</script>

</head>

<body>

<div class="container">
  <div class="header"><a href="index.php"><img src="../IMGS/Tz/TzTitle.jpg" alt="Insert Logo Here" name="Insert_logo" /></a> 
  <img src="../IMGS/Tz/TzHr.jpg" /></div>
  <div class="content" style="background-image:url(../IMGS/Tz/TzBG.jpg); background-repeat:no-repeat;">
  	
  <!-- end .content -->
  <table>
  <tr>
  <td width="200" valign="top">
  	
    <table>
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzNavigation.png" onclick="togglevisible('navtd');" />
    </td>
    </tr>
    
    <tr>
    <td style="font-family:'calibri'; display:block;" id="navtd">
            &nbsp;&nbsp;&nbsp;          <a href="index.php">Accueil               </a><br />
            &nbsp;&nbsp;&nbsp;    <a href="Application.php">Application           </a><br /> 
            &nbsp;&nbsp;&nbsp; <a href="ServeurWindows.php">Serveur Windows       </a><br />
            &nbsp;&nbsp;&nbsp;         <a href="Images.php">En images             </a><br />
            &nbsp;&nbsp;&nbsp;   <a href="Technilisees.php">Matériel              </a><br />
            &nbsp;&nbsp;&nbsp;            <a href="Qui.php">De qui parle-t-on?    </a><br />
    </td>
    </tr>
    
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzVersion.png" onclick="togglevisible('vertd');" />
    </td>
    </tr>
    <tr>
    <td style="display:none;" id="vertd">
    	<center>Version 0.8 Bêta -</center>
        <span style="font-size:12px; color:#555">
        Derrnière mise à jour le premier novembre 2011.
        </span>
    </td>
    </tr>
    
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzObtenir.png" onclick="togglevisible('obttd');" />
    </td>
    </tr>
    <tr>
    <td style="display:none;" id="obttd">
    	<center>
    	Télécharger -
    	</center>
        <span style="font-size:12px; color:#555">
        <a href="#">Application Android</a><br />
        <a href="#">Serveur Windows</a><br />
        <a href="Tranzoid - Document Analyse.pdf">Document d'analyse</a><br />
        </span>
    </td>
    </tr>
    
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzContacts.png" onclick="togglevisible('contd');" />
    </td>
    </tr>
    <tr>
    <td style="display:none;" id="contd">
    	<center>
    	Références -
    	</center>
        <span style="font-size:12px; color:#555">
        <a href="http://developer.android.com/index.html">Développeur Android</a><br />
        <a href="http://www.clg.qc.ca/">Collège Lionel-Groulx</a><br />
        <a href="http://www.multiservicessd.com/">Multiservices SD</a><br />
        <a href="http://www.mozilla.org/fr/firefox/fx/">Mozilla Firefox</a><br />
        <a href="http://www.kubuntu.org/">Kubuntu</a><br />
        <a href="http://www.eclipse.org/downloads/">Eclipse</a><br />
        </span>
    </td>
    </tr>           
    </table>
    
    
  </td>
  <td valign="top" width="800" style="font-family:Calibri">
  	<!-- InstanceBeginEditable name="EditableBody" -->
    <table>
    <tr>
    <td style="background-color:#EDF; border:thick; vertical-align:middle;" width="800" bordercolor="#FFFFFF">
    	<img src="../IMGS/Tz/TzUpdate.png" width="30" style="vertical-align:top;" /><b> MISE À JOUR : </b>La bêta 0.9 est maintenant disponible! Téléchargements en sus.
    </td>
    </tr>
    <tr>
    <td style="font-family:'Hand Of Sean'; font-size:24px;">
    	<br /><center>Contenu de la dernière mise à jour -</center>  
    </td>
    </tr>
    <tr>
    <td style="font-family:'Hand Of Sean'; font-size:14px;">
    	<table>
        <tr>
        <td width="200">
        </td>
        <td width="400">
        <img width="30"  src="../IMGS/Tz/icondownload.png"/> Navigation des disques durs<br />
        <img width="30"  src="../IMGS/Tz/iconmicro.png"/> Optimisation des commandes vocales<br />
        <img width="30"  src="../IMGS/Tz/iconmediacontrols.png"/> Télécommandes Windows Media Player<br />
        <img width="30"  src="../IMGS/Tz/TzIcon.png"/> Révision de l'interface graphique<br />
        <img width="30"  src="../IMGS/Tz/iconsystem.png"/> Correction de bugs<br />
        <img width="30"  src="../IMGS/Tz/iconinput.png" /> Contrôle du texte à distance, envoie de chaines
        </td>
        </tr>
        </table>
        <br />
    </td>
    </tr>
    <tr>
    <td>
    	<center><img src="../IMGS/Tz/TzHr.jpg" width="600" height="5" /></center>
    </td>
    </tr>
    <tr>
    <td>
    	<center><br />
        <span style="font-family:'Hand Of Sean'; font-size:24px;">Étapes d'installation de Tranzoid -</span><br /><br />
		<span style="font-family:'Hand Of Sean'">Étape #1:</span><br />
        Télécharger le serveur sur l'ordinateur et le configurer à votre aise<br />
        <span style="font-family:'Hand Of Sean'">Étape #2:</span><br />
        Télécharger l'application sur votre téléphone Android<br />
        <span style="font-family:'Hand Of Sean'">Étape #3:</span><br />
        Vous connecter et profiter d'une télécommande simple mais rapide!
        </center>
    </td>
    </tr>
    </table>
	
	<!-- InstanceEndEditable --></td>
  </tr>
  </table></div>
  <div class="footer">
    <p style="font-size:10px;">
    <a href="http://www.multiservicessd.com/Tranzoid/">Tranzoid - 2011</a> | 
    <a href="http://www.clg.qc.ca/">Collège Lionel-Groulx</a> | 
    <a href="mailto:erik@multiservicessd.com">Érik Desjardins</a> | 
    <a href="http://developer.android.com/index.html">Android - Google</a> | 
    <a href="http://www.mozilla.org/fr/firefox/fx/">Mozilla Firefox</a> | 
    <a href="http://www.kubuntu.org/">Kubuntu</a> | 
    <a href="http://www.eclipse.org/downloads/">Eclipse</a> |
     <a href="http://www.microsoft.com/visualstudio/fr-ca">Visual Studio</a>
     </p>
    <!-- end .footer --></div>
  <!-- end .container --></div>
</body>
<!-- InstanceEnd --></html>
