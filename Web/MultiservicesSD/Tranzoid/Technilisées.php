<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/Tranzoid_Template.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #42413C;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}
/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color: #42413C;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

/* ~~ this fixed width container surrounds the other divs ~~ */
.container {
	width: 1024px;
	max-width:1024px;
	min-width:1024px;
	background: #FFF;
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout */
}

/* ~~ the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo ~~ */
.header {
	background: #ADB96E;
}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/

.content {

	padding: 10px 0;
}

/* ~~ The footer ~~ */
.footer {
	padding: 10px 0;
	background: #CCC49F;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

a:visited
{
	color:#93F;
}

a:active
{
	text-decoration:none;	
	font-size:12px;
	text-transform:none;
	color:#90F;		
}

a
{
	text-decoration:none;	
	font-size:14px;
	text-transform:none;
	color:#90F;		
}

a:hover
{
	text-transform:uppercase;
	text-decoration:underline;
	color:#93F;
}

@font-face {
    font-family:"Hand Of Sean";
	src: url("file:///C|/Users/SD/Documents/SD/Fonts/handsean.ttf");
}
-->
</style>
<script language="javascript" type="text/javascript">
function shownav()
{
	var tdrow = document.getElementById("navtd");
	if (tdrow.style.visibility == "hidden") tdrow.style.visibility = "visible";	
	else tdrow.style.visibility = "hidden";	
}

</script>

</head>

<body>

<div class="container">
  <div class="header"><a href="#"><img src="../IMGS/Tz/TzTitle.jpg" alt="Insert Logo Here" name="Insert_logo" /></a> 
  <img src="../IMGS/Tz/TzHr.jpg" /></div>
  <div class="content" style="background-image:url(../IMGS/Tz/TzBG.jpg); background-repeat:no-repeat;">
  	
  <!-- end .content -->
  <table>
  <tr>
  <td width="200" valign="top">
  	
    <table>
    <tr>
    <td>
    	<img src="../IMGS/Tz/TzNavigation.png" onclick="shownav();" />
    </td>
    </tr>
    
    <tr>
    <td style="font-family:'Comic Sans MS', cursive; visibility:visible" id="navtd">
            &nbsp;&nbsp;&nbsp;       <a href="index.php">Accueil               </a><br />
            &nbsp;&nbsp;&nbsp; <a href="Application.php">Application           </a><br /> 
            &nbsp;&nbsp;&nbsp;       <a href="index.php">Serveur Windows       </a><br />
            &nbsp;&nbsp;&nbsp;       <a href="index.php">En images             </a><br />
            &nbsp;&nbsp;&nbsp;       <a href="index.php">Matériel              </a><br />
            &nbsp;&nbsp;&nbsp;       <a href="index.php">De qui parle-t-on?    </a><br />
    </td>
    </tr>
    </table>
    
    
  </td>
  <td valign="top" width="800" style="font-family:Calibri">
  	<!-- InstanceBeginEditable name="EditableBody" -->
        <center><span style="font-family:'Hand Of Sean'; font-size:24px;">Technilisées</span><br />
        <img src="../IMGS/Tz/TzHr.jpg" width="700" height="5" /><br /></center>
        <span style="text-align:right; color:#666; font-size:12px;">est un mot de j'ai inventé signifiant techniques utilisées</span><br />
        <span style="font-family:'Hand Of Sean'; font-size:14px; text-align:center">
        Bon, comment c'est fait? Oui, oui! C'est à vous que je parle! Vous ne savez pas encore? Voici la page qu'il vous faut alors!<br />
        </span>
        Le développement de Tranzoid se fait sur deux systèmes d'exploitations en même temps. Bien cetrainement, le serveur est contruit sous la plateforme Windows par Microsoft. L'application quant à elle est conçue sous la plateforme Linux distribution Kubuntu 11.04 avec des belles fenêtres qui brûlent à la fermeture.<br />
        Des logiciels? Ah, oui, bien entendu, il faut des logiciels pour construire un projet de sorte. Ceux-ci sont les suivants : <br />
        Sous Windows -
        <ol type="I">
        <li>Microsoft Visual Studio 2010</li>
        </ol>
        
     <!-- InstanceEndEditable --></td>
  </tr>
  </table></div>
  <div class="footer">
    <p style="font-size:10px;">Tranzoid - 2011 | Collège Lionel-Groulx | Érik Desjardins | Android - Google | Mozilla Firefox | Eclipse | Visual Studio</p>
    <!-- end .footer --></div>
  <!-- end .container --></div>
</body>
<!-- InstanceEnd --></html>
