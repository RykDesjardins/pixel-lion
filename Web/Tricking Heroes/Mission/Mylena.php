<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="background-color:#000; color:#999; font-family:'Courier New', Courier, monospace;">
<p><span style="font-family:'Comic Sans MS', cursive; font-size:24px;">
	<?php 
		if (!file_exists("answer.txt")){
	?>
    Mylena Gamache Godin
    </span>
    <br />
    Si vous acceptez cette mission, vous acceptez par de même de conserver<br />
    secret le contenu de l'enveloppe qui vous sera remise au point de rencontre.<br />
    Cette enveloppe contiendra des informations supplémentaires sur la mission<br />qui vous a été proposée.
    
<p>Pour plus d'information, vous pouvez télécharger la pièce jointe ci-dessous<br /> 
et ainsi pouvoir garder une copie de votre première rencontre.
<p>
 <a href="Rencontre.pdf#1.pdf" style="color:#FFF">Document joint</a>
 <br />
 <br />
 Après la lecture du document, vous pouvez accepter ou refuser la mission.<br />
 Dans les deux cas, cette page s'autodétruira après.<br />
 <br />
 Prendre votre temps sera de mise.
 <br /><br />
 <form action="Accept.php" method="post">
 <input type="submit" value="Accepter" id="BTN_ASubmit" style="width:200px;"/>
 </form>
  <form action="Decline.php" method="post">
 <input type="submit" value="Refuser" id="BTN_RSubmit"style="width:200px;" action="Decline.php" />
 </form>
 <?php } else { echo("Bon choix."); }?>
</body>
</html>