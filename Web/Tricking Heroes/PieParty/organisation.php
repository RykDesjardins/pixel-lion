<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/PartyTemplate.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #42413C;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}
/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color: #42413C;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

/* ~~ this fixed width container surrounds the other divs ~~ */
.container {
	width: 960px;
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout */
}

/* ~~ the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo ~~ */
.header {
	background: #FFF;
}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/

.content {
	background: #000;
	padding: 10px 0;
}

/* ~~ The footer ~~ */
.footer {
	padding: 10px 0;
	background: #CCC49F;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

@font-face 
{
    font-family:"fontscript";
	src: url("../Fonts/fontscript.TTF");
}
-->
</style></head>

<body style="background:url(IMGS/LightBackground.jpg); background-repeat:no-repeat; background-color:#000;">
 
<div class="container">
  <div class="header"><!-- end .header --><a href="index.php"><img src="IMGS/Banner.jpg" width="960" height="300" alt="Banniere" /></a><img src="../Tranzoid/IMGS/Tz/TzHr.jpg" width="961" height="5" /></div>
  <div class="content">
    
    <table style="color:#FFF; background:url(IMGS/FondParty.jpg); background-color:#000; background-repeat:no-repeat;">
    <tr>
    <td width="200" valign="top">
        <table width="195" style="color:#000" cellpadding="0" cellspacing="0">
        	<tr>
                <td width="20">
                    <img src="IMGS/TopLeftCorner.png" />
                </td>
                <td style="background:url(IMGS/Top.png); background-repeat:repeat-x;" width="700">
                
                </td>
                <td width="20">
                    <img src="IMGS/TopRightCorner.png" />
                </td>
            </tr>
            <tr>
                <td colspan="3" background="IMGS/WhiteBackground.png" height="10">
                    <img src="IMGS/NavigationTitre.png" />                
                </td>
            </tr>
            <tr>
                <td colspan="3" background="IMGS/WhiteBackground.png">
                    <img src="../Tranzoid/IMGS/CalendarIcon.png" width="20" /><a href="partys.php">Les partys</a>
                </td>
            </tr>
            <tr>
                <td colspan="3" background="IMGS/WhiteBackground.png">
                    <img src="../Tranzoid/IMGS/ShareIcon.png" width="20" /><a href="Gang.php">La gang</a>
                </td>
            </tr>
            <tr>
                <td colspan="3" background="IMGS/WhiteBackground.png">
                    <img src="../Tranzoid/IMGS/iconcontact.png" width="20" /><a href="organisation.php">Organisation</a>
                </td>
            </tr>
            <tr>
                <td colspan="3" background="IMGS/WhiteBackground.png">
                    <img src="../Tranzoid/IMGS/Tz/icondownload.png" width="20" /><a href="telechargement.php">Téléchargements</a>
                </td>
            </tr>    
            
        <tr>
        <td width="20">
        	<img src="IMGS/BottomLeftCorner.png" />
        </td>
        <td style="background:url(IMGS/Bottom.png); background-repeat:repeat-x;" width="700">
        
        </td>
        <td width="20">
        	<img src="IMGS/BottomRightCorner.png" />
        </td>
        </tr>                                    
        </table>
    </td>
    <td width="760" valign="top">
		<!-- InstanceBeginEditable name="EditRegion4" -->
        
		<span style="color:#000; font-size:18px;">
        Comment organisons-nous vos partys?
        </span>
        <br /><br />
        <span style="color:#000; font-size:12px;">
        
        Vos partys sont arganisés par nos pros. Nous avons tout ce qu'il faut pour faire de votre party une réussite totale!<br />
        Ce que vous avons à la carte : <br />
         - DJ de musique rock (Bon Jovi) et classique<br />
         - Boissons gaseuse<br />
         - Ballons de toutes les couleurs<br />
         - Nappes d'occasions<br />
         - Chandelles<br />
         - Et plus encore!<br /><br />
        
        Pas encore convaincu?<br />
        Nos partys sont les plus délirants au monde depuis longtemps!<br />
        Nous avons les meilleurs DJs du coin, la meilleur musique, les mailleures ambiances; laissez nous nous occuper de vos partys!
        <br /><br /><br />
        <br /><br /><br />
        <br /><br /><br />
        </span>
        
		<!-- InstanceEndEditable -->
    </td>
    </tr>
    </table>
    
    
    <!-- end .content --></div>
  <div class="footer">
    <p style="font-size:10px;">Pie Party - Érik Desjardins | Français 4 - Contre-Culture | American Pie | Dreamweaver </p>
    <!-- end .footer --></div>
  <!-- end .container --></div>
</body>
<!-- InstanceEnd --></html>
