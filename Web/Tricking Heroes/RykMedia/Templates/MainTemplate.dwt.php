<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- TemplateBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
<script type="application/javascript" language="javascript">

var bFooterShown = true;

function ToggleFooter(){
	if (bFooterShown){
		document.getElementById('footer').style.visibility = 'hidden';
		document.getElementById('footerbg').style.visibility = 'hidden';		
		document.getElementById('footerHandleHidden').style.visibility = 'visible';
	}else{
		document.getElementById('footer').style.visibility = 'visible';	
		document.getElementById('footerbg').style.visibility = 'visible';
		document.getElementById('footerHandleHidden').style.visibility = 'hidden';
	}
	
	bFooterShown = !bFooterShown;
}

</script>

<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #000;
	background-image:url(../Graphics/MainBackground.jpg);
	background-repeat:no-repeat;
	background-position:top;
	margin: 0;
	padding: 0;
	color: #EEE;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}
/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color: #42413C;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

/* ~~ this fixed width container surrounds the other divs ~~ */
.container {
	width: 1024px;
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout */
	background-image:url(../Graphics/TitleBanner.png);
	background-repeat:no-repeat;
	background-position:top;
}

/* ~~ the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo ~~ */
.header {

}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/

.content {
	position:absolute;
	bottom::100px;
	
	padding: 10px 0;
}

/* ~~ The footer ~~ */
.footer { 
	position: fixed; 
	clear: both; 
	width: 1024px; 
	height: 180px; 
	bottom: 0; 
	border: none; 
	padding: 13px 0 0 0; 
	text-align: center; 
	color: #FFFFFF;
	visibility:visible;
	z-index:800;
} 


.footerbar{
  position:absolute;
  top:0;
  left:0;
  width:1024px;
  background-color:#444;
  height:10px;
  z-index:900;
}

.dockedfooter{
	position: fixed; 
	clear: both; 
	width: 1024px; 
	height: 1px; 
	bottom: 0; 
	border: none; 
	padding: 13px 0 0 0; 
	text-align: center; 
	background-color: #444;
	visibility:hidden;
	z-index:900;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}
-->
</style></head>

<body>

<div class="container">


  <div class="header">
  	&nbsp;
    <div style="position:absolute; top:130px; width:475px; max-width:475px;">
	  <!-- TemplateBeginEditable name="LeftPane" -->
            This is where the Left Pane hardcore text goes
      <!-- TemplateEndEditable -->
  	</div>
    
    <div style="position:relative; top:240px; margin-left:500px; z-index:10;">
		<!-- TemplateBeginEditable name="RightPane" -->
            This is where the Right Pane hardcore text goes
        <!-- TemplateEndEditable -->
    </div>
    
  <div style="position:relative; top:-10px; margin-left:480px; z-index:5; ">
<div id="flashContent">
			<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="460" height="230" id="Banner" align="middle">
				<param name="movie" value="Flash/Banner.swf" />
				<param name="quality" value="best" />
				<param name="bgcolor" value="#ffffff" />
				<param name="play" value="true" />
				<param name="loop" value="true" />
				<param name="wmode" value="transparent" />
				<param name="scale" value="showall" />
				<param name="menu" value="true" />
				<param name="devicefont" value="false" />
				<param name="salign" value="" />
				<param name="allowScriptAccess" value="sameDomain" />
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="Flash/Banner.swf" width="460" height="230">
					<param name="movie" value="Flash/Banner.swf" />
					<param name="quality" value="best" />
					<param name="bgcolor" value="#ffffff" />
					<param name="play" value="true" />
					<param name="loop" value="true" />
					<param name="wmode" value="transparent" />
					<param name="scale" value="showall" />
					<param name="menu" value="true" />
					<param name="devicefont" value="false" />
					<param name="salign" value="" />
					<param name="allowScriptAccess" value="sameDomain" />
				<!--<![endif]-->
					<a href="http://www.adobe.com/go/getflash">
						<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
					</a>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>
		</div>
    </div>  
  
  
    <div class="footer" id="footer">
      <div id="footerHandleBTN" class="footerbar" onclick="ToggleFooter();"></div>
      <div id="footerbg" class="footer" style="z-index:-1; opacity:0.7; background-color:#111; background-image:url(../Graphics/FooterReflect.png)"></div>
         
      <div id="footer-content" style="z-index:1000;">
      	Footer
      </div>
    </div>  
    
    <div id="footerHandleHidden" class="dockedfooter" onclick="ToggleFooter();">
      
    </div> 
    
      
   </div>  
  </div><!-- end .container -->
</body>
</html>
