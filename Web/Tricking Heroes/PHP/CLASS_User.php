<?php
require_once("SCRIPT_MainDBConn.php");
require_once("SCRIPT_Points.php");

class User
{
	public $username;
	public $password;
	public $name;
	public $lastname;
	public $usertype;
	public $quote;
	public $age;
	public $desc;
	public $postcount;
	public $exp;
	public $level;
	public $title;
	public $photolink;
	public $row;
	public $newrow;
	public $location;
	
	public function __construct($infos)
	{
		if ($infos != "NULL")
		{
			$this->$row = mysql_fetch_row($infos);
			
			$this->$username = $row[0];
			$this->$password = $row[1];
			$this->$usertype = $row[2];
			$this->$name = $row[3];
			$this->$lastname = $row[4];
			$this->$quote = $row[5];
			$this->$age = $row[6];
			$this->$desc = $row[7];
			$this->$postcount = $row[8];
			$this->$exp = $row[9];
			$this->$level = $row[10];
			$this->$title = $row[11];
			$this->$photolink = $row[12];	
			$this->$location = $row[13];	
		}
	}
	
	private function CreateNewRow()
	{
		$this->$newrow = array();
		$this->$newrow[0] = $this->$username;
		$this->$newrow[1] = $this->$password;
		$this->$newrow[2] = $this->$usertype;
		$this->$newrow[3] = $this->$name;
		$this->$newrow[4] = $this->$lastname;
		$this->$newrow[5] = $this->$quote;
		$this->$newrow[6] = $this->$age;
		$this->$newrow[7] = $this->$desc;
		$this->$newrow[8] = $this->$postcount;
		$this->$newrow[9] = $this->$exp;
		$this->$newrow[10] = $this->$level;
		$this->$newrow[11] = $this->$title;
		$this->$newrow[12] = $this->$photolink;
		$this->$newrow[13] = $this->$location;
	}
	
	public function AddExp($num)
	{
		$this->$exp = $this->$exp + $num;
		if ($this->$exp > $this->$level * $this->$level * Points::$levelexplimit)
		{
			$this->$exp = $this->$exp - $this->$level * $this->$level * Points::$levelexplimit;
			$this->$level = $this->$level + 1;
		}
	}
	
	public function CommitUpdate()
	{
		ob_start();
	
		$db = mysql_connect($hostname_MainDBConn, $username_MainDBConn, $password_MainDBConn);
		mysql_select_db($database_MainDBConn, $db);
		mysql_query("SET NAMES UTF8");
		
		$sql = "UPDATE `a7792833_main`.`Users` SET `Username` = '[_0_]',
				`Password` = '[_1_]',
				`Usertype` = '[_2_]',
				`Firstname` = '[_3_]',
				`Lastname` = '[_4_]',
				`Quote` = '[_5_]',
				`Age` = [_6_],
				`Description` = '[_7_]',
				`Postcount` = '[_8_]',
				`Exp` = [_9_],
				`Level` = [_10_],
				`Title` = '[_11_]',
				`Photolink` = '[_12_]',
				`Location` = '[_13_]'
				 WHERE CONVERT( `Users`.`Username` USING utf8 ) = '[_0_]' LIMIT 1 ;";	
				
		CreateNewRow();			
		for ($i=0;$i<13;$i++) str_replace("[_" . "$i" . "_]", $this->$newrow[$i], $sql);
		
		mysql_query($sql, $MainDBConn) or die(mysql_error());
	}
}


?>