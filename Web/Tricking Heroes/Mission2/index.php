<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rencontre #2</title>
</head>

<body style="background-color:#000; color:#FFF; font-family:calibri">

	<?php 
		if (!file_exists("answer.txt")){
	?>

<span style="font-family:calibri; font-size:36px;">Rencontre #2 </span>
<hr />
<p>
Comme mentionné dans le document, vous aurez à vous rendre dans l'établissement de nuit nommé le HB.<br />
Votre mission est simple. Vous devez faire mine de rien et prendre un verre tranquille tout en analysant <br />
le comportement des gens qui y seront. <br /><br />
Si vous trouvez un comportement suspect, notez bien les caractéristiques de l'individu en question.<br />
Sinon, prenez la soirée tranquille!<br /><br />
</p>
<h4>Disponibilités</h4>
<form method="post" action="sub.php"  >
<table>

<tr>
<td>
Jeudi soir
</td>
<td>
<input type="checkbox" id="CBJeudi" name="CBJeudi" />
</td>
</tr>

<tr>
<td width="133">
Vendredi soir
</td>
<td width="360">
<input type="checkbox" id="CBVendredi" name="CBVendredi" />
</td>
</tr>

<tr>
<td>
Samedi soir
</td>
<td>
<input type="checkbox" id="CBSamedi" name="CBSamedi" />
</td>
</tr>

<tr>
<td>
Dimanche soir
</td>
<td>
<input type="checkbox" id="CBDimanche" name="CBDimanche" />
</td>
</tr>

<tr>
<td colspan="2">
<hr />
</td>
</tr>

<tr>
<td>
Votre drink favoris
</td>
<td>
<input type="text"  id="TBDrink" name="TBDrink" />
</td>
</tr>

<tr>
<td>
Serez vous...
</td>
<td>
<input type="radio"  id="RBAlone" name="RBAlone"  value="Accompagnée" /> accompagnée
<input type="radio"  id="RBAlone" name="RBAlone" value="Seule" /> seule
</td>
</tr>

</table>

<br /><br />
<h4>Annexe</h4>
Une fois le tout bien complet, vous n'avez qu'à confirmer l'acceptation de la mission.<br />
Si vous avez plusieurs journées de disponible, l'individu que vous allez rencontrer vous<br />
contactera pour vous confirmer la date à laquelle la rencontre pourra avoir lieu<br /><br />
<input type="submit" value="Accepter" style="width:300px; height:60px; font-size:18px;"  />
</form>

<?php } else { ?>
À bientôt!
<?php } ?>

</body>
</html>