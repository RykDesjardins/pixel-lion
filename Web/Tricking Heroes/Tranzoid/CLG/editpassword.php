<?php

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

// Récupération des objets session
// session_start();

// Objets de base
$valide = true;
$reason = "";

// Le mot de passe
$newpassword = $_POST['tbpassword'];
$confirmation = $_POST['tbpasswordc'];

// Validation
if ($newpassword != $confirmation) 
{
    header('Location: profile.php?change=failed&reason=diff');	
	exit;
}

if (strlen($newpassword) < 8)              $valide = false;
if (!preg_match('/[A-Z]/', $newpassword))  $valide = false;
if (!preg_match('/[a-z]/', $newpassword))  $valide = false;
if (!preg_match('/[0-9]/', $newpassword))  $valide = false;
if (!strstr($newpassword, ":") === false)  $valide = false;
if (!strpos($newpassword, "\\") === false) $valide = false;
if (!strpos($newpassword, "<") === false)  $valide = false;
if (!strpos($newpassword, ">") === false)  $valide = false;

try
{
	if ($valide) 
	{
		// Création des objets
		$source = 'users.csv';
		$target = 'temp_users.txt';
		$username = $_POST['husername'];
		$userlength = strlen($username);
		$cpt = 0;
		$userindex = $_POST['hindex'];
		$nomcomplet = $_POST['hcompletename'];
		
		// Copier les valeurs
		$sh = fopen($source, 'r');
		$th = fopen($target, 'a+');
		
		while (!feof($sh)) 
		{
			$cpt++;
			$line = fgets($sh);
			if ($cpt == $userindex) $line = substr($line, 0, $userlength) . ":" . $newpassword . ":" . $nomcomplet . "\n";
			fwrite($th, $line);
		}
		fclose($sh);
		fclose($th);
		
		// Supprimer le fichier source
		unlink($source);
		
		// Renommer le fichier temporaire
		rename($target, $source);
	
		// Redirection
		header('Location: profile.php?change=success&validation=success');
	}
	else
	{
		// Redirection
		header('Location: profile.php?change=failed&reason=validation');
	}
}
catch (Exception $e)
{
	header('Location: profile.php?change=failed&reason=exception');
}

?>