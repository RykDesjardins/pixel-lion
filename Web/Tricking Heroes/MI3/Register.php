<?php
	require_once("PHP/functions.php");
	
	function ValidateTelephone($tel)
	{
		$tel = str_replace('-', '', $tel);
		$tel = str_replace('+', '', $tel);
		$tel = str_replace(' ', '', $tel);
		$tel = str_replace('(', '', $tel);
		$tel = str_replace(')', '', $tel);
		if (strlen($tel) < 10) return false;
		if (!is_numeric($tel)) return false;
		
		return true;
	}
	
	if (isset($_POST['createuser']))
	{
		// Validation
		$firstname = 	$_POST['TBfirstname'];
		$lastname = 	$_POST['TBlastname'];
		$address = 		$_POST['TBaddress'];
		$city = 		$_POST['TBcity'];
		$state = 	 	substr($_POST['TBstate'], 4);
		$country =		substr($_POST['TBstate'], 0, 3);
		$telephone1 = 	$_POST['TBtelephone1'];
		$telephone2 = 	$_POST['TBtelephone2'];
		$email = 		$_POST['TBemail'];
		$username = 	$_POST['TBusername'];
		$password = 	$_POST['TBpassword'];
		
		$validation = array();
		if ($firstname == "") 	$validation[count($validation)] = "*First name is empty";
		if ($lastname == "") 	$validation[count($validation)] = "*Last name is empty";
		if ($address == "") 	$validation[count($validation)] = "*Address is empty";
		if ($city == "") 		$validation[count($validation)] = "*City is empty";
		if ($state == "") 		$validation[count($validation)] = "*State/Province is empty";
		if ($telephone1 == "") 	$validation[count($validation)] = "*Telephone 1 is empty";
		if ($email == "") 		$validation[count($validation)] = "*Email is empty";
		if ($username == "") 	$validation[count($validation)] = "*Username is empty";
		if ($password == "") 	$validation[count($validation)] = "*Password is empty";
		
		if (!ValidateTelephone($telephone1))
		{
			$validation[count($validation)] = "*Telephone is not valid";
		}
		
		if( strlen($password) < 8 ) $validation[count($validation)] = "*Password must contain 8 characters or more";
		
		if (count($validation) == 0)
		{
			$db = DBConnect();
			PrepareSQL($db);
			$exists = mysql_fetch_row(ExeQuery($db, "SELECT COUNT(*) FROM `utils` WHERE U_NAME = '$username'")); 
			if ($exists[0] != 0)
			{
				$validation[count($validation)] = "*Username already exists " . $exists;
			}
			else
			{
				AddUser(array($username, $password, $firstname, $lastname, $address, $city, $state, $country, $telephone1, $telephone2, $email));
				ENDSql($db);
				
				session_start();
				$_SESSION['MM_Username'] = $username;
				header("location: Intro.php");	
			}
			ENDSql($db);
		}
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Register</title>
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #4E5869;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Element/tag selectors ~~ */
ul, ol, dl { /* Due to variations between browsers, it's best practices to zero padding and margin on lists. For consistency, you can either specify the amounts you want here, or on the list items (LI, DT, DD) they contain. Remember that what you do here will cascade to the .nav list unless you write a more specific selector. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* removing the top margin gets around an issue where margins can escape from their containing div. The remaining bottom margin will hold it away from any elements that follow. */
	padding-right: 15px;
	padding-left: 15px; /* adding the padding to the sides of the elements within the divs, instead of the divs themselves, gets rid of any box model math. A nested div with side padding can also be used as an alternate method. */
}
a img { /* this selector removes the default blue border displayed in some browsers around an image when it is surrounded by a link */
	border: none;
}

/* ~~ Styling for your site's links must remain in this order - including the group of selectors that create the hover effect. ~~ */
a:link {
	color:#414958;
	text-decoration: underline; /* unless you style your links to look extremely unique, it's best to provide underlines for quick visual identification */
}
a:visited {
	color: #4E5869;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* this group of selectors will give a keyboard navigator the same hover experience as the person using a mouse. */
	text-decoration: none;
}

input, select {
	height: 30px;
	width: 300px;
	font-size:18px;	
}
select {
	font-size:12px;	
}
/* ~~ this container surrounds all other divs giving them their percentage-based width ~~ */
.container {
	width: 60%;
	max-width: 1260px;/* a max-width may be desirable to keep this layout from getting too wide on a large monitor. This keeps line length more readable. IE6 does not respect this declaration. */
	min-width: 780px;/* a min-width may be desirable to keep this layout from getting too narrow. This keeps line length more readable in the side columns. IE6 does not respect this declaration. */
	background: #FFF;
	margin: 0 auto; /* the auto value on the sides, coupled with the width, centers the layout. It is not needed if you set the .container's width to 100%. */
}

/* ~~the header is not given a width. It will extend the full width of your layout. It contains an image placeholder that should be replaced with your own linked logo~~ */
.header {
	background: #6F7D94;
}

/* ~~ This is the layout information. ~~ 

1) Padding is only placed on the top and/or bottom of the div. The elements within this div have padding on their sides. This saves you from any "box model math". Keep in mind, if you add any side padding or border to the div itself, it will be added to the width you define to create the *total* width. You may also choose to remove the padding on the element in the div and place a second div within it with no width and the padding necessary for your design.

*/
.content {
	padding: 10px 0;
}

/* ~~ This grouped selector gives the lists in the .content area space ~~ */
.content ul, .content ol { 
	padding: 0 15px 15px 40px; /* this padding mirrors the right padding in the headings and paragraph rule above. Padding was placed on the bottom for space between other elements on the lists and on the left to create the indention. These may be adjusted as you wish. */
}

/* ~~ The footer ~~ */
.footer {
	padding: 10px 0;
	background: #6F7D94;
}

/* ~~ miscellaneous float/clear classes ~~ */
.fltrt {  /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page. The floated element must precede the element it should be next to on the page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class can be placed on a <br /> or empty div as the final element following the last floated div (within the #container) if the #footer is removed or taken out of the #container */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}
.titlebar {
	background: #002152;
	background-image:url(Images/TitleBG.png);
	background-position:center;
	background-repeat:no-repeat;
	width:100%;
}
-->
</style></head>

<body>

<div style="" class="titlebar">
	<img src="Images/logo.png" width="268" height="52" />
</div>

<div class="container">
  <div class="header">
  </div>
  
  <div class="content">
  	
    <div style="background-color:#EEE; width:80%; border-color:#CCC; border-width:1px; margin: 0px auto; font-size:12px;
">
    	<h2 style="margin-bottom:0px; font-size:26px;">Register</h2>
        	<div style="width:90%; margin:0px auto;">
            
            	<p style="color:red;">
            	<?php
					if ($validation != null)
					{
						foreach($validation as $v)
						{
							echo($v . "</br >");
						}
					}
				?>
            	</p>
                <form method="post" name="Fcreateuser" id="Fcreateuser">
                <table>
                    <tr>
                        <td style="text-align:left; width:150px;">
                            First name
                        </td>
                        <td>
                            <input type="text" name="TBfirstname" value="<?=$firstname?>" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            Last name
                        </td>
                        <td>
                            <input type="text" name="TBlastname" value="<?=$lastname?>" />
                        </td>
                    </tr>
                    <tr>
                    	<td colspan="2"> 
                        	<hr />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            Address
                        </td>
                        <td>
                            <input type="text" name="TBaddress"  value="<?=$address?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            City
                        </td>
                        <td>
                            <input type="text" name="TBcity"  value="<?=$city?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            State/province
                        </td>
                        <td>
                            <select name="TBstate">
                            
                                <optgroup label="United States">
                                 
                                    <option id="USA-AL" value="USA-AL">Alabama (AL)</option>
                                    <option id="USA-AK" value="USA-AK">Alaska (AK)</option>
                                    <option id="USA-AZ" value="USA-AZ">Arizona (AZ)</option>
                                    <option id="USA-AR" value="USA-AR">Arkansas (AR)</option>             
                                    <option id="USA-CA" value="USA-CA">California (CA)</option>
                                    <option id="USA-CO" value="USA-CO">Colorado (CO)</option>
                                    <option id="USA-CT" value="USA-CT">Connecticut (CT)</option>
                                    <option id="USA-DE" value="USA-DE">Delaware (DE)</option>
                                    <option id="USA-DC" value="USA-DC">District of Columbia (DC)</option>    
                                    <option id="USA-FL" value="USA-FL">Florida (FL)</option>
                                    <option id="USA-GA" value="USA-GA">Georgia (GA)</option>
                                    <option id="USA-GU" value="USA-GU">Guam (GU)</option>
                                    <option id="USA-HI" value="USA-HI">Hawaii (HI)</option>
                                    <option id="USA-ID" value="USA-ID">Idaho (ID)</option>
                                    <option id="USA-IL" value="USA-IL">Illinois (IL)</option>
                                    <option id="USA-IN" value="USA-IN">Indiana (IN)</option>
                                    <option id="USA-IA" value="USA-IA">Iowa (IA)</option>
                                    <option id="USA-KS" value="USA-KS">Kansas (KS)</option>
                                    <option id="USA-KY" value="USA-KY">Kentucky (KY)</option>
                                    <option id="USA-LA" value="USA-LA">Louisiana (LA)</option>
                                    <option id="USA-ME" value="USA-ME">Maine (ME)</option>
                                    <option id="USA-MD" value="USA-MD">Maryland (MD)</option>
                                    <option id="USA-MA" value="USA-MA">Massachusetts (MA)</option>
                                    <option id="USA-MI" value="USA-MI">Michigan (MI)</option>
                                    <option id="USA-MN" value="USA-MN">Minnesota (MN)</option>
                                    <option id="USA-MS" value="USA-MS">Mississippi (MS)</option>
                                    <option id="USA-MO" value="USA-MO">Missouri (MO)</option>
                                    <option id="USA-MT" value="USA-MT">Montana (MT)</option>
                                    <option id="USA-NE" value="USA-NE">Nebraska (NE)</option>
                                    <option id="USA-NV" value="USA-NV">Nevada (NV)</option>
                                    <option id="USA-NH" value="USA-NH">New Hampshire (NH)</option>
                                    <option id="USA-NJ" value="USA-NJ">New Jersey (NJ)</option>
                                    <option id="USA-NM" value="USA-NM">New Mexico (NM)</option>
                                    <option id="USA-NY" value="USA-NY">New York (NY)</option>
                                    <option id="USA-NC" value="USA-NC">North Carolina (NC)</option>
                                    <option id="USA-ND" value="USA-ND">North Dakota (ND)</option>
                                    <option id="USA-OH" value="USA-OH">Ohio (OH)</option>
                                    <option id="USA-OK" value="USA-OK">Oklahoma (OK)</option>
                                    <option id="USA-OR" value="USA-OR">Oregon (OR)</option>
                                    <option id="USA-PA" value="USA-PA">Pennsylvania (PA)</option>
                                    <option id="USA-PR" value="USA-PR">Puerto Rico (PR)</option>
                                    <option id="USA-RI" value="USA-RI">Rhode Island (RI)</option>
                                    <option id="USA-SC" value="USA-SC">South Carolina (SC)</option>
                                    <option id="USA-SD" value="USA-SD">South Dakota (SD)</option>
                                    <option id="USA-TN" value="USA-TN">Tennessee (TN)</option>
                                    <option id="USA-TX" value="USA-TX">Texas (TX)</option>
                                    <option id="USA-UT" value="USA-UT">Utah (UT)</option>
                                    <option id="USA-VT" value="USA-VT">Vermont (VT)</option>
                                    <option id="USA-VA" value="USA-VA">Virginia (VA)</option>
                                    <option id="USA-VI" value="USA-VI">Virgin Islands (VI)</option>
                                    <option id="USA-WA" value="USA-WA">Washington (WA)</option>
                                    <option id="USA-WV" value="USA-WV">West Virginia (WV)</option>
                                    <option id="USA-WI" value="USA-WI">Wisconsin (WI)</option>
                                    <option id="USA-WY" value="USA-WY">Wyoming (WY)</option>
                                  
                                </optgroup>
                               
                                <optgroup label="Canada">
                               
                                    <option id="CAN-AB" value="CAN-AB">Alberta (AB)</option>
                                    <option id="CAN-BC" value="CAN-BC">British Columbia (BC)</option>
                                    <option id="CAN-MB" value="CAN-MB">Manitoba (MB)</option>
                                    <option id="CAN-NB" value="CAN-NB">New Brunswick (NB)</option>
                                    <option id="CAN-NL" value="CAN-NL">Newfoundland and Labrador (NL)</option>
                                    <option id="CAN-NT" value="CAN-NT">Northwest Territories (NT)</option>
                                    <option id="CAN-NS" value="CAN-NS">Nova Scotia (NS)</option>
                                    <option id="CAN-NU" value="CAN-NU">Nunavut (NU)</option>
                                    <option id="CAN-ON" value="CAN-ON">Ontario (ON)</option>
                                    <option id="CAN-PE" value="CAN-PE">Prince Edward Island (PE)</option>
                                    <option id="CAN-QC" value="CAN-QC">Quebec (QC)</option>
                                    <option id="CAN-SK" value="CAN-SK">Saskatchewan (SK)</option>
                                    <option id="CAN-YT" value="CAN-YT">Yukon (YT)</option>
                                  
                                </optgroup>
                               
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td colspan="2">
                        	<hr />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            Telephone 1
                        </td>
                        <td>
                            <input type="text" name="TBtelephone1"  value="<?=$telephone1?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            Telephone 2
                        </td>
                        <td>
                            <input type="text" name="TBtelephone2"  value="<?=$telephone2?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            Email
                        </td>
                        <td>
                            <input type="text" name="TBemail"  value="<?=$email?>"/>
                        </td>
                    </tr>
                    <tr>
                    	<td colspan="2">
                        	<hr />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            Username
                        </td>
                        <td>
                            <input type="text" name="TBusername"  value="<?=$username?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;">
                            Password
                        </td>
                        <td>
                            <input type="password" name="TBpassword"  value="<?=$password?>"/>
                        </td>
                    </tr> 
                    <tr>
                    	<td colspan="2">
                        	<hr />
                        </td>
                    </tr>                                                           
                    <tr>
                    	<td align="right" colspan="2" height="60" valign="bottom">
                        	<input type="submit" name="createuser" id="createuser" value="Create User" style="width:100%; height:40px;" />
                        </td>
                    </tr>
                </table>
                </form>
            </div>
    </div> 
    <!-- end .content -->
  </div>
  <div class="footer">
    <p>&nbsp;</p>
    <!-- end .footer --></div>
  <!-- end .container --></div>
</body>
</html>