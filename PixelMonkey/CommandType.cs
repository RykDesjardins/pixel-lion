﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PixelMonkey
{
    // Fonction       : A fonction called on itsown 
    // Method         : An object calling one of its method
    // Affectation    : Creation of an object or variable with a value
    // Property       : Alteration of an existing object's property's value
    // Condition      : An IF or UNLESS block
    // Loop           : A WHILE of UNTIL block
    // Choice         : A CHOICE block
    // Answer         : An ANSWER block
    // Get            : Get's a variable's value
    public enum CommandType
    {
        Fonction, Method, Affectation, Property, Condition, Loop, Choice, Answer, Get, BlockOpening, BlockClosure, Skiped, Comment
    }
}
