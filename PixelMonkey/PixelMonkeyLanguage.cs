﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;

namespace PixelMonkey
{
    public class PixelMonkeyLanguage
    {
        const String CoreKW         = "core";
        const String FonctionKW     = "function";
        const String InParameterKW  = "gets";
        const String OutParameterKW = "returns";
        const String CallingKW      = "calls";
        const String ClassKW        = "class";
        const String CtorKW         = "make";
        const String PropertyKW     = "has";
        const String TypeKW         = "for";
        const String MethodKW       = "does";
        const String CommentKW      = "#";

        Hashtable Types;
        List<PixelMonkeyFonction> Functions;
        List<PixelMonkeyClass> Classes;
        Type CoreClass;

        public PixelMonkeyLanguage()
        {
            Types = new Hashtable();
            Functions = new List<PixelMonkeyFonction>();
            Classes = new List<PixelMonkeyClass>();

            Types["STRING"] = typeof(String);
            Types["SWITCH"] = typeof(Boolean);
            Types["NUMBER"] = typeof(Double);
        }

        public Type GetCoreClass()
        {
            return CoreClass;
        }

        public IEnumerable<PixelMonkeyFonction> GetFunctions()
        {
            foreach (PixelMonkeyFonction function in Functions) yield return function;
        }

        public List<PixelMonkeyFonction> GetFunctionList()
        {
            return Functions;
        }

        public IEnumerable<PixelMonkeyClass> GetClasses()
        {
            foreach (PixelMonkeyClass pmclass in Classes) yield return pmclass;
        }

        public List<PixelMonkeyClass> GetClassList()
        {
            return Classes;
        }

        public String GetTypeToStr(Type type)
        {
            foreach (String key in Types.Keys) if ((Type)Types[key] == type) return key;
            
            return String.Empty;
        }

        public static PixelMonkeyLanguage LoadFromString(String str)
        {
            Byte[] byteArray = Encoding.ASCII.GetBytes(str);
            MemoryStream stream = new MemoryStream(byteArray);
            StreamReader reader = new StreamReader(stream);

            PixelMonkeyLanguage obj = new PixelMonkeyLanguage();
            obj.Load(reader);

            return obj;
        }

        public void Load(StreamReader reader)
        {
            String line = String.Empty;

            try
            {
                while (line == String.Empty)
                {
                    line = reader.ReadLine();

                    if (line == null) throw new PixelMonkeyCoreException("Core file is not well-formated.");
                    else if (line.Trim().StartsWith("#") || line.Trim() == String.Empty) line = String.Empty;
                }

                if (!line.StartsWith(CoreKW)) throw new PixelMonkeyCoreException("Missing core class. A PMSL Core file must start with a core indication.");

                else
                {
                    try
                    {
                        String typename = line.Split(' ')[1];
                        CoreClass = Type.GetType(typename);

                        if (CoreClass == null) throw new TypeLoadException();
                    }
                    catch (TypeLoadException)
                    {
                        throw new PixelMonkeyCoreException("Core class '" + line.Split(' ')[1] + "' cannot be found in current project");
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new PixelMonkeyCoreException("Core class indication is not well-fromated. The correct form is : core [CoreClassName]");
                    }
                }

                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Trim() == String.Empty || line.Trim().StartsWith(CommentKW)) continue;

                    String keyword = line.Substring(0, line.IndexOf(' '));

                    switch (keyword)
                    {
                        case FonctionKW:
                            LoadFunction(line);
                            break;

                        case ClassKW:
                            LoadClass(line, reader);
                            break;

                        default:
                            throw new PixelMonkeyCoreException("Unknown keyword " + keyword);
                    }
                }
            }
            finally
            {
                reader.Close();
            }
        }

        public void LoadFile(String filepath)
        {
            if (File.Exists(filepath))
            {
                StreamReader reader = new StreamReader(File.OpenRead(filepath));
                Load(reader);
            }
        }

        private void LoadClass(String str, StreamReader reader)
        {
            PixelMonkeyClass pmclass = new PixelMonkeyClass();
            pmclass.Name = str.Split(' ')[1];
            String line;

            while ((line = reader.ReadLine()) != null)
            {
                if (line.Trim() == String.Empty) break;

                String[] words = line.Split(' ');

                switch (words[0])
                {
                    case CtorKW:
                        LoadConstructor(words, pmclass);
                        break;

                    case PropertyKW:
                        LoadProperty(words, pmclass);
                        break;

                    case MethodKW:
                        LoadMethod(words, pmclass);
                        break;

                    default:
                        throw new PixelMonkeyCoreException("Unknown keyword " + words[0] + " in class " + pmclass.Name);
                }
            }

            Classes.Add(pmclass);
        }

        private void LoadConstructor(String[] words, PixelMonkeyClass c)
        {
            PixelMonkeyFonction function = new PixelMonkeyFonction();
            function.Name = "_CTOR";

            for (int i = 1; i < words.Count(); i++)
            {
                String type = words[i];
                if (type.EndsWith(",")) type = type.Substring(0, type.IndexOf(','));

                function.ParamsType.Add(Types[type] as Type);
            }

            function.CallBack = c.Name + "_CTOR";
            c.Methods.Add(function);
        }

        private void LoadMethod(String[] words, PixelMonkeyClass c)
        {
            PixelMonkeyFonction function = new PixelMonkeyFonction();
            Int32 TabIndex = 2;
            function.Name = words[1];

            while (TabIndex < words.Count())
            {
                switch (words[TabIndex])
                {
                    case InParameterKW:
                        do function.ParamsType.Add(Types[words[++TabIndex].TrimEnd(',')] as Type);
                        while (words[TabIndex].EndsWith(","));
                        TabIndex++;

                        break;

                    case CallingKW:                        
                        function.CallBack = words[++TabIndex];
                        TabIndex++;

                        break;

                    default:
                        throw new PixelMonkeyCoreException("Unknown keyword " + words[TabIndex] + " in class " + c.Name);
                }
            }

            if (function.CallBack == null || function.CallBack == String.Empty) throw new PixelMonkeyCoreException("Method does not call any C# method. -> " + words[1]);

            c.Methods.Add(function);
        }

        private void LoadProperty(String[] words, PixelMonkeyClass c)
        {
            c.Properties.Add(words[1]);
            c.PropTypes.Add(Types[words[3]] as Type);
        }

        private void LoadFunction(String str)
        {
            String[] words = str.Split(' ');
            Int32 TabIndex = 1;

            PixelMonkeyFonction function = new PixelMonkeyFonction();
            function.Name = words[TabIndex];
            TabIndex++;

            while (TabIndex < words.Count())
            {
                switch (words[TabIndex])
                {
                    case InParameterKW:
                        do function.ParamsType.Add(Types[words[++TabIndex].TrimEnd(',')] as Type);
                        while (words[TabIndex].EndsWith(","));
                        TabIndex++;

                        break;

                    case OutParameterKW:
                        function.ReturnType = Types[words[++TabIndex]] as Type;
                        TabIndex++;

                        break;

                    case CallingKW:
                        function.CallBack = words[++TabIndex];
                        TabIndex++;

                        break;

                    default:
                        throw new PixelMonkeyCoreException("Unknown keyword " + words[TabIndex] + " in " + str);
                }
            }

            if (function.CallBack == null || function.CallBack == String.Empty) throw new PixelMonkeyCoreException("Function does not call any C# method. -> " + words[1]);

            Functions.Add(function);
        }


    }
}
