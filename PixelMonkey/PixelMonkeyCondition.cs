﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PixelMonkey
{
    public class PixelMonkeyCondition
    {
        Object leftValue;
        Object rightValue;
        Object originalLeft;
        Object originalRight;
        PixelMonkeyComparisonType type;

        public PixelMonkeyCondition()
        {

        }

        public PixelMonkeyCondition(PixelMonkeyContext context, Object left, Object right, PixelMonkeyComparisonType type)
        {
            SetLeft(context, left);
            SetRight(context, right);
            originalLeft = left;
            originalRight = right;
            this.type = type;
        }

        public void SetLeft(PixelMonkeyContext context, Object str)
        {
            leftValue = context.Variables.ContainsKey(str.ToString()) ? context.Variables[str.ToString()] : str;
        }

        public void SetRight(PixelMonkeyContext context, Object str)
        {
            rightValue = context.Variables.ContainsKey(str.ToString()) ? context.Variables[str.ToString()] : str;
        }

        public void RefreshValues(PixelMonkeyContext context)
        {
            SetLeft(context, originalLeft);
            SetRight(context, originalRight);
        }

        public Boolean GetVeracity()
        {
            try
            {
                if (leftValue == null || rightValue == null)     throw new PixelMonkeyInterpretationException("Condition has null values");
                else if (type == PixelMonkeyComparisonType.Eq)   return  leftValue.ToString().Equals(rightValue.ToString());
                else if (type == PixelMonkeyComparisonType.Neq)  return !leftValue.ToString().Equals(rightValue.ToString());
                else if (type == PixelMonkeyComparisonType.Gt)   return Double.Parse(leftValue.ToString()) >  Double.Parse(rightValue.ToString());
                else if (type == PixelMonkeyComparisonType.Lt)   return Double.Parse(leftValue.ToString()) <  Double.Parse(rightValue.ToString());
                else if (type == PixelMonkeyComparisonType.Gteq) return Double.Parse(leftValue.ToString()) >= Double.Parse(rightValue.ToString());
                else if (type == PixelMonkeyComparisonType.Lteq) return Double.Parse(leftValue.ToString()) <= Double.Parse(rightValue.ToString());

                else throw new PixelMonkeyInterpretationException("[FATAL] Condition has an unknown type");
            }
            catch (FormatException)
            {
                throw new PixelMonkeyInterpretationException("Compared two objects that have not the same type.", leftValue.ToString() + " & " + rightValue.ToString());
            }
        }
    }

    public enum PixelMonkeyComparisonType
    {
        Eq, Neq, Gt, Lt, Gteq, Lteq, Null
    }
}
