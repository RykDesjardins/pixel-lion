﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PixelMonkey
{
    public class PixelMonkeyScript : IDisposable
    {
        List<PixelMonkeyCommand> Commands;
        PixelMonkeyContext Context;
        PixelMonkeyLoader Loader;
        PixelMonkeyLanguage Language;
        PixelMonkeyParser Parser;
        Int32 CommandCursor;
        
        public PixelMonkeyScript(PixelMonkeyLanguage lang)
        {
            Loader = new PixelMonkeyLoader(lang.GetCoreClass());
            Commands = new List<PixelMonkeyCommand>();
            Context = new PixelMonkeyContext();
            Language = lang;
            Parser = new PixelMonkeyParser();
            CommandCursor = 0;
        }

        public void LoadScript(String str)
        {
            PixelMonkeyParser parser = new PixelMonkeyParser();
            String line;

            using (StringReader reader = new StringReader( str ))
            while ((line = reader.ReadLine()) != null)
                Commands.Add(parser.Parse(line, Language, Context));
        }

        public void AddLine(String str)
        {
            PixelMonkeyCommand command = Parser.Parse(str, Language, Context);

            if (command == null) 
                throw new PixelMonkeyInterpretationException("Could not find anything related.", str);
            else 
                Commands.Add(command);
        }

        public CommandType SkipUntilClosure()
        {
            Parser.EnteredBloc();

            // The current command. This object will be reassigned during looping through following commands
            PixelMonkeyCommand command = Commands[CommandCursor];
            
            // Run though commands incrementally until a closure is reached
            while (command.GetCommandType() != CommandType.BlockClosure)
            {
                // If loop reachs end of array, there is an obvious missing end of block
                if (CommandCursor == Commands.Count) throw new PixelMonkeyInterpretationException("Could not find end of block");

                command = Commands[CommandCursor++];
            }

            Parser.SetVeracity(true);

            return CommandType.Skiped;
        }

        public void RunUntilClosure()
        {
            Parser.EnteredBloc();
            PixelMonkeyCommand command = Commands[CommandCursor];

            while (command.GetCommandType() != CommandType.BlockClosure)
            {
                if (CommandCursor == Commands.Count) throw new PixelMonkeyInterpretationException("Could not find end of block");

                ExecuteNext();
                command = Commands[CommandCursor];
            }
        }

        public void LoopUntilFalseVeracity(PixelMonkeyCondition condition)
        {
            Parser.EnteredBloc();
            PixelMonkeyCommand command = Commands[CommandCursor];

            // While condition passed as param is true
            while (condition.GetVeracity())
            {
                while (command.GetCommandType() != CommandType.BlockClosure)
                {
                    if (CommandCursor == Commands.Count) throw new PixelMonkeyInterpretationException("Could not find end of block");

                    ExecuteNext();
                    command = Commands[CommandCursor];
                }

                condition.RefreshValues(Context);
            }
        }

        public Object ExecuteNext()
        {
            if (CommandCursor < Commands.Count)
            {
                PixelMonkeyCommand command = Commands[CommandCursor++];

                command.ReplaceBracketsWithVars(Context);

                if (Parser.IsWaitingForOpeningBlock() && command.GetCommandType() == CommandType.BlockOpening) return CommandType.Skiped;
                if (Parser.MustSkip()) return SkipUntilClosure();

                switch (command.GetCommandType())
                {
                    // In case of a function, does not always return something
                    case CommandType.Fonction:
                        return command.Execute(Loader);

                    // Affectation of a variable, value is always returned
                    case CommandType.Affectation:
                        return command.Affect(Context);

                    // The requested variable's value is returned
                    case CommandType.Get:
                        return command.GetVariable(Context);

                    // The conditation veracity is returned
                    case CommandType.Condition:
                        Parser.SetVeracity(command.ExecuteCondition(Context));
                        return Parser.GetVeracity();

                    // The condition veracity is returned
                    // Looping procedure is implemented here
                    case CommandType.Loop:
                        Parser.SetVeracity(command.ExecuteCondition(Context));
                        if (Parser.GetVeracity()) LoopUntilFalseVeracity(command.MakeCondition(Context));

                        return Parser.GetVeracity();

                    // Show comments
                    case CommandType.Comment:
                        return command.GetComment();

                    // This is never supposed to happen
                    default:
                        return null;
                }
            }
            else
                return null;
        }

        public Boolean HasWorkToDo()
        {
            return CommandCursor < Commands.Count();
        }

        public void Dispose()
        {
            Loader.Dispose();
        }
    }
}
