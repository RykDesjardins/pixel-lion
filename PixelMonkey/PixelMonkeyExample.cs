﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.JScript;
using Microsoft.JScript.Vsa;
using VBMath;

namespace PixelMonkey
{
    public class PixelMonkeyExample
    {
        public String TeleportPlayer(String map, Double x, Double y, Double layer)
        {
            return String.Format("Teleported to {0} at position ({1} X {2}) on layer {3}", map, x, y, layer); 
        }

        public void MovePlayer(String Direction)
        {

        }

        public String ShowMessage(String Message)
        {
            // MessageBox.Show(Message, "Pixel Monkey - TEXT -> ShowMessage");
            return Message;
        }

        public void WaitSeconds(Double Seconds)
        {

        }

        public void WaitFrames(Double Frames)
        {

        }

        public void PlaySong(String Song)
        {

        }

        public void PlaySound(String Sound)
        {

        }

        public void FadeAudio(String Type, String InOut, Double Duration)
        {

        }

        public void ShowAnimation(String AnimationName, Double x, Double y)
        {

        }

        public void AlterPlayerVariable(String Var, String Value)
        {

        }

        public void AlterPlayerVariable(String Var, Double Value)
        {

        }

        public void AddPlayerVariable(String VariableName)
        {

        }

        public void ScrollCamera(Double x, Double y, Double Velocity)
        {

        }

        public void FlashCamera(Double R, Double G, Double B, Double Duration)
        {

        }

        public void ChangeTone(Double R, Double G, Double B, Double Duration)
        {

        }

        public void CallBattleScene(Double BattleIndex)
        {

        }

        public void OpenShop(Double ShopIndex)
        {

        }

        public void EndScript()
        {

        }

        public String Reverse(String Source)
        {
            return new String(Source.Reverse().ToArray());
        }

        public void MovePevent(Double TileCount)
        {

        }

        public void ShowAnimationOnPevent(String AnimationName)
        {

        }

        public void ChangePeventDirection(String Direction)
        {

        }

        public void MovePicture(Double x, Double y, Double PixelCount)
        {

        }

        public void ScrollPicture(Double x, Double y, Double Velocity)
        {

        }

        public void FadePicture(String InOut, Double TimeToFade)
        {

        }

        public Double Evaluate(String str)
        {
            return new mcCalc().evaluate(str); 
        }
    }
}
