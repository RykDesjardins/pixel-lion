﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace PixelMonkey
{
    public class PixelMonkeyContext
    {
        public String pEventName { get; set; }
        public Int32 pEventIndex { get; set; }
        public Hashtable Variables { get; set; }
        public Hashtable VariablesType { get; set; }

        public PixelMonkeyContext()
        {
            pEventName = String.Empty;
            pEventIndex = -1;
            Variables = new Hashtable();
            VariablesType = new Hashtable();
        }
    }
}
