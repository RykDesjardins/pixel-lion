﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace PixelMonkey
{
    public class PixelMonkeyClass
    {
        public List<PixelMonkeyFonction> Methods;
        public List<String> Properties;
        public List<Type> PropTypes;
        public String Name;

        public PixelMonkeyClass()
        {
            Methods = new List<PixelMonkeyFonction>();
            Properties = new List<String>();
            PropTypes = new List<Type>();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
