﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.Reflection.Emit;
using System.Linq;
using System.Text;

namespace PixelMonkey
{
    public class PixelMonkeyLoader : IDisposable
    {
        //String assemblyname;
        //Assembly assembly;
        Type library;

        public PixelMonkeyLoader(Type library)
        {
            //this.assemblyname = assemblyname;
            //assembly = Assembly.Load(assemblyname);

            this.library = library;
        }

        public Object LoadMethod(String method, Object[] Args)
        {
            Type type = library; //assembly.GetType(method);
            Object instance = Activator.CreateInstance(type);
            
            return type.InvokeMember(method, BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public, null, instance, Args);
        }

        public void Dispose()
        {
            
        }
    }
}
