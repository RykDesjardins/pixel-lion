﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PixelMonkey
{
    public class PixelMonkeyCommand
    {
        String CommandName;
        String CallBack;
        Object[] Parameters;
        Type[] ParamsType;

        CommandType CmdType;

        public PixelMonkeyCommand(String name, String callback, Object[] parameters, Type[] paramstype, CommandType cmdtype)
        {
            CommandName = name;
            CallBack = callback;
            Parameters = parameters;
            ParamsType = paramstype;
            CmdType = cmdtype;
        }

        public PixelMonkeyCommand(String name, Object parameter, Type paramtype, CommandType cmdtype)
        {
            CommandName = name;
            CallBack = String.Empty;
            Parameters = new Object[] { parameter };
            ParamsType = new Type[] { paramtype };
            CmdType = cmdtype;
        }

        public void ReplaceBracketsWithVars(PixelMonkeyContext context)
        {
            if (Parameters != null && ParamsType != null && ParamsType.Count() == Parameters.Count())
            {
                Regex brackets = new Regex(@"\[(.*?)\]");

                for (Int32 i = 0; i < Parameters.Count(); i++)
                    if (ParamsType[i] == typeof(String))
                        foreach (Match s in brackets.Matches(Parameters[i].ToString()))
                        {
                            String key = s.Value.Substring(1, s.Value.Length - 2);
                            if (context.Variables.ContainsKey(key)) Parameters[i] = Parameters[i].ToString().Replace(s.Value, context.Variables[key].ToString());
                        }
            }
        }

        public Object Execute(PixelMonkeyLoader Loader)
        {
            return Loader.LoadMethod(CallBack, Parameters);
        }

        public String Affect(PixelMonkeyContext context)
        {
            if (context.Variables.ContainsKey(CommandName)) Parameters[0] = context.Variables[CommandName];

            context.Variables[CommandName] = Parameters[0];
            context.VariablesType[CommandName] = ParamsType[0];

            return "Variable affected";
        }

        public String GetComment()
        {
            if (CommandName.StartsWith("#"))
                return CommandName.Substring(1).Trim();
            else
                throw new PixelMonkeyInterpretationException("Not a comment", CommandName);
        }

        public Object GetVariable(PixelMonkeyContext context)
        {
            return context.Variables[CommandName].ToString();
        }

        public Boolean ExecuteCondition(PixelMonkeyContext context)
        {
            // Create condition object
            PixelMonkeyCondition condition = MakeCondition(context);

            // Return if condition if true of not
            return condition.GetVeracity();
        }

        public PixelMonkeyCondition MakeCondition(PixelMonkeyContext context)
        {
            Object[] comparedObjs = new Object[2];
            Int32 ObjIndex = 0;

            // Search for first parameters in objects and in variables
            for (Int32 i = 0; i != 4; i += 2)
            {
                if (context.Variables.ContainsKey(Parameters[i]))
                    comparedObjs[ObjIndex] = context.Variables[Parameters[i]];
                else
                    comparedObjs[ObjIndex] = Parameters[i];

                ObjIndex++;
            }

            PixelMonkeyComparisonType type = PixelMonkeyComparisonType.Null;
            String symbol = (String)Parameters[1];

            // Parse symbol for comparison type
            switch (symbol)
            {
                case "==": type = PixelMonkeyComparisonType.Eq; break;
                case "=": type = PixelMonkeyComparisonType.Eq; break;
                case "!=": type = PixelMonkeyComparisonType.Neq; break;
                case "<": type = PixelMonkeyComparisonType.Lt; break;
                case ">": type = PixelMonkeyComparisonType.Gt; break;
                case "<=": type = PixelMonkeyComparisonType.Lteq; break;
                case ">=": type = PixelMonkeyComparisonType.Gteq; break;
                default: break;
            }

            return new PixelMonkeyCondition(context, comparedObjs[0], comparedObjs[1], type);
        }

        public CommandType GetCommandType()
        {
            return CmdType;
        }
    }
}
