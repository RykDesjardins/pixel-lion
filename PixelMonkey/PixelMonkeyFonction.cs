﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PixelMonkey
{
    public class PixelMonkeyFonction
    {
        public String Name;
        public String CallBack;
        public List<Type> ParamsType;
        public Type ReturnType;

        public PixelMonkeyFonction()
        {
            Name = "";
            CallBack = "";
            ParamsType = new List<Type>();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
