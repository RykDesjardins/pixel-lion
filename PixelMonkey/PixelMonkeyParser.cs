﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PixelMonkey
{
    public class PixelMonkeyParser
    {
        const String OperationKW = "DO";
        const String AccessorKW = "'s";
        const String InstanceKW = "=";

        const String ConditionKW = "IF";
        const String ConditionInvKW = "UNLESS";
        const String ConditionNotMetKW = "ELSE";
        const String LoopKW = "WHILE";
        const String LoopInvKW = "UNTIL";
        const String BlockOpenKW = "{";
        const String BlockEndKW = "}";

        const String ComaHK = "/v";
        Boolean WaitingForBlockKW = false;
        Boolean LastCondition = true;

        public PixelMonkeyParser()
        {

        }

        // Command will return a Command object according to the passed String.
        // If the commands cannot be parsed, an exception is thrown explaining the situation.
        // This method never returns null.
        public PixelMonkeyCommand Parse(String cmd, PixelMonkeyLanguage lang, PixelMonkeyContext context)
        {
            cmd = cmd.Trim();
            if (cmd.StartsWith("#")) return new PixelMonkeyCommand(cmd, null, null, CommandType.Comment);
            else if (Regex.IsMatch(cmd, @"^\d+")) throw new PixelMonkeyInterpretationException("Command cannot start with a number", cmd);

            // Get every words of sentence removing double-spacing and trailing spaces
            String[] words = new Regex(@"[ ]{2,}", RegexOptions.None).Replace(cmd, @" ").Trim().Split(' ');

            // This command will be returned at the end of function. Cannot be null.
            PixelMonkeyCommand command = null;

            // First of all, check for block opening and closure
            if (WaitingForBlockKW && words[0] == BlockOpenKW) command = new PixelMonkeyCommand(String.Empty, null, null, CommandType.BlockOpening);
            else if (words[0] == BlockEndKW) command = new PixelMonkeyCommand(String.Empty, null, null, CommandType.BlockClosure);

            // Search for conditions and loops, functions, then objects in context, and finally equality
            // True condition
            else if (cmd.StartsWith(ConditionKW))
            {
                command = new PixelMonkeyCommand(ConditionKW, String.Empty, new Object[] { words[1], words[2], words[3] }, null, CommandType.Condition);
                WaitingForBlockKW = true;
            }

            else if (cmd.StartsWith(LoopKW))
            {
                command = new PixelMonkeyCommand(LoopKW, String.Empty, new Object[] { words[1], words[2], words[3] }, null, CommandType.Loop);
                WaitingForBlockKW = true;
            }

            // Search for functions
            else if (lang.GetFunctions().Select(x => x.Name).Contains(words[0]))
            {
                PixelMonkeyFonction function = lang.GetFunctions().Where(x => x.Name == words[0]).FirstOrDefault();

                // Check if there are any parameters
                if (cmd.IndexOf(' ') != -1)
                {
                    IEnumerable<String> prms = cmd.Remove(0, cmd.IndexOf(' ')).Split(',').Select(x => x.Trim());

                    // List of parameters and their respective types
                    Object[] parameters = new Object[prms.Count()];
                    Type[] types = ParseParameters(prms, parameters).ToArray();

                    // Check if parameters match requirements
                    if (parameters.Count() != function.ParamsType.Count)
                        throw new PixelMonkeyInterpretationException("Wrong number of arguments (" + parameters.Count() + " / " + function.ParamsType.Count + ")", cmd);
                    else if (!CompareArrays(types, function.ParamsType.ToArray()))
                        throw new PixelMonkeyInterpretationException("Wrong type of arguments", cmd);

                    // Build command object
                    command = new PixelMonkeyCommand(function.Name, function.CallBack, parameters, types, CommandType.Fonction);
                }
                else if (function.ParamsType.Count() == 0)
                {
                    // Simply call function without any parameters
                    command = new PixelMonkeyCommand(function.Name, function.CallBack, new Object[0], new Type[0], CommandType.Fonction);
                }
                else
                {
                    // A function needing parameters was called without any
                    throw new PixelMonkeyInterpretationException("Fonction requires " + function.ParamsType.Count() + " argument(s)", cmd);
                }
            }

            // If an equality sign is present and variable does not currently exists
            else if (cmd.Contains('='))
            {
                if (words.Count() == 3 && lang.GetClasses().Select(x => x.Name).Contains(words[2]))
                {

                }
                else if (words.Count() == 3)
                {
                    // Variable value and type
                    Object value;
                    Type valuetype;

                    // Get value and type
                    ParseParameter(words[2], out value, out valuetype);

                    // Build affectation command to create variable with passes value
                    command = new PixelMonkeyCommand(words[0], value, valuetype, CommandType.Affectation);
                }
                else if (words.Count() > 3)
                    throw new PixelMonkeyInterpretationException("Unknown class name", words[2]);
            }

            // If a variable is present in the current context
            else if (context.Variables.ContainsKey(words[0].Replace("'s", "")))
            {
                if (words.Count() == 1 && !words[0].Contains("'s"))
                {
                    command = new PixelMonkeyCommand(words[0], null, null, CommandType.Get);
                }
            }

            // If nothing is found 
            else
            {
                // This happens when command cannot be understood
                throw new PixelMonkeyInterpretationException("Interpreter could not understand the purpose of the line", cmd);
            }


            return command;
        }

        public void SetVeracity(Boolean v)
        {
            LastCondition = v;
        }

        public Boolean GetVeracity()
        {
            return LastCondition;
        }

        public Boolean IsWaitingForOpeningBlock()
        {
            return WaitingForBlockKW;
        }

        public Boolean IsAnOpeningBlockString(String str)
        {
            return str.Trim() == BlockOpenKW;
        }

        public Boolean IsAClosureBlockString(String str)
        {
            return str.Trim() == BlockEndKW;
        }

        public Boolean MustSkip()
        {
            return !LastCondition;
        }

        public void EnteredBloc()
        {
            WaitingForBlockKW = false;
        }

        IEnumerable<Type> ParseParameters(IEnumerable<String> param, Object[] parameters)
        {
            Int32 Count = -1;

            foreach (String str in param)
            {
                Double dResult = 0;
                Boolean bResult = false;
                Count++;

                if (Double.TryParse(str, out dResult))
                {
                    parameters[Count] = dResult;
                    yield return typeof(Double);
                }
                else if (Boolean.TryParse(str, out bResult))
                {
                    parameters[Count] = bResult;
                    yield return typeof(Boolean);
                }
                else 
                {
                    parameters[Count] = str.Replace(ComaHK, ",");
                    yield return typeof(String); 
                }
            }
        }

        void ParseParameter(String str, out Object obj, out Type type)
        {
            Double dResult = 0;
            Boolean bResult = false;

            if (Double.TryParse(str, out dResult))
            {
                obj = dResult;
                type = typeof(Double);
            }
            else if (Boolean.TryParse(str, out bResult))
            {
                obj = bResult;
                type = typeof(Boolean);
            }
            else
            {
                obj = str;
                type = typeof(String);
            }
        }

        public Boolean CompareArrays<T>(T[] left, T[] right)
        {
            if (left.Count() != right.Count()) return false;

            for (int i = 0; i < left.Length; i++) if (!left[i].Equals(right[i])) return false;

            return true;
        }
    }
}
