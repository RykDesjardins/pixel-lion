﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PixelMonkey
{
    public class PixelMonkeyInterpretationException : Exception
    {
        public String CustomMessage { get; set; }
        public String Command { get; set; }

        public PixelMonkeyInterpretationException()
        {
            CustomMessage = String.Empty;
        }

        public PixelMonkeyInterpretationException(String msg)
        {
            CustomMessage = msg;
        }

        public PixelMonkeyInterpretationException(String msg, String cmd)
        {
            CustomMessage = msg;
            Command = cmd;
        }
    }

    public class PixelMonkeyCoreException : Exception
    {
        public String CustomMessage { get; set; }
        public String Command { get; set; }

        public PixelMonkeyCoreException()
        {
            CustomMessage = String.Empty;
        }

        public PixelMonkeyCoreException(String msg)
        {
            CustomMessage = msg;
        }

        public PixelMonkeyCoreException(String msg, String cmd)
        {
            CustomMessage = msg;
            Command = cmd;
        }
    }
}
